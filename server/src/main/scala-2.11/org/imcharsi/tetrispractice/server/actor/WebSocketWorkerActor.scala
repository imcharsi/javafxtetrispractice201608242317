/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ ActorRef, Actor }
import akka.stream.actor.ActorPublisherMessage.Request
import akka.stream.actor.ActorSubscriberMessage.{ OnComplete, OnError, OnNext }
import akka.stream.actor.{ ActorPublisher, ActorSubscriber, RequestStrategy, WatermarkRequestStrategy }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.fsm.Command
import org.imcharsi.tetrispractice.common.data.fsm.Command.StreamInput

import scala.collection.immutable.Queue

/**
 * Created by i on 9/3/16.
 */
class WebSocketWorkerActor[BT](gameRoomActorRef: ActorRef) extends Actor
    with ActorPublisher[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]]
    with ActorSubscriber {
  private[actor] var queue: Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]] = Queue()

  override def receive: Receive =
    receiveJoinRoomAsSubscriber
      .orElse(receiveJoinRoomAsActor)

  def enqueueGameRoomServerCommand(c: GameRoomServerCommand): Unit = {
    queue = queue.enqueue(Right[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]](Left(c)))
  }

  def enqueueGameRoomClientCommand(c: GameRoomClientCommand): Unit = {
    queue = queue.enqueue(Left[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]](c))
  }

  def enqueueStreamInput(i: StreamInput[BT]): Unit = {
    queue = queue.enqueue(Right[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]](Right(i)))
  }

  // QuitWebSocketWorkerActor 보다 먼저 입력된 자료들은 반드시 먼저 전송시도해야 하고
  // QuitWebSocketWorkerActor 는, totalDemand==0 이어도 Queue 의 처음에 발견되면 무조건 처리하도록 하기 위해
  // 아래와 같이 Queue 를 처리한다.
  def processQueue(queue: Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]]): Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]] = {
    queue.dequeueOption match {
      case Some((Right(Left(QuitWebSocketWorkerActor)), _)) =>
        // totalDemand 와 상관없이 꺼낼 수 있는 처음이 QuitWebSocketWorkerActor 이면 종료한다.
        onComplete()
        context.stop(self)
        Queue()
      case Some((dequeue @ Left(CreatorJoinedRoom(None) | JoinedRoom(None)), newQueue)) if (totalDemand > 0) =>
        onNext(dequeue)
        onComplete()
        context.stop(self)
        Queue()
      case Some((dequeue, newQueue)) if (totalDemand > 0) =>
        onNext(dequeue)
        processQueue(newQueue)
      case None => queue
      case _ if (totalDemand == 0) => queue
    }
  }

  def receiveJoinRoomAsSubscriber: Receive = {
    case OnNext(input @ Left(CreatorJoinRoom(_, _))) =>
      // 개설자인 Client 로부터 입장 명령을 받았다. GameRoomActor 에게 입장 가능 여부를 확인한 다음 응답을 해야 한다.
      gameRoomActorRef.tell(input, self)
    case OnNext(input @ Left(JoinRoom(_))) =>
      // 개설자가 아닌 Client 로부터 입장 명령을 받았다.
      gameRoomActorRef.tell(input, self)
    case Request(_) =>
      queue = processQueue(queue)
  }

  // GameRoomActor 가 입장 가능 여부를 판단하여 응답을 해주면, 이어서 Client 로 응답을 전달해준다.
  def receiveJoinRoomAsActor: Receive = {
    case input @ CreatorJoinedRoom(_) =>
      // 응답의 결과에 따라, Actor 를 종료해야 할 수도 있는데,
      // 종료는 여기서 하지 않고 processQueue() 에서 한다.
      context.become(receiveAsActor.orElse(receiveAsSubscriber))
      enqueueGameRoomClientCommand(input)
      queue = processQueue(queue)
    case input @ JoinedRoom(_) =>
      context.become(receiveAsActor.orElse(receiveAsSubscriber))
      enqueueGameRoomClientCommand(input)
      queue = processQueue(queue)
    case input @ QuitWebSocketWorkerActor =>
      enqueueGameRoomServerCommand(input)
      queue = processQueue(queue)
    case _ =>
      enqueueGameRoomServerCommand(QuitWebSocketWorkerActor)
      queue = processQueue(queue)
  }

  // 입장을 성공하면 그 다음부터는 receiveAsSubscriber 와 receiveAsActor 가 같이 동작한다.
  def receiveAsSubscriber: Receive = {
    case OnNext(input @ Right(Left(NotifiedBeAbleToStart))) =>
      // Server 가 보낸 NotifyBeAbleToStart 를 받았다는 Client 의 응답이다.
      // 응답을 받았으면 됐고 더 이상 할 것은 없다.
      // todo 그런데, 응답을 받아도 할 것이 없다면 응답을 받을 필요가 있나.
      ()
    case OnNext(input @ Right(Left(NotifiedChangeStarter))) =>
      // Server 가 보낸 NotifyChangeStarter 를 받았다는 Client 의 응답이다.
      // 응답을 받았으면 됐고 더 이상 할 것은 없다.
      ()
    case OnNext(input @ Right(Left(NotifiedJoinRoom(_, _, _)))) =>
      // Server 가 보낸 NotifyJoinRoom 을 받았다는 Client 의 응답이다.
      // 구상은, 준비를 성공했는지 여부를 응답으로 받는 것이었는데, 일단 무조건 성공한다고 보고 넘어간다.
      gameRoomActorRef.tell(input, self)
    case OnNext(input @ Left(StartGames(_))) =>
      // 방장이, 게임을 시작하겠다는 입력을 했다.
      // 이 입력은 GameRoomActor 로 전달되고 GameRoomActor 의 응답을 receiveAsActor 가 받아서 queue 에 쌓으면
      // WebSocket 을 타고 Client 로 전달된다.
      gameRoomActorRef.tell(input, self)
    case OnNext(input @ Right(Right(i: StreamInput[BT]))) =>
      // LocalPlayer 가 Server 로 StreamInput 을 전달하는 단계이다.
      // WebSocketWorkerActor 는 이 입력을 GameRoomActor 로 전달해야 하고,
      // GameRoomActor 는 LocalPlayer 에 대응하는 모든 RemotePlayer 로 이를 Broadcast 한다.
      gameRoomActorRef.tell(input, self)
    case OnNext(input @ Left(Ping)) =>
      // 아무것도 하지 않는다. WebSocket 을 연결해두고 1분 동안 아무것도 하지 않으면 연결이 자동으로 끊긴다.
      // 왜 이것이 문제되는가. 방을 개설해두고 1분 동안 아무도 오지 않고 아무것도 하지 않으면 연결이 끊긴다.
      Console.err.println("ping!")
      ()
    case Request(_) =>
      queue = processQueue(queue)
    case OnError(ex) =>
      onError(ex)
      context.stop(self)
    case OnComplete =>
      onComplete()
      context.stop(self)
  }

  // 여기서는 Client 가 받을 내용에 대한 판단을 하지 않는다.
  // 판단은 GameRoomActor 에서 전부 했고 WebSocketWorkerActor 는 단순히 전달역할만 한다.
  def receiveAsActor: Receive = {
    case input @ NotifyJoinRoom(_, _) =>
      // 이미 입장해 있는 모든 사용자에게 새 사용자의 입장을 알린다.
      // 또한 새 사용자가 입장할 때, 기존 사용자의 입장을 알릴 때도 쓴다.
      // 이 알림에 이어 방장에게는 NotifyBeAbleToStart 알림도 같이 간다.
      // 3명이 준비완료한 상태에서 1명이 새로 들어오면 게임을 시작할 수 없는 상태가 되기 때문이다.
      // 한 명령에 이어 다른 명령도 이어서 보내고 안 보내고의 판단은 GameRoomActor 가 한다.
      enqueueGameRoomServerCommand(input)
      queue = processQueue(queue)
    case input @ NotifyBeAbleToStart(_) =>
      // 방장에게 게임을 시작할 수 있는지 여부를 알린다.
      // 사용자들이 Prepare 나 Cancel 을 할 때마다 방장에게 전달되고 또한 사용자가 입장하거나 퇴장할 때마다 전달된다.
      // 4명 중 3명이 준비완료상태였고 준비완료하지 않은 1명이 퇴장하여 준비완료상태인 3명만 남았다면 게임을 시작할 수 있다.
      enqueueGameRoomServerCommand(input)
      queue = processQueue(queue)
    case input @ NotifyChangeStarter(_) =>
      // 게임을 시작할 수 있는 사람이 바뀌었음을 알린다.
      // 입장하는 사람에게는 NotifyChangeStarter 를 알린다. 따라서 개설자가 최초 입장할 때도 이 명령을 받는다.
      // 게임을 시작할 수 있는 사람이 바뀌어서 이를 알릴 때는 이어서 방장에게 NotifyBeAbleToStart 알림도 같이 간다.
      // 준비완료를 하지 않던 사용자가 방장이었는데 방장이 퇴장한 경우,
      // 남은 사용자는 전부 준비완료상태이고 그래서 새로운 방장은 게임을 시작할 수 있음을 알림받는다.
      enqueueGameRoomServerCommand(input)
      queue = processQueue(queue)
    case input @ StartedGames(_) =>
      // 방장이 보낸 StartGames 에 대해 GameRoomActor 가 하는 응답이다.
      enqueueGameRoomClientCommand(input)
      queue = processQueue(queue)
    case i: Command.StreamInput[BT] =>
      // LocalPlayer 의 StreamInput 을 Server 가 받아서,
      // 대응하는 모든 RemotePlayer 로 StreamInput 을 Broadcast 하기 위해 GameRoomActor 가 보낸다.
      enqueueStreamInput(i)
      queue = processQueue(queue)
    case input @ QuitWebSocketWorkerActor =>
      // 연결을 끊어야 한다는 판단을 했지만, 이 판단에 앞서 WebSocket 으로 보내기로 결정했던 자료는
      // 전부 보낼 수 있도록 보장하기 위해서 이와 같은 입력을 쓴다.
      // Queue 처리기가 이 입력을 처리할 순서에 이르렀을 때 Actor 를 종료하게 된다.
      enqueueGameRoomServerCommand(input)
      queue = processQueue(queue)
  }

  override protected def requestStrategy: RequestStrategy = WatermarkRequestStrategy(16)
}

