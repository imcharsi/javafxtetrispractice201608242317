/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.http

import akka.actor.{ ActorSystem, Props }
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5
import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5.BlockType
import org.imcharsi.tetrispractice.server.actor.{ ReplayRoom, WaitRoomActor }
import org.imcharsi.tetrispractice.server.route.RouteTemplate

import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * Created by i on 9/10/16.
 */
class HttpPreparing {
  implicit val actorSystem = ActorSystem()
  implicit val actorMaterializer = ActorMaterializer()
  val http = Http()

  val replayRoom = new ReplayRoom[BlockType.BlockType]()
  val replayRoomActorRef = actorSystem.actorOf(Props(new replayRoom.ReplayRoomActor()))
  val waitRoomActorRef = actorSystem.actorOf(Props(new WaitRoomActor[BlockType.BlockType](replayRoomActorRef, replayRoom, 3.seconds, 5.seconds)))
  val route = new RouteTemplate[BlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)

  def bind(serverName: String, serverPort: Int): Future[ServerBinding] =
    http.bindAndHandle(Route.handlerFlow(route.finalRoute(Block5x5.Block5x5JsonFormat)), serverName, serverPort)
}
