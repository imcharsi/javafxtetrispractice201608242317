/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.route

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.javadsl.server.Rejections
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ Directives, Route }
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.{ CreateWebSocketWorkerActor, CreatedWebSocketWorkerActor, GameRoomClientCommand, GameRoomServerCommand }
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ CreateRoom, GetRoomList, GotRoomList, WaitRoomResponse }
import org.imcharsi.tetrispractice.common.data.fsm.Command
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomClientCommandJsonFormat, GameRoomServerCommandJsonFormat, WaitRoomResponseJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm.StreamInputJsonFormat
import spray.json._
import akka.pattern.ask
import akka.util.ByteString
import org.imcharsi.tetrispractice.server.actor.ReplayRoom

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{ Failure, Success }

/**
 * Created by i on 9/9/16.
 */
class RouteTemplate[BT](replayRoomActorRef: ActorRef, replayRoom: ReplayRoom[BT], waitRoomActorRef: ActorRef)(implicit actorSystem: ActorSystem) extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val waitRoomResponseJsonFormat = WaitRoomResponseJsonFormat

  def validateAcceptHeader(route: Route): Route =
    extract(_.request.header[Accept]) {
      case Some(acceptHeader) if acceptHeader.mediaRanges.exists(_.matches(MediaTypes.`application/json`)) =>
        route
      case None =>
        complete(HttpResponse(StatusCodes.BadRequest))
    }

  val routeGetRoomList =
    path("getRoomList") {
      get {
        validateAcceptHeader {
          complete {
            waitRoomActorRef
              .ask(GetRoomList)(3.seconds)
              .map(_.asInstanceOf[WaitRoomResponse])(Implicits.global)
          }
        }
      }
    }

  val routeCreateRoom =
    path("createRoom") {
      post {
        validateAcceptHeader {
          complete {
            waitRoomActorRef
              .ask(CreateRoom)(3.seconds)
              .map(_.asInstanceOf[WaitRoomResponse])(Implicits.global)
          }
        }
      }
    }

  type FlowIOType = Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]

  def prepareWebSocketFlow(roomId: Int)(implicit blockTypeJsonFormat: JsonFormat[BT]): Future[Flow[Message, Message, _]] = {
    implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
    implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
    implicit val streamInputJsonFormat = new StreamInputJsonFormat[BT]
    actorSystem
      .actorSelection(waitRoomActorRef.path / roomId.toString)
      .resolveOne(3.seconds)
      .flatMap { actorRef =>
        actorRef
          .ask(CreateWebSocketWorkerActor)(3.seconds)
          .map {
            case CreatedWebSocketWorkerActor(actorRef) => actorRef
          }(Implicits.global)
      }(Implicits.global)
      .map { actorRef =>
        Flow.fromSinkAndSource(
          Flow[Message]
            .flatMapConcat {
              case TextMessage.Strict(x) =>
                Source.single(x.parseJson.convertTo[FlowIOType])
              case TextMessage.Streamed(source) =>
                source
                  .fold(StringBuilder.newBuilder)((z, s) => z.append(s))
                  .map(x => x.toString().parseJson.convertTo[FlowIOType])
            }
            .to(
              Sink.fromSubscriber(
                ActorSubscriber[FlowIOType](actorRef)
              )
            ),
          Source
            .fromPublisher(
              ActorPublisher[FlowIOType](actorRef)
            )
            .map(x => TextMessage.Strict(x.toJson.prettyPrint))
        )
      }(Implicits.global)
  }

  def routeWebSocket(implicit blockTypeJsonFormat: JsonFormat[BT]) =
    path("room" / IntNumber) { roomId =>
      extractUpgradeToWebSocket { upgrade =>
        complete {
          prepareWebSocketFlow(roomId)
            .map(upgrade.handleMessages(_))(Implicits.global)
            .recover { case ex => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
        }
      }
    }

  def routeGetReplayList =
    path("replay") {
      validateAcceptHeader {
        get {
          complete {
            replayRoomActorRef
              .ask(replayRoom.GetReplayList)(3.seconds)
              .map { x =>
                HttpResponse(
                  StatusCodes.OK,
                  List(),
                  HttpEntity.Strict(
                    ContentTypes.`application/json`,
                    ByteString(x.asInstanceOf[replayRoom.GotReplayList].list.toJson.prettyPrint)
                  )
                )
              }(Implicits.global)
              .recover { case ex => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
          }
        }
      }
    }

  def routeGetReplay(implicit blockTypeJsonFormat: JsonFormat[BT]) = {
    implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
    implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
    implicit val streamInputJsonFormat = new StreamInputJsonFormat[BT]
    path("replay" / IntNumber) { replayId =>
      validateAcceptHeader {
        get {
          complete {
            replayRoomActorRef
              .ask(replayRoom.GetReplay(replayId))(3.seconds)
              .map { x =>
                x.asInstanceOf[replayRoom.GotReplay] match {
                  case replayRoom.GotReplay(Some(stream)) =>
                    val resultStream =
                      stream.map { x =>
                        HttpEntity.ChunkStreamPart(x.toJson.prettyPrint)
                      }
                    HttpResponse(
                      StatusCodes.OK,
                      List(),
                      HttpEntity.Chunked(
                        ContentTypes.`application/json`,
                        Source.fromIterator(() => resultStream.toIterator)
                      )
                    )
                  case replayRoom.GotReplay(None) => HttpResponse(StatusCodes.NotFound)
                  case _ => HttpResponse(StatusCodes.InternalServerError)
                }
              }(Implicits.global)
              .recover { case ex => HttpResponse(StatusCodes.InternalServerError) }(Implicits.global)
          }
        }
      }
    }
  }

  def finalRoute(implicit blockTypeJsonFormat: JsonFormat[BT]) =
    routeCreateRoom ~ routeGetRoomList ~ routeWebSocket ~ routeGetReplayList ~ routeGetReplay
}
