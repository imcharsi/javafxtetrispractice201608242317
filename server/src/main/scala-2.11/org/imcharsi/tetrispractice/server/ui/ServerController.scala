/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.ui

import java.net.URL
import java.util.ResourceBundle
import javafx.beans.binding.When
import javafx.beans.property.SimpleBooleanProperty
import javafx.fxml.{ FXML, Initializable }
import javafx.scene.control.{ TextField, ToggleButton }
import javafx.stage.{ Stage, WindowEvent }

import akka.http.scaladsl.Http.ServerBinding
import org.imcharsi.tetrispractice.common.util.RxUtil
import org.imcharsi.tetrispractice.server.http.HttpPreparing
import rx.lang.scala.{ JavaConversions => JC, Observable, Scheduler, Worker }
import rx.observables.{ JavaFxObservable => JFO }

import scala.concurrent.ExecutionContext.Implicits
import scala.util.{ Failure, Success }

/**
 * Created by i on 9/10/16.
 */
class ServerController(
    stage: Stage,
    javaFxScheduler: Scheduler,
    ioScheduler: Scheduler,
    javaFxWorker: Worker,
    ioWorker: Worker
) extends Initializable {
  @FXML
  private var toggleButtonStart: ToggleButton = null
  @FXML
  private var textFieldServerName: TextField = null
  @FXML
  private var textFieldServerPort: TextField = null

  private var serverBinding: ServerBinding = null

  // Client 와 Server 의 ActorSystem 을 분리해야 한다.
  // Client 는 한 번만 만들면 되는데, Server 는 껐다 켰다를 반복할 수 있기 때문에 켤 때마다 매번 만들어야 한다.
  // 그래서 편의를 위해서 Server 의 ActorSystem 은 HttpPreparing 에 같이 넣었다.
  // HttpPreparing 에 ActorSystem 을 넣어야 할 이유가 없는데,
  // HttpPreparing 말고는 ActorSystem 을 쓰는 장소도 없기 때문에 편의를 위해서 이와 같이 했다.
  // Mutable State 로 두기로 했다. 이 단계에서는 굳이 Immutable 로 써야 할 이유가 없다.
  private var httpPreparing: Option[HttpPreparing] = None

  val selected = new SimpleBooleanProperty(false)
  val temporaryToggleButtonDisable = new SimpleBooleanProperty(false)

  def shutdown(): Unit = {
    httpPreparing
      .foreach(httpPreparing =>
        httpPreparing.http.shutdownAllConnectionPools()
          .flatMap(_ => httpPreparing.actorSystem.terminate())(Implicits.global))
    httpPreparing = None
  }

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    // 함수 객체 안에서 var 참조를 쓰니, null 예외가 생긴다.
    // Closure 안에서 사용되는 참조는 final 이어야 하는 규칙이 Scala Compiler 에 의해서는 확인되지 않는 듯 하다.
    // 대안은, 이 method 안에서 val 참조변수를 만들어서 쓰는 방법과, 지금 한 방법과 같이 bind 용 객체를 따로 만드는 방법이 있다.
    // bind 용 객체를 따로 만드는 방법이 계산과정을 나타낼 수 있어야 좋다.
    toggleButtonStart.disableProperty().bind(temporaryToggleButtonDisable)
    toggleButtonStart.selectedProperty().bindBidirectional(selected)
    textFieldServerName.disableProperty().bind(selected.or(temporaryToggleButtonDisable))
    textFieldServerPort.disableProperty().bind(selected.or(temporaryToggleButtonDisable))

    toggleButtonStart.textProperty()
      .bind(
        new When(toggleButtonStart.selectedProperty())
          .then("stop")
          .otherwise("start")
      )

    val observableToggleButton =
      RxUtil.combine(
        JC.toScalaObservable(JFO.fromActionEvents(toggleButtonStart)),
        JC.toScalaObservable(JFO.fromObservableValue(toggleButtonStart.selectedProperty()))
          .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty())))
          .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(RxUtil.convertJavaFXProperty((x: String) => Integer.valueOf(x))(textFieldServerPort.textProperty()))))
          .map { case (((selected, serverName), serverPort)) => (selected.booleanValue(), serverName, serverPort) }
      )

    observableToggleButton
      .filter {
        case (_, Some((true, _, Some(_)))) => true
        case _ => false
      }
      .map {
        case (_, Some((_, serverName, Some(serverPort)))) => (serverName, serverPort)
      }
      .observeOn(javaFxScheduler)
      .doOnNext { _ =>
        temporaryToggleButtonDisable.set(true)
        shutdown()
        httpPreparing = Some(new HttpPreparing)
      }
      .observeOn(ioScheduler)
      .flatMap {
        case (serverName, serverPort) =>
          Observable.from(
            httpPreparing.get.bind(serverName, serverPort)
              .map(Success(_))(Implicits.global)
              .recover { case ex => Failure(ex) }(Implicits.global)
          )(Implicits.global)
      }
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case Success(sb) => serverBinding = sb
          case Failure(_) =>
            selected.set(false)
            shutdown()
        }
        temporaryToggleButtonDisable.set(false)
      }

    observableToggleButton
      .filter {
        case (_, Some((false, _, _))) => true
        case _ => false
      }
      .observeOn(javaFxScheduler)
      .doOnNext(_ => temporaryToggleButtonDisable.set(true))
      .observeOn(ioScheduler)
      .flatMap {
        case (serverName, serverPort) =>
          Observable.from(
            // 한 가지 문제가 있다.
            // 당장에 unbind 를 해도, 이미 만들어져 있던 Connection Pool 이 남아있는 동안에는 계속 연결은 되는데,
            // 동작은 하지 않는다. 연결만 되는 상황이다.
            serverBinding.unbind().map(Success(_))(Implicits.global).recover { case ex => Failure(ex) }(Implicits.global)
          )(Implicits.global)
      }
      .subscribe { _ =>
        temporaryToggleButtonDisable.set(false)
        // impo 그래서, 대안은 Client 와 Server 의 ActorSystem 을 나눠서 써야 한다.
        shutdown()
      }
    JC.toScalaObservable(JFO.fromWindowEvents(stage, WindowEvent.WINDOW_CLOSE_REQUEST))
      .observeOn(ioScheduler)
      .subscribe { _ => shutdown() }
  }
}
