/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.Actor
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.{ GameRoomClientCommand, GameRoomServerCommand, NotifyJoinRoom }
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ Prepare, StreamInput }
import org.imcharsi.tetrispractice.common.data.fsm.Player
import org.imcharsi.tetrispractice.common.data.fsm.Player.Player

import scala.collection.immutable.Queue

/**
 * Created by i on 2016-09-19.
 */
class ReplayRoom[BT] {
  def prepare(o: Option[(Set[Player], Queue[StreamInput[BT]])]): Option[Stream[Either[GameRoomClientCommand, Either[GameRoomServerCommand, StreamInput[BT]]]]] = {
    o.map {
      case (playerSnapshot, replayQueue) =>
        playerSnapshot.toStream.map(x => Right(Left(NotifyJoinRoom(0, Player.convertToRemotePlayer(x)))))
          .append(replayQueue.toStream.map(x => Right(Right(x))))
    }
  }

  class ReplayRoomActor extends Actor {
    private[actor] var replayRepository: Map[Int, (Set[Player], Queue[StreamInput[BT]])] = Map()
    private[actor] var replayIdCounter: Int = 0

    override def receive: Receive = {
      case RegisterReplay(playerSnapshot, recordQueue) =>
        val newReplayId = replayIdCounter
        replayIdCounter = replayIdCounter + 1
        replayRepository = replayRepository + (newReplayId -> (playerSnapshot, recordQueue))
      case GetReplayList =>
        sender().tell(GotReplayList(replayRepository.keySet.toList), self)
      case GetReplay(replayId) =>
        sender().tell(
          GotReplay(prepare(replayRepository.get(replayId))),
          self
        )
    }
  }

  sealed case class RegisterReplay(playerSnapshot: Set[Player], recordQueue: Queue[StreamInput[BT]])

  case object GetReplayList

  sealed case class GotReplayList(list: List[Int])

  sealed case class GetReplay(replayId: Int)

  sealed case class GotReplay(replay: Option[Stream[Either[GameRoomClientCommand, Either[GameRoomServerCommand, StreamInput[BT]]]]])

  // 재생할 때는 WebSocket 이 필요없다. Server 로부터 자료를 받기만 하기 때문이다.
  // Server 가 Client 에게 재생용 자료를 보낼 때는 ChunkStreamPart 로 보낸다.
  // 또한, 실제 게임할 때 주고받는 자료의 양식 그대로
  // Right(Left(NotifyJoinRoom(...))) 을 보낸 뒤 이어서 Right(Right(StreamInput(...))) 을 보낸다.
  // 앞서 밝혔듯이 응답은 필요없고,
  // Client 는 실제 게임할 때 Side-Effect 처리하듯이,
  // NotifyJoinRoom 이 오면 RemotePlayer FSM 을 만들고
  // StreamInput 이 오면 대응하는 RemotePlayer FSM 으로 보내면 된다.

}
