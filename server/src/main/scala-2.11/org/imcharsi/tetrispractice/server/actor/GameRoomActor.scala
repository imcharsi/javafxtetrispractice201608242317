/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor._
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ RemoveFromWaitRoomList, UpdateWaitRoom }
import org.imcharsi.tetrispractice.common.data.fsm.Command._
import org.imcharsi.tetrispractice.common.data.fsm.Player
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ LocalPlayer, Player }

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration.FiniteDuration

/**
 * Created by i on 9/3/16.
 */
class GameRoomActor[BT](
    val waitRoomActorRef: ActorRef,
    val replayRoomActorRef: ActorRef,
    val replayRoom: ReplayRoom[BT],
    val roomId: Int,
    val password: Int,
    val timeoutCloseRoom: FiniteDuration,
    val timeoutNotifyJoinRoom: FiniteDuration
) extends Actor {
  // 왜 이와 같은 기능이 필요한가.
  // 방장이 게임을 시작하고자 했던 준비완료상태와
  // 실제로 방장의 게임 시작이 Server 로 도착하기 전에 더 빨리 도착된 Cancel/Prepare 입력으로
  // 새로 준비완료상태가 갖춰졌을 때 이 두 준비완료상태를 같은 준비완료로 볼 것인지 문제된다.
  // 구상에서는 다른 준비완료로 보고자 한다.
  // 따라서, 방장의 게임 시작이 Server 로 도착한 시점에서 결과적으로 모두 준비완료됐어도 게임 시작은 실패해야 한다.
  private[actor] var preparedId: Option[Int] = None
  private[actor] var preparedIdCount: Int = 0
  private[actor] var starter: Option[Player] = None
  // 각 Player 의 준비상태를 기록한다. 방장에게 게임을 시작할 수 있는 상태에 이르렀음을 알리는 기준이 된다.
  private[actor] var preparedMap: Map[Player, Boolean] = Map()
  // akka actor route 기능을 쓰기에는 불편한데,
  // LocalPlayer 가 Server 로 전달한 입력을 Broadcast 할 때는 LocalPlayer Client 는 제외하고
  // 나머지 Client 로 Broadcast 하기 때문이다.
  private[actor] var webSocketWorkerActorRefMap: Map[Player, ActorRef] = Map()
  private[actor] var userIdCounter: Int = 0
  private[actor] var alreadyStartedGame: Boolean = false
  // 일정시간 안에 개설자가 입장하지 않으면 방을 폐쇄한다.
  private[actor] var cancellableCloseRoom: Option[Cancellable] = None
  private[actor] var responseIdCounterNotifyJoinRoom: Int = 0
  // 이와 같이 Player 와 Cancellable 을 묶어야, 예를 들어 3개의 RemotePlayer 를 준비해야 하는 중
  // 2번째 응답으로부터 실패를 알림받았을 때 세번째 시간초과를 놓치지 않고 취소할 수 있다.
  private[actor] var cancellableMapNotifyJoinRoom: Map[Int, (Player, Cancellable)] = Map()
  // 이 기능은, LocalPlayer 가 깬 줄을 누구에게 전달할지 판단하기 위해 쓴다.
  // key 는 LocalPlayer 이고 value 는 LocalPlayer 가 깬 줄을 보낸 회수이다.
  // 우선 Server 가 깬 줄을 받을 때마다 누가 몇번을 보냈는지 확인하고
  // webSocketWorkerActorRefMap 에서, 자신을 제외한 나머지 사용자 중 한사람을 고르는데
  // 보낸 회수를 사용자 수로 나눈 나머지로 고른다.
  // 따라서 총 4명이 있고 어느 한 사용자가 혼자서 8번의 깬 줄을 보냈다면
  // 8번째의 깬 줄 보내기에서 이 줄을 받을 대상은
  // 0에서 시작하고 대상을 고른 후 회수를 더하는 방식에 따라 7 % 3 = 1 과 같이 계산하여
  // 자신을 제외한 나머지 사용자 중 2번째 사용자에게 보내기로 한다.
  // 이와 같이 하는 것이 만들기 쉬우면서 비교적 공정한 분배방법이다.
  private[actor] var turnToReceiveBePushed: Map[Player, Int] = Map()
  // 재생용 Queue 이다. Server 가 Client 로 Broadcast 한 입력만 기록한다.
  // 따라서, PreparePush 입력을 받고 특정 LocalPlayer FSM 으로 Push 입력을 해주는 것과
  // StartGames 입력을 받고 모든 LocalPlayer FSM 으로 Start 입력을 해주는 것은 기록하지 않는다.
  // 재생은 실제 게임을 진행할 때와는 달리 Broadcast 가 필요없다.
  // 예를 들어, LocalPlayer1 이 보낸 입력을 Client2,3,4 에 있는 RemotePlayer1 에 Broadcast 해야 하지만
  // 재생할 때에는 Client1 에 있는 RemotePlayer1 에만 재생용 자료를 보내면 되기 때문이다.
  // 원칙은, Server 가 Broadcast 를 했으면 한 개의 재생용 자료 기록한다.
  // 예외는, 연결이 끊겼거나 비정상 종료했을 때
  // 실제로는 Stop Broadcast 요청을 받은 적이 없지만 결과적으로 Stop Broadcast 하기 때문에 이 단계에서는 재생용 자료 기록을 한다.
  private[actor] var recordQueue: Option[Queue[StreamInput[BT]]] = None
  private[actor] var playerSnapshot: Option[Set[Player]] = None

  // 문제를 정리하면
  // 준비상태에서 준비취소할 때와 게임 시작이 겹칠 때
  // 준비상태에서 퇴장하기와 게임 시작이 겹칠 때
  //
  // 가장 단순한 경우부터 쓰면 아래와 같다.
  //
  // 1. 준비상태에서 준비취소하면 LocalPlayer 가 FSM 으로 Cancel 을 입력하게 되고
  //    Cancelled 의 출력을 받으면 Server 로 Cancel 입력을 전달한다.
  // 2. Server 는 Cancel 입력을 전달받으면 LocalPlayer 에 대응하는 모든 RemotePlayer 로 Cancel 입력을 Broadcast 한다.
  //
  //
  // 1. 준비상태에서 퇴장하면 LocalPlayer 가 FSM 으로 Leave 를 입력하게 되고
  //    Leaved 의 출력을 받으면 Server 로 Leave 입력을 전달한다.
  // 2. Server 는 Leave 입력을 전달받으면 LocalPlayer 에 대응하는 모든 RemotePlayer 로 Leave 입력을 Broadcast 한다.
  //
  // 연결에 아무 문제가 없으면서 Server 의 Start Broadcast 와 겹치면 아래와 같다.
  //
  // 1. Server 가 Start Broadcast 를 했는데, 아직 도착하지 않는 중
  //    LocalPlayer FSM 으로 Cancel 을 입력하면 Cancelled 의 출력에 이어 Server 로 Cancel 입력을 전달한다.
  // 2. Server 가 보낸 Start Broadcast 가 NotYetPreparedGame 상태의 LocalPlayer FSM 으로 입력되고 안되고는
  //    중요하지 않고 Start Broadcast 이후에 Cancel 을 받았다는 사실만이 중요하다.
  //    이 때는, Server 가 Leave 를 입력받은 것처럼 다루고, 연결을 끊는다.
  // 3. LocalPlayer FSM 에서 보기에, Cancel 을 Server 로 전달하고 이어서 Start Broadcast 입력을 받게 될 수 있는데,
  //    이 때는 Leave 입력을 받은 것처럼 다룬다.
  //    따라서 Server 로부터 LocalPlayer Start 을 받고 LocalPlayer Leave 를 Server 로 전달하게 된다.
  //    그런데, Server 는 해당 연결을 이미 끊었거나 끊을 예정이므로,
  //    이는 처리되지 않을 것이고 결국 이유를 알 수 없는 연결 끊김의 결과가 된다.
  //
  // 1. Server 가 Start Broadcast 를 했는데, 아직 도착하지 않는 중 퇴장을 하면
  //    LocalPlayer FSM 으로 Leave 를 입력하게 되고 Server 로 Leave 입력을 전달하고 화면을 끝낸다.
  //    화면에 아무것도 남지 않게 되고 FSM 도 기능하지 않는다.
  // 2. 이어서 Server 의 Start Broadcast 는 LocalPlayer 에 의해 처리되지 않는다.
  // 3. Server 가 전달받은 Leave 입력을 Leave 가 아닌 Stop 으로서 RemotePlayer 로 Broadcast 하여
  //    각 RemotePlayer 들이 게임을 그만한 것처럼 화면에 보이게 한다.
  //    Server 내부에서 해당 LocalPlayer 에 관한 연결을 끊는다.
  //
  // 연결에 문제가 있으면서 Server 의 Start Broadcast 와 겹치면 아래와 같이 한다.
  //
  // Start Broadcast 를 보낼 수 있었다는 말은 모든 사용자가 준비완료했다는 말이고
  // NotYetStartedGame 상태에 있으므로 Stop 입력을 받을 수 있는 상태이다.
  // Start Broadcast 를 보내고 난 이후의 연결끊김은 대응하는 모든 RemotePlayer 로 Stop 입력을 해주고
  // Start Broadcast 를 보내기 전 연결끊김은 대응하는 모든 RemotePlayer 로 Leave 입력을 해준다.

  def calculatePreparedId(): Unit = {
    // 아직 게임을 시작하지 않았음을 전제로 한다.
    // 따라서, 이미 게임을 시작했으면 결과는 항상 preparedId = None 이 된다.
    preparedMap.foldLeft(!alreadyStartedGame) {
      case (true, (_, b)) => b
      case (false, _) => false
    } match {
      case true =>
        val newPreparedId = preparedIdCount
        preparedIdCount = preparedIdCount + 1
        preparedId = Some(newPreparedId)
      case false =>
        preparedId = None
    }
  }

  def postGone(player: Player): Unit = {
    // 이 기능은 Server 가 사용자의 퇴장/연결종료/잘못된 입력을 인식했을 때 Leave 출력이나 Stop 출력을 한다.
    // 예를 들면 게임 시작 전에 실제로 Leave 가 입력됐거나 잘못된 입력이 있었으면 Leave 출력을 한다.
    // 게임 시작 후에, 잘못된 입력이 있었거나 연결이 끊겼으면 Stop 출력을 한다.
    val remotePlayer = Player.convertToRemotePlayer(player)

    if (!alreadyStartedGame) {
      // 아무도 없으면 아무것도 실행되지 않는다. 따로 빈 방인지 여부를 검사할 필요가 없다.
      webSocketWorkerActorRefMap
        .filterNot(x => x._1 == player)
        .foreach {
          case (_, webSocketWorkerActorRef) =>
            webSocketWorkerActorRef.tell(Leave(remotePlayer), self)
        }
      if (starter.exists(_ == player)) {
        starter = webSocketWorkerActorRefMap.headOption.map(_._1)
        starter.foreach { starterPlayer =>
          webSocketWorkerActorRefMap
            .filterNot(x => (x._1 == player))
            .foreach {
              case (_, webSocketWorkerActorRef) =>
                // 앞의 구상에서는 RemotePlayer 로서 전달했는데, 굳이 그럴 필요가 없다.
                // LocalPlayer 로 전달해도 된다.
                webSocketWorkerActorRef.tell(NotifyChangeStarter(starterPlayer), self)
            }
        }
        // 이 단계에서 새 방장을 찾지 못했다면 어떻게 되는가.
        // 이 단계에서는 아무것도 할 것이 없고, WebSocketWorkerActor 를 종료하게 되면
        // 이 Actor 의 Terminated 를 받아서 방에 아무도 없음을 확인했을 때 방을 폐쇄하게 된다.
      }
      starter
        .flatMap(webSocketWorkerActorRefMap.get(_))
        .foreach(_.tell(NotifyBeAbleToStart(preparedId), self))
    } else {
      val remotePlayer = Player.convertToRemotePlayer(player)
      webSocketWorkerActorRefMap.foreach {
        case (_, webSocketWorkerActorRef) =>
          webSocketWorkerActorRef.tell(Stop[BT](remotePlayer, ""), self)
      }
      // 이 단계는, Client 로부터 명시적인 Stop Broadcast 요청은 없었지만,
      // 연결이 끊어졌거나 비정상적으로 종료했거나 하는 이유로 Stop Broadcast 요청을 받은 것처럼 다루는 단계이다.
      // 따라서, 재생용 자료 기록을 한다.
      recordQueue = recordQueue.map(_.enqueue(Stop[BT](remotePlayer, "")))
    }

    // 아직 게임을 시작하지 않았을 때만 목록을 갱신한다. 게임을 시작하면 목록에서 방이 없어진다.
    if (!alreadyStartedGame)
      waitRoomActorRef.tell(UpdateWaitRoom(roomId, webSocketWorkerActorRefMap.size), self)
  }

  def removeFromMap(player: Player): Unit = {
    webSocketWorkerActorRefMap = webSocketWorkerActorRefMap - player
    preparedMap = preparedMap - player
    turnToReceiveBePushed = turnToReceiveBePushed - player
    cancellableMapNotifyJoinRoom
      .filter(_._2._1 == player)
      .foreach(_._2._2.cancel())
    cancellableMapNotifyJoinRoom
      .filter(_._2._1 == player)
      .map(_._1)
      .foldLeft(cancellableMapNotifyJoinRoom) { case (map, responseId) => map - responseId }
  }

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    cancellableCloseRoom = Some(context.system.scheduler.scheduleOnce(timeoutCloseRoom, self, PoisonPill)(Implicits.global))
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
    cancellableCloseRoom.foreach(_.cancel())
    cancellableCloseRoom = None
    Console.out.println("GameRoomActor stop.")
    playerSnapshot.foreach { playerSnapshot =>
      recordQueue.foreach { recordQueue =>
        replayRoomActorRef.tell(replayRoom.RegisterReplay(playerSnapshot, recordQueue), ActorRef.noSender)
      }
    }
  }

  override def receive: Receive = {
    case CreateWebSocketWorkerActor =>
      // WebSocketWorkerActor 를 만들 때 watch 를 하지 않고 입장을 성공할 때 watch 를 한다.
      sender().tell(
        CreatedWebSocketWorkerActor(
          context.actorOf(Props(new WebSocketWorkerActor[BT](self)))
        ),
        self
      )
    case Terminated(terminated) =>
      // 정말로 연결이 끊겨서 이 단계에 이른 경우라면 예상외의 입력/연결종료이 있었다고 보고 이에 대한 처리를 하면 되고,
      // 예상외의 입력이 있었다거나 하는 이유로 관련된 처리를 전부 끝내놓고
      // 정리 작업으로서 WebSocketWorkerActor 를 종료하면서 이 단계에 이른 거라면
      // 빈 방 확인 목적으로 이 단계에 이르는 것이다.
      // 빈 방 확인은 GameRoomActor 의 child 인 WebSocketWorkerActor 가 종료할 때 한다.
      webSocketWorkerActorRefMap
        .toStream
        .map { case (player, actorRef) => (actorRef, player) }
        .filter { case (actorRef, _) => actorRef == terminated }
        .headOption
        .map(_._2) match {
          case Some(terminatedPlayer) =>
            removeFromMap(terminatedPlayer)
            calculatePreparedId()
            postGone(terminatedPlayer)
          case None =>
            // 빈 방 확인의 목적으로 이 단계에 이를 수 있다.
            // 따라서 이 경우는 있을 수 있는 경우이다.
            ()
        }
      if (webSocketWorkerActorRefMap.isEmpty) {
        waitRoomActorRef.tell(RemoveFromWaitRoomList(roomId), self)
        context.stop(self)
      }
    case Left(CreatorJoinRoom(receivedPassword, name)) =>
      if (starter.isEmpty && password == receivedPassword) {
        val newUserId = userIdCounter
        userIdCounter = userIdCounter + 1
        val newPlayer = LocalPlayer(newUserId, name)
        webSocketWorkerActorRefMap = webSocketWorkerActorRefMap + (newPlayer -> sender())
        preparedMap = preparedMap + (newPlayer -> false)
        turnToReceiveBePushed = turnToReceiveBePushed + (newPlayer -> 0)
        alreadyStartedGame = false
        starter = Some(newPlayer)
        sender().tell(CreatorJoinedRoom(Some(newPlayer)), self)
        sender().tell(NotifyChangeStarter(newPlayer), self)
        sender().tell(NotifyBeAbleToStart(preparedId), self)
        cancellableCloseRoom.foreach(_.cancel())
        cancellableCloseRoom = None
        // sender 는 항상 WebSocketWorkerActor 라 가정한다.
        // 왜 watch 를 WebSocketWorkerActor 를 만드는 단계에서 하지 않고 입장을 성공한 단계에서 하는가.
        // CreatorJoinGame/JoinGame 의 시도는 한 번만 할 수 있다.
        // 따라서, 실패하면 WebSocketWorkerActor 를 종료하게 된다.
        // watch 를 WebSocketWorkerActor 생성 단계에서 하면, 입장 시도를 실패할 때 사실상 입장이 없었던 것과 같은 상황인데
        // Terminated 를 받는 단계에서 빈 방이라고 인식하게 되어 방을 폐쇄하는 단계에 이른다.
        context.watch(sender())
        // 개설자가 입장했을 때 WaitRoomActor 의 방 목록을 갱신한다.
        waitRoomActorRef.tell(UpdateWaitRoom(roomId, webSocketWorkerActorRefMap.size), self)
      } else {
        // 입장 시도는 한번만 할 수 있고, 실패하면 연결을 끊는다.
        // WebSocketWorkerActor 가 실패의 응답을 인식하고 스스로 종료해야 한다.
        sender().tell(CreatorJoinedRoom(None), self)
        // 방장 입장에 실패해도 GameRoomActor 는 종료하지 않는다. WebSocketWorkerActor 만 종료한다.
      }
    case Left(JoinRoom(name)) =>
      if (starter.isDefined) {
        if (!alreadyStartedGame) {
          val newUserId = userIdCounter
          userIdCounter = userIdCounter + 1
          val newPlayer = LocalPlayer(newUserId, name)
          val newRemotePlayer = Player.convertToRemotePlayer(newPlayer)
          sender().tell(JoinedRoom(Some(newPlayer)), self)
          webSocketWorkerActorRefMap
            .foreach {
              case (player, webSocketWorkerActorRef) =>
                val responseId1 = responseIdCounterNotifyJoinRoom
                val responseId2 = responseIdCounterNotifyJoinRoom + 1
                responseIdCounterNotifyJoinRoom = responseIdCounterNotifyJoinRoom + 2
                // 이미 입장해 있던 사용자한테, 새로 입장한 사용자의 입장을 알린다.
                webSocketWorkerActorRef.tell(NotifyJoinRoom(responseId1, newRemotePlayer), self)
                // 새로 입장한 사용자한테, 앞에서 입장해 있었던 사용자의 입장을 알린다.
                sender().tell(NotifyJoinRoom(responseId2, Player.convertToRemotePlayer(player)), self)
                // 각 Client 한테, RemotePlayer 를 준비하라는 알림을 하고 이 알림에 대한 응답을 기다린다.
                cancellableMapNotifyJoinRoom =
                  cancellableMapNotifyJoinRoom +
                    (responseId1 ->
                      (player, context.system.scheduler.scheduleOnce(
                        timeoutNotifyJoinRoom,
                        self,
                        Right(Left(NotifiedJoinRoom(responseId1, player, false)))
                      )(Implicits.global))) +
                        (responseId2 ->
                          (newPlayer, context.system.scheduler.scheduleOnce(
                            timeoutNotifyJoinRoom,
                            self,
                            Right(Left(NotifiedJoinRoom(responseId2, newPlayer, false)))
                          )(Implicits.global)))
            }
          // 새로 입장한 사용자한테, 방장이 누구인지 알린다.
          starter.foreach(player => sender().tell(NotifyChangeStarter(player), self))
          // 이미 입장해 있었던 사용자가 준비완료 상태에 있다면 이 또한 알려야 한다.
          // 이를 알리지 않으면 새로 입장한 사용자는 이미 입장해 있는 사용자가 준비완료도 했다는 사실을 모르게 된다.
          preparedMap
            .filter(_._2)
            .flatMap(x => webSocketWorkerActorRefMap.get(x._1).map(xx => (x._1, xx)).toList)
            .foreach {
              case (player, actorRef) =>
                sender().tell(Prepare(Player.convertToRemotePlayer(player)), self)
            }
          webSocketWorkerActorRefMap = webSocketWorkerActorRefMap + (newPlayer -> sender())
          preparedMap = preparedMap + (newPlayer -> false)
          turnToReceiveBePushed = turnToReceiveBePushed + (newPlayer -> 0)
          calculatePreparedId()
          // 이번 입장 바로 직전의 모든 사용자들이 모두 준비완료를 했었어도
          // 새로운 사용자의 입장으로 게임을 시작할 수 없는 상태로 바뀐다.
          starter
            .flatMap(webSocketWorkerActorRefMap.get(_))
            .foreach(_.tell(NotifyBeAbleToStart(preparedId), self))
          context.watch(sender())
          waitRoomActorRef.tell(UpdateWaitRoom(roomId, webSocketWorkerActorRefMap.size), self)
        } else {
          // 이미 게임을 시작했으면 입장할 수 없다.
          // WebSocketWorkerActor 가 입력을 인식하여 Actor 를 끝낼지 판단한다.
          // 따로 정지 입력을 해주지 않는다.
          sender().tell(JoinedRoom(None), self)
        }
      } else {
        // 개설자가 입장하기 전에는 입장할 수 없다.
        sender().tell(JoinedRoom(None), self)
      }
    case Right(Right(leave @ Leave(player))) =>
      webSocketWorkerActorRefMap.get(player) match {
        case Some(webSocketWorkerActorRef) =>
          removeFromMap(player)
          calculatePreparedId()
          // 이 설명은 이 Actor 에서 자주 사용되는 removeFromMap -> calculatePreparedId -> postGone pattern 의
          // 일반적인 원리를 다룬다.
          // 이미 게임을 시작했고 시간차로 Leave/Cancel 을 입력했는데
          // Leave/Cancel 을 입력한 사용자에 대응하는 RemotePlayer 는 이미 Start 입력을 받고 RunningGame 상태에 이르는 경우는 없다.
          // 왜냐하면 RemotePlayer 가 RunningGame 에 이르기 위해서는
          // LocalPlayer 가 Server 로 보낸 Start 입력을, Server 가 대응하는 모든 RemotePlayer 로 Broadcast 해야 하기 때문이다.
          // 따라서 대응하는 RemotePlayer 는 아직 NotYetStartedGame 상태에 있게 되고 Leave/Cancel 입력을 받을 수 있다.
          // 그러나, Client 의 문제로 인해 RemotePlayer 도 RunningGame 상태에 이르고 난 이후에
          // LocalPlayer 가 Server 로 Leave/Cancel 을 입력할 수 있다.
          // 그래서 Server 가 Start Broadcast 를 한 이후에는 문제되는 LocalPlayer 에 대응하는 RemotePlayer 가
          // 아직 NotYetStartedGame 상태에 있을거라 예상된다 해도 Stop Broadcast 를 한다.
          // 이 단계를 포함하여 예상외의 입력이 주어지는 경우에도 모두 똑같이 설명할 수 있다.
          postGone(player)
        case None =>
          // 입장하지 않았던 사용자로부터 입력이 있으면 연결을 끊어야 하는데,
          // Leave 입력의 경우 항상 연결을 끊으므로, 이 단계에서 굳이 연결을 끊지 않고 다음 단계에서 끊는다.
          ()
      }
      // 이 단계에서는 빈 방 여부를 판단하지 않는다.
      sender().tell(QuitWebSocketWorkerActor, self)
    case Right(Right(prepare @ Prepare(player))) =>
      webSocketWorkerActorRefMap.get(player) match {
        case Some(webSocketWorkerActorRef) =>
          if (!alreadyStartedGame) {
            preparedMap = preparedMap + (player -> true)
            calculatePreparedId()
            // Client 가 Server 로 보내는 StreamInput 은 전부 응답을 바라지 않는 통보다. 응답을 바라는 요청이 아니다.
            val remotePlayer = Player.convertToRemotePlayer(player)
            // 다른 Client 에 있는 대응하는 모든 RemotePlayer 로 준비완료를 알린다.
            webSocketWorkerActorRefMap
              .filterNot(x => x._1 == player)
              .foreach {
                case (_, webSocketWorkerActorRef) =>
                  webSocketWorkerActorRef.tell(Prepare(remotePlayer), self)
              }
            // 방장에게, 게임을 시작할 수 있는지 알린다.
            starter
              .flatMap(webSocketWorkerActorRefMap.get(_))
              .foreach(_.tell(NotifyBeAbleToStart(preparedId), self))
          } else {
            removeFromMap(player)
            // 이미 게임을 시작했기 때문에 preparedId 를 계산할 필요가 없다.
            postGone(player)
            // 예상되지 않은 입력을 받으면 연결을 끊는다. 따로 복구 절차를 거치지 않는다.
            sender().tell(QuitWebSocketWorkerActor, self)
          }
        case None =>
          sender().tell(QuitWebSocketWorkerActor, self)
      }
    case Right(Right(cancel @ Cancel(player))) =>
      webSocketWorkerActorRefMap.get(player) match {
        case Some(webSocketWorkerActorRef) =>
          val remotePlayer = Player.convertToRemotePlayer(player)
          if (!alreadyStartedGame) {
            preparedMap = preparedMap + (player -> false)
            calculatePreparedId()
            webSocketWorkerActorRefMap
              .filterNot(x => x._1 == player)
              .foreach {
                case (_, webSocketWorkerActorRef) =>
                  webSocketWorkerActorRef.tell(Cancel(remotePlayer), self)
              }
            starter
              .flatMap(webSocketWorkerActorRefMap.get(_))
              .foreach(_.tell(NotifyBeAbleToStart(preparedId), self))
          } else {
            removeFromMap(player)
            postGone(player)
            sender().tell(QuitWebSocketWorkerActor, self)
          }
        case None =>
          sender().tell(QuitWebSocketWorkerActor, self)
      }
    case Left(StartGames(receivedPreparedId)) =>
      // Prepare/Cancel 에 의해 preparedId 는 항상 최신상태를 유지하고 있어야 한다.
      preparedId.exists(_ == receivedPreparedId) match {
        // 아직 게임을 시작하지 않았으면서 게임을 시작할 수 있어야 한다.
        case true if !alreadyStartedGame =>
          alreadyStartedGame = true
          // 이 응답은 방장 Client 로 가야 한다.
          sender().tell(StartedGames(true), self)
          // Start 입력을 보낼 당시에 방에 남아있던 사용자만 재생용 자료에 기록한다.
          playerSnapshot = Some(webSocketWorkerActorRefMap.keySet)
          recordQueue = Some(Queue())
          webSocketWorkerActorRefMap.foreach {
            case (localPlayer, webSocketWorkerActorRef) =>
              // todo seed 바꾸기.
              // LocalPlayer 로서 Start 입력을 해줘야 한다.
              // LocalPlayer 가 Start 입력을 처리하여 다시 Server 로 Start 입력을 전달할 때
              // 이를 대응하는 모든 RemotePlayer 로 Broadcast 한다.
              webSocketWorkerActorRef.tell(Start(localPlayer, 0), self)
              // 여기서는 각각의 LocalPlayer FSM 으로 Start 입력을 해주는 단계이다.
              // 각각 Client 로부터 Start 입력을 Broadcast 해 달라는 요청을 받은 단계가 아니다.
              // 따라서 여기서는 재생용 자료 기록을 하지 않는다.
              // impo 그런데, 여기서 Prepare 재생용 자료를 기록하는 이유는 무엇인가.
              // NotYetPreparedGame 상태에서 NotYetStartedGame 상태로 바뀌어야 이어질 Start 입력을 받을 수 있게 된다.
              recordQueue = recordQueue.map(_.enqueue(Prepare[BT](Player.convertToRemotePlayer(localPlayer))))
          }
          waitRoomActorRef.tell(RemoveFromWaitRoomList(roomId), self)
        case _ =>
          // 게임 시작 시도는 실패할 수 있다.
          // 극단적인 경우, StartGames 가 Server 에 도착하기 전에
          // 다른 사용자의 Cancel/Prepare 가 먼저 도착할 수 있다.
          sender().tell(StartedGames(false), self)
      }
    case Right(Left(NotifiedJoinRoom(responseId, responsePlayer, result))) =>
      if (result) {
        cancellableMapNotifyJoinRoom.get(responseId).foreach(_._2.cancel())
        cancellableMapNotifyJoinRoom = cancellableMapNotifyJoinRoom - responseId
      } else {
        // sender 를 쓰지 않는 이유는 시간초과 설정으로 입력이 올 수도 있기 때문이다.
        // map 을 먼저 정리하면 WebSocketWorkerActor 를 찾을 수 없기 때문에 먼저 찾고 다음에 지운다.
        val webSocketWorkerActorRef = webSocketWorkerActorRefMap.get(responsePlayer)
        removeFromMap(responsePlayer)
        // 각 Client 로 RemotePlayer 를 준비하라는 알림을 했는데, 준비를 실패했다면 해당 Client 는 연결을 끊고
        // 다른 Client 로는, 연결이 끊긴 Client 의 퇴장으로 입력해준다. 현재 구상에서는 왜 나갔는지에 대한 알림이 없다.
        calculatePreparedId()
        postGone(responsePlayer)
        webSocketWorkerActorRef.foreach(_.tell(QuitWebSocketWorkerActor, self))
      }
    case Right(Right(x @ PreparePush(player, rowCount, bePushed))) =>
      if (alreadyStartedGame) {
        val filteredWebSocketWorkerActorRefMap =
          webSocketWorkerActorRefMap.filterNot(_._1 == player)
        // 자신을 제외한 나머지 사용자가 있을 때만 분배를 한다.
        if (!filteredWebSocketWorkerActorRefMap.isEmpty) {
          val count =
            turnToReceiveBePushed
              .get(player)
          count
            .flatMap { count =>
              filteredWebSocketWorkerActorRefMap
                .drop(count % filteredWebSocketWorkerActorRefMap.size)
                .headOption
            }
            .foreach {
              case (player, actorRef) =>
                actorRef.tell(Push(player, rowCount, bePushed, false), self)
              // 이 단계는, Client 가 Server 에 대해 Push 입력을 Broadcast 해 달라고 요청한 것을 받는 단계가 아니고
              // Server 가 특정 Client 의 LocalPlayer FSM 에게 Push 입력을 해주는 단계이다.
              // 그래서, 재생용 자료 기록을 하지 않는다.
            }
          count.foreach(count => turnToReceiveBePushed = turnToReceiveBePushed + (player -> (count + 1)))
        }
      } else {
        sender().tell(QuitWebSocketWorkerActor, self)
      }
    case Right(Right(streamInput: StreamInput[BT])) =>
      if (alreadyStartedGame) {
        val newStreamInput =
          streamInput match {
            case streamInput @ BlockTransform(player, _) =>
              Some(streamInput.copy[BT](player = Player.convertToRemotePlayer(player)))
            case streamInput @ Push(player, _, _, _) =>
              Some(streamInput.copy[BT](player = Player.convertToRemotePlayer(player)))
            case streamInput @ RealizeNextBlock(player) =>
              Some(streamInput.copy[BT](player = Player.convertToRemotePlayer(player)))
            case streamInput @ Stop(player, _) =>
              Some(streamInput.copy[BT](player = Player.convertToRemotePlayer(player)))
            // Starter 가 보낸 StartGames 요청을 Server 가 받아서
            // Server 가 각 Client 의 LocalPlayer FSM 으로 Start Broadcast 를 하고 나서
            // 각 Client 의 LocalPlayer FSM 으로부터 Start 입력 Broadcast 요청을 받아야 된다.
            // 한편, 지금 구상에서는 Client 가 Server 로 Start 입력을 중복하는 것을 막을 방법이 없다.
            // 이 경우 Client FSM 이 이를 인식하여 연결을 끊는 방법 밖에 없다.
            case streamInput @ Start(player, _) =>
              Some(streamInput.copy[BT](player = Player.convertToRemotePlayer(player)))
            case _ => None
          }
        newStreamInput match {
          case Some(newStreamInput) =>
            webSocketWorkerActorRefMap
              .filterNot(_._2 == sender())
              .foreach {
                case (_, actorRef) =>
                  actorRef.tell(newStreamInput, self)
              }
            // Client 로부터 Broadcast 요청을 받은 단계이므로, 재생용 자료 기록을 한다.
            recordQueue = recordQueue.map(_.enqueue(newStreamInput))
          case None =>
            sender().tell(QuitWebSocketWorkerActor, self)
        }
      } else {
        sender().tell(QuitWebSocketWorkerActor, self)
      }
  }
}
