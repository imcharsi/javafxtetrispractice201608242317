/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ Props, Actor, ActorRef }
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand._

import scala.concurrent.duration.FiniteDuration

/**
 * Created by i on 9/3/16.
 */
class WaitRoomActor[BT](
    val replayRoomActorRef: ActorRef,
    val replayRoom: ReplayRoom[BT],
    val timeoutCloseRoom: FiniteDuration,
    val timeoutNotifyJoinRoom: FiniteDuration
) extends Actor {
  private[actor] var roomMap: Map[Int, Int] = Map()
  private[actor] var roomIdCounter: Int = 0
  private[actor] var passwordCounter: Int = 0

  override def receive: Receive = {
    case CreateRoom =>
      val newRoomId = roomIdCounter
      roomIdCounter = roomIdCounter + 1
      val newPassword = passwordCounter
      passwordCounter = passwordCounter + 1
      context.actorOf(
        Props(new GameRoomActor[BT](self, replayRoomActorRef, replayRoom, newRoomId, newPassword, timeoutCloseRoom, timeoutNotifyJoinRoom)),
        newRoomId.toString
      )
      // 방 목록은 GameRoomActor 가 갱신하도록 한다. 개설자가 입장하기 전까지는 목록에 나타나지 않아야 한다.
      // 어차피 개설자는 개설 후 최신의 목록을 구해야 할 필요도 없다. 이미 입장에 필요한 단서를 모두 알았기 때문이다.
      sender().tell(CreatedRoom(newPassword, newRoomId), self)
    case RemoveFromWaitRoomList(roomId) =>
      roomMap = roomMap - roomId
    case UpdateWaitRoom(roomId, playerCount) =>
      roomMap = roomMap + (roomId -> playerCount)
    case GetRoomList =>
      sender().tell(GotRoomList(roomMap.map(x => (x._1, x._2))), self)
  }
}
