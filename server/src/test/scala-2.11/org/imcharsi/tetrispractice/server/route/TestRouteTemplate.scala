/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.route

import akka.actor.{ ActorRef, PoisonPill, Props }
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import akka.http.scaladsl.model.ws.TextMessage
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{ ScalatestRouteTest, WSProbe }
import akka.stream.scaladsl.Source
import akka.testkit.TestProbe
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ CreatedRoom, WaitRoomResponse }
import org.imcharsi.tetrispractice.common.data.fsm.Command
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ Leave, Start, StreamInput }
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ Player, RemotePlayer }
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomClientCommandJsonFormat, GameRoomServerCommandJsonFormat, WaitRoomResponseJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm.StreamInputJsonFormat
import org.imcharsi.tetrispractice.server.actor.{ ReplayRoom, WaitRoomActor }
import org.scalatest.{ BeforeAndAfterEach, FunSuite, Matchers }
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by i on 9/9/16.
 */
class TestRouteTemplate extends FunSuite with ScalatestRouteTest with SprayJsonSupport with Matchers with BeforeAndAfterEach {
  def applyHeader(request: HttpRequest)(list: List[RequestTransformer]): HttpRequest =
    list.foldLeft(request)((request, transformer) => transformer(request))

  val replayRoom = new ReplayRoom[TestBlockType.BlockType]()
  var replayRoomActorRef: ActorRef = null
  var waitRoomActorRef: ActorRef = null
  implicit val waitRoomResponseJsonFormat = WaitRoomResponseJsonFormat
  implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
  implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
  implicit val streamInputJsonFormat = new StreamInputJsonFormat[TestBlockType.BlockType]()(TestBlockType.TestBlockTypeJsonFormat)

  type FlowIOType[BT] = Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]

  override def beforeEach(): Unit = {
    super.beforeEach()
    replayRoomActorRef = system.actorOf(Props(new replayRoom.ReplayRoomActor()))
    waitRoomActorRef = system.actorOf(Props(new WaitRoomActor[TestBlockType.BlockType](replayRoomActorRef, replayRoom, 1.second, 1.second)))
  }

  override def afterEach(): Unit = {
    waitRoomActorRef.tell(PoisonPill, ActorRef.noSender)
    replayRoomActorRef.tell(PoisonPill, ActorRef.noSender)
    super.afterEach()
  }

  def expectMessageUtil[A](wsProbe: WSProbe)(implicit jsonFormat: JsonFormat[A]): A = {
    wsProbe.expectMessage() match {
      case TextMessage.Strict(m) =>
        val x = m.parseJson.convertTo[A]
        Console.out.println(x)
        x
      case _ => fail()
    }
  }

  test("GetRoomList") {
    val routeTemplate = new RouteTemplate[TestBlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)
    val route = Route.seal(routeTemplate.routeGetRoomList)

    applyHeader(Get("/getRoomList"))(List(addHeader(Accept(MediaTypes.`application/json`)))) ~> route ~> check {
      status should equal(StatusCodes.OK)
    }
  }

  test("GetRoomList fail") {
    val routeTemplate = new RouteTemplate[TestBlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)
    val route = Route.seal(routeTemplate.routeGetRoomList)

    applyHeader(Get("/getRoomList"))(List()) ~> route ~> check {
      status should equal(StatusCodes.BadRequest)
    }
  }

  test("CreateRoom/WebSocket") {
    val routeTemplate = new RouteTemplate[TestBlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)
    val route = Route.seal(routeTemplate.routeCreateRoom ~ routeTemplate.routeWebSocket(TestBlockType.TestBlockTypeJsonFormat))
    val creatorWsProbe = WSProbe()
    val joinerWsProbe = WSProbe()
    val createdRoom =
      applyHeader(Post("/createRoom"))(List(addHeader(Accept(MediaTypes.`application/json`)))) ~> route ~> check {
        status should equal(StatusCodes.OK)
        responseAs[WaitRoomResponse] match {
          case x @ CreatedRoom(password, roomId) => x
        }
      }
    WS(s"/room/${createdRoom.roomId}", creatorWsProbe.flow) ~> route ~> check {
      isWebSocketUpgrade should equal(true)
      val m1: FlowIOType[TestBlockType.BlockType] = Left(CreatorJoinRoom(createdRoom.password, "hi"))
      creatorWsProbe.sendMessage(m1.toJson.prettyPrint)
    }
    val creatorPlayer =
      expectMessageUtil[FlowIOType[TestBlockType.BlockType]](creatorWsProbe) match {
        case Left(CreatorJoinedRoom(Some(player))) => player
        case _ => fail()
      }
    expectMessageUtil[FlowIOType[TestBlockType.BlockType]](creatorWsProbe)
    expectMessageUtil[FlowIOType[TestBlockType.BlockType]](creatorWsProbe)
    WS(s"/room/${createdRoom.roomId}", joinerWsProbe.flow) ~> route ~> check {
      isWebSocketUpgrade should equal(true)
      val m1: FlowIOType[TestBlockType.BlockType] = Left(JoinRoom("hi2"))
      joinerWsProbe.sendMessage(m1.toJson.prettyPrint)
    }
    val joinerPlayer =
      expectMessageUtil[FlowIOType[TestBlockType.BlockType]](joinerWsProbe) match {
        case Left(JoinedRoom(Some(player))) => player
        case _ => fail()
      }
    expectMessageUtil[FlowIOType[TestBlockType.BlockType]](joinerWsProbe) match {
      case Right(Left(NotifyJoinRoom(responseId, _))) =>
        val m1: FlowIOType[TestBlockType.BlockType] = Right(Left(NotifiedJoinRoom(responseId, joinerPlayer, true)))
        val x = m1.toJson.prettyPrint
        val (x1, x2) = x.splitAt(x.length / 2)
        assertResult(x)(x1 + x2)
        // Streamed 방식으로 보낼 수 있음을 확인하는 검사를 여기에 끼워서 했다.
        joinerWsProbe.sendMessage(TextMessage.Streamed(Source.apply(List(x1, x2))))
      case _ => fail()
    }
    expectMessageUtil[FlowIOType[TestBlockType.BlockType]](joinerWsProbe)
    expectMessageUtil[FlowIOType[TestBlockType.BlockType]](creatorWsProbe) match {
      case Right(Left(NotifyJoinRoom(responseId, _))) =>
        val m1: FlowIOType[TestBlockType.BlockType] = Right(Left(NotifiedJoinRoom(responseId, creatorPlayer, true)))
        creatorWsProbe.sendMessage(m1.toJson.prettyPrint)
      case _ => fail()
    }
    expectMessageUtil[FlowIOType[TestBlockType.BlockType]](creatorWsProbe)
    creatorWsProbe.expectNoMessage(0.3.seconds)
    joinerWsProbe.expectNoMessage(0.3.seconds)
  }

  test("CreateRoom/WebSocket fail") {
    val routeTemplate = new RouteTemplate[TestBlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)
    val route = Route.seal(routeTemplate.routeCreateRoom ~ routeTemplate.routeWebSocket(TestBlockType.TestBlockTypeJsonFormat))
    val wsProbe = WSProbe()
    WS(s"/room/0", wsProbe.flow) ~> route ~> check {
      isWebSocketUpgrade should equal(false)
    }
  }

  val remotePlayer = RemotePlayer(0, "hi")
  val playerSnapshot: Set[Player] = Set(remotePlayer)
  val recordQueue: Queue[StreamInput[TestBlockType.BlockType]] = Queue(Start(remotePlayer, 0))

  test("GetReplayList") {
    val routeTemplate = new RouteTemplate[TestBlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)
    val route = Route.seal(routeTemplate.routeGetReplayList)

    applyHeader(Get("/replay"))(List(addHeader(Accept(MediaTypes.`application/json`)))) ~> route ~> check {
      status should equal(StatusCodes.OK)
      assertResult(0)(responseAs[List[Int]].size)
    }
    replayRoomActorRef.tell(replayRoom.RegisterReplay(playerSnapshot, recordQueue), ActorRef.noSender)
    applyHeader(Get("/replay"))(List(addHeader(Accept(MediaTypes.`application/json`)))) ~> route ~> check {
      status should equal(StatusCodes.OK)
      assertResult(1)(responseAs[List[Int]].size)
    }
  }

  test("GetReplay") {
    val routeTemplate = new RouteTemplate[TestBlockType.BlockType](replayRoomActorRef, replayRoom, waitRoomActorRef)
    val route = Route.seal(routeTemplate.routeGetReplay(TestBlockType.TestBlockTypeJsonFormat))
    val testProbe = TestProbe()

    applyHeader(Get(s"/replay/0"))(List(addHeader(Accept(MediaTypes.`application/json`)))) ~> route ~> check {
      status should equal(StatusCodes.NotFound)
    }
    replayRoomActorRef.tell(replayRoom.RegisterReplay(playerSnapshot, recordQueue), ActorRef.noSender)
    replayRoomActorRef.tell(replayRoom.GetReplayList, testProbe.ref)
    val replayId =
      testProbe.expectMsgType[Any] match {
        case replayRoom.GotReplayList(List(replayId)) => replayId
        case _ => fail()
      }
    // 이 문장을 두는 이유는, 동일한 replay id 를 사용했다는 것을 확인하기 위함이다.
    assertResult(0)(replayId)
    applyHeader(Get(s"/replay/${replayId}"))(List(addHeader(Accept(MediaTypes.`application/json`)))) ~> route ~> check {
      status should equal(StatusCodes.OK)
      val result =
        responseEntity.dataBytes
          .map { x =>
            x.utf8String.parseJson.convertTo[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]]
          }
          .runFold(Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]]())((queue, x) => queue.enqueue(x))
      assertResult(Queue(Right(Left(NotifyJoinRoom(0, remotePlayer))), Right(Right(Start(remotePlayer, 0)))))(Await.result(result, 10.seconds))
    }
  }
}
