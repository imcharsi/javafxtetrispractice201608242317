/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ ActorRef, PoisonPill, Props, ActorSystem }
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ RemoveFromWaitRoomList, UpdateWaitRoom }
import org.imcharsi.tetrispractice.common.data.fsm.Command._
import org.imcharsi.tetrispractice.common.data.fsm.{ BlockCell, Position, Player }
import org.imcharsi.tetrispractice.common.data.fsm.Player.Player
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by i on 9/5/16.
 */
class TestGameRoomActor extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  val replayRoom = new ReplayRoom[TestBlockType.BlockType]()
  var replayRoomActor: TestProbe = null
  var waitRoomActor: TestProbe = null
  override def beforeEach(): Unit = {
    super.beforeEach()
    replayRoomActor = TestProbe()
    waitRoomActor = TestProbe()
  }

  override def afterEach(): Unit = {
    waitRoomActor.ref.tell(PoisonPill, ActorRef.noSender)
    replayRoomActor.ref.tell(PoisonPill, ActorRef.noSender)
    super.afterEach()
  }

  def prepareGameRoomActor(): TestActorRef[GameRoomActor[TestBlockType.BlockType]] = {
    TestActorRef[GameRoomActor[TestBlockType.BlockType]](Props(new GameRoomActor[TestBlockType.BlockType](waitRoomActor.ref, replayRoomActor.ref, replayRoom, 0, 1, 1.second, 1.seconds)))
  }

  test("CreatorJoinRoom") {
    val testProbe = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    assert(gameRoomActor.underlyingActor.cancellableCloseRoom.isDefined)
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi")), testProbe.ref)
    testProbe.expectMsgType[CreatorJoinedRoom] match {
      case CreatorJoinedRoom(Some(_)) =>
      case _ => fail()
    }
    testProbe.expectMsgType[NotifyChangeStarter]
    testProbe.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(None) =>
      case _ => fail()
    }
    assert(gameRoomActor.underlyingActor.cancellableCloseRoom.isEmpty)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    testProbe.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("CreatorJoinRoom then connection lost") {
    val testProbe = TestProbe()
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    assert(gameRoomActor.underlyingActor.cancellableCloseRoom.isDefined)
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi")), creator.ref)
    creator.expectMsgType[CreatorJoinedRoom] match {
      case CreatorJoinedRoom(Some(_)) =>
      case _ => fail()
    }
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(None) =>
      case _ => fail()
    }
    assert(gameRoomActor.underlyingActor.cancellableCloseRoom.isEmpty)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    testProbe.watch(gameRoomActor)
    // 최초의 입장이 성공한 이후에, 연결이 끊기면 빈 방 확인을 한다.
    creator.ref.tell(PoisonPill, testProbe.ref)
    testProbe.expectTerminated(gameRoomActor)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 0))
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectNoMsg(0.3.seconds)
    testProbe.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("CreatorJoinRoom fail1") {
    val testProbe = TestProbe()
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    assert(gameRoomActor.underlyingActor.cancellableCloseRoom.isDefined)
    // 암호가 틀렸다.
    gameRoomActor.tell(Left(CreatorJoinRoom(2, "hi")), creator.ref)
    creator.expectMsg(CreatorJoinedRoom(None))
    assert(gameRoomActor.underlyingActor.cancellableCloseRoom.isDefined)
    testProbe.watch(gameRoomActor)
    // 입장시도를 실패하면 WebSocketWorkerActor 를 종료한다.
    creator.ref.tell(PoisonPill, testProbe.ref)
    // 입장에 실패했지만 GameRoomActor 는 여전히 빈 방이라는 인식을 하지 않아야 한다.
    creator.expectNoMsg(0.3.seconds)
    testProbe.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("CreatorJoinRoom fail2") {
    val testProbe = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi")), testProbe.ref)
    testProbe.expectMsgType[CreatorJoinedRoom]
    testProbe.expectMsgType[NotifyChangeStarter]
    testProbe.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    // 이미 방장이 입장했는데, 또 입장하려고 한다.
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi")), testProbe.ref)
    testProbe.expectMsg(CreatorJoinedRoom(None))
    testProbe.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("GameRoomActor Timeout") {
    val gameRoomActor = prepareGameRoomActor()
    watch(gameRoomActor)
    expectTerminated(gameRoomActor)
  }

  test("JoinRoom fail") {
    val testProbe = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    // 방장이 입장하지 않은 방에 입장하려고 한다.
    gameRoomActor.tell(Left(JoinRoom("hi")), testProbe.ref)
    testProbe.expectMsg(JoinedRoom(None))
    testProbe.expectNoMsg(0.3.seconds)
  }

  def playerFromCJR(x: CreatorJoinedRoom): Player = {
    x match {
      case CreatorJoinedRoom(Some(player)) => player
      case _ => fail()
    }
  }

  def playerFromJR(x: JoinedRoom): Player = {
    x match {
      case JoinedRoom(Some(player)) => player
      case _ => fail()
    }
  }

  def responseIdFromNJR(expectedPlayer: Player, x: NotifyJoinRoom): Int = {
    x match {
      case NotifyJoinRoom(responseId, joinerPlayer) =>
        assertResult(Player.convertToRemotePlayer(expectedPlayer))(joinerPlayer)
        responseId
      case _ => fail()
    }
  }

  test("JoinRoom") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Left(JoinRoom("hi2")), joiner.ref)
    val joinerPlayer = playerFromJR(joiner.expectMsgType[JoinedRoom])
    // joiner 가 입장하면서 joiner 는, 이미 입장해 있었던 creator 의 존재를 NotifyJoinRoom 으로 인식한다.
    val joinerResponseId = responseIdFromNJR(creatorPlayer, joiner.expectMsgType[NotifyJoinRoom])
    joiner.expectMsgType[NotifyChangeStarter]
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(joinerResponseId, joinerPlayer, true))), joiner.ref)
    // joiner 가 입장하면서 이미 입장해 있었던 creator 가 joiner 의 입장을 인식한다.
    val creatorResponseId = responseIdFromNJR(joinerPlayer, creator.expectMsgType[NotifyJoinRoom])
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))), creator.ref)
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(None) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))
    joiner.expectNoMsg(0.3.seconds)
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Prepare") {
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Right(Right(Prepare(creatorPlayer))), creator.ref)
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(Some(_)) =>
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Cancel") {
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Right(Right(Prepare(creatorPlayer))), creator.ref)
    creator.expectMsgType[NotifyBeAbleToStart]
    gameRoomActor.tell(Right(Right(Cancel(creatorPlayer))), creator.ref)
    creator.expectMsg(NotifyBeAbleToStart(None))
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  def joinCreatorAndJoiner(creator: TestProbe, joiner: TestProbe, gameRoomActor: TestActorRef[GameRoomActor[TestBlockType.BlockType]]): (Player, Player) = {
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    gameRoomActor.tell(Left(JoinRoom("hi2")), joiner.ref)
    val joinerPlayer = playerFromJR(joiner.expectMsgType[JoinedRoom])
    // 입장하기 전에 이미 입장해 있었던 사용자에 관해 먼저 전달받고
    val joinerResponseId = responseIdFromNJR(creatorPlayer, joiner.expectMsgType[NotifyJoinRoom])
    // 이어서, 앞서 전달받은 사용자 중에 누가 방장인지 전달받는다.
    joiner.expectMsgType[NotifyChangeStarter]
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(joinerResponseId, joinerPlayer, true))), joiner.ref)
    val creatorResponseId = responseIdFromNJR(joinerPlayer, creator.expectMsgType[NotifyJoinRoom])
    creator.expectMsgType[NotifyBeAbleToStart]
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))), creator.ref)

    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))
    (creatorPlayer, joinerPlayer)
  }

  test("Prepare creator and joiner") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer) = joinCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Right(Right(Prepare[TestBlockType.BlockType](creatorPlayer))), creator.ref)
    // 방장은 스스로의 입력을 포함해서 Prepare 입력이 있을 때마다 NotifyBeAbleToStart 를 받는다.
    creator.expectMsg(NotifyBeAbleToStart(None))
    // creator 의 준비완료를 joiner 가 알림받는다. joiner Client 에 만들어진 creator RemotePlayer 로 Prepare 입력을 해 준다.
    // 이 입력은 WebSocketWorkerActor 가 받아서 다시 Client 로 전달하게 된다.
    joiner.expectMsgType[Prepare[TestBlockType.BlockType]] match {
      case Prepare(player) =>
        assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    joiner.expectNoMsg(0.3.seconds)

    gameRoomActor.tell(Right(Right(Prepare[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    joiner.expectNoMsg(0.3.seconds)
    creator.expectMsgType[Prepare[TestBlockType.BlockType]] match {
      case Prepare(player) =>
        assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(Some(_)) =>
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  def prepareCreatorAndJoiner(creator: TestProbe, joiner: TestProbe, gameRoomActor: TestActorRef[GameRoomActor[TestBlockType.BlockType]]): (Player, Player, Int) = {
    val (creatorPlayer, joinerPlayer) = joinCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Right(Right(Prepare[TestBlockType.BlockType](creatorPlayer))), creator.ref)
    creator.expectMsg(NotifyBeAbleToStart(None))
    joiner.expectMsgType[Prepare[TestBlockType.BlockType]]
    gameRoomActor.tell(Right(Right(Prepare[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    creator.expectMsgType[Prepare[TestBlockType.BlockType]]
    val preparedId =
      creator.expectMsgType[NotifyBeAbleToStart] match {
        case NotifyBeAbleToStart(Some(preparedId)) => preparedId
      }
    (creatorPlayer, joinerPlayer, preparedId)
  }

  test("Cancel creator and joiner") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, _) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Right(Right(Cancel[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    joiner.expectNoMsg(0.3.seconds)
    creator.expectMsgType[Cancel[TestBlockType.BlockType]] match {
      case Cancel(player) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    // joiner 가 준비취소했으므로 creator 는 게임을 시작할 수 없어야 한다.
    creator.expectMsg(NotifyBeAbleToStart(None))
    creator.expectNoMsg(0.3.seconds)

    gameRoomActor.tell(Right(Right(Cancel[TestBlockType.BlockType](creatorPlayer))), creator.ref)
    creator.expectMsg(NotifyBeAbleToStart(None))
    creator.expectNoMsg(0.3.seconds)
    joiner.expectMsgType[Cancel[TestBlockType.BlockType]] match {
      case Cancel(player) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Leave") {
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Right(Right(Leave(creatorPlayer))), creator.ref)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 0))
    creator.expectMsg(QuitWebSocketWorkerActor)
    creator.ref.tell(PoisonPill, ActorRef.noSender)
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("All Prepare then Leave joiner") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, _) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    // joiner 가 퇴장한다.
    gameRoomActor.tell(Right(Right(Leave[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    joiner.expectMsg(QuitWebSocketWorkerActor)
    joiner.expectNoMsg(0.3.seconds)
    creator.expectMsgType[Leave[TestBlockType.BlockType]] match {
      case Leave(player) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    // 전부 준비된 상태에서 한 사람이 나가면, 역시 준비된 상태가 된다.
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(Some(_)) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("All Prepare then Leave creator") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()

    val (creatorPlayer, joinerPlayer, _) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    // creator 가 퇴장한다.
    gameRoomActor.tell(Right(Right(Leave[TestBlockType.BlockType](creatorPlayer))), creator.ref)
    creator.expectMsg(QuitWebSocketWorkerActor)
    creator.expectNoMsg(0.3.seconds)
    joiner.expectMsgType[Leave[TestBlockType.BlockType]] match {
      case Leave(player) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    // joiner 가 starter 가 된다.
    // starter 가 되는 사람은 자신이 starter 가 된다는 알림을 RemotePlayer 가 아니라 LocalPlayer 로서 받는다.
    joiner.expectMsgType[NotifyChangeStarter] match {
      case NotifyChangeStarter(player) => assertResult(joinerPlayer)(player)
      case _ => fail()
    }
    // 전부 준비된 상태에서 한 사람이 나가면, 역시 준비된 상태가 된다.
    joiner.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(Some(_)) =>
      case _ => fail()
    }
    joiner.expectNoMsg(0.3.seconds)
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Prepare joiner then Leave joiner") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer) = joinCreatorAndJoiner(creator, joiner, gameRoomActor)

    // joiner 가 퇴장하고 creator 가 남아있을 것이므로, creator 는 준비완료를 하지 않는다.
    gameRoomActor.tell(Right(Right(Prepare[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    creator.expectMsgType[Prepare[TestBlockType.BlockType]]
    creator.expectMsg(NotifyBeAbleToStart(None))

    // joiner 가 퇴장한다.
    gameRoomActor.tell(Right(Right(Leave[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    joiner.expectMsg(QuitWebSocketWorkerActor)
    joiner.expectNoMsg(0.3.seconds)
    creator.expectMsgType[Leave[TestBlockType.BlockType]] match {
      case Leave(player) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    // creator 는 준비완료를 하지 않았다. 준비완료를 한 joiner 가 나갔으므로 여전히 준비완료상태가 아니다.
    creator.expectMsg(NotifyBeAbleToStart(None))
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Prepare joiner then Leave creator") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()

    val (creatorPlayer, joinerPlayer) = joinCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Right(Right(Prepare[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    creator.expectMsgType[Prepare[TestBlockType.BlockType]]
    creator.expectMsg(NotifyBeAbleToStart(None))

    // creator 가 퇴장한다.
    gameRoomActor.tell(Right(Right(Leave[TestBlockType.BlockType](creatorPlayer))), creator.ref)
    creator.expectMsg(QuitWebSocketWorkerActor)
    creator.expectNoMsg(0.3.seconds)
    joiner.expectMsgType[Leave[TestBlockType.BlockType]] match {
      case Leave(player) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    // joiner 가 starter 가 된다.
    joiner.expectMsgType[NotifyChangeStarter] match {
      case NotifyChangeStarter(player) => assertResult(joinerPlayer)(player)
      case _ => fail()
    }
    // creator 는 준비완료를 하지 않았고 joiner 는 준비완료를 했는데, creator 가 퇴장했고
    // 남은 사람인 joiner 는 준비완료상태이므로 게임을 시작할 수 있게 된다.
    joiner.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(Some(_)) =>
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Terminated before Start") {
    val testProbe = TestProbe()
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer) = joinCreatorAndJoiner(creator, joiner, gameRoomActor)

    joiner.ref.tell(PoisonPill, testProbe.ref)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    creator.expectMsgType[Leave[TestBlockType.BlockType]] match {
      case Leave(player) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    creator.expectMsgType[NotifyBeAbleToStart]
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  // Server 의 Start Broadcast 가 있고 이의 도달이 있기 전에 Client 의 Cancel 입력이 있는 경우에 대한 검사이다.
  test("Cancel after Start") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, preparedId) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    // 게임을 시작했다.
    gameRoomActor.tell(Left(StartGames(preparedId)), creator.ref)
    creator.expectMsgType[StartedGames] match {
      case StartedGames(true) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectMsgType[Start[TestBlockType.BlockType]]

    // joiner 가 Start 를 받기 전에 Cancel 을 입력하는 상황을 검사한다.
    // 그런데, 검사를 쓰는 시점에서는 기왕에 GameRoomActor 가 입력한 Start 입력의 순서를 무시할 수 없기 때문에,
    // 형식적인 문제로서 Start 입력을 확인한다.
    joiner.expectMsgType[Start[TestBlockType.BlockType]]
    // joiner 는 아직 Start 입력을 받지 못했다고 가정한다.
    gameRoomActor.tell(Right(Right(Cancel[TestBlockType.BlockType](joinerPlayer))), joiner.ref)
    // 연결이 끊기게 된다. Client 로서는 왜 연결이 끊기게 됐는지 이유를 알 수 없는 상황이다.
    joiner.expectMsg(QuitWebSocketWorkerActor)
    joiner.expectNoMsg(0.3.seconds)

    // creator Client 에서 만들어진 joiner RemotePlayer 로 Cancel 이 아닌, Stop 이 입력된다.
    // 시작 이후에 Cancel 이 입력되면 Server 는 Stop 를 Broadcast 한다.
    creator.expectMsgType[Stop[TestBlockType.BlockType]] match {
      case Stop(player, _) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    // 시작 후 Cancel 이면 UpdateWaitRoom 을 보내지 않아야 한다. 이미 목록에서 지워졌기 때문이다.
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("StartGames fail") {
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Left(StartGames(100)), creator.ref)
    creator.expectMsgType[StartedGames] match {
      case StartedGames(false) =>
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("StartGames") {
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Right(Right(Prepare(creatorPlayer))), creator.ref)
    val preparedId =
      creator.expectMsgType[NotifyBeAbleToStart] match {
        case NotifyBeAbleToStart(Some(preparedId)) => preparedId
        case _ => fail()
      }
    gameRoomActor.tell(Left(StartGames(preparedId)), creator.ref)
    creator.expectMsgType[StartedGames] match {
      case StartedGames(true) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(creatorPlayer)(player)
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Terminated after Start") {
    val testProbe = TestProbe()
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, preparedId) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Left(StartGames(preparedId)), creator.ref)
    creator.expectMsgType[StartedGames] match {
      case StartedGames(true) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(creatorPlayer)(player)
      case _ => fail()
    }
    // 이 단계에서 joiner 에 대한 검사는 형식적인 검사고,
    // 검사의 의도는 Server 가 보낸 Start 입력이 joiner 에게는 이르지 않고 연결이 끊긴 상황을 검사하는 데 있다.
    joiner.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(joinerPlayer)(player)
      case _ => fail()
    }
    joiner.ref.tell(PoisonPill, testProbe.ref)
    creator.expectMsgType[Stop[TestBlockType.BlockType]] match {
      case Stop(player, _) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Terminated then close room") {
    val testProbe = TestProbe()
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    testProbe.watch(gameRoomActor)
    creator.ref.tell(PoisonPill, testProbe.ref)
    testProbe.expectTerminated(gameRoomActor)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 0))
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("Leave then close room") {
    val testProbe = TestProbe()
    val creator = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    watch(gameRoomActor)
    gameRoomActor.tell(Right(Right(Leave(creatorPlayer))), creator.ref)
    creator.expectMsg(QuitWebSocketWorkerActor)
    creator.ref.tell(PoisonPill, testProbe.ref)
    // watch 중인 WebSocketWorkerActor 가 모두 Terminated 되었을 때, 빈 방을 폐쇄한다.
    expectTerminated(gameRoomActor)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 0))
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("CreateWebSocketWorkerActor") {
    val testProbe = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(CreateWebSocketWorkerActor, testProbe.ref)
    testProbe.expectMsgType[CreatedWebSocketWorkerActor] match {
      case CreatedWebSocketWorkerActor(actorRef) =>
        assertResult(actorRef)(
          Await.result(system.actorSelection(actorRef.path).resolveOne(3.seconds), 4.seconds)
        )
      case _ => fail()
    }
  }

  test("NotifiedJoinRoom fail") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Left(JoinRoom("hi2")), joiner.ref)
    val joinerPlayer = playerFromJR(joiner.expectMsgType[JoinedRoom])
    val joinerResponseId = responseIdFromNJR(creatorPlayer, joiner.expectMsgType[NotifyJoinRoom])
    joiner.expectMsgType[NotifyChangeStarter]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))
    // RemotePlayer 의 준비를 실패했을 때
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(joinerResponseId, joinerPlayer, false))), joiner.ref)
    val creatorResponseId = responseIdFromNJR(joinerPlayer, creator.expectMsgType[NotifyJoinRoom])
    creator.expectMsgType[NotifyBeAbleToStart]
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))), creator.ref)
    joiner.expectMsg(QuitWebSocketWorkerActor)
    // 다른 사용자인 creator 는 joiner 가 퇴장했다는 알림을 받는다.
    creator.expectMsgType[Leave[TestBlockType.BlockType]] match {
      case Leave(player) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    joiner.expectNoMsg(0.3.seconds)
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("NotifiedJoinRoom timeout") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Left(JoinRoom("hi2")), joiner.ref)
    val joinerPlayer = playerFromJR(joiner.expectMsgType[JoinedRoom])
    val joinerResponseId = responseIdFromNJR(creatorPlayer, joiner.expectMsgType[NotifyJoinRoom])
    joiner.expectMsgType[NotifyChangeStarter]
    val creatorResponseId = responseIdFromNJR(joinerPlayer, creator.expectMsgType[NotifyJoinRoom])
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))), creator.ref)
    // joiner 가 받은 NotifyJoinRoom 에 대해서는 NotifiedJoinRoom 응답을 하지 않는다.
    // 그래서 시간초과가 나타나는지 확인한다.
    joiner.expectMsg(QuitWebSocketWorkerActor)
    creator.expectMsgType[Leave[TestBlockType.BlockType]]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    creator.expectMsgType[NotifyBeAbleToStart]
    joiner.expectNoMsg(0.3.seconds)
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("NotifiedJoinRoom") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Left(JoinRoom("hi2")), joiner.ref)
    val joinerPlayer = playerFromJR(joiner.expectMsgType[JoinedRoom])
    val joinerResponseId = responseIdFromNJR(creatorPlayer, joiner.expectMsgType[NotifyJoinRoom])
    joiner.expectMsgType[NotifyChangeStarter]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(joinerResponseId, joinerPlayer, true))), joiner.ref)
    val creatorResponseId = responseIdFromNJR(joinerPlayer, creator.expectMsgType[NotifyJoinRoom])
    creator.expectMsgType[NotifyBeAbleToStart]
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))), creator.ref)
    // 응답에 실패했을 때만 WebSocketWorkerActor 를 종료한다.
    joiner.expectNoMsg(0.3.seconds)
    creator.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("JoinRoom after one Prepare") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    gameRoomActor.tell(Left(CreatorJoinRoom(1, "hi1")), creator.ref)
    val creatorPlayer = playerFromCJR(creator.expectMsgType[CreatorJoinedRoom])
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    gameRoomActor.tell(Right(Right(Prepare(creatorPlayer))), creator.ref)
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(Some(_)) =>
      case _ => fail()
    }

    gameRoomActor.tell(Left(JoinRoom("hi2")), joiner.ref)
    val joinerPlayer = playerFromJR(joiner.expectMsgType[JoinedRoom])
    val joinerResponseId = responseIdFromNJR(creatorPlayer, joiner.expectMsgType[NotifyJoinRoom])
    joiner.expectMsgType[NotifyChangeStarter]
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))
    // creator 는 이미 준비완료를 했으므로, 이에 대한 알림을 받아야 한다.
    joiner.expectMsg(Prepare(Player.convertToRemotePlayer(creatorPlayer)))
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(joinerResponseId, joinerPlayer, true))), joiner.ref)
    val creatorResponseId = responseIdFromNJR(joinerPlayer, creator.expectMsgType[NotifyJoinRoom])
    gameRoomActor.tell(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))), creator.ref)
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(None) =>
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("StreamInput") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, preparedId) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Left(StartGames(preparedId)), creator.ref)
    creator.expectMsgType[StartedGames] match {
      case StartedGames(true) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectMsgType[Start[TestBlockType.BlockType]]
    joiner.expectMsgType[Start[TestBlockType.BlockType]]

    // creator 가 보낸 Start 는 joiner 의 Client 에 있는 creator 의 RemotePlayer 가 받아야 하고
    // joiner 가 보낸 Start 는 creator 의 Client 에 있는 joiner 의 RemotePlayer 가 받아야 한다.
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](creatorPlayer, 0))), creator.ref)
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](joinerPlayer, 0))), joiner.ref)
    creator.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    joiner.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    creator.expectNoMsg(0.3.seconds)
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("StreamInput fail1") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, preparedId) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)
    // 아직 StartGames 를 입력하지 않았는데 Start 입력을 시도했다. 틀렸다.
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](creatorPlayer, 0))), creator.ref)
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](joinerPlayer, 0))), joiner.ref)
    creator.expectMsg(QuitWebSocketWorkerActor)
    joiner.expectMsg(QuitWebSocketWorkerActor)
    joiner.ref.tell(PoisonPill, ActorRef.noSender)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    creator.expectMsgType[Leave[TestBlockType.BlockType]]
    creator.expectMsgType[NotifyBeAbleToStart]
    creator.ref.tell(PoisonPill, ActorRef.noSender)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 0))
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectNoMsg(0.3.seconds)
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("PreparePush") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, preparedId) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    gameRoomActor.tell(Left(StartGames(preparedId)), creator.ref)
    creator.expectMsgType[StartedGames] match {
      case StartedGames(true) =>
      case _ => fail()
    }
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectMsgType[Start[TestBlockType.BlockType]]
    joiner.expectMsgType[Start[TestBlockType.BlockType]]

    // creator 가 보낸 Start 는 joiner 의 Client 에 있는 creator 의 RemotePlayer 가 받아야 하고
    // joiner 가 보낸 Start 는 creator 의 Client 에 있는 joiner 의 RemotePlayer 가 받아야 한다.
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](creatorPlayer, 0))), creator.ref)
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](joinerPlayer, 0))), joiner.ref)
    creator.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    joiner.expectMsgType[Start[TestBlockType.BlockType]] match {
      case Start(player, _) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    gameRoomActor.tell(
      Right(
        Right(
          PreparePush[TestBlockType.BlockType](
            joinerPlayer,
            1,
            List((Position(0, 0) -> BlockCell.UninitializedBlockCell(TestBlockType.BlockOne)))
          )
        )
      ),
      joiner.ref
    )
    creator.expectMsgType[Push[TestBlockType.BlockType]] match {
      case Push(player, rowCount, bePushed, isBroadcast) =>
        assertResult(1)(rowCount)
        assertResult(false)(isBroadcast)
        assertResult(creatorPlayer)(player)
        assertResult(List((Position(0, 0), BlockCell.UninitializedBlockCell(TestBlockType.BlockOne))))(bePushed)
    }
    creator.expectNoMsg(0.3.seconds)
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }

  test("PreparePush fail") {
    val creator = TestProbe()
    val joiner = TestProbe()
    val gameRoomActor = prepareGameRoomActor()
    val (creatorPlayer, joinerPlayer, preparedId) = prepareCreatorAndJoiner(creator, joiner, gameRoomActor)

    // 아직 게임을 시작하지 않았는데, PreparePush 를 입력하면 실패해야 한다.
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](creatorPlayer, 0))), creator.ref)
    gameRoomActor.tell(Right(Right(Start[TestBlockType.BlockType](joinerPlayer, 0))), joiner.ref)
    creator.expectMsg(QuitWebSocketWorkerActor)
    joiner.expectMsg(QuitWebSocketWorkerActor)
    joiner.ref.tell(PoisonPill, ActorRef.noSender)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))
    creator.expectMsgType[Leave[TestBlockType.BlockType]]
    creator.expectMsgType[NotifyBeAbleToStart]
    creator.ref.tell(PoisonPill, ActorRef.noSender)
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 0))
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    creator.expectNoMsg(0.3.seconds)
    joiner.expectNoMsg(0.3.seconds)
    waitRoomActor.expectNoMsg(0.3.seconds)
  }
}
