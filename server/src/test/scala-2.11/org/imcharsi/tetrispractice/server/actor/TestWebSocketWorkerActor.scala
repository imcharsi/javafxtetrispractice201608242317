/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ ActorRef, ActorSystem, PoisonPill, Props }
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.stream.testkit.{ TestPublisher, TestSubscriber }
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.fsm.Player
import org.imcharsi.tetrispractice.common.data.fsm.{ Player, Command }
import org.imcharsi.tetrispractice.common.data.fsm.Command.Start
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ Player, LocalPlayer }
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }

import scala.concurrent.duration._

/**
 * Created by i on 9/6/16.
 */
class TestWebSocketWorkerActor extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  implicit var actorMaterializer = ActorMaterializer()(system)
  var gameRoomActor: TestProbe = null
  var webSocketWorkerActorRef: TestActorRef[WebSocketWorkerActor[TestBlockType.BlockType]] = null
  var sourceProbe: TestPublisher.Probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null
  var sinkProbe: TestSubscriber.Probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null

  override def beforeEach(): Unit = {
    gameRoomActor = TestProbe()
    webSocketWorkerActorRef =
      TestActorRef[WebSocketWorkerActor[TestBlockType.BlockType]](
        Props(new WebSocketWorkerActor[TestBlockType.BlockType](gameRoomActor.ref))
      )
    val x =
      Flow
        .fromSinkAndSource(
          Sink.fromSubscriber(ActorSubscriber[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]](webSocketWorkerActorRef)),
          Source.fromPublisher(ActorPublisher[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]](webSocketWorkerActorRef))
        )
        .runWith(
          TestSource.probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]],
          TestSink.probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]]
        )
    sourceProbe = x._1
    sinkProbe = x._2
  }

  override def afterEach(): Unit = {
    gameRoomActor.ref.tell(PoisonPill, ActorRef.noSender)
    system.stop(webSocketWorkerActorRef)
  }

  test("CreatorJoinGame/CreatedJoinGame") {
    sourceProbe.sendNext(Left(CreatorJoinRoom(1, "hi")))
    gameRoomActor.expectMsg(Left(CreatorJoinRoom(1, "hi")))
    val expectedPlayer = LocalPlayer(1, "hi")
    gameRoomActor.lastSender.tell(CreatorJoinedRoom(Some(expectedPlayer)), ActorRef.noSender)
    sinkProbe.requestNext(Left(CreatorJoinedRoom(Some(expectedPlayer))))
    sinkProbe.expectNoMsg(0.3.seconds)
  }

  test("CreatorJoinGame/CreatedJoinGame fail") {
    sourceProbe.sendNext(Left(CreatorJoinRoom(1, "hi")))
    gameRoomActor.expectMsg(Left(CreatorJoinRoom(1, "hi")))
    gameRoomActor.lastSender.tell(CreatorJoinedRoom(None), ActorRef.noSender)
    sinkProbe.requestNext(Left(CreatorJoinedRoom(None)))
    sinkProbe.expectComplete()
  }

  test("JoinGame/JoinGame") {
    sourceProbe.sendNext(Left(JoinRoom("hi")))
    gameRoomActor.expectMsg(Left(JoinRoom("hi")))
    val expectedPlayer = LocalPlayer(1, "hi")
    gameRoomActor.lastSender.tell(JoinedRoom(Some(expectedPlayer)), ActorRef.noSender)
    sinkProbe.requestNext(Left(JoinedRoom(Some(expectedPlayer))))
    sinkProbe.expectNoMsg(0.3.seconds)
  }

  test("JoinGame/JoinGame fail") {
    sourceProbe.sendNext(Left(JoinRoom("hi")))
    gameRoomActor.expectMsg(Left(JoinRoom("hi")))
    gameRoomActor.lastSender.tell(JoinedRoom(None), ActorRef.noSender)
    sinkProbe.requestNext(Left(JoinedRoom(None)))
    sinkProbe.expectComplete()
  }

  test("QuitWebSocketWorkerActor1") {
    sinkProbe.expectSubscription()
    sinkProbe.expectNoMsg(0.3.seconds)
    webSocketWorkerActorRef.tell(QuitWebSocketWorkerActor, ActorRef.noSender)
    sinkProbe.expectComplete()
  }

  def joinRoom(expectedPlayer: Player): Unit = {
    sourceProbe.sendNext(Left(JoinRoom("hi")))
    gameRoomActor.expectMsg(Left(JoinRoom("hi")))
    val expectedPlayer = LocalPlayer(1, "hi")
    gameRoomActor.lastSender.tell(JoinedRoom(Some(expectedPlayer)), ActorRef.noSender)
    sinkProbe.requestNext(Left(JoinedRoom(Some(expectedPlayer))))
  }

  test("QuitWebSocketWorkerActor2") {
    val expectedPlayer = LocalPlayer(1, "hi")
    joinRoom(expectedPlayer)
    webSocketWorkerActorRef.tell(QuitWebSocketWorkerActor, ActorRef.noSender)
    sinkProbe.expectComplete()
  }

  test("NotifyJoinRoom fail before JoinRoom") {
    sinkProbe.expectSubscription()
    webSocketWorkerActorRef.tell(NotifyJoinRoom(1, LocalPlayer(1, "hi")), ActorRef.noSender)
    sinkProbe.expectComplete()
    sinkProbe.expectNoMsg(0.3.seconds)
  }

  test("NotifyJoinRoom/NotifiedJoinRoom") {
    val expectedPlayer = LocalPlayer(1, "hi")
    joinRoom(expectedPlayer)
    webSocketWorkerActorRef.tell(NotifyJoinRoom(1, expectedPlayer), ActorRef.noSender)
    sinkProbe.requestNext(Right(Left(NotifyJoinRoom(1, expectedPlayer))))
    sourceProbe.sendNext(Right(Left(NotifiedJoinRoom(1, expectedPlayer, true))))
    sinkProbe.expectNoMsg(0.3.seconds)
    gameRoomActor.expectMsg(Right(Left(NotifiedJoinRoom(1, expectedPlayer, true))))
    gameRoomActor.expectNoMsg(0.3.seconds)
  }

  test("NotifyBeAbleToStart/NotifiedBeAbleToStart") {
    val expectedPlayer = LocalPlayer(1, "hi")
    joinRoom(expectedPlayer)
    webSocketWorkerActorRef.tell(NotifyBeAbleToStart(Some(1)), ActorRef.noSender)
    sinkProbe.requestNext(Right(Left(NotifyBeAbleToStart(Some(1)))))
    sourceProbe.sendNext(Right(Left(NotifiedBeAbleToStart)))
    sinkProbe.expectNoMsg(0.3.seconds)
  }

  test("NotifyChangeStarter/NotifiedChangeStarter") {
    val expectedPlayer = LocalPlayer(1, "hi")
    joinRoom(expectedPlayer)
    webSocketWorkerActorRef.tell(NotifyChangeStarter(expectedPlayer), ActorRef.noSender)
    sinkProbe.requestNext(Right(Left(NotifyChangeStarter(expectedPlayer))))
    sourceProbe.sendNext(Right(Left(NotifiedChangeStarter)))
    sinkProbe.expectNoMsg(0.3.seconds)
  }

  test("StartGames/StartedGames") {
    val expectedPlayer = LocalPlayer(1, "hi")
    joinRoom(expectedPlayer)
    // Client 가 게임의 시작을 요청한다.
    sourceProbe.sendNext(Left(StartGames(1)))
    gameRoomActor.expectMsg(Left(StartGames(1)))
    // Client 에게 게임의 시작을 성공했음을 알린다. Client 가 이 응답을 받았다는 자체로 게임이 시작되는 것은 아니고
    // 이어서 각 LocalPlayer 에게 Start 입력이 주어진다는 것을 알리는 뜻으로 성공의 응답을 한다.
    webSocketWorkerActorRef.tell(StartedGames(true), ActorRef.noSender)
    sinkProbe.requestNext(Left(StartedGames(true)))
    sinkProbe.expectNoMsg(0.3.seconds)
  }

  test("StreamInput then Broadcast") {
    val expectedPlayer = LocalPlayer(1, "hi")
    joinRoom(expectedPlayer)
    sourceProbe.sendNext(Right(Right(Start(expectedPlayer, 0))))
    gameRoomActor.expectMsg(Right(Right(Start(expectedPlayer, 0))))
    webSocketWorkerActorRef.tell(Start[TestBlockType.BlockType](Player.convertToRemotePlayer(expectedPlayer), 0), ActorRef.noSender)
    sinkProbe.requestNext(Right(Right(Start[TestBlockType.BlockType](Player.convertToRemotePlayer(expectedPlayer), 0))))
    sinkProbe.expectNoMsg(0.3.seconds)
  }
}
