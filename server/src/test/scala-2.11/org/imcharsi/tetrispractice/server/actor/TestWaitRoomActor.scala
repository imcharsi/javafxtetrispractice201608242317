/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ ActorRef, PoisonPill, ActorSystem }
import akka.testkit.{ TestProbe, TestActorRef, TestKit }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand._
import org.imcharsi.tetrispractice.common.data.fsm.Command.Leave
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.imcharsi.tetrispractice.common.fsm.TestBlockType.BlockType
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Created by i on 9/8/16.
 */
class TestWaitRoomActor extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  var waitRoomActorRef: TestActorRef[WaitRoomActor[TestBlockType.BlockType]] = null
  var testProbe: TestProbe = null
  var replayRoomActor: TestProbe = null
  val replayRoom = new ReplayRoom[TestBlockType.BlockType]()

  test("CreateRoom/UpdateWaitRoom/GetRoomList/RemoveFromWaitRoomList") {
    // 이 기능은 같이 모아서 검사해야 편하다.
    // 특히 RemoveFromWaitRoomList 를 검사하려면 어차피 방을 만들고 입장을 해야 한다.
    val creator = TestProbe()
    waitRoomActorRef.tell(CreateRoom, testProbe.ref)
    val (password, gameRoomActorRef, roomId) =
      testProbe.expectMsgType[CreatedRoom] match {
        case CreatedRoom(password, roomId) =>
          Tuple3(
            password,
            Await.result(
              system.actorSelection(waitRoomActorRef.path / roomId.toString).resolveOne(1.second),
              1.second
            ),
            roomId
          )
      }
    waitRoomActorRef.tell(GetRoomList, testProbe.ref)
    testProbe.expectMsg(GotRoomList(Map[Int, Int]()))

    gameRoomActorRef.tell(Left(CreatorJoinRoom(password, "hi")), creator.ref)

    val player =
      creator.expectMsgType[CreatorJoinedRoom] match {
        case CreatorJoinedRoom(Some(player)) => player
        case _ => fail()
      }
    creator.expectMsgType[NotifyChangeStarter]
    creator.expectMsgType[NotifyBeAbleToStart] match {
      case NotifyBeAbleToStart(None) =>
      case _ => fail()
    }
    Thread.sleep(300)
    waitRoomActorRef.tell(GetRoomList, testProbe.ref)
    testProbe.expectMsg(GotRoomList(Map[Int, Int]((roomId -> 1))))
    gameRoomActorRef.tell(Right(Right(Leave[TestBlockType.BlockType](player))), creator.ref)
    creator.expectMsg(QuitWebSocketWorkerActor)
    testProbe.watch(gameRoomActorRef)
    creator.ref.tell(PoisonPill, ActorRef.noSender)
    testProbe.expectTerminated(gameRoomActorRef)
    Thread.sleep(300)
    waitRoomActorRef.tell(GetRoomList, testProbe.ref)
    testProbe.expectMsg(GotRoomList(Map[Int, Int]()))
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    replayRoomActor = TestProbe()
    waitRoomActorRef = TestActorRef(new WaitRoomActor[BlockType](replayRoomActor.ref, replayRoom, 1.second, 1.second))
    testProbe = TestProbe()
  }

  override protected def afterEach(): Unit = {
    testProbe.ref.tell(PoisonPill, ActorRef.noSender)
    waitRoomActorRef.tell(PoisonPill, ActorRef.noSender)
    replayRoomActor.ref.tell(PoisonPill, ActorRef.noSender)
    super.afterEach()
  }
}
