/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ ActorRef, ActorSystem, PoisonPill, Props }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.NotifyJoinRoom
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ Prepare, Start, StreamInput }
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ Player, RemotePlayer }
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }

import scala.collection.immutable.Queue
import scala.concurrent.duration._

/**
 * Created by i on 2016-09-19.
 */
class TestReplayRoom extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  val replayRoom = new ReplayRoom[TestBlockType.BlockType]()
  var replayRoomActorRef: TestActorRef[replayRoom.ReplayRoomActor] = null
  var testProbe: TestProbe = null

  val remotePlayer = RemotePlayer(0, "hi")
  val playerSnapshot: Set[Player] = Set(remotePlayer)
  val recordQueue: Queue[StreamInput[TestBlockType.BlockType]] = Queue(Prepare(remotePlayer), Start(remotePlayer, 0))

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    replayRoomActorRef = TestActorRef(Props(new replayRoom.ReplayRoomActor()))
    testProbe = TestProbe()
  }

  override protected def afterEach(): Unit = {
    testProbe.ref.tell(PoisonPill, ActorRef.noSender)
    replayRoomActorRef.tell(PoisonPill, ActorRef.noSender)
    super.afterEach()
  }

  test("prepare") {
    val expected = Some(List(Right(Left(NotifyJoinRoom(0, remotePlayer))), Right(Right(Prepare(remotePlayer))), Right(Right(Start(remotePlayer, 0)))))
    assertResult(expected)(replayRoom.prepare(Some((playerSnapshot, recordQueue))))
  }

  test("RegisterReplay/GetReplayList/GetReplay") {
    replayRoomActorRef.tell(replayRoom.RegisterReplay(playerSnapshot, recordQueue), testProbe.ref)
    testProbe.expectNoMsg(0.3.seconds)
    val expected = List((playerSnapshot, recordQueue))
    assertResult(expected)(replayRoomActorRef.underlyingActor.replayRepository.values.toList)
    replayRoomActorRef.tell(replayRoom.GetReplayList, testProbe.ref)
    val replayId =
      testProbe.expectMsgType[Any] match {
        case replayRoom.GotReplayList(List(replayId)) => replayId
        case _ => fail()
      }
    replayRoomActorRef.tell(replayRoom.GetReplay(replayId), testProbe.ref)
    val resultStream =
      testProbe.expectMsgType[Any] match {
        case replayRoom.GotReplay(Some(stream)) => Some(stream.toList)
        case _ => fail()
      }
    val expectedStream = Some(List(Right(Left(NotifyJoinRoom(0, remotePlayer))), Right(Right(Prepare(remotePlayer))), Right(Right(Start(remotePlayer, 0)))))
    assertResult(expectedStream)(resultStream)
  }
}
