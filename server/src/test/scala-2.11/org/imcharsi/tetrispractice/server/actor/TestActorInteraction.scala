/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.server.actor

import akka.actor.{ ActorRef, ActorSystem, PoisonPill, Props }
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.stream.testkit.{ TestPublisher, TestSubscriber }
import akka.testkit.{ TestActorRef, TestKit, TestProbe }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ RemoveFromWaitRoomList, UpdateWaitRoom }
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ Prepare, RealizeNextBlock, Start, Stop }
import org.imcharsi.tetrispractice.common.data.fsm.{ Command, Player }
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }

import scala.concurrent.duration._

/**
 * Created by i on 9/7/16.
 */
class TestActorInteraction extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  implicit var actorMaterializer = ActorMaterializer()(system)
  val replayRoom = new ReplayRoom[TestBlockType.BlockType]()
  var replayRoomActor: TestProbe = null
  var waitRoomActor: TestProbe = null
  var gameRoomActorRef: TestActorRef[GameRoomActor[TestBlockType.BlockType]] = null
  var creatorWebSocketWorkerActorRef: TestActorRef[WebSocketWorkerActor[TestBlockType.BlockType]] = null
  var creatorSourceProbe: TestPublisher.Probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null
  var creatorSinkProbe: TestSubscriber.Probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null
  var joinerWebSocketWorkerActorRef: TestActorRef[WebSocketWorkerActor[TestBlockType.BlockType]] = null
  var joinerSourceProbe: TestPublisher.Probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null
  var joinerSinkProbe: TestSubscriber.Probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null

  override def beforeEach(): Unit = {
    replayRoomActor = TestProbe()
    waitRoomActor = TestProbe()
    gameRoomActorRef = TestActorRef(Props(new GameRoomActor[TestBlockType.BlockType](waitRoomActor.ref, replayRoomActor.ref, replayRoom, 0, 1, 1.second, 1.second)))
    creatorWebSocketWorkerActorRef =
      TestActorRef[WebSocketWorkerActor[TestBlockType.BlockType]](
        Props(new WebSocketWorkerActor[TestBlockType.BlockType](gameRoomActorRef))
      )
    joinerWebSocketWorkerActorRef =
      TestActorRef[WebSocketWorkerActor[TestBlockType.BlockType]](
        Props(new WebSocketWorkerActor[TestBlockType.BlockType](gameRoomActorRef))
      )
    val x =
      Flow
        .fromSinkAndSource(
          Sink.fromSubscriber(ActorSubscriber[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]](creatorWebSocketWorkerActorRef)),
          Source.fromPublisher(ActorPublisher[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]](creatorWebSocketWorkerActorRef))
        )
        .runWith(
          TestSource.probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]],
          TestSink.probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]]
        )
    creatorSourceProbe = x._1
    creatorSinkProbe = x._2

    val y =
      Flow
        .fromSinkAndSource(
          Sink.fromSubscriber(ActorSubscriber[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]](joinerWebSocketWorkerActorRef)),
          Source.fromPublisher(ActorPublisher[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]](joinerWebSocketWorkerActorRef))
        )
        .runWith(
          TestSource.probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]],
          TestSink.probe[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]]
        )
    joinerSourceProbe = y._1
    joinerSinkProbe = y._2
  }

  override def afterEach(): Unit = {
    gameRoomActorRef.tell(PoisonPill, ActorRef.noSender)
    waitRoomActor.ref.tell(PoisonPill, ActorRef.noSender)
    replayRoomActor.ref.tell(PoisonPill, ActorRef.noSender)
    system.stop(creatorWebSocketWorkerActorRef)
    system.stop(joinerWebSocketWorkerActorRef)
  }

  test("one GameRoomActor/two WebSocketWorkerActors usage/recordQueue") {
    creatorSourceProbe.sendNext(Left(CreatorJoinRoom(1, "hi1")))
    val creatorPlayer =
      creatorSinkProbe.requestNext() match {
        case Left(CreatorJoinedRoom(Some(player))) => player
        case _ => fail()
      }
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 1))

    creatorSinkProbe.requestNext(Right(Left(NotifyChangeStarter(creatorPlayer))))
    creatorSinkProbe.requestNext(Right(Left(NotifyBeAbleToStart(None))))

    joinerSourceProbe.sendNext(Left(JoinRoom("hi2")))
    val joinerPlayer =
      joinerSinkProbe.requestNext() match {
        case Left(JoinedRoom(Some(player))) => player
      }
    val creatorResponseId =
      creatorSinkProbe.requestNext() match {
        case Right(Left(NotifyJoinRoom(responseId, player))) =>
          assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
          responseId
      }
    creatorSourceProbe.sendNext(Right(Left(NotifiedJoinRoom(creatorResponseId, creatorPlayer, true))))
    val joinerResponseId =
      joinerSinkProbe.requestNext() match {
        case Right(Left(NotifyJoinRoom(responseId, player))) =>
          assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
          responseId
      }
    joinerSourceProbe.sendNext(Right(Left(NotifiedJoinRoom(joinerResponseId, joinerPlayer, true))))
    joinerSinkProbe.requestNext(Right(Left(NotifyChangeStarter(creatorPlayer))))
    creatorSinkProbe.requestNext(Right(Left(NotifyBeAbleToStart(None))))
    waitRoomActor.expectMsg(UpdateWaitRoom(0, 2))

    joinerSourceProbe.sendNext(Right(Right(Prepare(joinerPlayer))))
    creatorSinkProbe.requestNext(Right(Right(Prepare[TestBlockType.BlockType](Player.convertToRemotePlayer(joinerPlayer)))))
    creatorSinkProbe.requestNext(Right(Left(NotifyBeAbleToStart(None))))

    creatorSourceProbe.sendNext(Right(Right(Prepare(creatorPlayer))))
    joinerSinkProbe.requestNext(Right(Right(Prepare[TestBlockType.BlockType](Player.convertToRemotePlayer(creatorPlayer)))))
    val preparedId =
      creatorSinkProbe.requestNext() match {
        case Right(Left(NotifyBeAbleToStart(Some(preparedId)))) => preparedId
        case _ => fail()
      }

    creatorSourceProbe.sendNext(Left(StartGames(preparedId)))
    creatorSinkProbe.requestNext(Left(StartedGames(true)))

    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))

    creatorSinkProbe.requestNext() match {
      case Right(Right(Start(player, _))) => assertResult(creatorPlayer)(player)
      case _ => fail()
    }
    joinerSinkProbe.requestNext() match {
      case Right(Right(Start(player, _))) => assertResult(joinerPlayer)(player)
      case _ => fail()
    }

    creatorSourceProbe.sendNext(Right(Right(Start(creatorPlayer, 0))))
    joinerSourceProbe.sendNext(Right(Right(Start(joinerPlayer, 0))))
    creatorSinkProbe.requestNext() match {
      case Right(Right(Start(player, _))) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    joinerSinkProbe.requestNext() match {
      case Right(Right(Start(player, _))) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }

    creatorSourceProbe.sendNext(Right(Right(RealizeNextBlock(creatorPlayer))))
    joinerSourceProbe.sendNext(Right(Right(RealizeNextBlock(joinerPlayer))))
    creatorSinkProbe.requestNext() match {
      case Right(Right(RealizeNextBlock(player))) => assertResult(Player.convertToRemotePlayer(joinerPlayer))(player)
      case _ => fail()
    }
    joinerSinkProbe.requestNext() match {
      case Right(Right(RealizeNextBlock(player))) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }

    watch(gameRoomActorRef)
    creatorWebSocketWorkerActorRef.tell(PoisonPill, ActorRef.noSender)
    joinerSinkProbe.requestNext() match {
      case Right(Right(Stop(player, _))) => assertResult(Player.convertToRemotePlayer(creatorPlayer))(player)
      case _ => fail()
    }
    creatorSinkProbe.expectComplete()
    joinerWebSocketWorkerActorRef.tell(PoisonPill, ActorRef.noSender)
    joinerSinkProbe.expectComplete()
    expectTerminated(gameRoomActorRef)
    waitRoomActor.expectMsg(RemoveFromWaitRoomList(0))
    waitRoomActor.expectNoMsg(0.3.seconds)
    // GameRoomActor 가 ReplayRoomActor 로 자료를 보냈다는 것을 검사하기 위해 상황을 또 만들어야 하는 번거로움이 있다.
    // 여기서 같이 섞어서 검사한다.
    replayRoomActor.expectMsgType[Any] match {
      case replayRoom.RegisterReplay(playerSnapshot, recordQueue) =>
        val creatorRemotePlayer = Player.convertToRemotePlayer(creatorPlayer)
        val joinerRemotePlayer = Player.convertToRemotePlayer(joinerPlayer)
        val expected =
          Some(
            List(
              Right(Left(NotifyJoinRoom(0, creatorRemotePlayer))),
              Right(Left(NotifyJoinRoom(0, joinerRemotePlayer))),
              Right(Right(Prepare(creatorRemotePlayer))),
              Right(Right(Prepare(joinerRemotePlayer))),
              Right(Right(Start(creatorRemotePlayer, 0))),
              Right(Right(Start(joinerRemotePlayer, 0))),
              Right(Right(RealizeNextBlock(creatorRemotePlayer))),
              Right(Right(RealizeNextBlock(joinerRemotePlayer))),
              Right(Right(Stop(creatorRemotePlayer, ""))),
              Right(Right(Stop(joinerRemotePlayer, "")))
            )
          )
        val result = replayRoom.prepare(Some((playerSnapshot, recordQueue))).map(_.toList)
        assertResult(expected)(result)
      case _ => fail()
    }
    replayRoomActor.expectNoMsg(0.3.seconds)
  }
}
