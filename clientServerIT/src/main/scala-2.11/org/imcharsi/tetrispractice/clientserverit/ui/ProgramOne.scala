/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.clientserverit.ui

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.layout.{ Priority, VBox }
import javafx.scene.{ Parent, Scene }
import javafx.stage.Stage
import javafx.util.Callback

import org.imcharsi.tetrispractice.client.ui.{ ClientMasterController, WaitRoomController }
import org.imcharsi.tetrispractice.server.ui.ServerController
import rx.lang.scala.JavaConversions
import rx.lang.scala.schedulers.IOScheduler
import rx.schedulers.JavaFxScheduler

/**
 * Created by i on 9/11/16.
 */
object ProgramOne {
  val javaFxScheduler = JavaConversions.javaSchedulerToScalaScheduler(JavaFxScheduler.getInstance())
  val javaFxWorker = javaFxScheduler.createWorker
  val ioScheduler = IOScheduler()
  val ioWorker = ioScheduler.createWorker

  class TestApplication extends Application {

    class Xbox(stage: Stage) extends VBox {
      val serverUI =
        FXMLLoader.load[Parent](
          getClass.getResource("/org/imcharsi/tetrispractice/server/ui/server.fxml"),
          null,
          null,
          new Callback[Class[_], AnyRef] {
            override def call(param: Class[_]): AnyRef =
              new ServerController(stage, javaFxScheduler, ioScheduler, javaFxWorker, ioWorker)
          }
        )
      val clientUI =
        FXMLLoader.load[Parent](
          getClass.getResource("/org/imcharsi/tetrispractice/client/ui/clientMaster.fxml"),
          null,
          null,
          new Callback[Class[_], AnyRef] {
            override def call(param: Class[_]): AnyRef = {
              new ClientMasterController(
                stage,
                javaFxScheduler,
                ioScheduler,
                javaFxWorker,
                ioWorker
              )
            }
          }
        )
      getChildren.addAll(serverUI, clientUI)
      VBox.setVgrow(clientUI, Priority.ALWAYS)
    }

    override def start(primaryStage: Stage): Unit = {
      primaryStage.setScene(new Scene(new Xbox(primaryStage)))
      primaryStage.show()
    }
  }

  def main(args: Array[String]): Unit = Application.launch(classOf[TestApplication], args: _*)
}
