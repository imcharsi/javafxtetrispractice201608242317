/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class Explain {
  type State[S, A] = S => (S, A)

  sealed case class StateA(valA: Int)

  def f1[S, A, B](a: A): State[S, B] = ??? // heavy!

  def f2[S, B, C](b: B): State[S, C] = ??? // heavy!

  def bind1[S, A, B](f: A => State[S, B]): State[S, A] => State[S, B] =
    (stateA: State[S, A]) => {
      val stateB: State[S, B] =
        (s: S) => {
          val (newS, newA) = stateA(s)
          val newStateB = f(newA)
          val (newS2, newB) = newStateB(newS)
          (newS2, newB)
        }
      stateB
    }

  def bind2[S, A, B](f: A => (S => (S, B))): (S => (S, A)) => (S => (S, B)) =
    (stateA: (S => (S, A))) => {
      val stateB: (S => (S, B)) =
        (s: S) => {
          val (newS, newA) = stateA(s)
          val newStateB = f(newA)
          val (newS2, newB) = newStateB(newS)
          (newS2, newB)
        }
      stateB
    }

  def liftedF1[S, A, B]: State[S, A] => State[S, B] = bind1(f1)

  def a(param: Int)(stateA: StateA): (StateA, Int) = {
    val newStateA = stateA.copy(valA = param)
    (newStateA, stateA.valA)
  }

  val resultX: State[StateA, Int] = liftedF1(a(1))
  val resultX2: StateA => (StateA, Int) = liftedF1(a(1))
  val resultX3: (StateA, Int) = resultX(StateA(1))

  def liftedF2[S, B, C]: State[S, B] => State[S, C] = bind1(f2)

  def composedF[S, A, C]: State[S, A] => State[S, C] = liftedF1.andThen(liftedF2)

  def composedF2[S, A, C]: (S => (S, A)) => (S => (S, C)) = liftedF1.andThen(liftedF2)

  val result1: (StateA, Int) = composedF(a(1))(StateA(1))
  val result2: (StateA => (StateA, Int)) = composedF2(a(1))
  val result3: (StateA, Int) = result2(StateA(1))

}
