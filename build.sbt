import de.heikoseeberger.sbtheader.license.Apache2_0

name := "JavaFXTetrisPractice201608242317"

version := "1.0-SNAPSHOT"
scalacOptions := Seq("-unchecked", "-deprecation")

val akkaVersion: SettingKey[String] = SettingKey("AkkaVersion")
val rxJavaVersion: SettingKey[String] = SettingKey("RxJavaVersion")
val scalatestVersion: SettingKey[String] = SettingKey("ScalatestVersion")

val headersMap = Map(
  "scala" -> Apache2_0("2016", "KangWoo, Lee (imcharsi@hotmail.com)")
)

val scalaDependencies = Seq(
  scalaVersion := "2.11.8",
  libraryDependencies += "org.scala-lang" % "scala-library" % scalaVersion.value,
  headers := headersMap
)

val commonDependencies = Seq(
  akkaVersion := "2.4.9",
  rxJavaVersion := "1.1.9",
  scalatestVersion := "3.0.0",
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-actor" % akkaVersion.value,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-testkit_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % akkaVersion.value,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-stream_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-stream" % akkaVersion.value,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-stream-testkit_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion.value,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-spray-json-experimental_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaVersion.value,
  // https://mvnrepository.com/artifact/io.reactivex/rxjava
  libraryDependencies += "io.reactivex" % "rxjava" % "1.1.9",
  // https://mvnrepository.com/artifact/io.reactivex/rxscala_2.11
  libraryDependencies += "io.reactivex" %% "rxscala" % "0.26.2",
  // https://mvnrepository.com/artifact/org.scalatest/scalatest_2.11
  // sbt version 을 0.13.12 로 하면 scalatest 의 의존성 중 일부가 중복된다는 경고문제를 해결할 수 있다.
  libraryDependencies += "org.scalatest" %% "scalatest" % scalatestVersion.value % Test,
  libraryDependencies += "io.reactivex" % "rxjavafx" % "0.1.4"
)

// commonDependencies 에 이어서 써야 한다.
val serverDependencies = Seq(
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-core_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-http-core" % akkaVersion.value,
  // https://mvnrepository.com/artifact/com.typesafe.akka/akka-http-testkit_2.11
  libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion.value
)

val clientServerIDSettings = Seq(
  fork in run := true
)

val common =
  (project in file("common"))
    .settings(scalaDependencies)
    .settings(commonDependencies)
    .enablePlugins(AutomateHeaderPlugin)

val server =
  (project in file("server"))
    .settings(scalaDependencies)
    .settings(commonDependencies)
    .settings(serverDependencies)
    .dependsOn(common % "compile->compile")
    .dependsOn(common % "test->test")
    .enablePlugins(AutomateHeaderPlugin)

val client =
  (project in file("client"))
    .settings(scalaDependencies)
    .settings(commonDependencies)
    .dependsOn(common % "compile->compile")
    .dependsOn(common % "test->test")
    .enablePlugins(AutomateHeaderPlugin)

val clientServerIT =
  (project in file("clientServerIT"))
    .settings(scalaDependencies)
    .settings(commonDependencies)
    .settings(clientServerIDSettings)
    .dependsOn(common % "compile->compile")
    .dependsOn(common % "test->test")
    .dependsOn(client)
    .dependsOn(server)
    .enablePlugins(AutomateHeaderPlugin)

val root =
  (project in file("."))
    .settings(scalaDependencies)
    .aggregate(common, server, client, clientServerIT)