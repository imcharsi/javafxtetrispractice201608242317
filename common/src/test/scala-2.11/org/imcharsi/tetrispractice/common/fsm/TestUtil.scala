/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import org.imcharsi.tetrispractice.common.data.fsm._
import BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import Command.BlockCellList
import GameState.RunningGame
import Player.{ LocalPlayer, Player }
import org.imcharsi.tetrispractice.common.fsm.Game.{ LogS, LogSS }
import org.imcharsi.tetrispractice.common.util.State
import org.scalatest.{ BeforeAndAfterEach, FunSuite }

/**
 * Created by i on 8/30/16.
 */
class TestUtil extends FunSuite with BeforeAndAfterEach {

  implicit val runningGameSMB = new State.StateMonadBehavior[Game.RunningGameSS[TestBlockType.BlockType]]
  var idCounter: Int = 0
  var timerIdCounter: Int = 0

  def allocUiId(map: Map[Position, BlockCell[TestBlockType.BlockType]]): Map[Position, BlockCell[TestBlockType.BlockType]] = {
    // mapValues 를 쓰면 안 된다. map 에 대한 조회를 할 때마다 mapValues 내용을 평가한다. 그래서 side-effect 를 하면 문제된다.
    map.foldLeft(map) {
      case (map, (position, blockCell)) =>
        blockCell match {
          case UninitializedBlockCell(cellType) =>
            val newId = idCounter
            idCounter = idCounter + 1
            map + ((position -> InitializedBlockCell[TestBlockType.BlockType](newId, cellType)))
          case cell @ InitializedBlockCell(_, _) => map
        }
    }
  }

  def allocTimerId(): Int = {
    val newTimerId = timerIdCounter
    timerIdCounter = timerIdCounter + 1
    newTimerId
  }

  def allocateBePushed(bePushed: BlockCellList[TestBlockType.BlockType]) =
    bePushed.map {
      case (position, blockCell) =>
        val newId = idCounter
        idCounter = idCounter + 1
        (position, InitializedBlockCell[TestBlockType.BlockType](newId, TestBlockType.BlockOne))
    }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    idCounter = 0
    timerIdCounter = 0
  }
  // 여기의 모든 검사는 TestBlockType 의 구성을 전제한다.
  // 그래서, 난수는 사실상 0, 1, 2, ... 과 같이 주어짐을 미리 알고 시작한다.

  test("prepareClearedMap") {
    val x = Util.prepareClearedMap(UninitializedBlockCell(TestBlockType.BlockOne), Position(1, 1))(Map())
    assertResult(5)(x.size)
    val position = Position(0, 0)
    val positionList =
      (0).to(1)
        .foldLeft(List[Position]())((z, y) =>
          (-1).to(1).foldLeft(z)((z, x) => Position(x, y) :: z))
        .filter(_ != position)
    assert(x.forall(x => positionList.exists(_ == x._1)))
    assert(x.get(position).isEmpty)
  }

  test("generateRandom") {
    val player: Player = LocalPlayer(0, "hi")
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        GameMap(Position(0, 0), Map()),
        (0, GameMap(Position(0, 0), Map())),
        None,
        None,
        0,
        Block(Position(0, 0), Map())
      )
    val logS: LogS = State[LogSS, Unit](())
    val ((resultRunningGame, _), resultRandomNumber) = Util.generateRandom(TestBlockType.random)((runningGame, logS))
    val ((expectedSeed, _), expectedRandomNumber) = TestBlockType.random((runningGame.seed, logS))
    assertResult(expectedRandomNumber)(resultRandomNumber)
    assertResult(expectedSeed)(resultRunningGame.seed)
  }

  test("generateNext") {
    val logS: LogS = State[LogSS, Unit](())
    val ((resultSeed, _), resultBlock) =
      Util.generateNext(TestBlockType.random, TestBlockType.generateBlockF, Position(0, 0), TestBlockType.rotateF)((0, logS))
    // 0 에서 시작하여 벽돌 종류를 선택하면서 난수 0 을 사용하였으므로 BlockOne 이 사용되었고,
    // 1 에서 시작하여 벽돌을 회전하면서 난수 1 을 사용하여 1 번의 회전을 하였으므로 아래의 결과와 같다.
    // 인자 함수를 사용하여 예상 결과를 계산할 수도 있었지만,
    // 그와 같이 하면 generateNext 의 내용을 검사에 그대로 옮겨 쓰는 결과가 되어 번거롭다.
    assertResult(
      Block(
        Position(0, 0),
        Map(Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
      )
    )(resultBlock)
    // 두 번의 난수 사용이 있었음을 확인한다.
    assertResult(2)(resultSeed)
  }

  test("generateBlock") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        None,
        None,
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())
    val ((resultRunningGame, _), x) =
      Util.generateBlock[TestBlockType.BlockType](
        TestBlockType.random,
        TestBlockType.generateBlockF,
        TestBlockType.rotateF,
        Position(0, 0)
      ).f((runningGame, logS))
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame)
    )
    // 현재 벽돌이 만들어졌다.
    assertResult(
      TestBlockType.generateBlockF(0).copy(position = Position(0, 0))
    )(resultRunningGame.currentBlock.map(_._1).get)
    assertResult(
      TestBlockType.generateBlockF(0).copy(position = Position(0, 2))
    )(resultRunningGame.currentBlock.map(_._2).get)
    // 현재의 TestBlockType 구성에 따라,
    // 0 의 seed 에서 시작하여 만든 다음 벽돌은 현재 벽돌과 다르다.
    assert(
      TestBlockType.generateBlockF(0).copy(position = Position(0, 0)) != resultRunningGame.next
    )
  }

  test("rotateBlockSuccess") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 2))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())
    val ((resultRunningGame, _), x) =
      Util.rotateBlock[TestBlockType.BlockType](gameMap, TestBlockType.rotateF)(BlockTransformType.LeftToDowToRight)((runningGame, logS))
    x match {
      case Some(Some((block, expectedLanding))) =>
        assertResult(
          Block(
            Position(0, 0),
            Map(Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
        assertResult(
          Block(
            Position(0, 2),
            Map(Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(expectedLanding)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame)
    )
  }

  test("rotate expectedLandingBlock") {
    // 이 검사의 요지는 제목을 봐서는 알 수가 없는데,
    // ??..
    // ....
    // __** 와 같을 때
    // __.. 예상낙하지점 벽돌을
    // ....
    // ....
    // ..** 배경에 대해 회전시도하지 않고 빈 배경에 대해 무조건 회전성공하도록 하여
    // ..??
    // ..__
    // ..** 가 되는지 확인하는 것이 이 검사의 요지이다.
    // 이와 같은 문제를 알고 대안을 제시하기 전까지는
    // 예상낙하지점 벽돌을 회전할 때도 게임 판 배경을 대상으로 회전을 시도하여
    // 결국 예상낙하지점 벽돌을 회전하지 못해 전체 회전이 실패하는 것이 문제였었다.
    // 이 예의 경우 대안을 제시하기 전에는, 예상낙하지점 벽돌 __ 를 회전시도했을 때 배경 ** 에 걸려서 회전하지 못하게 된다.
    // 전체 검사가 구상한 대로 되고 있다는 것을 알기 위해서는 추가적인 계산이 필요하고
    // 그래서 expectLanding 과 decoratorExpectedLanding 기능을 같이 썼다.
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map((Position(1, 2) -> UninitializedBlockCell(TestBlockType.BlockOne))))
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 2))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())
    import org.imcharsi.tetrispractice.common.util.FP.StateMonadSyntaxSugar
    val ((resultRunningGame, _), x) =
      Util.rotateBlock[TestBlockType.BlockType](gameMap, TestBlockType.rotateF)(BlockTransformType.LeftToDowToRight)
        .passThrough(Util.expectedLanding[TestBlockType.BlockType])
        .bind(Util.decoratorExpectedLanding[TestBlockType.BlockType](_))((runningGame, logS))
    x match {
      case Some(Some((block, expectedLanding))) =>
        assertResult(
          Block(
            Position(0, 0),
            Map(Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
        assertResult(
          Block(
            Position(0, 1),
            Map(Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(expectedLanding)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame)
    )
  }

  test("rotateBlockFail") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map(Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)))
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 2))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())
    val ((resultRunningGame, _), x) =
      Util.rotateBlock[TestBlockType.BlockType](gameMap, TestBlockType.rotateF)(BlockTransformType.LeftToDowToRight)((runningGame, logS))
    x match {
      case Some(None) =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame)
    )
  }

  test("moveBlockOneStep") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 1))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    val ((resultRunningGame1, _), x1) =
      Util.moveBlockOneStep[TestBlockType.BlockType](1, 0)((runningGame, logS))
    x1 match {
      case Some(Some((block, position))) =>
        assertResult(
          Block(
            Position(1, 0),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )

    val ((resultRunningGame2, _), x2) =
      Util.moveBlockOneStep[TestBlockType.BlockType](1, 0)((resultRunningGame1, logS))
    x2 match {
      case Some(None) =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame2)
    )

    val ((resultRunningGame3, _), x3) =
      Util.moveBlockOneStep[TestBlockType.BlockType](-1, 0)((resultRunningGame2, logS))
    x3 match {
      case Some(Some((block, position))) =>
        assertResult(
          Block(
            Position(0, 0),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame3)
    )

    val ((resultRunningGame4, _), x4) =
      Util.moveBlockOneStep[TestBlockType.BlockType](-1, 0)((resultRunningGame3, logS))
    x4 match {
      case Some(None) =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame4)
    )

    val ((resultRunningGame5, _), x5) =
      Util.moveBlockOneStep[TestBlockType.BlockType](0, 1)((resultRunningGame4, logS))
    x5 match {
      case Some(Some((block, position))) =>
        assertResult(
          Block(
            Position(0, 1),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame5)
    )

    val ((resultRunningGame6, _), x6) =
      Util.moveBlockOneStep[TestBlockType.BlockType](0, 1)((resultRunningGame5, logS))
    x6 match {
      case Some(None) =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame6)
    )
  }

  test("moveBlockOneStepThenStop") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)))
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap.copy(map = Map())),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 1))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    val ((resultRunningGame1, _), x1) =
      Util.moveBlockOneStep[TestBlockType.BlockType](1, 0)((runningGame, logS))
    x1 match {
      case None =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )
  }

  test("moveDownRepeat") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 2))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    val ((resultRunningGame1, _), x1) =
      Util.moveDownRepeat[TestBlockType.BlockType]((runningGame, logS))
    x1 match {
      case Some(Some((block, position))) =>
        assertResult(
          Block(
            Position(0, 2),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )
  }

  test("expectedLanding") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 0))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    assertResult(Position(0, 0))(runningGame.currentBlock.map(_._2).get.position)
    val ((resultRunningGame1, _), _) =
      Util.expectedLanding[TestBlockType.BlockType]((runningGame, logS))
    assertResult(Position(0, 2))(resultRunningGame1.currentBlock.map(_._2).get.position)

    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )
  }

  test("overlapWithBlockCellList") {
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val blockCellList =
      List[(Position, BlockCell[TestBlockType.BlockType])](
        (Position(0, 0), UninitializedBlockCell(TestBlockType.BlockOne))
      )

    assert(gameMap.map.get(Position(0, 0)).isEmpty)
    val resultGameMap =
      Util.overlapWithBlockCellList[TestBlockType.BlockType](gameMap, blockCellList)
    assertResult(
      Some(UninitializedBlockCell(TestBlockType.BlockOne))
    )(resultGameMap.map.get(Position(0, 0)))
  }

  test("overlap") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 0))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    assert(runningGame.gameMap.map.get(Position(0, 0)).isEmpty)
    assert(runningGame.currentBlock.isDefined)
    assert(runningGame.currentTimerId.isDefined)
    val ((resultRunningGame1, _), _) =
      Util.overlap[TestBlockType.BlockType]((runningGame, logS))
    assertResult(
      Some(UninitializedBlockCell(TestBlockType.BlockOne))
    )(resultRunningGame1.gameMap.map.get(Position(0, 0)))
    assert(runningGame.currentBlock.isDefined)
    assert(runningGame.currentTimerId.isDefined)

    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )
  }

  test("beBrokenOrNot") {
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(2, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
          ))
      )
    assert(Util.beBrokenOrNot(0, gameMap).isRight)
    assert(Util.beBrokenOrNot(1, gameMap).isLeft)
  }

  // 이 검사는 두 개를 같이 하는 것이 편하다.
  // collapse 의 입력이 separateBeCollapsedAndDeleted 의 출력이기 때문이다.
  test("separateBeCollapsedAndDeleted/collapse") {
    val cleared =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(Map())
      )
    val gameMap =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 1) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 2) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(0, 3) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
          ))
      )
    val (beCollapsed, beDeleted) = Util.separateBeCollapsedAndDeleted(gameMap)
    val expectedCollapsed =
      List(
        Tuple2(
          2,
          List(
            (Position(1, 2), UninitializedBlockCell(TestBlockType.BlockOne))
          )
        ),
        Tuple2(
          0,
          List(
            (Position(0, 0), UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )
      )
    val expectedDeleted =
      List(
        List(
          (Position(1, 3), UninitializedBlockCell(TestBlockType.BlockOne)),
          (Position(0, 3), UninitializedBlockCell(TestBlockType.BlockOne))
        ),
        List(
          (Position(1, 1), UninitializedBlockCell(TestBlockType.BlockOne)),
          (Position(0, 1), UninitializedBlockCell(TestBlockType.BlockOne))
        )
      )
    assertResult(expectedCollapsed)(beCollapsed)
    assertResult(expectedDeleted)(beDeleted)

    val expectedGameMap =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(Map(
            (Position(0, 2) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
          ))
      )
    val (resultGameMap, resultMoved) = Util.collapse(cleared, gameMap, beCollapsed)
    assertResult(expectedGameMap)(resultGameMap)
    val expectedMoved =
      List(
        List(
          (Position(0, 2) -> UninitializedBlockCell(TestBlockType.BlockOne))
        ),
        List(
          (Position(1, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
        )
      )
    assertResult(expectedMoved)(resultMoved)
  }

  test("breakAndCollapse") {
    val player: Player = LocalPlayer(0, "hi")
    val cleared =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(Map())
      )
    // 이 검사는, 내려오는 벽돌과 게임 판을 겹치는 단계부터 해야 검사결과를 확인하기 쉽다.
    val gameMap =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(allocUiId(Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 2) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(1, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )))
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, cleared),
        Some(
          Tuple2(
            Block[TestBlockType.BlockType](
              Position(0, 1),
              allocUiId(Map(
                (Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(0, 2) -> UninitializedBlockCell(TestBlockType.BlockOne))
              ))
            ),
            Block[TestBlockType.BlockType](
              Position(0, 1),
              allocUiId(Map(
                (Position(1, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(0, 2) -> UninitializedBlockCell(TestBlockType.BlockOne))
              ))
            )
          )
        ),
        None,
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    // 햇갈리는 검사내용이다.
    //  0..
    //  1..
    // .. 2
    // .. 3 와 같은 게임 판이 있고
    // .. 4
    // ....
    //  5.. 와 같은 내려오는 벽돌이 있어서
    //  0..
    //  1 4
    // .. 2
    //  5 3 와 같은 겹치는 모양이 되었을 때
    import org.imcharsi.tetrispractice.common.util.FP.StateMonadSyntaxSugar
    // 검사의 편의를 위해 일부러 TestBlockType 의 구상과는 다른 벽돌 크기를 정했다.
    // 이는, 단순히 출력의 편의를 위함이고 계산과는 상관없다.
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 3), UninitializedBlockCell(TestBlockType.BlockOne), "")(runningGame))
    val ((resultRunningGame, _), resultMoveBlockR) =
      (Util.overlap[TestBlockType.BlockType] >> Util.breakAndCollapse(cleared)(Some(None)))((runningGame, logS))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 3), UninitializedBlockCell(TestBlockType.BlockOne), "")(resultRunningGame))
    Console.err.println(resultMoveBlockR)
    resultMoveBlockR match {
      case Some(Left((moved, deleted, arrangedDeleted))) =>
        // 줄을 깨고 모은 결과 게임 판에 남는 것은
        // ....
        // ....
        //  0..
        // .. 2 가 되어 남는 벽돌은 이와 같이 이동해야 하고
        val expectedMoved =
          List(
            List((Position(0, 2), InitializedBlockCell(0, TestBlockType.BlockOne))),
            List((Position(1, 3), InitializedBlockCell(2, TestBlockType.BlockOne)))
          )
        assertResult(expectedMoved)(moved)
        // 화면에서 지워지는 벽돌은 1, 3, 4, 5 와, 예상낙하지점을 그리는 데 쓴 벽돌 6, 7 이 되고
        val expectedDeleted = Set(1, 3, 4, 5, 6, 7)
        assertResult(expectedDeleted)(deleted.flatMap(_.map { case (_, InitializedBlockCell(uiId, _)) => uiId }).toSet)
        // 상대방에게 넘길 벽돌은 1, 3 자리에 있던 벽돌인데 ui 식별번호가 할당되지 않은 벽돌로 바뀌므로
        // ....
        // ....
        // ??..
        // ..?? 가 된다.
        val expectedArrangedDeleted =
          List(
            List((Position(1, 3), UninitializedBlockCell(TestBlockType.BlockOne))),
            List((Position(0, 2), UninitializedBlockCell(TestBlockType.BlockOne)))
          )
        assertResult(expectedArrangedDeleted)(arrangedDeleted)
      case _ => fail()
    }
    assert(resultRunningGame.currentBlock.isEmpty)
    assert(resultRunningGame.currentTimerId.isEmpty)
  }

  test("moveUpGameMap") {
    val cleared =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(
            allocUiId(
              Map(
                (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )
    val expectedGameMap =
      gameMap.copy(map = (gameMap.map - Position(0, 1)) + (Position(0, 0) -> gameMap.map(Position(0, 1))))
    val expectedMoved = List(Position(0, 0) -> gameMap.map(Position(0, 1)))
    val expectedDeleted = List(Position(0, 0) -> gameMap.map(Position(0, 0)))
    val (resultGameMap, resultMoved, resultDeleted) = Util.moveUpGameMap(cleared, 1, gameMap)
    Console.err.println(Printer.mapToString(resultGameMap))
    assertResult(expectedGameMap)(resultGameMap)
    assertResult(expectedMoved)(resultMoved)
    assertResult(expectedDeleted)(resultDeleted)
  }

  test("moveUpGameMap2") {
    // 이 검사는 제목만 봐서는 요지를 알 수가 없다.
    // 이 검사를 쓰게 됐을 당시에, 미처 알지 못했던 문제가 하나 있어서 이 문제를 고쳤다는 확인을 하기 위해 이 검사를 썼다.
    // 무슨 문제인가.
    // gameMap 의 벽돌을 위로 올리는 계산에서, 지우면 안 되는 벽돌을 계산결과에서 지우는 문제가 있었다.
    // 따라서, 실제 게임에서는 새 줄을 쌓을 때마다 이미 쌓아놨던 벽돌이 위로 올라가면서 일부가 없어진다.
    // 그래서, 이 문제를 보이기 위해서 아래와 같이 gameMap 을 만든 후 한 줄 위로 올리는 검사를 했다.
    // ....
    //  0..
    //  1..
    //  2..
    // 문제를 풀기 전에는, moveUpGameMap 을 한 결과는
    //  0..
    // ....
    //  2..
    // .... 가 되었었다.
    val cleared =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(Map())
      )
    val gameMap =
      GameMap(
        Position(2, 4),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 4)
        )(
            allocUiId(
              Map(
                (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(0, 2) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(0, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )
    val expectedGameMap =
      gameMap.copy(map =
        gameMap.map + (Position(0, 0) -> gameMap.map(Position(0, 1)))
          + (Position(0, 1) -> gameMap.map(Position(0, 2)))
          + (Position(0, 2) -> gameMap.map(Position(0, 3)))
          - Position(0, 3))
    val expectedMoved =
      List(
        Position(0, 0) -> gameMap.map(Position(0, 1)),
        Position(0, 1) -> gameMap.map(Position(0, 2)),
        Position(0, 2) -> gameMap.map(Position(0, 3))
      )
    val expectedDeleted = List()
    val (resultGameMap, resultMoved, resultDeleted) = Util.moveUpGameMap(cleared, 1, gameMap)
    Console.err.println(Printer.mapToString(resultGameMap))
    assertResult(expectedGameMap)(resultGameMap)
    assertResult(expectedMoved.toSet)(resultMoved.toSet)
    assertResult(expectedDeleted)(resultDeleted)
  }

  test("pushToBePushed") {
    // 그림으로 봐야 검사의 내용을 쉽게 알 수 있다.
    //  0..
    //  1.. 와 같이 줄을 쌓아놨고
    // ....
    // ..?? 와 같이 줄을 더 쌓으려고 하면
    //  0..
    //  1..
    // ..?? 와 같이 줄을 쌓게 되는데, 2 줄만 가능하므로 위의 한 줄은 잘려서
    val player: Player = LocalPlayer(0, "hi")
    val cleared =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(
            allocUiId(
              Map(
                (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        cleared,
        (2, gameMap),
        None,
        None,
        0,
        TestBlockType.generateBlockF(0)
      )
    val bePushed =
      List[(Position, BlockCell[TestBlockType.BlockType])](
        Position(1, 1) -> UninitializedBlockCell(TestBlockType.BlockOne)
      )
    val logS: LogS = State(())
    val expectedGameMap =
      gameMap.copy(
        map =
        (gameMap.map - Position(0, 1)) +
          (Position(0, 0) -> gameMap.map(Position(0, 1))) +
          (Position(1, 1) -> UninitializedBlockCell[TestBlockType.BlockType](TestBlockType.BlockOne))
      )
    val expectedBePushed = List(Position(1, 1) -> UninitializedBlockCell[TestBlockType.BlockType](TestBlockType.BlockOne))
    val expectedMoved = List(Position(0, 0) -> gameMap.map(Position(0, 1)))
    val expectedDeleted = List(Position(0, 0) -> gameMap.map(Position(0, 0)))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 2), UninitializedBlockCell(TestBlockType.BlockOne), "")(runningGame))
    val ((resultRunningGame, _), (resultBePushed, resultMoved, resultDeleted)) =
      Util.pushToBePushed(cleared)(1, bePushed)((runningGame, logS))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 2), UninitializedBlockCell(TestBlockType.BlockOne), "")(resultRunningGame))
    //  1..
    // ..?? 가 된다.
    assertResult(expectedGameMap)(resultRunningGame.bePushed._2)
    // 아래의 ?? 는 Server 로부터 전달받았고 아직 ui 식별번호를 할당하지 않아서 구독단계에서 식별번호를 붙여야 한다.
    // 처음부터 ui 식별번호를 붙여놓고 FSM 으로 Push 입력을 하는 식으로 구상하지 않았다.
    // 구상은, ui 식별번호를 붙여야 한다는 FSM 의 판단에 이어서 식별번호를 붙인 후 FSM 으로 응답을 다시 해주는 식이다.
    assertResult(expectedBePushed)(resultBePushed)
    // 위의 1 은 아래에 있던 1 이 위로 올라간 것이고 그래서 화면표시에서 이동을 그려야 한다.
    assertResult(expectedMoved)(resultMoved)
    // 위에 있던 0 은 밀려 올라가면서 지워진다.
    assertResult(expectedDeleted)(resultDeleted)
  }

  test("pushToMap overflow") {
    val player: Player = LocalPlayer(0, "hi")
    val cleared =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    //  0..
    // .. 1 가 있고
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(
            allocUiId(
              Map(
                (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
                (Position(1, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )
    // ....
    //  2.. 와 같이 쌓아놨고
    val bePushed =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(
            allocUiId(
              Map(
                (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )

    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (1, bePushed),
        None,
        None,
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State(())
    val expectedGameMap =
      gameMap.copy(
        map =
          Util.prepareClearedMap[TestBlockType.BlockType](
            UninitializedBlockCell(TestBlockType.BlockOne),
            Position(2, 2)
          )(Map(
              (Position(1, 0) -> gameMap.map(Position(1, 1))),
              (Position(0, 1) -> bePushed.map(Position(0, 1)))
            ))
      )
    val expectedMoved = List(Position(1, 0) -> gameMap.map(Position(1, 1)))
    val expectedDeleted = List(Position(0, 0) -> gameMap.map(Position(0, 0)))
    val expectedMovedFromBePushedToMap = List(Position(0, 1) -> bePushed.map(Position(0, 1)))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 2), UninitializedBlockCell(TestBlockType.BlockOne), "")(runningGame))
    // 쌓아뒀던 것을 게임 판으로 밀어 올리면
    val ((resultRunningGame, _), (resultMoved, resultDeleted, resultMovedFromBePushedToMap, resultContinue)) =
      Util.pushToMap(cleared)((runningGame, logS))
    //  0..
    // .. 1
    //  2.. 가 되는데, 2 줄만 가능하므로 위의 첫줄은 짤려서
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 2), UninitializedBlockCell(TestBlockType.BlockOne), "")(resultRunningGame))
    // .. 1
    //  2.. 가 된다.
    assertResult(expectedGameMap)(resultRunningGame.gameMap)
    // 아래에 있던 1 이 위로 올라가야 하고
    assertResult(expectedMoved)(resultMoved)
    // 위에 있던 0 은 지워져야 하고
    assertResult(expectedDeleted)(resultDeleted)
    // 쌓아뒀던 벽돌 2 는 게임 판으로 가야 하고
    assertResult(expectedMovedFromBePushedToMap)(resultMovedFromBePushedToMap)
    // 게임을 계속 할 수 없다.
    assertResult(false)(resultContinue)
  }

  test("pushToMap") {
    val player: Player = LocalPlayer(0, "hi")
    val cleared =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    // ....
    // .. 1 가 있고
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(
            allocUiId(
              Map(
                (Position(1, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )
    // ....
    //  2.. 와 같이 쌓아놨고
    val bePushed =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(
            allocUiId(
              Map(
                (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
      )

    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (1, bePushed),
        None,
        None,
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State(())
    val expectedGameMap =
      gameMap.copy(
        map =
          Util.prepareClearedMap[TestBlockType.BlockType](
            UninitializedBlockCell(TestBlockType.BlockOne),
            Position(2, 2)
          )(Map(
              (Position(1, 0) -> gameMap.map(Position(1, 1))),
              (Position(0, 1) -> bePushed.map(Position(0, 1)))
            ))
      )
    val expectedMoved = List(Position(1, 0) -> gameMap.map(Position(1, 1)))
    val expectedMovedFromBePushedToMap = List(Position(0, 1) -> bePushed.map(Position(0, 1)))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 2), UninitializedBlockCell(TestBlockType.BlockOne), "")(runningGame))
    // 쌓아뒀던 것을 게임 판으로 밀어 올리면
    val ((resultRunningGame, _), (resultMoved, resultDeleted, resultMovedFromBePushedToMap, resultContinue)) =
      Util.pushToMap(cleared)((runningGame, logS))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 2), UninitializedBlockCell(TestBlockType.BlockOne), "")(resultRunningGame))
    // .. 1
    //  2.. 가 되고 넘치는 없다.
    assertResult(expectedGameMap)(resultRunningGame.gameMap)
    // 아래에 있던 1 이 위로 올라가야 하고
    assertResult(expectedMoved)(resultMoved)
    // 지울 것은 없고
    assert(resultDeleted.isEmpty)
    // 쌓아뒀던 벽돌 2 는 게임 판으로 가야 하고
    assertResult(expectedMovedFromBePushedToMap)(resultMovedFromBePushedToMap)
    // 게임을 계속 할 수 있다.
    assertResult(true)(resultContinue)
  }

  test("moveBlock") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 1))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    val ((resultRunningGame1, _), x1) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Right).f((runningGame, logS))
    x1 match {
      case Some(Right(Some((block, expectedLanding)))) =>
        assertResult(
          Block(
            Position(1, 0),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
        assertResult(
          Block(
            Position(1, 1),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(expectedLanding)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )

    val ((resultRunningGame2, _), x2) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Right).f((resultRunningGame1, logS))
    x2 match {
      case Some(Right(None)) =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame2)
    )

    val ((resultRunningGame3, _), x3) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Left).f((resultRunningGame2, logS))
    x3 match {
      case Some(Right(Some((block, expectedLanding)))) =>
        assertResult(
          Block(
            Position(0, 0),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
        assertResult(
          Block(
            Position(0, 1),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(expectedLanding)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame3)
    )

    val ((resultRunningGame4, _), x4) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Left).f((resultRunningGame3, logS))
    x4 match {
      case Some(Right(None)) =>
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame4)
    )

    val ((resultRunningGame5, _), x5) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Down).f((resultRunningGame4, logS))
    x5 match {
      case Some(Right(Some((block, expectedLanding)))) =>
        assertResult(
          Block(
            Position(0, 1),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
        assertResult(
          Block(
            Position(0, 1),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(expectedLanding)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame5)
    )

    val ((resultRunningGame6, _), x6) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Down).f((resultRunningGame5, logS))
    x6 match {
      case Some(Left((List(), deleted, List()))) =>
        // 벽돌이 바닥에 닿았을 때, 예상낙하지점을 그리는 데 쓴 ui 객체도 지운다는 판단을 FSM 이 했는지에 대해 검사한다.
        assertResult(
          List(Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)).toList)
        )(deleted)
      case x =>
        Console.err.println(x)
        fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame6)
    )
  }

  test("moveBlock Bottom") {
    val player: Player = LocalPlayer(0, "hi")
    val gameMap =
      GameMap(
        Position(2, 3),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 3)
        )(Map())
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, gameMap),
        Some(
          Tuple2(
            TestBlockType.generateBlockF(0),
            TestBlockType.generateBlockF(0).copy[TestBlockType.BlockType](position = Position(0, 1))
          )
        ),
        Some(allocTimerId()),
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    val ((resultRunningGame1, _), x1) =
      Util.moveBlock[TestBlockType.BlockType](gameMap)(BlockTransformType.Bottom).f((runningGame, logS))
    x1 match {
      case Some(Right(Some((block, expectedLanding)))) =>
        assertResult(
          Block(
            Position(0, 2),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(block)
        assertResult(
          Block(
            Position(0, 2),
            Map(Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )(expectedLanding)
      case _ => fail()
    }
    Console.out.println(
      Printer.runningGameToString[TestBlockType.BlockType](
        Position(2, 1),
        UninitializedBlockCell(TestBlockType.BlockOne),
        ""
      )(resultRunningGame1)
    )
  }

  test("moveBlock breakAndCollapse") {
    val player: Player = LocalPlayer(0, "hi")
    val cleared =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(Map())
      )
    val gameMap =
      GameMap(
        Position(2, 2),
        Util.prepareClearedMap[TestBlockType.BlockType](
          UninitializedBlockCell(TestBlockType.BlockOne),
          Position(2, 2)
        )(allocUiId(Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne)),
            (Position(0, 1) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )))
      )
    val runningGame =
      RunningGame[TestBlockType.BlockType](
        player,
        gameMap,
        (0, cleared),
        Some(
          Tuple2(
            Block[TestBlockType.BlockType](
              Position(1, 1),
              allocUiId(Map(
                (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
              ))
            ),
            Block[TestBlockType.BlockType](
              Position(0, 1),
              allocUiId(Map(
                (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
              ))
            )
          )
        ),
        None,
        0,
        TestBlockType.generateBlockF(0)
      )
    val logS: LogS = State[LogSS, Unit](())

    //  0..
    //  1.. 와 같은 게임 판이 있고
    // .. 2 와 같은 내려오는 벽돌이 있어서
    //  0..
    //  1 2 와 같은 겹치는 모양이 되었을 때
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 1), UninitializedBlockCell(TestBlockType.BlockOne), "")(runningGame))
    val ((resultRunningGame, _), resultMoveBlockR) =
      Util.moveBlock[TestBlockType.BlockType](cleared)(BlockTransformType.Bottom).f((runningGame, logS))
    Console.err.println(Printer.runningGameToString[TestBlockType.BlockType](Position(2, 1), UninitializedBlockCell(TestBlockType.BlockOne), "")(resultRunningGame))
    Console.err.println(resultMoveBlockR)
    resultMoveBlockR match {
      case Some(Left((moved, deleted, arrangedDeleted))) =>
        // 줄을 깨고 모은 결과 게임 판에 남는 것은
        // ....
        //  0.. 가 되어 남는 벽돌은 이와 같이 이동해야 하고
        val expectedMoved =
          List(
            List((Position(0, 1), InitializedBlockCell(0, TestBlockType.BlockOne)))
          )
        assertResult(expectedMoved)(moved)
        // 화면에서 지워지는 벽돌은 1, 2 과 예상낙하지점을 그리는 데 쓴 3 이 되고
        val expectedDeleted = Set(1, 2, 3)
        assertResult(expectedDeleted)(deleted.flatMap(_.map { case (_, InitializedBlockCell(uiId, _)) => uiId }).toSet)
        // 상대방에게 넘길 벽돌은 1 자리에 있던 벽돌인데 ui 식별번호가 할당되지 않은 벽돌로 바뀌므로
        // ....
        // ??.. 가 된다.
        val expectedArrangedDeleted =
          List(
            List((Position(0, 1), UninitializedBlockCell(TestBlockType.BlockOne)))
          )
        assertResult(expectedArrangedDeleted)(arrangedDeleted)
      case _ => fail()
    }
    assert(resultRunningGame.currentBlock.isEmpty)
    assert(resultRunningGame.currentTimerId.isEmpty)
  }
}
