/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import org.imcharsi.tetrispractice.common.data.fsm._
import BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import Command._
import GameState.NotYetStartedGame
import Player.{ LocalPlayer, Player }
import org.imcharsi.tetrispractice.common.fsm.Game.{ FSMInput, FSMState, LogS }
import org.imcharsi.tetrispractice.common.util.{ State, Valve }
import org.scalatest.{ BeforeAndAfterEach, FunSuite }
import rx.lang.scala.Subject

import scala.collection.mutable

/**
 * Created by i on 9/1/16.
 */
class TestStreamUsage extends FunSuite with BeforeAndAfterEach {
  var idCounter: Int = 0
  var timerIdCounter: Int = 0
  implicit val runningGameSMB = new State.StateMonadBehavior[Game.RunningGameSS[TestBlockType.BlockType]]

  def allocUiId(map: Map[Position, BlockCell[TestBlockType.BlockType]]): Map[Position, BlockCell[TestBlockType.BlockType]] = {
    map.foldLeft(map) {
      case (map, (position, blockCell)) =>
        blockCell match {
          case UninitializedBlockCell(cellType) =>
            val newId = idCounter
            idCounter = idCounter + 1
            map + ((position -> InitializedBlockCell[TestBlockType.BlockType](newId, cellType)))
          case cell @ InitializedBlockCell(_, _) => map
        }
    }
  }

  def allocUiId2(list: BlockCellList[TestBlockType.BlockType]): BlockCellList[TestBlockType.BlockType] = {
    list.map {
      case (position, blockCell) =>
        blockCell match {
          case UninitializedBlockCell(cellType) =>
            val newId = idCounter
            idCounter = idCounter + 1
            (position -> InitializedBlockCell[TestBlockType.BlockType](newId, cellType))
          case cell @ InitializedBlockCell(_, _) => (position, cell)
        }
    }
  }

  def allocTimerId(): Int = {
    val newTimerId = timerIdCounter
    timerIdCounter = timerIdCounter + 1
    newTimerId
  }

  def allocateBePushed(bePushed: BlockCellList[TestBlockType.BlockType]) =
    bePushed.map {
      case (position, UninitializedBlockCell(cellType)) =>
        val newId = idCounter
        idCounter = idCounter + 1
        (position, InitializedBlockCell[TestBlockType.BlockType](newId, cellType))
    }

  // 이와 같이 모든 구독을 일반화해놓고 검사를 하는 것이 좋겠다.
  def subscribeRealizedNextBlock(
    outputMap: mutable.Map[Position, BlockCell[TestBlockType.BlockType]],
    reverseOutputMap: mutable.Map[Int, Position],
    bePushedOutputMap: mutable.Map[Position, BlockCell[TestBlockType.BlockType]],
    bePushedReverseOutputMap: mutable.Map[Int, Position],
    subjectFeedback: Subject[FSMInput[TestBlockType.BlockType]],
    subjectRemote: Option[Subject[FSMInput[TestBlockType.BlockType]]] = None
  )(fsmOutput: (StreamOutput[TestBlockType.BlockType], LogS)): Unit = {
    fsmOutput match {
      case (RealizedNextBlock(player, Some(((currentBlock, expectedLanding), next)), moved, deleted, movedFromBePushed, realizeNextMove), _) =>
        val newCurrentBlock = currentBlock.copy(map = allocUiId(currentBlock.map))
        val newNext = currentBlock.copy(map = allocUiId(next.map))
        val newExpectedLanding = expectedLanding.copy(map = allocUiId(expectedLanding.map))
        val newTimerId = allocTimerId()
        subjectFeedback.onNext(
          Left(
            RealizeNextBlockFeedback[TestBlockType.BlockType](
              player,
              Some(
                Tuple4(
                  newTimerId,
                  newCurrentBlock,
                  newNext,
                  newExpectedLanding
                )
              )
            )
          )
        )
        moved.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap -= reverseOutputMap(uiId) += (position -> blockCell)
            reverseOutputMap += (uiId -> position)
        }
        deleted.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap -= reverseOutputMap(uiId)
            reverseOutputMap -= uiId
        }
        movedFromBePushed.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap += (position -> blockCell)
            reverseOutputMap += (uiId -> position)
            bePushedOutputMap -= bePushedReverseOutputMap(uiId)
            bePushedReverseOutputMap -= uiId
        }
        // 구상에서는, currentBlock 은 next 의 자리에 그려졌던 ui 객체를 게임 판에 옮기는 식으로 만든다.
        newCurrentBlock.map.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap += ((newCurrentBlock.position + position) -> blockCell)
            reverseOutputMap += (uiId -> (newCurrentBlock.position + position))
        }
        // 구상에서는, next 의 모양과 같은 expectedLanding 이 ui 식별번호 없이 주어지고
        // 예상낙하지점은 ui 식별번호를 붙여야 한다.
        newExpectedLanding.map.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap += ((newExpectedLanding.position + position) -> blockCell)
            reverseOutputMap += (uiId -> (newExpectedLanding.position + position))
        }
        Console.out.println("after RealizedNextBlock")
        Console.out.println(Printer.mapToString(GameMap(Position(2, 4), outputMap.toMap)))
        Console.out.println(Printer.mapToString(GameMap(Position(2, 4), bePushedOutputMap.toMap)))
        Console.out.println(s"timerId:${newTimerId}")
        subjectRemote.foreach(_.onNext(Right(realizeNextMove.copy(player = Player.convertToRemotePlayer(realizeNextMove.player)))))
    }
  }

  def subscribeStarted(
    subject: Subject[FSMInput[TestBlockType.BlockType]],
    subjectFeedback: Subject[FSMInput[TestBlockType.BlockType]],
    subjectRemote: Option[Subject[FSMInput[TestBlockType.BlockType]]] = None
  )(fsmOutput: (StreamOutput[TestBlockType.BlockType], LogS)): Unit = {
    fsmOutput match {
      case (Started(player, true, start), _) =>
        subjectFeedback.onNext(Left(StartFeedback(player)))
        Console.out.println(s"${player} game start.")
        subjectRemote.foreach(_.onNext(Right(start.copy(player = Player.convertToRemotePlayer(start.player)))))

      // 구상에 따르면, 이 단계에서 시작의 응답을 한 이후 이어서 새 벽돌 만들기 입력을 해줘야 한다.
      // 여기서 입력을 해주지 않는 이유는, 검사를 편하게 하기 위해서이다. 입력에 대한 출력을 기다려야 하는데,
      // 입력을 다른 데서 하면 뭘 언제 입력했는지 알 수 없어서,
      // 무슨 출력을 위해서 언제까지 기다려야 하는지 알 수 없는 문제가 생긴다.
      // subject.onNext(Right(RealizeNextBlock(player)))
    }
  }

  def subscribePushed(
    outputMap: mutable.Map[Position, BlockCell[TestBlockType.BlockType]],
    reverseOutputMap: mutable.Map[Int, Position],
    bePushedOutputMap: mutable.Map[Position, BlockCell[TestBlockType.BlockType]],
    bePushedReverseOutputMap: mutable.Map[Int, Position],
    subjectFeedback: Subject[FSMInput[TestBlockType.BlockType]],
    subjectRemote: Option[Subject[FSMInput[TestBlockType.BlockType]]] = None
  )(fsmOutput: (StreamOutput[TestBlockType.BlockType], LogS)): Unit = {
    fsmOutput match {
      case (Pushed(player, beAllocated, moved, deleted, push), _) =>
        val newBeAllocated = allocUiId2(beAllocated)
        moved.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            bePushedOutputMap -= reverseOutputMap(uiId) += (position -> blockCell)
            bePushedReverseOutputMap += (uiId -> position)
        }
        deleted.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            bePushedOutputMap -= reverseOutputMap(uiId)
            bePushedReverseOutputMap -= uiId
        }
        newBeAllocated.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            bePushedOutputMap += (position -> blockCell)
            bePushedReverseOutputMap += (uiId -> position)
        }
        Console.out.println("after Pushed")
        Console.out.println(Printer.mapToString(GameMap(Position(2, 4), bePushedOutputMap.toMap)))
        subjectFeedback.onNext(Left(PushFeedback(player, Some(newBeAllocated))))
        subjectRemote.foreach(_.onNext(Right(push.copy(player = Player.convertToRemotePlayer(push.player)))))
    }
  }

  def subscribeBlockTransformed(currentPlayer: Player)(
    outputMap: mutable.Map[Position, BlockCell[TestBlockType.BlockType]],
    reverseOutputMap: mutable.Map[Int, Position],
    subjectFeedback: Subject[FSMInput[TestBlockType.BlockType]],
    subjectRemote: Option[Subject[FSMInput[TestBlockType.BlockType]]] = None,
    subjectEnemy: Option[(Player, Subject[FSMInput[TestBlockType.BlockType]])] = None
  )(fsmOutput: (StreamOutput[TestBlockType.BlockType], LogS)): Unit = {
    Console.out.println("after BlockTransformed")
    fsmOutput match {
      case (BlockTransformed(player, Some(Right(Some((currentBlock, expectedLanding)))), blockTransform @ BlockTransform(_, blockTransformType)), _) =>
        expectedLanding.map.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap -= reverseOutputMap(uiId) += (expectedLanding.position + position -> blockCell)
            reverseOutputMap += (uiId -> (expectedLanding.position + position))
        }
        currentBlock.map.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap -= reverseOutputMap(uiId) += (currentBlock.position + position -> blockCell)
            reverseOutputMap += (uiId -> (currentBlock.position + position))
        }
        Console.out.println(Printer.mapToString(GameMap(Position(2, 4), outputMap.toMap)))
        blockTransformType match {
          case BlockTransformType.Down | BlockTransformType.Bottom | BlockTransformType.TimeElapsed(_) =>
            val newTimerId = allocTimerId()
            subjectFeedback.onNext(
              Left(
                BlockTransformFeedback[TestBlockType.BlockType](
                  player, Some(newTimerId)
                )
              )
            )
            Console.out.println(s"updated timerId:${newTimerId}")
          case _ =>
            subjectFeedback.onNext(
              Left(
                BlockTransformFeedback[TestBlockType.BlockType](
                  player, None
                )
              )
            )
            Console.out.println("not updated timerId.")
        }
        subjectRemote.foreach(_.onNext(Right(blockTransform.copy(player = Player.convertToRemotePlayer(blockTransform.player)))))

      case (BlockTransformed(player, Some(Left((moved, deleted, transferred))), blockTransform @ BlockTransform(_, BlockTransformType.Down | BlockTransformType.TimeElapsed(_) | BlockTransformType.Bottom)), _) =>
        moved.flatten.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap -= reverseOutputMap(uiId) += (position -> blockCell)
            reverseOutputMap += (uiId -> position)
        }
        deleted.flatten.foreach {
          case (position, blockCell @ InitializedBlockCell(uiId, _)) =>
            outputMap -= reverseOutputMap(uiId)
            reverseOutputMap -= uiId
        }
        Console.out.println(Printer.mapToString(GameMap(Position(2, 4), outputMap.toMap)))
        Console.out.println(s"reached at bottom. transferred:${transferred}")
        // 내려오는 벽돌이 바닥에 닿았다. 새 시간초과설정을 할 필요가 없다.
        subjectFeedback.onNext(
          Left(
            BlockTransformFeedback[TestBlockType.BlockType](
              player, None
            )
          )
        )

        // 이 기능은 LocalPlayer 에 의해서만 실행되어야 한다.
        // RemotePlayer 에서는 subjectEnemy==None 이어야 한다.
        subjectRemote.foreach(_.onNext(Right(blockTransform.copy(player = Player.convertToRemotePlayer(blockTransform.player)))))
        if (transferred.size > 0) {
          subjectEnemy.foreach(x => x._2.onNext(Right(Push(x._1, transferred.size, transferred.flatten, false))))
        }
      // 구상에서는, 이 단계에서 응답의 입력에 이어서 다시 한번 아래로 이동 입력을 해줘야 한다.
      // 다시 한번 아래로 이동을 시도하면 바닥에 닿았음을 판단하게 되고 이어서 새 벽돌 만들기를 또 입력해 준다.
      case (BlockTransformed(player, Some(Right(None)), blockTransform @ BlockTransform(_, BlockTransformType.Left | BlockTransformType.Right | BlockTransformType.Rotate(_))), _) =>
        // 벽돌을 움직일 수 없다. 아직 바닥에 닿은 것은 아니다.
        subjectFeedback.onNext(
          Left(
            BlockTransformFeedback[TestBlockType.BlockType](
              player, None
            )
          )
        )
        subjectRemote.foreach(_.onNext(Right(blockTransform.copy(player = Player.convertToRemotePlayer(blockTransform.player)))))
        Console.out.println(s"cannot move.")
    }
  }

  val cleared =
    GameMap[TestBlockType.BlockType](
      Position(2, 4),
      Util.prepareClearedMap[TestBlockType.BlockType](
        UninitializedBlockCell(TestBlockType.BlockOne),
        Position(2, 4)
      )(Map())
    )
  val functionHolder = Game.FunctionHolder(TestBlockType.random, TestBlockType.generateBlockF, TestBlockType.rotateF)
  val scanF = Game.scanF(cleared, functionHolder, Position(0, 0), runningGameSMB) _

  test("single player scenario") {
    val localPlayer1: Player = LocalPlayer(1, "player1")
    val subjectPlayer1 = Subject[FSMInput[TestBlockType.BlockType]]
    val subjectPlayer1Feedback = Subject[FSMInput[TestBlockType.BlockType]]
    val subscribeQueue = mutable.Queue[(StreamOutput[TestBlockType.BlockType], LogS)]()
    val outputMap = mutable.Map[Position, BlockCell[TestBlockType.BlockType]]()
    val reverseOutputMap = mutable.Map[Int, Position]()
    val bePushedOutputMap = mutable.Map[Position, BlockCell[TestBlockType.BlockType]]()
    val bePushedReverseOutputMap = mutable.Map[Int, Position]()

    Valve.valve[FSMInput[TestBlockType.BlockType]](_.isLeft)(subjectPlayer1.merge(subjectPlayer1Feedback))
      .filter(Game.filterF(localPlayer1))
      .scan[FSMState[TestBlockType.BlockType]]((Right(NotYetStartedGame(localPlayer1)), State(()), None))(scanF)
      .flatMap(Game.mapF)
      .filter(x => x._1 != NothingToDo[TestBlockType.BlockType]())
      .subscribe(subscribeQueue.enqueue(_))

    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Start(localPlayer1, 0))
    Console.out.println(s"input:${fsmInput1}")
    subjectPlayer1.onNext(fsmInput1)
    subscribeStarted(subjectPlayer1, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput2: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(localPlayer1))
    Console.out.println(s"input:${fsmInput2}")
    subjectPlayer1.onNext(fsmInput2)
    subscribeRealizedNextBlock(
      outputMap,
      reverseOutputMap,
      bePushedOutputMap,
      bePushedReverseOutputMap,
      subjectPlayer1Feedback
    )(subscribeQueue.dequeue())
    val fsmInput3: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Left))
    Console.out.println(s"input:${fsmInput3}")
    subjectPlayer1.onNext(fsmInput3)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput4: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Left))
    Console.out.println(s"input:${fsmInput4}")
    subjectPlayer1.onNext(fsmInput4)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput5: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Right))
    Console.out.println(s"input:${fsmInput5}")
    subjectPlayer1.onNext(fsmInput5)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput6: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Down))
    Console.out.println(s"input:${fsmInput6}")
    subjectPlayer1.onNext(fsmInput6)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput7: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Rotate(BlockTransformType.LeftToDowToRight)))
    Console.out.println(s"input:${fsmInput7}")
    subjectPlayer1.onNext(fsmInput7)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput8: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Bottom))
    Console.out.println(s"input:${fsmInput8}")
    subjectPlayer1.onNext(fsmInput8)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput9: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Down))
    Console.out.println(s"input:${fsmInput9}")
    subjectPlayer1.onNext(fsmInput9)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())

    val fsmInput10: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(localPlayer1))
    Console.out.println(s"input:${fsmInput10}")
    subjectPlayer1.onNext(fsmInput10)
    subscribeRealizedNextBlock(
      outputMap,
      reverseOutputMap,
      bePushedOutputMap,
      bePushedReverseOutputMap,
      subjectPlayer1Feedback
    )(subscribeQueue.dequeue())

    val fsmInput11: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Bottom))
    Console.out.println(s"input:${fsmInput11}")
    subjectPlayer1.onNext(fsmInput11)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
    val fsmInput12: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Down))
    Console.out.println(s"input:${fsmInput12}")
    subjectPlayer1.onNext(fsmInput12)
    subscribeBlockTransformed(localPlayer1)(outputMap, reverseOutputMap, subjectPlayer1Feedback)(subscribeQueue.dequeue())
  }

  test("dual player scenario") {
    class Client(val me: Player, val enemy: Player) {
      val subjectLocalMe = Subject[FSMInput[TestBlockType.BlockType]]
      val subjectRemoteEnemy = Subject[FSMInput[TestBlockType.BlockType]]
      val subjectLocalMeFeedback = Subject[FSMInput[TestBlockType.BlockType]]
      val subjectRemoteEnemyFeedback = Subject[FSMInput[TestBlockType.BlockType]]
      val outputMapLocalMe = mutable.Map[Position, BlockCell[TestBlockType.BlockType]]()
      val reverseOutputMapLocalMe = mutable.Map[Int, Position]()
      val outputMapRemoteEnemy = mutable.Map[Position, BlockCell[TestBlockType.BlockType]]()
      val reverseOutputMapRemoteEnemy = mutable.Map[Int, Position]()
      val bePushedOutputMapLocalMe = mutable.Map[Position, BlockCell[TestBlockType.BlockType]]()
      val bePushedReverseOutputMapLocalMe = mutable.Map[Int, Position]()
      val bePushedOutputMapRemoteEnemy = mutable.Map[Position, BlockCell[TestBlockType.BlockType]]()
      val bePushedReverseOutputMapRemoteEnemy = mutable.Map[Int, Position]()
      val subscribeQueueLocalMe = mutable.Queue[(StreamOutput[TestBlockType.BlockType], LogS)]()
      val subscribeQueueRemoteEnemy = mutable.Queue[(StreamOutput[TestBlockType.BlockType], LogS)]()

      Valve.valve[FSMInput[TestBlockType.BlockType]](_.isLeft)(subjectLocalMe.merge(subjectLocalMeFeedback))
        .filter(Game.filterF(me))
        .scan[FSMState[TestBlockType.BlockType]]((Right(NotYetStartedGame(me)), State(()), None))(scanF)
        .flatMap(Game.mapF)
        .filter(x => x._1 != NothingToDo[TestBlockType.BlockType]())
        .subscribe(subscribeQueueLocalMe.enqueue(_))
      Valve.valve[FSMInput[TestBlockType.BlockType]](_.isLeft)(subjectRemoteEnemy.merge(subjectRemoteEnemyFeedback))
        .filter(Game.filterF(enemy))
        .scan[FSMState[TestBlockType.BlockType]]((Right(NotYetStartedGame(enemy)), State(()), None))(scanF)
        .flatMap(Game.mapF)
        .filter(x => x._1 != NothingToDo[TestBlockType.BlockType]())
        .subscribe(subscribeQueueRemoteEnemy.enqueue(_))
    }
    val localPlayer1 = LocalPlayer(1, "player1")
    val localPlayer2 = LocalPlayer(2, "player2")
    val remotePlayer1 = Player.convertToRemotePlayer(localPlayer1)
    val remotePlayer2 = Player.convertToRemotePlayer(localPlayer2)

    val client1 = new Client(localPlayer1, remotePlayer2)
    val client2 = new Client(localPlayer2, remotePlayer1)

    val fsmInput1_1: FSMInput[TestBlockType.BlockType] = Right(Start(localPlayer1, 0))
    client1.subjectLocalMe.onNext(fsmInput1_1)
    Console.out.println(s"client1 ${localPlayer1}")
    // 햇갈리는 구절이다.
    // 세번째 인자인 Some(client2.subjectRemoteEnemy) 는, Broadcast 에 의해 입력을 받게 되는 Stream 을 말한다.
    // 해당 사용자에 대응하는 여러 RemotePlayer 가 진행하는 게임의 Stream 이다.
    // 이 예에서는 두 명의 사용자가 등장하므로, localPlayer1 에 대응하는 RemotePlayer 는 remotePlayer1 하나 뿐이다.
    // client1 에는 하나의 LocalPlayer 인 localPlayer1 이 있고 여럿의 RemotePlayer 가 있을 수 있는데 일단 remotePlayer2 가 있다.
    // client2 에는 하나의 LocalPlayer 인 localPlayer2 이 있고 RemotePlayer 인 remotePlayer1 이 있다.
    // RemotePlayer 가 받는 입력은 전부 LocalPlayer 가 만든 출력을 바탕으로 Server 를 거쳐서 온다.
    subscribeStarted(client1.subjectLocalMe, client1.subjectLocalMeFeedback, Some(client2.subjectRemoteEnemy))(client1.subscribeQueueLocalMe.dequeue())
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeStarted(client2.subjectRemoteEnemy, client2.subjectRemoteEnemyFeedback, None)(client2.subscribeQueueRemoteEnemy.dequeue())
    val fsmInput1_2: FSMInput[TestBlockType.BlockType] = Right(Start(localPlayer2, 0))
    client2.subjectLocalMe.onNext(fsmInput1_2)
    Console.out.println(s"client2 ${localPlayer2}")
    subscribeStarted(client2.subjectLocalMe, client2.subjectLocalMeFeedback, Some(client1.subjectRemoteEnemy))(client2.subscribeQueueLocalMe.dequeue())
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribeStarted(client1.subjectRemoteEnemy, client1.subjectRemoteEnemyFeedback, None)(client1.subscribeQueueRemoteEnemy.dequeue())

    // impo localMe 는 remoteEnemy 와 같다.
    def subscribeRealizedNextBlockAsLocalPlayer(localPlayerClient: Client, remotePlayerClient: Client) =
      subscribeRealizedNextBlock(
        localPlayerClient.outputMapLocalMe,
        localPlayerClient.reverseOutputMapLocalMe,
        localPlayerClient.bePushedOutputMapLocalMe,
        localPlayerClient.bePushedReverseOutputMapLocalMe,
        localPlayerClient.subjectLocalMeFeedback,
        Some(remotePlayerClient.subjectRemoteEnemy)
      )(localPlayerClient.subscribeQueueLocalMe.dequeue())

    def subscribeRealizedNextBlockAsRemotePlayer(remotePlayerClient: Client) =
      subscribeRealizedNextBlock(
        remotePlayerClient.outputMapRemoteEnemy,
        remotePlayerClient.reverseOutputMapRemoteEnemy,
        remotePlayerClient.bePushedOutputMapRemoteEnemy,
        remotePlayerClient.bePushedReverseOutputMapRemoteEnemy,
        remotePlayerClient.subjectRemoteEnemyFeedback,
        None
      )(remotePlayerClient.subscribeQueueRemoteEnemy.dequeue())

    def subscribeRealizedNextBlockC1L1() =
      subscribeRealizedNextBlockAsLocalPlayer(client1, client2)

    def subscribeRealizedNextBlockC2R1() =
      subscribeRealizedNextBlockAsRemotePlayer(client2)

    def subscribeRealizedNextBlockC2L2() =
      subscribeRealizedNextBlockAsLocalPlayer(client2, client1)

    def subscribeRealizedNextBlockC1R2() =
      subscribeRealizedNextBlockAsRemotePlayer(client1)

    val fsmInput2_1: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(localPlayer1))
    client1.subjectLocalMe.onNext(fsmInput2_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeRealizedNextBlockC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeRealizedNextBlockC2R1()

    val fsmInput2_2: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(localPlayer2))
    client2.subjectLocalMe.onNext(fsmInput2_2)
    Console.out.println(s"client2 ${localPlayer2}")
    subscribeRealizedNextBlockC2L2()
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribeRealizedNextBlockC1R2()

    def subscribeBlockTransformedAsLocalPlayer(localPlayerClient: Client, remotePlayerClient: Client) =
      subscribeBlockTransformed(localPlayerClient.me)(
        localPlayerClient.outputMapLocalMe,
        localPlayerClient.reverseOutputMapLocalMe,
        localPlayerClient.subjectLocalMeFeedback,
        Some(remotePlayerClient.subjectRemoteEnemy),
        // impo 중요한 내용이다. 이 부분이 이 연습의 핵심 구상 중 하나이다.
        // 하나의 client 에 하나의 LocalPlayer 가 있고 여럿의 RemotePlayer 가 있을 수 있다. 이 설명은 위에도 있고
        // 이 설명에서는 player1, player2 둘만 있다고 가정한다.
        // 여러 줄을 깬 client1 LocalPlayer player1 의 BlockTransformed 입력을 구독 단계에서 받아서
        // client1 에서 Push 입력을 만든 후 Server 로 전달하고,
        // 이를 받은 Server 가 여럿의 LocalPlayer 중 하나를 선택하여
        // 선택된 LocalPlayer 의 Push 입력인 것처럼 입력을 전달하는 식으로 한다.
        // client1 LocalPlayer player1 이 BlockTransformed 구독 단계에서 만든 Push 입력은
        // Server 를 거친 후, Broadcast 되지 않고 client2 LocalPlayer player2 에게만 전달된다.
        // client2 LocalPlayer player2 가 Push 입력을 받아서 처리한 후
        // Pushed 구독 단계에서 Server 로 RemotePlayer Push 입력을 전달하면
        // 이 때 Server 가 이렇게 받은 Push 입력을 Broadcast 하게 되고
        // client1 RemotePlayer player2 가 이 Push 를 받으면
        // client1 의 화면에, player2 가 깨진 줄을 넘겨 받았다는 화면표시가 나타나게 된다.
        // 이와 같이 복잡한 단계를 거치는 이유에 대한 설명은, object Command.class Push 에 썼다.
        Some((remotePlayerClient.me, remotePlayerClient.subjectLocalMe))
      )(localPlayerClient.subscribeQueueLocalMe.dequeue())

    def subscribeBlockTransformedAsRemotePlayer(remotePlayerClient: Client) =
      subscribeBlockTransformed(remotePlayerClient.enemy)(
        remotePlayerClient.outputMapRemoteEnemy,
        remotePlayerClient.reverseOutputMapRemoteEnemy,
        remotePlayerClient.subjectRemoteEnemyFeedback,
        None,
        None
      )(remotePlayerClient.subscribeQueueRemoteEnemy.dequeue())

    def subscribeBlockTransformedC1L1() =
      subscribeBlockTransformedAsLocalPlayer(client1, client2)

    def subscribeBlockTransformedC2R1() =
      subscribeBlockTransformedAsRemotePlayer(client2)

    val fsmInput3_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Bottom))
    client1.subjectLocalMe.onNext(fsmInput3_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeBlockTransformedC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeBlockTransformedC2R1()

    def subscribeBlockTransformedC2L2() =
      subscribeBlockTransformedAsLocalPlayer(client2, client1)

    def subscribeBlockTransformedC1R2() =
      subscribeBlockTransformedAsRemotePlayer(client1)

    val fsmInput3_2: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer2, BlockTransformType.Left))
    client2.subjectLocalMe.onNext(fsmInput3_2)
    Console.out.println(s"client2 ${localPlayer2}")
    subscribeBlockTransformedC2L2()
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribeBlockTransformedC1R2()

    val fsmInput4_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Down))
    client1.subjectLocalMe.onNext(fsmInput4_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeBlockTransformedC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeBlockTransformedC2R1()

    Console.out.println(s"realize next block.")
    val fsmInput5_1: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(localPlayer1))
    client1.subjectLocalMe.onNext(fsmInput5_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeRealizedNextBlockC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeRealizedNextBlockC2R1()

    val fsmInput6_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Left))
    client1.subjectLocalMe.onNext(fsmInput6_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeBlockTransformedC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeBlockTransformedC2R1()

    val fsmInput7_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Bottom))
    client1.subjectLocalMe.onNext(fsmInput7_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeBlockTransformedC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeBlockTransformedC2R1()

    val fsmInput8_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer1, BlockTransformType.Down))
    client1.subjectLocalMe.onNext(fsmInput8_1)
    Console.out.println(s"client1 ${localPlayer1}")
    subscribeBlockTransformedC1L1()
    Console.out.println(s"client2 ${remotePlayer1}")
    subscribeBlockTransformedC2R1()

    def subscribePushedC2L2() = subscribePushed(
      client2.outputMapLocalMe,
      client2.reverseOutputMapLocalMe,
      client2.bePushedOutputMapLocalMe,
      client2.bePushedReverseOutputMapLocalMe,
      client2.subjectLocalMeFeedback,
      Some(client1.subjectRemoteEnemy)
    )(client2.subscribeQueueLocalMe.dequeue())
    def subscribePushedC1R2() =
      subscribePushed(
        client1.outputMapRemoteEnemy,
        client1.reverseOutputMapRemoteEnemy,
        client1.bePushedOutputMapRemoteEnemy,
        client1.bePushedReverseOutputMapRemoteEnemy,
        client1.subjectRemoteEnemyFeedback,
        None
      )(client1.subscribeQueueRemoteEnemy.dequeue())

    Console.out.println(s"client2 ${localPlayer2}")
    subscribePushedC2L2()
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribePushedC1R2()

    val fsmInput9_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer2, BlockTransformType.Bottom))
    client2.subjectLocalMe.onNext(fsmInput9_1)
    Console.out.println(s"client2 ${localPlayer2}")
    subscribeBlockTransformedC2L2()
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribeBlockTransformedC1R2()

    val fsmInput10_1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(localPlayer2, BlockTransformType.Down))
    client2.subjectLocalMe.onNext(fsmInput10_1)
    Console.out.println(s"client2 ${localPlayer2}")
    subscribeBlockTransformedC2L2()
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribeBlockTransformedC1R2()

    val fsmInput11_2: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(localPlayer2))
    client2.subjectLocalMe.onNext(fsmInput11_2)
    Console.out.println(s"client2 ${localPlayer2}")
    subscribeRealizedNextBlockC2L2()
    Console.out.println(s"client1 ${remotePlayer2}")
    subscribeRealizedNextBlockC1R2()

    // 여기까지 하면 대충 검사는 됐다. 이 검사에서 한 내용은, 한 사용자가 깬 줄이 다른 사용자에게 넘어가는지 확인하는 것이다.
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    idCounter = 0
    timerIdCounter = 0
  }
}
