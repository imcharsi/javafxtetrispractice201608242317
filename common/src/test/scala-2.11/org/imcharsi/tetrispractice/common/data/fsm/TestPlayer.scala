/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

import org.imcharsi.tetrispractice.common.data.fsm.Player.{ LocalPlayer, RemotePlayer }
import org.scalatest.FunSuite

/**
 * Created by i on 9/14/16.
 */
class TestPlayer extends FunSuite {
  test("convertToRemotePlayer") {
    val remotePlayer = RemotePlayer(0, "hi")
    val localPlayer = LocalPlayer(0, "hi")
    assertResult(remotePlayer)(Player.convertToRemotePlayer(localPlayer))
  }

  test("convertToLocalPlayer") {
    val remotePlayer = RemotePlayer(0, "hi")
    val localPlayer = LocalPlayer(0, "hi")
    assertResult(localPlayer)(Player.convertToLocalPlayer(remotePlayer))
  }

  test("isLocalPlayer") {
    val remotePlayer = RemotePlayer(0, "hi")
    val localPlayer = LocalPlayer(0, "hi")
    assertResult(true)(Player.isLocalPlayer(localPlayer))
    assertResult(false)(Player.isLocalPlayer(remotePlayer))
  }
}
