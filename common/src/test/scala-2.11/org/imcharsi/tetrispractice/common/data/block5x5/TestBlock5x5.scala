/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.block5x5

import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5.{ Block5x5JsonFormat, BlockType }
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ UninitializedBlockCell, BlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.{ BlockTransformType, Position }
import org.imcharsi.tetrispractice.common.fsm.Printer
import org.scalatest.FunSuite
import spray.json._

/**
 * Created by i on 9/9/16.
 */
class TestBlock5x5 extends FunSuite {
  test("positionMap") {
    Console.err.println(Block5x5.positionMap)
    def loop(start: Position, curr: Position, selF: (Position, Position, Int) => Position)(x: List[(Position, (Position, Position, Int))]): List[(Position, (Position, Position, Int))] = {
      x match {
        case Nil =>
          val xx = Block5x5.positionMap(curr)
          loop(start, xx._2, selF)(List((curr, xx)))
        case _ =>
          if (start == curr) x
          else {
            val xx = Block5x5.positionMap(curr)
            loop(start, xx._2, selF)((curr, xx) :: x)
          }
      }
    }
    def selF1(prev: Position, next: Position, count: Int): Position = next
    def selF2(prev: Position, next: Position, count: Int): Position = prev
    assertResult(16)(loop(Position(0, 0), Position(0, 0), selF1)(List()).toSet.size)
    assertResult(16)(loop(Position(0, 0), Position(0, 0), selF2)(List()).toSet.size)
    assertResult(8)(loop(Position(1, 1), Position(1, 1), selF1)(List()).toSet.size)
    assertResult(8)(loop(Position(1, 1), Position(1, 1), selF2)(List()).toSet.size)
    assertResult(1)(loop(Position(2, 2), Position(2, 2), selF1)(List()).toSet.size)
    assertResult(1)(loop(Position(2, 2), Position(2, 2), selF2)(List()).toSet.size)
  }

  test("rotateF") {
    val blockMap1 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(0, 0) -> UninitializedBlockCell(BlockType.BlockOne)),
        (Position(1, 1) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    val expectedBlockMap1LTDTR =
      Some(
        Map[Position, BlockCell[BlockType.BlockType]](
          (Position(0, 4) -> UninitializedBlockCell(BlockType.BlockOne)),
          (Position(1, 3) -> UninitializedBlockCell(BlockType.BlockOne))
        )
      )
    val expectedBlockMap1RTDTL =
      Some(
        Map[Position, BlockCell[BlockType.BlockType]](
          (Position(4, 0) -> UninitializedBlockCell(BlockType.BlockOne)),
          (Position(3, 1) -> UninitializedBlockCell(BlockType.BlockOne))
        )
      )
    val blockMap2 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(2, 0) -> UninitializedBlockCell(BlockType.BlockOne)),
        (Position(2, 1) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    val expectedBlockMap2LTDTR =
      Some(
        Map[Position, BlockCell[BlockType.BlockType]](
          (Position(0, 2) -> UninitializedBlockCell(BlockType.BlockOne)),
          (Position(1, 2) -> UninitializedBlockCell(BlockType.BlockOne))
        )
      )
    val gameMap1 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(0, 4) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    val gameMap2 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(4, 0) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    val gameMap3 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(1, 3) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    val gameMap4 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(3, 1) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    val gameMap5 =
      Map[Position, BlockCell[BlockType.BlockType]](
        (Position(0, 0) -> UninitializedBlockCell(BlockType.BlockOne))
      )
    assertResult(expectedBlockMap1LTDTR)(Block5x5.rotate(Map(), BlockTransformType.LeftToDowToRight, Position(0, 0), blockMap1))
    assertResult(expectedBlockMap1RTDTL)(Block5x5.rotate(Map(), BlockTransformType.RightToDownToLeft, Position(0, 0), blockMap1))
    assertResult(None)(Block5x5.rotate(gameMap1, BlockTransformType.LeftToDowToRight, Position(0, 0), blockMap1))
    assertResult(None)(Block5x5.rotate(gameMap2, BlockTransformType.RightToDownToLeft, Position(0, 0), blockMap1))
    assertResult(None)(Block5x5.rotate(gameMap3, BlockTransformType.LeftToDowToRight, Position(0, 0), blockMap1))
    assertResult(None)(Block5x5.rotate(gameMap4, BlockTransformType.RightToDownToLeft, Position(0, 0), blockMap1))
    // 내려오는 벽돌의 기준 위치를 계산에 반영했는지에 대한 검사이다.
    // 예를 들어
    // ....**....
    // ....**....
    // ..........
    // ..........
    // .......... 를 왼쪽으로 회전하면
    // ..........
    // ..........
    // ****......
    // ..........
    // .......... 가 되는데, 만약 배경이
    // ??........
    // ..........
    // ..........
    // ..........
    // .......... 와 같다면 회전에 실패해야 하지만 기준위치가 (1,0) 이라면 결국
    // ??....**....
    // ......**....
    // ............
    // ............
    // ............ 와 같이 되어서 회전에 성공하게 된다.

    assertResult(expectedBlockMap2LTDTR)(Block5x5.rotate(gameMap5, BlockTransformType.LeftToDowToRight, Position(1, 0), blockMap2))
    assertResult(None)(Block5x5.rotate(gameMap5, BlockTransformType.LeftToDowToRight, Position(0, 0), blockMap2))
  }

  test("generateBlock") {
    // 이 기능은 따로 검사를 할 만한 것이 없다. 검사의 대상이 실질에 있어서 그림이기 때문이다.
    assertResult(Block5x5.blockMap(0))(Block5x5.generateBlock(0))
    0.to(6).foreach(i => Console.out.println(Printer.blockToString(Position(5, 5))(Block5x5.generateBlock(i))))
  }

  def compare[A](expected: A)(implicit format: JsonFormat[A]): Unit = {
    val json = expected.toJson.prettyPrint
    Console.err.println(s"${expected}\n${json}\n")
    assertResult(expected)(json.parseJson.convertTo[A])
  }

  test("json") {
    implicit val block5x5JsonFormat = Block5x5JsonFormat
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockOne)
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockTwo)
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockThree)
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockFour)
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockFive)
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockSix)
    compare[Block5x5.BlockType.BlockType](Block5x5.BlockType.BlockSeven)
  }
}
