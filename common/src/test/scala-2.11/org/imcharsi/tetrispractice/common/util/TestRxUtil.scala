/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

import javafx.beans.property.SimpleStringProperty

import org.scalatest.FunSuite
import rx.lang.scala.{ Subject, JavaConversions }
import rx.lang.scala.observers.TestSubscriber
import rx.observables.JavaFxObservable
import scala.reflect.runtime.universe._
import rx.observers

/**
 * Created by i on 9/14/16.
 */
class TestRxUtil extends FunSuite {
  val mirror = scala.reflect.runtime.currentMirror

  // 이와 같이 검사를 바꾸는 시점에서 RxJava 1.1.9 로 바뀌면서 assertValuesAndClear 라는 기능은 만들어졌지만
  // RxScala 에는 여전히 기능이 없다. 그래서 일단 reflection 으로 한다.
  def reflect[A](testSubscriber: TestSubscriber[A]): observers.TestSubscriber[A] = {
    val termSymbol =
      mirror.classSymbol(classOf[TestSubscriber[(Int, Int)]]).toType.members.filter {
        case m: TermSymbol => m.name.toString == "jTestSubscriber"
        case _ => false
      }.head.asInstanceOf[TermSymbol]
    mirror.reflect(testSubscriber)
      .reflectField(termSymbol).get
      .asInstanceOf[observers.TestSubscriber[A]]
  }

  test("convertJavaFXProperty") {
    val stringProperty = new SimpleStringProperty("1")
    val testProperty = RxUtil.convertJavaFXProperty((s: String) => s.toInt)(stringProperty)
    val testSubscriber = TestSubscriber[Option[Int]]
    JavaConversions.toScalaObservable(JavaFxObservable.fromObservableValue(testProperty)).subscribe(testSubscriber)
    val jTestSubscriber = reflect(testSubscriber)
    jTestSubscriber.assertValuesAndClear(Some(1))
    stringProperty.set("hi")
    jTestSubscriber.assertValuesAndClear(None)
    stringProperty.set("2")
    jTestSubscriber.assertValuesAndClear(Some(2))
  }

  test("combine") {
    val subjectA = Subject[Int]
    val subjectB = Subject[Int]
    val testSubscriber = TestSubscriber[(Int, Option[Int])]
    val jTestSubscriber = reflect(testSubscriber)
    RxUtil.combine(subjectA, subjectB).subscribe(testSubscriber)
    jTestSubscriber.assertNoValues()
    subjectB.onNext(1)
    jTestSubscriber.assertNoValues()
    subjectA.onNext(1)
    jTestSubscriber.assertValuesAndClear((1, Some(1)))
    subjectB.onNext(2)
    jTestSubscriber.assertNoValues()
    subjectA.onNext(2)
    jTestSubscriber.assertValuesAndClear((2, Some(2)))
  }
}
