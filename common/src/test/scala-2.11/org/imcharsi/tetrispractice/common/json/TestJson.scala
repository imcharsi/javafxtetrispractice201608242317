/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json

import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ CreatedRoom, GotRoomList, WaitRoomResponse }
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.{ BlockTransformType, LeftToDowToRight, RightToDownToLeft, RotateDirection }
import org.imcharsi.tetrispractice.common.data.fsm.Command._
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ LocalPlayer, Player, RemotePlayer }
import org.imcharsi.tetrispractice.common.data.fsm.{ Block, BlockTransformType, Position }
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomServerCommandJsonFormat, GameRoomClientCommandJsonFormat, WaitRoomResponseJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm._
import org.scalatest.FunSuite
import spray.json._

/**
 * Created by i on 9/8/16.
 */
class TestJson extends FunSuite {
  def compare[A](expected: A)(implicit format: JsonFormat[A]): Unit = {
    val json = expected.toJson.prettyPrint
    Console.err.println(s"${expected}\n${json}\n")
    assertResult(expected)(json.parseJson.convertTo[A])
  }

  test("Position") {
    implicit val positionJsonFormat = PositionJsonFormat
    compare(Position(1, 2))
  }

  test("BlockCell") {
    implicit val blockCellJsonFormat =
      new BlockCellJsonFormat[TestBlockType.BlockType]()(TestBlockType.TestBlockTypeJsonFormat)
    compare[BlockCell[TestBlockType.BlockType]](UninitializedBlockCell(TestBlockType.BlockOne))
  }

  test("Block") {
    implicit val blockCellJsonFormat =
      new BlockCellJsonFormat[TestBlockType.BlockType]()(TestBlockType.TestBlockTypeJsonFormat)
    implicit val blockJsonFormat = new BlockJsonFormat[TestBlockType.BlockType]()(blockCellJsonFormat)
    compare[Block[TestBlockType.BlockType]](
      Block(
        Position(1, 2),
        Map((Position(2, 3) -> InitializedBlockCell(4, TestBlockType.BlockTwo)))
      )
    )
  }

  test("Player") {
    implicit val playerJsonFormat = PlayerJsonFormat
    compare[Player](LocalPlayer(1, "hi"))
    compare[Player](RemotePlayer(1, "hi"))
  }

  test("RotateDirection") {
    implicit val rotateDirectionJsonFormat = RotateDirectionJsonFormat
    compare[RotateDirection](LeftToDowToRight)
    compare[RotateDirection](RightToDownToLeft)
  }

  test("BlockTransformType") {
    implicit val blockTransformTypeJsonFormat = BlockTransformTypeJsonFormat
    compare[BlockTransformType](BlockTransformType.Left)
    compare[BlockTransformType](BlockTransformType.Right)
    compare[BlockTransformType](BlockTransformType.Down)
    compare[BlockTransformType](BlockTransformType.Bottom)
    compare[BlockTransformType](BlockTransformType.Rotate(LeftToDowToRight))
    compare[BlockTransformType](BlockTransformType.TimeElapsed(1))
  }

  test("StreamInput") {
    implicit val streamInputJsonFormat =
      new StreamInputJsonFormat[TestBlockType.BlockType]()(TestBlockType.TestBlockTypeJsonFormat)
    compare[StreamInput[TestBlockType.BlockType]](Prepare(LocalPlayer(1, "hi")))
    compare[StreamInput[TestBlockType.BlockType]](Cancel(LocalPlayer(1, "hi")))
    compare[StreamInput[TestBlockType.BlockType]](Start(LocalPlayer(1, "hi"), 1))
    compare[StreamInput[TestBlockType.BlockType]](
      BlockTransform(
        LocalPlayer(1, "hi"),
        BlockTransformType.Rotate(BlockTransformType.LeftToDowToRight)
      )
    )
    compare[StreamInput[TestBlockType.BlockType]](
      Push(
        LocalPlayer(1, "hi"),
        1,
        List((Position(1, 2) -> UninitializedBlockCell[TestBlockType.BlockType](TestBlockType.BlockTwo))),
        false
      )
    )
    compare[StreamInput[TestBlockType.BlockType]](
      PreparePush(
        LocalPlayer(1, "hi"),
        1,
        List((Position(1, 2) -> UninitializedBlockCell[TestBlockType.BlockType](TestBlockType.BlockTwo)))
      )
    )
    compare[StreamInput[TestBlockType.BlockType]](RealizeNextBlock(LocalPlayer(1, "hi")))
    compare[StreamInput[TestBlockType.BlockType]](Stop(LocalPlayer(1, "hi"), "hi"))
    compare[StreamInput[TestBlockType.BlockType]](Leave(LocalPlayer(1, "hi")))
  }

  test("WaitRoomResponse") {
    implicit val waitRoomResponseJsonFormat = WaitRoomResponseJsonFormat
    compare[WaitRoomResponse](CreatedRoom(1, 2))
    compare[WaitRoomResponse](GotRoomList(Map((1 -> 2), (2 -> 3))))
  }

  test("GameRoomClientCommand") {
    implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
    compare[GameRoomClientCommand](CreatorJoinRoom(1, "hi"))
    compare[GameRoomClientCommand](CreatorJoinedRoom(Some(LocalPlayer(1, "hi"))))
    compare[GameRoomClientCommand](JoinRoom("hi"))
    compare[GameRoomClientCommand](JoinedRoom(Some(LocalPlayer(1, "hi"))))
    compare[GameRoomClientCommand](StartGames(1))
    compare[GameRoomClientCommand](StartedGames(false))
    compare[GameRoomClientCommand](Ping)
  }

  test("GameRoomServerCommand") {
    implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
    compare[GameRoomServerCommand](NotifyJoinRoom(1, LocalPlayer(1, "hi")))
    compare[GameRoomServerCommand](NotifiedJoinRoom(1, LocalPlayer(1, "hi"), false))
    compare[GameRoomServerCommand](NotifyBeAbleToStart(Some(1)))
    compare[GameRoomServerCommand](NotifiedBeAbleToStart)
    compare[GameRoomServerCommand](NotifyChangeStarter(LocalPlayer(1, "hi")))
    compare[GameRoomServerCommand](NotifiedChangeStarter)
  }
}
