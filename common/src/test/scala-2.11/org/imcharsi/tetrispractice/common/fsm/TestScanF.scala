/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import org.imcharsi.tetrispractice.common.data.fsm._
import BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import BlockTransformType.Down
import Command._
import GameState.GameState
import GameState._
import Player.{ Player, LocalPlayer }
import org.imcharsi.tetrispractice.common.data._
import org.imcharsi.tetrispractice.common.fsm.Game.{ FSMState, FSMInput }
import org.imcharsi.tetrispractice.common.util.State
import org.scalatest.{ BeforeAndAfterEach, FunSuite }

/**
 * Created by i on 8/25/16.
 */
class TestScanF extends FunSuite with BeforeAndAfterEach {

  val intSM = new State.StateMonadBehavior[Game.IntSS]
  implicit val runningGameSMB = new State.StateMonadBehavior[Game.RunningGameSS[TestBlockType.BlockType]]

  var idCounter: Int = 0
  var timerIdCounter: Int = 0

  def allocUiId(map: Map[Position, BlockCell[TestBlockType.BlockType]]): Map[Position, BlockCell[TestBlockType.BlockType]] = {
    // mapValues 를 쓰면 안 된다. map 에 대한 조회를 할 때마다 mapValues 내용을 평가한다. 그래서 side-effect 를 하면 문제된다.
    map.foldLeft(map) {
      case (map, (position, blockCell)) =>
        blockCell match {
          case UninitializedBlockCell(cellType) =>
            val newId = idCounter
            idCounter = idCounter + 1
            map + ((position -> InitializedBlockCell[TestBlockType.BlockType](newId, cellType)))
          case cell @ InitializedBlockCell(_, _) => map
        }
    }
  }

  def allocTimerId(): Int = {
    val newTimerId = timerIdCounter
    timerIdCounter = timerIdCounter + 1
    newTimerId
  }

  def allocateBePushed(bePushed: BlockCellList[TestBlockType.BlockType]) =
    bePushed.map {
      case (position, UninitializedBlockCell(cellType)) =>
        val newId = idCounter
        idCounter = idCounter + 1
        (position, InitializedBlockCell[TestBlockType.BlockType](newId, cellType))
    }

  val expectedLandingBlock: BlockCell[TestBlockType.BlockType] =
    UninitializedBlockCell(TestBlockType.BlockOne)
  val cleared =
    GameMap[TestBlockType.BlockType](
      Position(2, 4),
      Util.prepareClearedMap[TestBlockType.BlockType](
        UninitializedBlockCell(TestBlockType.BlockOne),
        Position(2, 4)
      )(Map())
    )
  val functionHolder = Game.FunctionHolder(TestBlockType.random, TestBlockType.generateBlockF, TestBlockType.rotateF)
  val scanF = Game.scanF(cleared, functionHolder, Position(0, 0), runningGameSMB) _
  val player: Player = LocalPlayer(1, "player1")

  // impo 상태를 확인하기 위해서는 비교대상이 되는 예상자료를 또 만들어야 하는데 TestUtil 에서 했던, 예상자료를 만들기를 여기서 또 반복해야 해서 번거롭다.
  // Game.scanF 의 내용이 사실상 한개나 두개의 Util 함수를 사용한 결과를 중계할 뿐이라는 것을 전제로 검사 내용을 짧게 줄였다.

  test("1 NotYetStartedGame -> Start -> RunningGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] = (Right(NotYetStartedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Start(player, 0))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitStartFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(Started(player, result, _))) =>
          Left(StartFeedback(player))
        case _ => fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), None, None, _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(Printer.mapToString(gameMap))
        assertResult(expectedGameMap)(Printer.mapToString(bePushed))
      case _ => fail()
    }
    // 구상에서는, StartFeedback 에 이어 바로 RealizeNextBlock 이 입력되어야 한다.
  }

  test("2 RunningGame -> RealizeNextBlock -> RunningGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            None,
            None,
            1,
            Block(
              Position(0, 0),
              Map(
                (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitRealizeNextBlockFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(RealizedNextBlock(_, Some(((block, expectedLanding), next)), List(), List(), List(), _))) =>
          // 이 단계에서, ui 식별번호 할당과 화면 출력을 해야 한다.
          // impo ui 식별번호 할당을 실패했다면 결과를 어떻게 알려줄 것인가.
          // fixme 일단은, None 을 알려주는 식으로 하면 게임의 중지가 되는데, 왜 게임을 중지해야 하는지에 대한 알림이 없다.
          // FSM 은 게임을 계속하겠다는 판단을 했는데,
          // Side-Effect 에서 게임을 그만해야 한다는 판단을 했다면 FSM 에게 이유를 알려줄 만 하다.
          Left(
            RealizeNextBlockFeedback(
              player,
              Some((
                allocTimerId(),
                block.copy(map = allocUiId(block.map)),
                next.copy(map = allocUiId(next.map)),
                expectedLanding.copy(map = allocUiId(expectedLanding.map))
              ))
            )
          )
        case _ => fail()
      }

    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), currentBlock @ Some(_), Some(_), _, _)), _, None) =>
        val expectedGameMap = "\n 0..\n....\n....\n 2..\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case _ => fail()
    }
  }

  test("3 RunningGame -> Move -> RunningGame") {
    val firstTimerId = allocTimerId()
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              Block(
                Position(0, 0),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              ),
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            Some(firstTimerId),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(player, BlockTransformType.Down))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)

    val (fsmInput2, expectedTimerId): (FSMInput[TestBlockType.BlockType], Int) =
      fsmState2 match {
        case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(BlockTransformed(player, Some(Right(block)), _))) =>
          val newTimerId = allocTimerId()
          (Left(BlockTransformFeedback(player, Some(newTimerId))), newTimerId)
        case _ => fail()
      }

    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), currentBlock @ Some(_), Some(currentTimerId), _, _)), _, None) =>
        val expectedGameMap = "\n....\n 0..\n....\n 1..\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
        assertResult(expectedTimerId)(currentTimerId)
        assert(firstTimerId != currentTimerId)
      case _ => fail()
    }

    val fsmInput3: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(player, BlockTransformType.Right))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState4 = scanF(fsmState1, fsmInput3)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState4._1))
    Console.out.println(fsmState4._3.get)

    val (fsmInput4, expectedTimerId2): (FSMInput[TestBlockType.BlockType], Int) =
      fsmState4 match {
        case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, _, Some(currentTimerId), _, _))), _, Some(BlockTransformed(player, Some(Right(block)), _))) =>
          (Left(BlockTransformFeedback(player, None)), currentTimerId)
        case _ => fail()
      }

    val fsmState5 = scanF(fsmState4, fsmInput4)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState5._1))
    fsmState5 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), currentBlock @ Some(_), Some(currentTimerId), _, _)), _, None) =>
        val expectedGameMap = "\n.. 0\n....\n....\n.. 1\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
        assertResult(expectedTimerId2)(currentTimerId)
        assert(firstTimerId == currentTimerId)
      case _ => fail()
    }
  }

  test("4 RunningGame -> Move -> RunningGame") {
    // allocUiId 는 Side-Effect 를 만든다.
    // 따라서, 벽돌이 바닥에 닿았을 때 예상낙하지점 ui 객체의 삭제를 검사하기 위해
    // testExpectedLanding 만 밖으로 빼면 내려오는 벽돌의 ui 식별번호보다 앞의 식별번호가 만들어지게 되어
    // 예상낙하지점 식별번호가 0 이고 내려오는 벽돌 식별번호가 1 이 되어 햇갈린다.
    // 그래서, 내려오는 벽돌도 밖으로 빼서 만들어줬다.
    // 단순히 아래의 그림 비교에서 0 을 1 로 바꿔줘도 되는데 왜 그래야 하는지 햇갈릴 수 있어서 이와 같이 설명을 쓴다.
    val testCurrentBlock: Block[TestBlockType.BlockType] =
      Block(
        Position(0, 3),
        allocUiId(
          Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )
      )
    val testExpectedLanding: Block[TestBlockType.BlockType] =
      Block(
        Position(0, 3),
        allocUiId(
          Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )
      )
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              testCurrentBlock,
              testExpectedLanding
            ),
            Some(allocTimerId()),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(player, Down))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(BlockTransformed(player, Some(Left((List(), deleted, List()))), _))) =>
          assertResult(List(testExpectedLanding.map.toList))(deleted)
          Left(BlockTransformFeedback(player, None))
        case _ =>
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), None, None, _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n 0..\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(None, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case _ => fail()
    }
  }

  test("5 RunningGame -> RealizeNextBlock -> StoppedGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            GameMap(
              Position(2, 4),
              Util.prepareClearedMap[TestBlockType.BlockType](UninitializedBlockCell(TestBlockType.BlockOne), Position(5, 5))(
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            (0, cleared),
            None,
            None,
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        // 이번 새 벽돌 만들기 시도에서, 움직인 것도 없고 넘쳐서 지워져야 하는 것도 없고 쌓아뒀던 줄을 옮긴 것도 없다.
        case (Left(WaitRealizeNextBlockFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(RealizedNextBlock(player, None, List(), List(), List(), _))) =>
          // 다음 벽돌을 만들 수 없으면 게임을 끝낸다.
          // 아직 게임은 RunningGame 상태에 있고, Feedback 에서 아무것도 알려주지 않아야 StoppedGame 으로 바뀐다.
          Left(RealizeNextBlockFeedback(player, None))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(StoppedGame(_, gameMap, bePushed, _)), _, None) =>
        val expectedGameMap = "\n 0..\n....\n....\n....\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(None, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case _ => fail()
    }
  }

  test("6 RunningGame -> Push -> RunningGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              ),
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            Some(allocTimerId()),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] =
      Right(
        Push(
          player,
          1,
          List[(Position, BlockCell[TestBlockType.BlockType])](
            (Position(1, 3), UninitializedBlockCell(TestBlockType.BlockOne))
          ),
          false
        )
      )
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitPushFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(Pushed(player, beAllocated, _, _, _))) =>
          Left(PushFeedback(player, Some(allocateBePushed(beAllocated))))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (1, bePushed), currentBlock @ Some(_), Some(_), _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n 0..\n"
        val expectedBePushed = "\n....\n....\n....\n.. 3\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("7 RunningGame -> Push -> RunningGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (1,
              GameMap(
                Position(2, 4),
                Util.prepareClearedMap[TestBlockType.BlockType](UninitializedBlockCell(TestBlockType.BlockOne), Position(5, 5))(
                  allocUiId(
                    Map(
                      (Position(1, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
                    )
                  )
                )
              )),
            Some(
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              ),
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            Some(allocTimerId()),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] =
      Right(
        Push(
          player,
          1,
          List[(Position, BlockCell[TestBlockType.BlockType])](
            (Position(0, 3), UninitializedBlockCell(TestBlockType.BlockOne))
          ),
          false
        )
      )
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitPushFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(Pushed(player, beAllocated, _, _, _))) =>
          Left(PushFeedback(player, Some(allocateBePushed(beAllocated))))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (2, bePushed), currentBlock @ Some(_), Some(_), _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n 1..\n"
        val expectedBePushed = "\n....\n....\n.. 0\n 4..\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("8 RunningGame -> RealizeNextBlock -> RunningGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            GameMap(
              Position(2, 4),
              Util.prepareClearedMap[TestBlockType.BlockType](UninitializedBlockCell(TestBlockType.BlockOne), Position(5, 5))(
                allocUiId(
                  Map(
                    (Position(0, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            (1,
              GameMap(
                Position(2, 4),
                Util.prepareClearedMap[TestBlockType.BlockType](UninitializedBlockCell(TestBlockType.BlockOne), Position(5, 5))(
                  allocUiId(
                    Map(
                      (Position(1, 3) -> UninitializedBlockCell(TestBlockType.BlockOne))
                    )
                  )
                )
              )),
            None,
            None,
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(RealizeNextBlock(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)

    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitRealizeNextBlockFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(RealizedNextBlock(player, Some(((block, expectedLanding), next)), moved, deleted, movedFromBePushedToMap, _))) =>
          // 이 단계에서는, moved/deleted 의해 게임 판의 벽돌 이동/삭제을 다뤄야 하고
          // movedFromBePushedToMap 에 의해, 쌓아 놓은 줄을 그렸던 ui 요소들을 게임 판을 그리는 ui 요소로 옮기는 것도 다뤄야 한다.
          Left(RealizeNextBlockFeedback(player, None))
          Left(
            RealizeNextBlockFeedback(
              player,
              Some((
                allocTimerId(),
                block.copy(map = allocUiId(block.map)),
                next.copy(map = allocUiId(next.map)),
                expectedLanding.copy(map = allocUiId(expectedLanding.map))
              ))
            )
          )
        case x =>
          Console.err.println(x)
          fail()
      }

    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), currentBlock @ Some(_), Some(_), _, _)), _, None) =>
        val expectedGameMap = "\n 2..\n 4..\n 0..\n.. 1\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("9 RunningGame -> Move -> RunningGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              Block(
                Position(0, 0),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              ),
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            Some(allocTimerId()),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(player, BlockTransformType.Bottom))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(BlockTransformed(player, Some(Right(Some(_))), _))) =>
          //        case BlockTransformed(player, Some(Right(block)), _) =>
          // 의미에 있어서, 한꺼번에 내리기에서는 더 이상 내릴 장소가 없으므로 사실상 바닥에 닿은 것을 확인한 것이나 마찬가지이다.
          // 따라서, 한꺼번에 내리는 경우에 한해서 Right 의 응답을 받았으면 인위적으로 한 번 더 움직여줘서 줄 깨기와 줄 모으기를 시도하도록 계기를 만들어야 한다.
          // 이와 같이 하지 않으면 다음과 같은 이상한 상황이 벌어지게 되는데,
          // 우선 현재 위치에서 바닥 내리기를 해서 벽돌이 바닥 위치까지 떨어졌지만, 여기서 즉시 줄 깨기/줄 모으기/다음 벽돌 만들기를 하지 않고
          // 미리 약속된 대기시간만큼 기다렸다가 시간초과로 벽돌 아래로 내리기를 다시 시도해서 이 때 줄 깨기와 줄 모으기, 다음 벽돌 만들기를 시도하게 된다.
          Left(BlockTransformFeedback(player, Some(allocTimerId())))
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    // 바닥으로 내리기에서는, 응답을 받은 후 이어서 아래로 이동 입력을 해준다.
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), currentBlock @ Some(_), Some(_), _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n 0..\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(currentBlock, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case x =>
        Console.err.println(x)
        fail()
    }
    val fsmInput3: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(player, BlockTransformType.Down))
    val fsmState4 = scanF(fsmState3, fsmInput3)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "4")(fsmState4._1))
    val fsmInput4: FSMInput[TestBlockType.BlockType] =
      fsmState4 match {
        case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(BlockTransformed(player, Some(Left((List(), _, List()))), _))) =>
          // 벽돌이 바닥에 닿았을 때 예상낙하지점을 그렸던 ui 객체는 지운다는 검사는 4 번 검사에서 했다.
          // 여기서는 내용 비교를 생략한다.
          Left(BlockTransformFeedback(player, None))
      }

    val fsmState5 = scanF(fsmState4, fsmInput4)
    // Right 의 응답을 받았던 경우에는, 이어서 아래의 입력을 해줘야 한다.
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState5._1))
    fsmState5 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), None, None, _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n 0..\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(None, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("10 RunningGame -> Stop -> StoppedGame") {
    val firstTimerId = allocTimerId()
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              Block(
                Position(0, 0),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              ),
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            Some(firstTimerId),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Stop(player, "hi"))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitStopFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(Stopped(player, _, _))) =>
          Left(StopFeedback(player))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(StoppedGame(_, gameMap, bePushed, _)), _, None) =>
      // 이 단계의 검사는 Stop 입력을 받아서 StoppedGame 상태에 이르게 됐는지를 검사하는 데 목적이 있다.
      // 그래서 다른 상태 검사는 하지 않는다.
      case _ => fail()
    }
  }

  test("11 NotYetStartedGame -> Stop -> StoppedGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      (Right(NotYetStartedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Stop(player, "hi"))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        // 이번 새 벽돌 만들기 시도에서, 움직인 것도 없고 넘쳐서 지워져야 하는 것도 없고 쌓아뒀던 줄을 옮긴 것도 없다.
        case (Left(WaitStopFeedback(NotYetStartedGame(_))), _, Some(Stopped(player, _, _))) =>
          // 다음 벽돌을 만들 수 없으면 게임을 끝낸다.
          // 아직 게임은 RunningGame 상태에 있고, Feedback 에서 아무것도 알려주지 않아야 StoppedGame 으로 바뀐다.
          Left(StopFeedback(player))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(StoppedGame(_, gameMap, bePushed, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n....\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(None, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case _ => fail()
    }
  }

  test("12 RunningGame -> Push -> StoppedGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              ),
              Block(
                Position(0, 3),
                allocUiId(
                  Map(
                    (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                  )
                )
              )
            ),
            Some(allocTimerId()),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] =
      Right(
        Push(
          player,
          1,
          List[(Position, BlockCell[TestBlockType.BlockType])](
            (Position(1, 3), UninitializedBlockCell(TestBlockType.BlockOne))
          ),
          false
        )
      )
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitPushFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(Pushed(player, beAllocated, _, _, _))) =>
          Left(PushFeedback(player, None))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(StoppedGame(_, _, _, _)), _, None) =>
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("13 RunningGame -> Duplicated Move -> RunningGame") {
    // 현재 내려오는 벽돌이 없어서 RealizeNextBlock 입력을 기다려야 하는 상황에서
    // 또 BlockTransform 입력이 주어질 때에 대한 대안이 제대로 동작하는지 검사한다.
    val testCurrentBlock: Block[TestBlockType.BlockType] =
      Block(
        Position(0, 3),
        allocUiId(
          Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )
      )
    val testExpectedLanding: Block[TestBlockType.BlockType] =
      Block(
        Position(0, 3),
        allocUiId(
          Map(
            (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
          )
        )
      )
    val fsmState1: FSMState[TestBlockType.BlockType] =
      Tuple3(
        Right(
          RunningGame(
            player,
            cleared,
            (0, cleared),
            Some(
              testCurrentBlock,
              testExpectedLanding
            ),
            Some(allocTimerId()),
            1,
            Block(
              Position(0, 0),
              allocUiId(
                Map(
                  (Position(0, 0) -> UninitializedBlockCell(TestBlockType.BlockOne))
                )
              )
            )
          )
        ),
        State(()),
        None
      )
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(BlockTransform(player, Down))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    Console.out.println(fsmState2._3.get)
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, _, _, _, _))), _, Some(BlockTransformed(player, Some(Left((List(), deleted, List()))), _))) =>
          assertResult(List(testExpectedLanding.map.toList))(deleted)
          Left(BlockTransformFeedback(player, None))
        case _ =>
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(RunningGame(_, gameMap, (0, bePushed), None, None, _, _)), _, None) =>
        val expectedGameMap = "\n....\n....\n....\n 0..\n"
        val expectedBePushed = "\n....\n....\n....\n....\n"
        assertResult(expectedGameMap)(
          Printer.mapToString(
            Printer.overlapMapAndBlock(Position(2, 1))(None, gameMap)
          )
        )
        assertResult(expectedBePushed)(Printer.mapToString(bePushed))
      case _ => fail()
    }
    val fsmState4 = scanF(fsmState3, fsmInput1)
    fsmState4 match {
      case (Left(WaitBlockTransformFeedback(RunningGame(_, _, _, None, _, _, _))), _, Some(BlockTransformed(player, Some(Right(None)), _))) =>
      case _ =>
        fail()
    }
  }

  test("NotYetPreparedGame -> Prepare -> NotYetStartedGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      (Right(NotYetPreparedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Prepare(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitPrepareFeedback(NotYetPreparedGame(_))), _, Some(Prepared(player, _, _))) =>
          Left(PrepareFeedback(player))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(NotYetStartedGame(_)), _, None) =>
      case _ => fail()
    }
  }

  test("NotYetStartedGame -> Cancel -> NotYetPreparedGame") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      (Right(NotYetStartedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Cancel(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    val fsmInput2: FSMInput[TestBlockType.BlockType] =
      fsmState2 match {
        case (Left(WaitCancelFeedback(NotYetStartedGame(_))), _, Some(Cancelled(player, _, _))) =>
          Left(CancelFeedback(player))
        case x =>
          Console.err.println(x)
          fail()
      }
    val fsmState3 = scanF(fsmState2, fsmInput2)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "3")(fsmState3._1))
    fsmState3 match {
      case (Right(NotYetPreparedGame(_)), _, None) =>
      case _ => fail()
    }
  }

  test("NotYetPreparedGame -> Start") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      (Right(NotYetPreparedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Start(player, 0))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    fsmState2 match {
      case (Right(StoppedGame(_, _, _, _)), _, Some(Leaved(player, _, _))) =>
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("NotYetPreparedGame -> Leave") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      (Right(NotYetPreparedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Leave(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    fsmState2 match {
      case (Right(StoppedGame(_, _, _, _)), _, Some(Leaved(player, _, _))) =>
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  test("NotYetStartedGame -> Leave") {
    val fsmState1: FSMState[TestBlockType.BlockType] =
      (Right(NotYetPreparedGame(player)), State(()), None)
    val fsmInput1: FSMInput[TestBlockType.BlockType] = Right(Leave(player))
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "1")(fsmState1._1))
    val fsmState2 = scanF(fsmState1, fsmInput1)
    Console.out.println(Printer.printState(Position(2, 1), expectedLandingBlock, "2")(fsmState2._1))
    fsmState2 match {
      case (Right(StoppedGame(_, _, _, _)), _, Some(Leaved(player, _, _))) =>
      case x =>
        Console.err.println(x)
        fail()
    }
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    idCounter = 0
    timerIdCounter = 0
  }
}
