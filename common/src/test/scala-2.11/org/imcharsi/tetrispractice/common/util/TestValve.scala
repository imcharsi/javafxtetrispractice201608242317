/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

import org.scalatest.FunSuite
import rx.lang.scala.Subject
import rx.lang.scala.observers.TestSubscriber
import rx.observers
import scala.reflect.runtime.universe._

/**
 * Created by i on 9/1/16.
 */
class TestValve extends FunSuite {

  val mirror = scala.reflect.runtime.currentMirror

  // 앞서 JavaFXChattingPractice 에서 썼던 검사 기능을 그대로 복사해서 썼다.
  def reflect[A](testSubscriber: TestSubscriber[A]): observers.TestSubscriber[A] = {
    val termSymbol =
      mirror.classSymbol(classOf[TestSubscriber[(Int, Int)]]).toType.members.filter {
        case m: TermSymbol => m.name.toString == "jTestSubscriber"
        case _ => false
      }.head.asInstanceOf[TermSymbol]
    mirror.reflect(testSubscriber)
      .reflectField(termSymbol).get
      .asInstanceOf[observers.TestSubscriber[A]]
  }

  def determineOnInputF(a: Int): Boolean =
    a match {
      case 1 => false
      case 2 => true
    }

  test("valve") {
    val subject = Subject[Int]
    val subscriber = TestSubscriber[Int]
    val innerSubscriber = reflect[Int](subscriber)

    Valve.valve(determineOnInputF)(subject).subscribe(subscriber)
    // 1 이 들어가면서 문을 닫았다.
    subject.onNext(1)
    innerSubscriber.assertValuesAndClear(1)
    // 이미 문이 닫혀서 1 을 다시 넣어도 흐르지 않는다. 두 개를 넣어둔다.
    subject.onNext(1)
    subject.onNext(1)
    innerSubscriber.assertNoValues()
    // 2 가 들어가면서 문이 열렸다.
    subject.onNext(2)
    // 문을 연 자료에 더해서 쌓여 있던 자료가 하나만 들어온다.
    // 1 이 입력되면서 문을 닫고 흘러 나온다.
    innerSubscriber.assertValuesAndClear(2, 1)
    innerSubscriber.assertNoValues()
    subject.onNext(2)
    // 문을 연 자료에 더해서 쌓여 있던 자료가 또 다시 하나만 들어온다.
    innerSubscriber.assertValuesAndClear(2, 1)
    innerSubscriber.assertNoValues()
    subject.onNext(2)
    // 마지막으로 다시 문을 열었고, 더 이상 쌓아둔 자료가 없다.
    innerSubscriber.assertValuesAndClear(2)
    innerSubscriber.assertNoValues()
  }

}
