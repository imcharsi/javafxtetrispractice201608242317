/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

import org.scalatest.FunSuite

/**
 * Created by i on 8/25/16.
 */
class TestStateModule extends FunSuite {
  test("usage") {
    implicit val intStateModule = new State.StateMonadBehavior[Int]
    import FP.StateMonadSyntaxSugar
    import FP.StateApplicativeSyntaxSugar
    val stateA = intStateModule(1)
    val stateB = intStateModule("h")
    def f = intStateModule.bind { (a: Int) =>
      State.State(s => (s + 1, a + 1))
    }
    def recoverF[A](oldS: Int, newS: Int, a: A): (Int, Unit) = {
      Console.err.println(s"${oldS} ${newS} ${a}")
      if (newS < 2) (newS, ()) else (oldS, ())
    }
    val stateD = State.recover(recoverF[String])(f(f(stateB >> stateA) >> stateA) >> stateB)
    State.recover(recoverF[String])(stateB)
    Console.err.println(stateD(1))
    val stateE = intStateModule((a: Int) => (b: Int) => a + b + 1)
    Console.err.println((stateE <*> intStateModule(1) <*> intStateModule(2)).f(0))
  }

}
