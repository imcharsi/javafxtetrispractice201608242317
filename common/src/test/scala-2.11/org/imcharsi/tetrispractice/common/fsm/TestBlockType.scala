/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.{ Position, Block, BlockTransformType, BlockCell }
import BlockCell.{ BlockCell, UninitializedBlockCell }
import BlockTransformType.RotateDirection
import org.imcharsi.tetrispractice.common.util.State
import spray.json._

/**
 * Created by i on 8/30/16.
 */
object TestBlockType {

  sealed trait BlockType

  case object BlockOne extends BlockType

  case object BlockTwo extends BlockType

  object TestBlockTypeJsonFormat extends RootJsonFormat[BlockType] with SprayJsonSupport {
    override def write(obj: BlockType): JsValue = {
      obj match {
        case BlockOne => JsObject(Map(("clazz" -> JsString(BlockOne.toString))))
        case BlockTwo => JsObject(Map(("clazz" -> JsString(BlockTwo.toString))))
      }
    }

    override def read(json: JsValue): BlockType = {
      val fields = json.asJsObject.fields
      fields.get("clazz") match {
        case Some(JsString(clazz)) if BlockOne.toString == clazz => BlockOne
        case Some(JsString(clazz)) if BlockTwo.toString == clazz => BlockTwo
        case None => deserializationError("")
      }
    }
  }

  def generateBlockF(blockType: Int): Block[BlockType] = {
    val newBlockType = if (blockType > 0) blockType else blockType * -1
    if (newBlockType % 2 == 0)
      Block(Position(0, 0), Map(Position(0, 0) -> UninitializedBlockCell(BlockOne)))
    else
      Block(Position(0, 0), Map(Position(1, 0) -> UninitializedBlockCell(BlockTwo)))
  }

  def rotateF(
    gameMap: Map[Position, BlockCell[BlockType]],
    rotateDirection: RotateDirection,
    blockPosition: Position,
    blockMap: Map[Position, BlockCell[BlockType]]
  ): Option[Map[Position, BlockCell[BlockType]]] = {
    val newBlockMap =
      blockMap.foldLeft(Map[Position, BlockCell[BlockType]]()) {
        case (map, (position, blockCell)) =>
          position match {
            case Position(0, 0) => map + (Position(1, 0) -> blockCell)
            case Position(1, 0) => map + (Position(0, 0) -> blockCell)
          }
      }
    newBlockMap.foldLeft(true) {
      case (true, (position, _)) =>
        gameMap.get(position + blockPosition).isEmpty
      case (false, _) => false
    } match {
      case true => Some(newBlockMap)
      case false => None
    }
  }

  val random =
    State.State[Game.IntSS, Int] {
      case (seed, queue) =>
        ((seed + 1, queue), seed)
    }
}
