/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ InitializedBlockCell, UninitializedBlockCell, BlockCell }
import spray.json._

/**
 * Created by i on 9/9/16.
 */
class BlockCellJsonFormat[BT](implicit val blockTypeFormat: JsonFormat[BT]) extends RootJsonFormat[BlockCell[BT]] with SprayJsonSupport {

  import spray.json.DefaultJsonProtocol._

  private implicit val uninitializedBlockCellJsonFormat = jsonFormat1(UninitializedBlockCell[BT])
  private implicit val initializedBlockCellJsonFormat = jsonFormat2(InitializedBlockCell[BT])

  override def write(obj: BlockCell[BT]): JsValue = {
    obj match {
      case obj @ UninitializedBlockCell(_) =>
        JsObject(
          obj.toJson.asJsObject
            .fields + ("clazz" -> JsString(UninitializedBlockCell.toString))
        )
      case obj @ InitializedBlockCell(_, _) =>
        JsObject(
          obj.toJson.asJsObject
            .fields + ("clazz" -> JsString(InitializedBlockCell.toString))
        )
    }
  }

  override def read(json: JsValue): BlockCell[BT] = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if UninitializedBlockCell.toString == clazz =>
        JsObject(fields - "clazz").convertTo[UninitializedBlockCell[BT]]
      case Some(JsString(clazz)) if InitializedBlockCell.toString == clazz =>
        JsObject(fields - "clazz").convertTo[InitializedBlockCell[BT]]
      case _ => deserializationError("")
    }
  }
}
