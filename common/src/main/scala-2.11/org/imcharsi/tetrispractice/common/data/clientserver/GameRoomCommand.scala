/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.clientserver

import akka.actor.ActorRef
import org.imcharsi.tetrispractice.common.data.TransferDirection.{ ServerToClient, ClientToServer }
import org.imcharsi.tetrispractice.common.data.fsm.Command
import org.imcharsi.tetrispractice.common.data.fsm.Player.Player

/**
 * Created by i on 9/3/16.
 */
object GameRoomCommand {

  case object CreateWebSocketWorkerActor

  sealed case class CreatedWebSocketWorkerActor(actorRef: ActorRef)

  sealed trait GameRoomClientCommand

  sealed trait GameRoomServerCommand

  // 개설자는 개설자용 입장명령을 쓴다. 개설할 때 받은 암호로 입장을 해야 한다.
  // 개설 따로, 입장 따로 하기로 구상했으므로, 개설자가 입장하기 전에 다른 사람이 입장해서는 안 된다.
  sealed case class CreatorJoinRoom(password: Int, name: String) extends GameRoomClientCommand with ClientToServer

  sealed case class CreatorJoinedRoom(player: Option[Player]) extends GameRoomClientCommand with ServerToClient

  // 각 사용자는 자기를 표시하는 데 쓸 이름을 쓸 수 있다. 이름은 중복될 수 있다.
  // 이 입력은 WebSocket 으로 접속하면 WebSocketWorkerActor 가 만드는 입력이다.
  sealed case class JoinRoom(name: String) extends GameRoomClientCommand with ClientToServer

  // 이 입력은 GameRoomActor 가 입장절차를 거친 결과를 WebSocketWorkerActor 에게 알리이 위해 쓴다.
  // Client 로 다시 전달하지 않는다.
  sealed case class JoinedRoom(player: Option[Player]) extends GameRoomClientCommand with ServerToClient

  // 이미 입장해 있는 사용자에게, 이후의 사용자가 입장했음을 알린다.
  // 이 알림은, Server 가 JoinedRoom 응답을 한 뒤 다른 모든 사용자에게 Broadcast 로 한다.
  sealed case class NotifyJoinRoom(responseId: Int, player: Player) extends GameRoomServerCommand with ServerToClient

  // 각 Client 는 NotifyJoining 을 받은 후, NotYetStartedGame 상태에 있는 RemotePlayer 게임을 준비해야 하고
  // 준비를 실패하면 실패했다는 알림을 하고 연결을 끊는다. Server 는 실패의 알림을 받은 후 연결을 끊는다.
  // 입력에 대해서는 항상 응답이 있도록 구상했다. 따라서, Server 가 응답을 받는 시간이 초과되면 연결을 끊어야 한다.
  // 어떻게 끊을 것인가. Akka Actor Scheduler 기능으로 시간초과를 건다.
  // 시간초과 기능을 쓰려면 Server 가 보내는 입력에 식별번호를 붙이고 Client 가 보내는 응답에 이 식별번호를 쓰는 식으로 해야 한다.
  // 응답으로 받는 Player 는 준비의 대상이 되는 RemotePlayer 가 아니고 준비의 주체인 Client LocalPlayer 이다.
  sealed case class NotifiedJoinRoom(responseId: Int, responsePlayer: Player, result: Boolean) extends GameRoomServerCommand with ClientToServer

  // 게임을 시작할 수 있음을 Server 가 방장에게 알린다.
  sealed case class NotifyBeAbleToStart(preparedId: Option[Int]) extends GameRoomServerCommand with ServerToClient

  // 방장이 게임을 시작할 수 있다는 상태를 알림받고 이에 대한 응답을 하지 않으면 어떻게 하는가.
  // 응답을 하지 않은 Client 는 연결을 끊어야 한다. 따라서 방장은 퇴장되어야 한다.
  // 이어서 다른 사람을 방장으로 하는 방법도 있을 수 있는데, 방을 폐쇄하는 방법으로 한다.
  case object NotifiedBeAbleToStart extends GameRoomServerCommand with ClientToServer

  // 게임을 시작할 수 있는 사람이 바뀌었음을 알린다. 개설자가 퇴장한 경우이다.
  sealed case class NotifyChangeStarter(player: Player) extends GameRoomServerCommand with ServerToClient

  case object NotifiedChangeStarter extends GameRoomServerCommand with ClientToServer

  // fixme 이름이 이상하다. 다른 이름 없나.
  sealed case class StartGames(preparedId: Int) extends GameRoomClientCommand with ClientToServer

  sealed case class StartedGames(result: Boolean) extends GameRoomClientCommand with ServerToClient

  case object Ping extends GameRoomClientCommand with ClientToServer

  // 이 입력을 둔 이유는, WebSocket 으로 나가야 하는 입력을 전부 보낸 후에 Actor 를 종료할 수 있도록 하기 위해서이다.
  // PoisonPill 은 Actor Message Queue 가 전부 처리된다는 보장만 할 뿐,
  // Akka Stream 의 ActorPublisher Queue 가 전부 처리된다는 보장은 하지 않기 때문이다.
  case object QuitWebSocketWorkerActor extends GameRoomServerCommand with ServerToClient
}
