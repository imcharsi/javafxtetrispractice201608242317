/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ RemotePlayer, LocalPlayer, Player }
import spray.json._

/**
 * Created by i on 9/9/16.
 */
object PlayerJsonFormat extends RootJsonFormat[Player] with SprayJsonSupport {

  import DefaultJsonProtocol._

  private implicit val localPlayerJsonFormat = jsonFormat2(LocalPlayer)
  private implicit val remotePlayerJsonFormat = jsonFormat2(RemotePlayer)

  override def write(obj: Player): JsValue = {
    obj match {
      case obj @ LocalPlayer(_, _) =>
        JsObject(obj.toJson.asJsObject.fields + ("clazz" -> (JsString(LocalPlayer.toString()))))
      case obj @ RemotePlayer(_, _) =>
        JsObject(obj.toJson.asJsObject.fields + ("clazz" -> (JsString(RemotePlayer.toString()))))
    }
  }

  override def read(json: JsValue): Player = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == LocalPlayer.toString() =>
        JsObject(fields - "clazz").convertTo[LocalPlayer]
      case Some(JsString(clazz)) if clazz == RemotePlayer.toString() =>
        JsObject(fields - "clazz").convertTo[RemotePlayer]
      case _ => deserializationError("")
    }
  }
}
