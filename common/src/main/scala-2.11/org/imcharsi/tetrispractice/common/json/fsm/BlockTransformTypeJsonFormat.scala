/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.BlockTransformType
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.{ TimeElapsed, Rotate, BlockTransformType }
import spray.json._

/**
 * Created by i on 9/9/16.
 */
object BlockTransformTypeJsonFormat extends RootJsonFormat[BlockTransformType] with SprayJsonSupport {
  private implicit val rotateDirectionJsonFormat = RotateDirectionJsonFormat

  import spray.json.DefaultJsonProtocol._

  implicit val rotateJsonFormat = jsonFormat1(Rotate)
  implicit val timeElapsedJsonFormat = jsonFormat1(TimeElapsed)

  override def write(obj: BlockTransformType): JsValue = {
    obj match {
      case BlockTransformType.Left => JsObject(Map(("clazz" -> JsString(BlockTransformType.Left.toString))))
      case BlockTransformType.Right => JsObject(Map(("clazz" -> JsString(BlockTransformType.Right.toString))))
      case BlockTransformType.Down => JsObject(Map(("clazz" -> JsString(BlockTransformType.Down.toString))))
      case BlockTransformType.Bottom => JsObject(Map(("clazz" -> JsString(BlockTransformType.Bottom.toString))))
      case obj @ BlockTransformType.Rotate(_) =>
        JsObject(obj.toJson.asJsObject.fields + ("clazz" -> JsString(BlockTransformType.Rotate.toString())))
      case obj @ BlockTransformType.TimeElapsed(_) =>
        JsObject(obj.toJson.asJsObject.fields + ("clazz" -> JsString(BlockTransformType.TimeElapsed.toString())))
    }
  }

  override def read(json: JsValue): BlockTransformType = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == BlockTransformType.Left.toString =>
        BlockTransformType.Left
      case Some(JsString(clazz)) if clazz == BlockTransformType.Right.toString =>
        BlockTransformType.Right
      case Some(JsString(clazz)) if clazz == BlockTransformType.Down.toString =>
        BlockTransformType.Down
      case Some(JsString(clazz)) if clazz == BlockTransformType.Bottom.toString =>
        BlockTransformType.Bottom
      case Some(JsString(clazz)) if clazz == BlockTransformType.Rotate.toString =>
        JsObject(fields - "clazz").convertTo[Rotate]
      case Some(JsString(clazz)) if clazz == BlockTransformType.TimeElapsed.toString =>
        JsObject(fields - "clazz").convertTo[TimeElapsed]
      case _ => deserializationError("")
    }
  }
}
