/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.BlockCell
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.RotateDirection
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ BlockTransform, _ }
import org.imcharsi.tetrispractice.common.data.fsm.GameState.{ GameState, _ }
import org.imcharsi.tetrispractice.common.data.fsm.Player.Player
import org.imcharsi.tetrispractice.common.data.fsm._
import org.imcharsi.tetrispractice.common.util.Log.Log
import org.imcharsi.tetrispractice.common.util.{ FP, State }
import rx.lang.scala.Observable

import scala.collection.immutable.Queue

/**
 * Created by i on 8/25/16.
 */
object Game {
  type LogSS = Queue[Log]
  type IntSS = (Int, LogS)
  type RunningGameSS[BT] = (RunningGame[BT], LogS)

  type LogS = State.State[LogSS, Unit]
  type IntS[BT, A] = State.State[IntSS, A]
  // 실용적인 이유에서, 상태의 type 을 GameState 로 하지 않고 구체적인 사례 중 하나로 했다.
  // GameState 로 받으면 어차피 또 pattern-match 를 해야 하기 때문이다.
  type RunningGameS[BT, A] = State.State[RunningGameSS[BT], A]

  type GenerateBlockR[BT] = Option[((Block[BT], Block[BT]), Block[BT])]
  // Left 의 첫번째는 게임 판 안의 이동하는 벽돌을 나타내고
  // Left 의 두번째는 게임 판 안의 지워지는 벽돌을 나타내고
  // Left 의 세번째는 넘기는 줄을 나타내고
  // Right 는 이동하는 currentBlock 을 나타낸다.
  // 바깥의 Option 은 게임을 중지해야 하는지를 판단한다.
  // 예로서, RunningGame 의 gameMap 과 currentBlock 이 처음부터 겹쳐 있을 때 게임을 중지해야 한다.
  // 이에 관한 설명은 BlockTransformed 에 썼다.
  type MoveBlockR[BT] = Option[Either[(List[BlockCellList[BT]], List[BlockCellList[BT]], List[BlockCellList[BT]]), CurrentBlock[BT]]]
  // 첫번째 반환값인 새로 넘겨받은 줄은 아직 할당이 되지 않았다. 구독단계에서 ui 식별번호가 할당되어야 한다.
  // 두번째 반환값은 위로 올라간, 기존에 있던 줄들이다. 화면 표시에서 이동으로 나타나야 한다.
  // 세번째 반환값은 넘쳐서 지워져야 하는 줄들이다.
  type PushToBePushedR[BT] = (BlockCellList[BT], BlockCellList[BT], BlockCellList[BT])
  // 이에 관한 설명은 RealizedNextBlock 에 썼다.
  // 네 번째는, bePushed 에 있던 줄들을 게임판으로 옮긴 결과 게임 판에 있던 벽돌들이 넘친 경우 게임이 끝났음을 표시한다.
  // 세 번째 항목인, 게임 판에 있던 지워지는 벽돌이 있는지 여부를 보면 넘쳤음을 알 수 있는데
  // 굳이 네 번째 항목을 두는 이유는 무엇인가.
  // 예를 들어서, 1자 벽돌을 한쪽 벽으로 붙여서 여러개를 쌓은 후 bePushed 를 게임 판으로 밀어넣어서 넘쳤다고 할 때
  // 다음 벽돌을 만들 자리는 여전히 남는데 이것을 게임이 끝난 것으로 판단할 것인지 문제된다.
  type PushToMapR[BT] = (BlockCellList[BT], BlockCellList[BT], BlockCellList[BT], Boolean)

  type FSMInput[BT] = Either[StreamFeedback[BT], StreamInput[BT]]
  type FSMState[BT] = (Either[WaitFeedback[BT], GameState[BT]], LogS, Option[StreamOutput[BT]])

  type RotateF[BT] = (Map[Position, BlockCell[BT]], RotateDirection, Position, Map[Position, BlockCell[BT]]) => Option[Map[Position, BlockCell[BT]]]

  sealed case class FunctionHolder[BT](
    random: State.State[Game.IntSS, Int],
    generateBlockF: Int => Block[BT],
    rotateF: RotateF[BT]
  )

  def scanF[BT](
    clearedMap: GameMap[BT],
    fs: FunctionHolder[BT],
    origin: Position,
    runningGameSMB: State.StateMonadBehavior[RunningGameSS[BT]]
  )(fsmState: FSMState[BT], input: FSMInput[BT]): FSMState[BT] = {
    implicit val runningGameSMBImplicit = runningGameSMB
    val queue: LogS = State(())
    (fsmState, input) match {
      case ((Right(gameState @ NotYetPreparedGame(_)), _, _), Right(input @ Prepare(player))) =>
        (Left(WaitPrepareFeedback(gameState)), queue, Some(Prepared(player, true, input)))
      case ((Left(WaitPrepareFeedback(NotYetPreparedGame(_))), _, _), Left(PrepareFeedback(player))) =>
        (Right(NotYetStartedGame(player)), queue, None)
      case ((Right(gameState @ NotYetStartedGame(_)), _, _), Right(input @ Cancel(player))) =>
        (Left(WaitCancelFeedback(gameState)), queue, Some(Cancelled(player, true, input)))
      case ((Left(WaitCancelFeedback(NotYetStartedGame(_))), _, _), Left(CancelFeedback(player))) =>
        (Right(NotYetPreparedGame(player)), queue, None)
      case ((Right(gameState @ NotYetPreparedGame(_)), _, _), Right(input @ Start(player, _))) =>
        (Right(StoppedGame(player, clearedMap, clearedMap, fs.generateBlockF(0))), queue, Some(Leaved(player, true, Leave(player))))
      case ((Right(NotYetPreparedGame(_) | NotYetStartedGame(_)), _, _), Right(input @ Leave(player))) =>
        // Leaved 출력의 경우 Feedback 을 받지 않는다. Leaved 구독 단계에서, 화면의 관련 기능을 전부 지울 것이기 때문이다.
        (Right(StoppedGame(player, clearedMap, clearedMap, fs.generateBlockF(0))), queue, Some(Leaved(player, true, input)))
      // FSM 에서 상태의 player 와 입력의 player 가 같은지 확인할 필요는 없다.
      // 이와 같은 구분은 앞의 분배단계에서 한다.
      case ((Right(gameState), _, _), Right(input @ Start(player, seed))) =>
        gameState match {
          case NotYetStartedGame(_) =>
            // fixme 설명 다시 쓰기. 구독 단계에서, 성공의 게임시작결과를 받으면 Feedback 에 이어서 RealizeNextBlock 을 입력해준다.
            val ((newSeed, newQueue), newBlock) =
              Util.generateNext[BT](fs.random, fs.generateBlockF, origin, fs.rotateF)((seed, queue))
            // 굳이 Tuple3 라고 쓰는 이유는, () 와 같이 썼을 때 Scalariform 에서 들여쓰기를 정확하게 해주지 않기 때문이다.
            Tuple3(
              Left(
                WaitStartFeedback(
                  RunningGame(player, clearedMap, (0, clearedMap), None, None, newSeed, newBlock)
                )
              ),
              newQueue,
              Some(Started(player, true, input))
            )
          case gameState =>
            Tuple3(
              Left(WaitStartFeedback(gameState)),
              queue,
              Some(Started(player, false, input))
            )
        }
      case ((Left(WaitStartFeedback(gameState @ RunningGame(_, _, _, _, _, _, _))), _, _),
        Left(StartFeedback(player))) =>
        Tuple3(Right(gameState), queue, None)
      case ((Right(runningGame @ RunningGame(_, _, _, currentBlock @ Some((_, _)), _, _, _)), _, _),
        Right(blockTransform @ BlockTransform(player, blockTransformType))) =>
        val state =
          blockTransformType match {
            case BlockTransformType.Left | BlockTransformType.Right | BlockTransformType.Down | BlockTransformType.Bottom =>
              Util.moveBlock(clearedMap)(blockTransformType)
            case BlockTransformType.Rotate(rotateDirection) =>
              import FP.{ StateFunctorSyntaxSugar, StateMonadSyntaxSugar }
              Util.rotateBlock(clearedMap, fs.rotateF)(rotateDirection)
                .passThrough(Util.expectedLanding[BT])
                .bind(Util.decoratorExpectedLanding[BT](_))
                .fmap(_.map(Right(_)))
            case BlockTransformType.TimeElapsed(timerId) =>
              // 만약 아래로의 이동입력을 잘 처리했고 상태에서는 시간초과설정을 지웠는데,
              // Side-Effect 단계에서 시간초과설정 취소가 늦어서 결국 시간초과 알림을 받게 될 수 있다.
              if (runningGame.currentTimerId.exists(_ == timerId))
                Util.moveBlock(clearedMap)(BlockTransformType.Down)
              else
                State[RunningGameSS[BT], MoveBlockR[BT]](Some(Right(None)))
          }
        val ((newRunningGame, newQueue), result) = state((runningGame, queue))
        Console.err.println(newRunningGame.currentBlock)
        Tuple3(
          Left(WaitBlockTransformFeedback(newRunningGame)),
          newQueue,
          Some(BlockTransformed(player, result, blockTransform))
        )
      case ((Right(runningGame @ RunningGame(_, _, _, None, _, _, _)), _, _),
        Right(blockTransform @ BlockTransform(player, _))) =>
        // 왜 이와 같은 경우를 다뤄야 하는가.
        // FSM 의 BlockTransformed 출력을 Side-Effect 에서 처리하고 RealizeNextBlock 입력을 FSM 으로 해주는 것이 계획인데,
        // 실제로 Side-Effect 에서 BlockTransformed 출력을 처리하고 아직 FSM 으로 RealizeNextBlock 입력을 해주기 전에
        // BlockTransform 입력이 또 주어질 수 있다. 기계가 바쁘면 이와 같은 일이 생길 수 있다.
        // 이유는, Rx Stream 의 계산단계와 Side-Effect 처리 단계가 다른 Thread 로 분리되어 있어서,
        // Side-Effect 처리 단계에서의 동작이 느려서 하나의 출력을 다 처리하여 끝내지 못하고 있는 동안
        // 계산단계에서는 여러개의 계산을 하여 여러개의 출력을 할 수 있기 때문이다.
        // 일부러 이와 같은 상황을 만드는 방법은,
        // Side-Effect 처리 단계에서 Started 출력을 받고 RealizeNextBlock 입력을 하기 직전에 Thread.sleep 로 시간을 끌고
        // 그 시간 동안 계속 바닥으로 내리기 입력을 하는 것이다.
        // 실제로 어떤 문제가 생기는가.
        // Side-Effect 단계에서 MoveBlockR 의 Some(Left((List(),List(),List()))) 출력을 계속 받게 되고 이어서 RealizeNextBlock 입력을 계속하게 된다.
        // 그래서 최초의 RealizeNextBlock 입력으로 인해 RunningGame 의 currentBlock 이 Some 으로 바뀌고 나서도 계속 RealizeNextBlock 을 받게 된다.
        // 이것은 일반적인 문제이고, Side-Effect 단계에서 이를 구분할 수 있는 단서가 있는 것도 아니라서
        // 이와 같이 판단 단계에서 대안을 준비해야 한다.
        // 대안은, 현재 내려오는 벽돌이 없는데 이동 입력을 받으면, 아무것도 하지 않았고 그래서 아무것도 할 필요가 없다는 뜻의 출력을 하는 것이다.
        Tuple3(
          Left(WaitBlockTransformFeedback(runningGame)),
          queue,
          Some(BlockTransformed(player, Some(Right(None)), blockTransform))
        )
      case ((Left(WaitBlockTransformFeedback(runningGame @ RunningGame(_, _, _, _, _, _, _))), _, _),
        Left(BlockTransformFeedback(_, newTimerId))) =>
        newTimerId match {
          case Some(_) =>
            Tuple3(Right(runningGame.copy(currentTimerId = newTimerId)), queue, None)
          case None =>
            Tuple3(Right(runningGame), queue, None)
        }
      // 새 벽돌을 만들어야 할 때는 항상 현재 벽돌과 Timer 설정이 없어야 한다.
      case ((Right(runningGame @ RunningGame(_, _, _, None, None, _, _)), _, _),
        Right(realizeNextBlock @ RealizeNextBlock(player))) =>
        import FP.{ StateFunctorSyntaxSugar, StateMonadSyntaxSugar }
        val compositedState =
          Util.pushToMap(clearedMap)
            .bind {
              case (a, b, c, true) =>
                Util.generateBlock[BT](fs.random, fs.generateBlockF, fs.rotateF, origin)
                  .fmap(RealizedNextBlock(player, _, a, b, c, realizeNextBlock))
                  // generateBlock 은 상태의 seed, 현재 벽돌, 다음 벽돌을 전부 바뀌어야 한다.
                  // 그래야 아래에서 예상낙하지점을 계산할 수 있다.
                  .passThrough(Util.expectedLanding)
              case (a, b, c, false) =>
                State(RealizedNextBlock(player, None, a, b, c, realizeNextBlock))
            }
            .fmap(Some(_))
        val ((newRunningGame, newQueue), result) = compositedState((runningGame, queue))
        Tuple3(Left(WaitRealizeNextBlockFeedback(newRunningGame)), newQueue, result)
      case ((Left(WaitRealizeNextBlockFeedback(runningGame @ RunningGame(_, _, _, currentBlock, None, _, _))), _, _),
        Left(RealizeNextBlockFeedback(player, newTimerIdAllocatedBlock))) =>
        (newTimerIdAllocatedBlock, currentBlock) match {
          case (Some((newTimerId, newCurrentBlock, newNext, newExpectedLanding)), Some((_, _))) =>
            // 새 벽돌 만들기를 성공했다는 판단은 앞 단계에서 이미 했다.
            // 이 단계는 ui 식별번호가 할당된 Block 으로 바꾸는 것만 한다.
            Tuple3(
              Right(
                runningGame.copy(
                  currentBlock = Some(newCurrentBlock, newExpectedLanding),
                  next = newNext,
                  currentTimerId = Some(newTimerId)
                )
              ),
              queue,
              None
            )
          case (None, None) =>
            // 새 벽돌 만들기를 시도하는 단계에서 실패할 때 StoppedGame 으로 상태를 바꾸는 방법도 있다.
            // 이 구상에서는, 여기서 응답을 받아서 게임 상태를 StoppedGame 으로 바꾼다.
            // 예상하고 구상한 것은 아니었는데, 응답을 받아서 게임 상태를 StoppedGame 으로 바꾸는 구상은
            // ui 식별번호 할당을 실패한 결과 게임을 중지해야 하는 경우에 대한 대안으로 쓸 수도 될 수 있다.
            // 이와 같이 하지 않고 RealizedNextBlock 출력을 만드는 시점에서 StoppedGame 으로 바꾼다면
            // WaitRealizeNextBlockFeedback(RunningGame) 일 때 RealizeNextBlockFeedback 을 받는 경우와
            // WaitRealizeNextBlockFeedback(StoppedGame) 일 때 RealizeNextBlockFeedback 을 받는 경우 두 개를 써야 한다.
            Tuple3(
              Right(
                StoppedGame(
                  player,
                  runningGame.gameMap,
                  runningGame.bePushed._2,
                  runningGame.next
                )
              ),
              queue,
              None
            )
        }
      case ((Right(runningGame @ RunningGame(_, _, _, currentBlock, _, _, _)), _, _),
        Right(push @ Push(player, rowCount, bePushed, isBroadcast))) =>
        val ((newRunningGame, newQueue), (beAllocated, moved, deleted)) =
          Util.pushToBePushed(clearedMap)(rowCount, bePushed)((runningGame, queue))
        Tuple3(
          Left(WaitPushFeedback(newRunningGame)),
          newQueue,
          Some(Pushed(player, beAllocated, moved, deleted, push))
        )
      case ((Left(WaitPushFeedback(runningGame @ RunningGame(_, _, bePushed, _, _, _, _))), _, _),
        Left(PushFeedback(player, allocatedBePushed))) =>
        allocatedBePushed match {
          case Some(allocatedBePushed) =>
            Tuple3(
              Right(
                runningGame.copy(
                  bePushed =
                    Tuple2(
                      bePushed._1,
                      Util.overlapWithBlockCellList(bePushed._2, allocatedBePushed)
                    )
                )
              ),
              queue,
              None
            )
          case None =>
            Tuple3(
              Right(
                StoppedGame(
                  player,
                  runningGame.gameMap,
                  runningGame.bePushed._2,
                  runningGame.next
                )
              ),
              queue,
              None
            )
        }
      // impo Stop 입력은 NotYetStartedGame/RunningGame 상태에서 받을 수 있어야 한다.
      // 게임 방에 입장하면 무조건 NotYetStartedGame 상태로 만들어 놓는 것이 좋겠다.
      // 따라서, 사용자2 가 입장했음을 알게 된 사용자1 의 화면에는 사용자2 가 RemotePlayer 로서 NotYetStartedGame 상태에 있게 된다.
      // 문제되는 것은 사용자1 은 입장 후 게임을 정상적으로 시작했고 사용자2 는 게임 시작 후 Start 입력이 도착하기 전에 퇴장한 경우이다.
      // 왜 문제되는가.
      // 사용자2 는 Start 입력을 처리하지 않기 때문에 자신의 RemotePlayer 들에게 Broadcast 로 전달될 Start 입력을 만들지 않는다.
      // 따라서 사용자1 의 화면에 사용자2 는 아무것도 하지 않는 것처럼 보이게 된다.
      // 어떻게 해결할 것인가.
      // Server 는 각 Client 와의 연결상태를 확인하여 끊기면 Stop 입력을 해주도록 하는 것이 좋겠다.
      // 이와 같이 하려면 NotYetStartedGame 상태에 있어도 Stop 입력을 받을 수 있도록 해야 한다.
      // 지금까지의 구상에 따르면 StoppedGame 상태는 항상 현재 게임 판, 쌓아 둔 줄, 다음 벽돌을 다루도록 했는데,
      // 아직 시작도 하지 않은 게임에서 이와 같은 정보를 구할 수는 없다.
      // 따라서, 아무것도 없는 빈칸이 되도록 하고, 특히 다음 벽돌의 경우 0 으로 벽돌을 만들어서 next 에 그려넣는 것이 좋겠다.
      // 그런데 일반적으로 게임 시작 후 게임 중지상태에 이르게 되더라도, 게임 안의 벽돌에 아무런 변화가 없기 때문에 따로 그릴 것이 없다.
      // 그래서, 일반적인 기능을 그대로 따르면, 시작하지 않은 게임이 중지되면 화면에는 아무것도 그려지지 않는 결론이 된다.
      case ((Right(runningGame @ RunningGame(_, _, _, _, _, _, _)), _, _),
        Right(stop @ Stop(player, reason))) =>
        // 새 벽돌 만들기를 실패했을 때 게임을 중지 상태로 바꾸기를, 응답을 받는 단계에서 하는 것과 짝을 맞추기 위해서
        // 명시적으로 정지입력을 받았을 때 게임을 중지 상태로 바꾸는 것도 응답을 받는 단계에서 하도록 한다.
        // 한 쪽에서는 이렇게 하고 다른 쪽에서는 저렇게 하면 햇갈린다.
        Tuple3(
          Left(WaitStopFeedback(runningGame)),
          queue,
          Some(Stopped(player, reason, stop))
        )
      case ((Left(WaitStopFeedback(runningGame @ RunningGame(_, _, _, _, _, _, _))), _, _),
        Left(StopFeedback(player))) =>
        Tuple3(
          Right(
            StoppedGame(
              player,
              runningGame.gameMap,
              runningGame.bePushed._2,
              runningGame.next
            )
          ),
          queue,
          None
        )
      case ((Right(notYetStartedGame @ NotYetStartedGame(_)), _, _),
        Right(stop @ Stop(player, reason))) =>
        // 새 벽돌 만들기를 실패했을 때 게임을 중지 상태로 바꾸기를, 응답을 받는 단계에서 하는 것과 짝을 맞추기 위해서
        // 명시적으로 정지입력을 받았을 때 게임을 중지 상태로 바꾸는 것도 응답을 받는 단계에서 하도록 한다.
        // 한 쪽에서는 이렇게 하고 다른 쪽에서는 저렇게 하면 햇갈린다.
        Tuple3(
          Left(WaitStopFeedback(notYetStartedGame)),
          queue,
          Some(Stopped(player, reason, stop))
        )
      case ((Left(WaitStopFeedback(notYetStartedGame @ NotYetStartedGame(_))), _, _),
        Left(StopFeedback(player))) =>
        Tuple3(
          Right(
            StoppedGame(
              player,
              clearedMap,
              clearedMap,
              fs.generateBlockF(0)
            )
          ),
          queue,
          None
        )
    }
  }

  def mapF[BT](fsmState: FSMState[BT]): Observable[(StreamOutput[BT], LogS)] =
    fsmState match {
      case (_, queue, Some(streamOutput)) => Observable.just((streamOutput, queue))
      case (_, queue, None) => Observable.just((NothingToDo[BT], queue))
    }

  def filterF[BT](player: Player)(input: FSMInput[BT]): Boolean =
    input match {
      case Right(Start(currPlayer, _)) => player == currPlayer
      case Right(BlockTransform(currPlayer, _)) => player == currPlayer
      case Right(RealizeNextBlock(currPlayer)) => player == currPlayer
      case Right(Stop(currPlayer, _)) => player == currPlayer
      case Right(Push(currPlayer, _, _, _)) => player == currPlayer
      case Right(Prepare(currPlayer)) => player == currPlayer
      case Right(Cancel(currPlayer)) => player == currPlayer
      case Right(Leave(currPlayer)) => player == currPlayer
      case Left(StartFeedback(currPlayer)) => player == currPlayer
      case Left(BlockTransformFeedback(currPlayer, _)) => player == currPlayer
      case Left(RealizeNextBlockFeedback(currPlayer, _)) => player == currPlayer
      case Left(StopFeedback(currPlayer)) => player == currPlayer
      case Left(PushFeedback(currPlayer, _)) => player == currPlayer
      case Left(PrepareFeedback(currPlayer)) => player == currPlayer
      case Left(CancelFeedback(currPlayer)) => player == currPlayer
      case Left(LeaveFeedback(currPlayer)) => player == currPlayer
    }

  //  val o: Observable[FSMInput] = Observable.empty
  //  val player = LocalPlayer(1, "1")
  //  val state: FSMState = (Right(NotYetStartedGame(player)), Queue(), None)
  //  o.filter(filterF(player)).scan(state)(scanF(Map(), FunctionHolder(???, ???, ???))).flatMap(mapF)

}
