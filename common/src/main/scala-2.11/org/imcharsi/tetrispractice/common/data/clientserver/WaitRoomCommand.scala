/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.clientserver

import org.imcharsi.tetrispractice.common.data.TransferDirection.{ ServerToClient, ClientToServer }

/**
 * Created by i on 9/3/16.
 */
object WaitRoomCommand {

  sealed trait WaitRoomRequest extends ClientToServer

  sealed trait WaitRoomResponse extends ServerToClient

  // 구상에 따라 사용자 기능을 만들지 않기로 했으므로, 매 게임 방 입장마다 각 사용자를 구분하는 식별번호를 새로 만든다.
  // 이 사용자 식별번호는 게임 방에 대해 고유하므로, 새로운 게임 방을 만들 때 같은 식별번호를 받을 수 있다.
  // 구상에 따라, 방 개설 따로 방 입장 따로 하기로 했다. 따라서 방장은 방을 만든 후, 방에 입장해야 한다.
  case object CreateRoom extends WaitRoomRequest

  // 개설하면 개설자는 식별번호를 받는 것이 아니라 암호를 받는다.
  // 이 암호로 개설자 입장을 한 뒤, 입장이 성공하면 응답으로서 식별번호를 받는다.
  sealed case class CreatedRoom(password: Int, roomId: Int) extends WaitRoomResponse

  // 게임을 시작했거나 방을 폐쇄하면 목록에서 지운다.
  sealed case class RemoveFromWaitRoomList(roomId: Int) extends WaitRoomRequest

  // 게임을 아직 시작하지 않았고 인원 구성이 바뀌면 목록에서 자료를 갱신한다.
  sealed case class UpdateWaitRoom(roomId: Int, playerCount: Int) extends WaitRoomRequest

  case object GetRoomList extends WaitRoomRequest

  case class GotRoomList(roomMap: Map[Int, Int]) extends WaitRoomResponse
}
