/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.{ RightToDownToLeft, LeftToDowToRight, RotateDirection }
import spray.json._

/**
 * Created by i on 9/9/16.
 */
object RotateDirectionJsonFormat extends RootJsonFormat[RotateDirection] with SprayJsonSupport {
  override def read(json: JsValue): RotateDirection = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == LeftToDowToRight.toString => LeftToDowToRight
      case Some(JsString(clazz)) if clazz == RightToDownToLeft.toString => RightToDownToLeft
      case _ => deserializationError("")
    }
  }

  override def write(obj: RotateDirection): JsValue = {
    obj match {
      case obj @ LeftToDowToRight => JsObject(Map(("clazz" -> JsString(LeftToDowToRight.toString))))
      case obj @ RightToDownToLeft => JsObject(Map(("clazz" -> JsString(RightToDownToLeft.toString))))
    }
  }
}
