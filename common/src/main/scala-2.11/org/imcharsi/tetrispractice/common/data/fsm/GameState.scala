/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

import org.imcharsi.tetrispractice.common.data.fsm.Player.Player

/**
 * Created by i on 8/25/16.
 */
object GameState {

  sealed trait GameState[BT]

  sealed trait WaitFeedback[BT]

  // 방 안의 모든 사용자가 준비완료됐을 때 게임을 시작할 수 있기 위해서는 아직 준비완료가 되지 않았음을 구분하는 상태도 필요하다.
  // todo 아직 scanF 에서 이 기능을 다루지 않았다.
  // NotYetPreparedGame/NotYetStartedGame 에서 가능한 입력으로 Leave 가 있고
  // NotYetStartedGame 에서 가능한 입력으로 Cancel 이 있다.
  // 구상에 따를 때, NotYetStartedGame 상태에서 Stop 입력을 받을 수 있다.
  // 세 사용자가 있고 모두 준비완료하여 방장이 게임을 시작하였는데, 사용자3 의 연결이 끊긴 경우
  // 결국 나머지 사용자의 사용자3 RemotePlayer 는 사용자3 LocalPlayer 의 Start 입력을 Server 로부터 중계받지 못하여
  // 여전히 NotYetStartedGame 상태에 있게 된다. 이 상태에서 Server 가 사용자3 의 연결 끊김을 인식하여
  // Server 가 직접 모든 사용자3 RemotePlayer 로 Stop 입력을 해주게 된다.
  sealed case class NotYetPreparedGame[BT](player: Player) extends GameState[BT]

  sealed case class NotYetStartedGame[BT](player: Player) extends GameState[BT]

  // 현재 내려오는 벽돌과 예상낙하지점의 벽돌을 나타낸다.
  type CurrentBlock[BT] = Option[(Block[BT], Block[BT])]

  // impo 이 구상에 따르면, 처음부터 gameMap 과 currentBlock 이 겹치는 상태가 만들어지는 것을 막을 방법이 없다.
  // 그래서 대안은, moveBlockOneStep 에서 gameMap 의, 각 이동 시작 위치에 벽돌이 있는지 판단하여 있으면 게임을 중지하는 판단을 한다.
  // 구독단계에서 이 판단을 확인하여 Stop 입력을 해준다.
  // 이와 같이 해도 완전하진 않은데, 예를 들어 회전을 시도하면 실패하는 것은 맞지만 회전을 시도한 시점에서 중지의 판단을 하는 것이 아니고
  // 회전 이후 시간초과로 이동을 하거나 직접 이동을 하는 등 이동의 실패가 있을 때 중지의 판단을 하게 된다.
  sealed case class RunningGame[BT](
    player: Player,
    gameMap: GameMap[BT],
    // 쌓아둔 줄의 수를 미리 세어두면 게임 판으로 밀어넣을 때 따로 줄 수를 셀 필요가 없어서 편하다.
    bePushed: (Int, GameMap[BT]),
    // fixme 이 설명은 다른 데 써야 한다.
    // 벽돌이 바닥에 닿았으면, 현재 벽돌은 없는 상태로 바꾼다.
    // 구상은, WaitMoveFeedback 으로 바꾸고 Stream 으로 Moved 를 출력하고
    // Side-Effect 에서 MoveFeedback 에 이어 RealizeNextBlock 을 Stream 으로 입력해준다.
    // FSM 에서는 WaitMoveFeedback 일 때 MoveFeedback 을 받아서 상태를 RunningGame 상태로 바꾸고
    // 이어서 RunningGame 일 때 RealizeNextBlock 을 받으면
    // 새 벽돌을 계산하여 WaitAllocation 상태로 바꾸고 RealizedNextBlock 을 출력하고
    // Side-Effect 에서 ui 식별번호를 할당하여 RealizeNextBlockFeedback 을 Stream 으로 입력해주면
    // FSM 에서 WaitRealizeNextBlockFeedback 일 때 RealizeNextBlockFeedback 을 받아서
    // currentBlock 을 바꾸고 다시 RunningGame 상태로 바꾼다.
    // 두번째 항목은, 예상낙하지점의 벽돌을 나타낸다. 벽돌을 두개를 써야 한다.
    // 현재 내려오는 벽돌의 위치, 벽돌의 모양, 예상낙하지점에 그릴 벽돌을 나타낸다.
    currentBlock: CurrentBlock[BT],
    // 이 항목은 무슨 용도인가.
    // Timer event 는 취소를 시도하기 전에 이미 발생되었을 수 있다.
    // 발생되지 말아야 하는 Timer event 를 받은 경우 어떻게 무시할 것인지 문제된다.
    // 매 Timer event 마다 식별번호를 붙여서 동일한 식별번호가 왔을 때만
    // Timer event 로 인한 자동 이동을 처리하면 된다.
    // 한편, Timer event 를 무시할 때도 출력은 있어야 하는데, 어떻게 출력할 것인가.
    // 아무런 이동/삭제/새로 만들기를 하지 않는 출력을 하면 되겠다.
    // 아래로 이동이거나 Timer event 로 인한 이동일 때만 Timer event 를 갱신한다.
    currentTimerId: Option[Int],
    seed: Int,
    next: Block[BT]
  ) extends GameState[BT]

  sealed case class StoppedGame[BT](
    player: Player,
    map: GameMap[BT],
    bePushed: GameMap[BT],
    next: Block[BT]
  ) extends GameState[BT]

  // 이와 같은 용법은 FP 에서는 가능하지 않다. 실용적인 관점에서 이와 같이 한다.
  // 만약 FP 와 같은 방식으로 한다면, 내부 자료를 꺼내는 단계에서 pattern-match 를 해야 한다.
  // 할당을 기다리는 단계는 구체적이라서 pattern-match 를 할 필요가 없다.
  sealed case class WaitRealizeNextBlockFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]

  sealed case class WaitBlockTransformFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]

  sealed case class WaitStartFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]

  sealed case class WaitStopFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]

  sealed case class WaitPushFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]

  sealed case class WaitPrepareFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]

  sealed case class WaitCancelFeedback[BT](gameState: GameState[BT]) extends WaitFeedback[BT]
}
