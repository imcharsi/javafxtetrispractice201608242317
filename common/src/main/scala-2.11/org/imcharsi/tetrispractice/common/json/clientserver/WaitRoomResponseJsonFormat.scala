/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.clientserver

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ GotRoomList, CreatedRoom, WaitRoomResponse }
import spray.json._

/**
 * Created by i on 9/9/16.
 */
object WaitRoomResponseJsonFormat extends RootJsonFormat[WaitRoomResponse] with SprayJsonSupport {

  import DefaultJsonProtocol._

  private implicit val createdRoomJsonFormat = jsonFormat2(CreatedRoom)

  override def read(json: JsValue): WaitRoomResponse = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == CreatedRoom.toString() =>
        JsObject(fields - "clazz").convertTo[CreatedRoom]
      case Some(JsString(clazz)) if clazz == GotRoomList.toString() =>
        GotRoomList(fields("roomMap").convertTo[List[(Int, Int)]].toMap)
      case _ => deserializationError("")
    }
  }

  override def write(obj: WaitRoomResponse): JsValue = {
    obj match {
      case obj @ CreatedRoom(_, _) =>
        JsObject(obj.toJson.asJsObject.fields + ("clazz" -> JsString(CreatedRoom.toString())))
      case obj @ GotRoomList(_) =>
        JsObject(Map(("roomMap" -> obj.roomMap.toList.toJson)) + ("clazz" -> JsString(GotRoomList.toString())))
      case _ => serializationError("")
    }
  }
}
