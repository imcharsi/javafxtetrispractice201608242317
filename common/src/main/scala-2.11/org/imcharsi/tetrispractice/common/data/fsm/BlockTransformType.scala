/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

/**
 * Created by i on 8/25/16.
 */
object BlockTransformType {

  sealed trait RotateDirection

  case object RightToDownToLeft extends RotateDirection

  case object LeftToDowToRight extends RotateDirection

  sealed trait BlockTransformType

  case object Left extends BlockTransformType

  case object Right extends BlockTransformType

  case object Down extends BlockTransformType

  case object Bottom extends BlockTransformType

  // 회전에 관해 따로 입력을 둘 수도 있지만 이와 같이 해도 된다.
  // 왜냐하면, 회전과 이동 둘 다 벽돌의 이동/삭제의 조합으로 결과가 만들어지기 때문이다.
  sealed case class Rotate(rotateDirection: RotateDirection) extends BlockTransformType

  // RemotePlayer 는 Timer 를 쓰지 않는다.
  // 이 말은 RemotePlayer 에서 만든 출력을 구독했을 때 Timer 설정을 하지 않는다는 말이다.
  sealed case class TimeElapsed(timerId: Int) extends BlockTransformType

}
