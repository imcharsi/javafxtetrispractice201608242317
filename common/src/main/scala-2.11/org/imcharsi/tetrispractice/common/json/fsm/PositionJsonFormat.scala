/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.Position
import spray.json._

/**
 * Created by i on 9/8/16.
 */
object PositionJsonFormat extends RootJsonFormat[Position] with SprayJsonSupport {

  import spray.json.DefaultJsonProtocol._

  private implicit val jsonFormat = jsonFormat2(Position)

  override def write(obj: Position): JsValue =
    JsObject(
      obj.toJson.asJsObject.fields + ("clazz" -> JsString(Position.toString()))
    )

  override def read(json: JsValue): Position = {
    json match {
      case JsObject(fields) if fields.exists(_ == ("clazz" -> JsString(Position.toString()))) =>
        JsObject(fields - "clazz").convertTo[Position]
      case _ => deserializationError("")
    }
  }
}
