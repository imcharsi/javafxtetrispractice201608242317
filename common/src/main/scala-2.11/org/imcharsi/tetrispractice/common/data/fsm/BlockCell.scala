/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

/**
 * Created by i on 8/25/16.
 */
object BlockCell {

  // todo 이와 같이 하면 여러 가지 벽돌 집합을 바꿔 쓸 수 있지만, BlockCell 을 사용하는 모든 단계에서 type parameter 를 붙여야 한다. 번거로운 문제와 확장성 사이에서 절충을 해야 한다.
  sealed trait BlockCell[BT]

  sealed case class UninitializedBlockCell[BT](cellType: BT) extends BlockCell[BT]

  sealed case class InitializedBlockCell[BT](id: Int, cellType: BT) extends BlockCell[BT]

}
