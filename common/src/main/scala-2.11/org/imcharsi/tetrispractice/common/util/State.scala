/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

import org.imcharsi.tetrispractice.common.util.FP.CommonMonadBehavior

/**
 * Created by i on 8/25/16.
 */
object State {

  // type State[S, A] = S => (S, A) 와 같이 해봤으나,
  // Applicative Syntax Sugar 에서 막혔다. Compiler 가 State[S, A=>B] 의 A 의 형을 못찾는다.
  sealed case class State[S, A](f: S => (S, A)) {
    def apply(s: S): (S, A) = f(s)
  }

  def apply[S, A](a: A): State[S, A] = State(s => (s, a))

  class StateMonadBehavior[S] extends CommonMonadBehavior[({ type X[A] = State[S, A] })#X] {
    override def unit[A](a: A): State[S, A] = State(s => (s, a))

    override def bind[A, B](f: (A) => State[S, B]): (State[S, A]) => State[S, B] =
      fa => State {
        s1 =>
          val (s2, a) = fa.f(s1)
          f(a).f(s2)
      }
  }

  def recover[S, A, B](recoverF: (S, S, A) => (S, B))(stateA: State[S, A]): State[S, B] =
    State { s1 =>
      val (s2, a) = stateA.f(s1)
      recoverF(s1, s2, a)
    }

  implicit class StateRecoverSyntaxSugar[S, A](fsa: State[S, A]) {
    def recover[B](recoverF: (S, S, A) => (S, B)): State[S, B] =
      // 외부 object State 와 내부 case class State 의 이름이 같아서 전체 이름을 써야 한다.
      org.imcharsi.tetrispractice.common.util.State.recover(recoverF)(fsa)
  }
}
