/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.Command._
import spray.json._

/**
 * Created by i on 9/9/16.
 */
class StreamInputJsonFormat[BT](implicit blockTypeJsonFormat: JsonFormat[BT]) extends RootJsonFormat[StreamInput[BT]] with SprayJsonSupport {

  import spray.json.DefaultJsonProtocol._

  private implicit val positionJsonFormat = PositionJsonFormat
  private implicit val playerJsonFormat = PlayerJsonFormat
  private implicit val blockTransformTypeJsonFormat = BlockTransformTypeJsonFormat
  private implicit val blockCellJsonFormat = new BlockCellJsonFormat[BT]()(blockTypeJsonFormat)
  private implicit val prepareJsonFormat = jsonFormat1(Prepare[BT])
  private implicit val cancelJsonFormat = jsonFormat1(Cancel[BT])
  private implicit val startJsonFormat = jsonFormat2(Start[BT])
  private implicit val blockTransformJsonFormat = jsonFormat2(BlockTransform[BT])
  private implicit val pushJsonFormat = jsonFormat4(Push[BT])
  private implicit val preparePushJsonFormat = jsonFormat3(PreparePush[BT])
  private implicit val realizeNextBlockJsonFormat = jsonFormat1(RealizeNextBlock[BT])
  private implicit val stopJsonFormat = jsonFormat2(Stop[BT])
  private implicit val leaveJsonFormat = jsonFormat1(Leave[BT])

  override def read(json: JsValue): StreamInput[BT] = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == Prepare.toString =>
        JsObject(fields - "clazz").convertTo[Prepare[BT]]
      case Some(JsString(clazz)) if clazz == Cancel.toString =>
        JsObject(fields - "clazz").convertTo[Cancel[BT]]
      case Some(JsString(clazz)) if clazz == Start.toString =>
        JsObject(fields - "clazz").convertTo[Start[BT]]
      case Some(JsString(clazz)) if clazz == BlockTransform.toString =>
        JsObject(fields - "clazz").convertTo[BlockTransform[BT]]
      case Some(JsString(clazz)) if clazz == Push.toString =>
        JsObject(fields - "clazz").convertTo[Push[BT]]
      case Some(JsString(clazz)) if clazz == PreparePush.toString =>
        JsObject(fields - "clazz").convertTo[PreparePush[BT]]
      case Some(JsString(clazz)) if clazz == RealizeNextBlock.toString =>
        JsObject(fields - "clazz").convertTo[RealizeNextBlock[BT]]
      case Some(JsString(clazz)) if clazz == Stop.toString =>
        JsObject(fields - "clazz").convertTo[Stop[BT]]
      case Some(JsString(clazz)) if clazz == Leave.toString =>
        JsObject(fields - "clazz").convertTo[Leave[BT]]
      case _ => deserializationError("")
    }
  }

  def clazzAppend(map: Map[String, JsValue])(clazz: String): JsValue =
    JsObject(map + ("clazz" -> JsString(clazz)))

  override def write(obj: StreamInput[BT]): JsValue = {
    obj match {
      case obj @ Prepare(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(Prepare.toString)
      case obj @ Cancel(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(Cancel.toString)
      case obj @ Start(_, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(Start.toString)
      case obj @ BlockTransform(_, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(BlockTransform.toString)
      case obj @ Push(_, _, _, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(Push.toString)
      case obj @ PreparePush(_, _, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(PreparePush.toString)
      case obj @ RealizeNextBlock(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(RealizeNextBlock.toString)
      case obj @ Stop(_, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(Stop.toString)
      case obj @ Leave(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(Leave.toString)
      case _ => serializationError("")
    }
  }
}
