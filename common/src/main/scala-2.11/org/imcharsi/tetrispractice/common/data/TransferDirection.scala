/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data

/**
 * Created by i on 8/25/16.
 */
object TransferDirection {

  // marker interface 이다. 실제 전송에 앞서서 queue 에 쌓을 수 있는데,
  // queue 에 쌓을 수 있는 것과 그렇지 않을 것을 구분하려고 쓴다.
  // 또한, 실제로 전송 단위에만 붙인다. 전송내용을 이루긴 하지만 단독으로 전송되지 않는 자료에는 붙이지 않는다.
  sealed trait TransferDirection

  trait ServerToClient extends TransferDirection

  trait ClientToServer extends TransferDirection

}
