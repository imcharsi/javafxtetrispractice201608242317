/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

/**
 * Created by i on 8/25/16.
 */
object FP {

  trait FunctorBehavior[F[_]] {
    def fmap[A, B](f: A => B): F[A] => F[B]
  }

  trait ApplicativeBehavior[F[_]] {
    def app[A, B](f: F[A => B]): F[A] => F[B]
  }

  trait MonadBehavior[F[_]] {
    def unit[A](a: A): F[A]

    def bind[A, B](f: A => F[B]): F[A] => F[B]

    def apply[A](a: A): F[A] = unit(a)
  }

  trait CommonMonadBehavior[F[_]] extends MonadBehavior[F] with FunctorBehavior[F] with ApplicativeBehavior[F] {
    override def fmap[A, B](f: (A) => B): (F[A]) => F[B] =
      fa => bind(f.andThen(unit))(fa)

    override def app[A, B](f: F[(A) => B]): (F[A]) => F[B] =
      fa => bind((f: A => B) => fmap(f)(fa))(f)
  }

  implicit class MonadSyntaxSugar[F[_], A](fa: F[A])(implicit monadBehavior: MonadBehavior[F]) {
    // fa 무시하고 fb 로 바꾸는 의미의 기호를 >> 로 하는지는 모르겠다.
    def >>[B](fb: F[B]): F[B] = monadBehavior.bind((_: A) => fb)(fa)

    def bind[B](f: A => F[B]): F[B] = monadBehavior.bind(f)(fa)
  }

  implicit class FunctorSyntaxSugar[F[_], A](fa: F[A])(implicit functorBehavior: FunctorBehavior[F]) {
    def fmap[B](f: A => B): F[B] = functorBehavior.fmap(f)(fa)
  }

  implicit class ApplicativeSyntaxSugar[F[_], A, B](f: F[A => B])(implicit applicativeBehavior: ApplicativeBehavior[F]) {
    def <*>(fa: F[A]): F[B] = applicativeBehavior.app(f)(fa)
  }

  implicit class StateMonadSyntaxSugar[F[_, _], S, A](fsa: F[S, A])(implicit monadBehavior: MonadBehavior[({ type X[Y] = F[S, Y] })#X]) {
    def >>[B](fsb: F[S, B]): F[S, B] = monadBehavior.bind[A, B]((_: A) => fsb)(fsa)

    def passThrough[B](fsb: F[S, B]): F[S, A] = monadBehavior.bind[A, A]((a: A) => fsb.bind(_ => monadBehavior.unit(a)))(fsa)

    def bind[C](f: A => F[S, C]): F[S, C] = monadBehavior.bind(f)(fsa)
  }

  implicit class StateApplicativeSyntaxSugar[F[_, _], S, A, B](fsab: F[S, A => B])(implicit applicativeBehavior: ApplicativeBehavior[({ type X[Y] = F[S, Y] })#X]) {
    def <*>(fa: F[S, A]): F[S, B] = applicativeBehavior.app(fsab)(fa)
  }

  implicit class StateFunctorSyntaxSugar[F[_, _], S, A](fsa: F[S, A])(implicit functorBehavior: FunctorBehavior[({ type X[Y] = F[S, Y] })#X]) {
    def fmap[B](f: A => B): F[S, B] = functorBehavior.fmap(f)(fsa)
  }
}
