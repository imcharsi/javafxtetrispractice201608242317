/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.fsm

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.fsm.{ Position, Block }
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.BlockCell
import spray.json._

/**
 * Created by i on 9/9/16.
 */
class BlockJsonFormat[BT](implicit blockCellJsonFormat: JsonFormat[BlockCell[BT]]) extends RootJsonFormat[Block[BT]] with SprayJsonSupport {

  import DefaultJsonProtocol._

  private implicit val positionJsonFormat = PositionJsonFormat
  private implicit val blockJsonFormat = jsonFormat2(Block[BT])

  override def write(obj: Block[BT]): JsValue = {
    val position = obj.position.toJson
    val map = obj.map.toList.toJson
    JsObject(Map(("position" -> position), ("map" -> map), ("clazz" -> JsString(Block.toString))))
  }

  override def read(json: JsValue): Block[BT] = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == Block.toString =>
        Block(
          fields("position").convertTo[Position],
          fields("map").convertTo[List[(Position, BlockCell[BT])]].toMap
        )
      case _ => deserializationError("")
    }
  }
}
