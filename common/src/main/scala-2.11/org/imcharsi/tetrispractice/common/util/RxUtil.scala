/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

import javafx.beans.binding.ObjectBinding
import javafx.beans.value.ObservableObjectValue

import rx.lang.scala.{ Observable, Observer, Subscription }

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 9/11/16.
 */
object RxUtil {
  def convertJavaFXProperty[A, B](f: A => B)(p: ObservableObjectValue[A]) =
    new ObjectBinding[Option[B]] {
      bind(p)

      override def computeValue(): Option[B] = {
        Try {
          f(p.get())
        } match {
          case Success(n) => Some(n)
          case Failure(ex) => None
        }
      }
    }

  def combine[A, B](observableA: Observable[A], observableB: Observable[B]): Observable[(A, Option[B])] = {
    var cacheB: Option[B] = None
    def fx(observer: Observer[(A, Option[B])]): Subscription = {
      def onNextA(a: A): Unit = observer.onNext((a, cacheB))
      def onNextB(b: B): Unit = cacheB = Some(b)
      val subscriptionA = observableA.subscribe(onNextA, observer.onError, observer.onCompleted)
      val subscriptionB = observableB.subscribe(onNextB, observer.onError, observer.onCompleted)
      Subscription {
        subscriptionA.unsubscribe()
        subscriptionB.unsubscribe()
      }
    }
    Observable.create(fx)
  }
}
