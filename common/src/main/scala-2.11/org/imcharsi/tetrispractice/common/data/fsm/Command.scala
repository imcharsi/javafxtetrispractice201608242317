/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

import org.imcharsi.tetrispractice.common.data.TransferDirection.{ ClientToServer, ServerToClient }
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.BlockCell
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.BlockTransformType
import org.imcharsi.tetrispractice.common.data.fsm.GameState.CurrentBlock
import org.imcharsi.tetrispractice.common.data.fsm.Player.Player

/**
 * Created by i on 8/25/16.
 */
object Command {

  type BlockCellList[BT] = List[(Position, BlockCell[BT])]

  // impo 여러 구상들이, 망 연결 상태에 대해 아주 낙관적인 가정을 하고 있다.

  sealed trait StreamInput[BT] extends ServerToClient with ClientToServer

  sealed trait StreamOutput[BT]

  // 왜 StreamFeedback 을 받는가. 출력에 대한 응답을 받아서 상태를 갱신해야 하는 경우가 있다.
  // 대표적인 예가 새 벽돌 만들기이다. 새 벽돌 각각에 고유번호를 붙여야 하는데, 밖에서 할당받는 것이 더 낫다.
  // 가능성은 높지 않지만 안에서 써서 밖으로 보냈는데 밖에서 해당 번호를 쓸 수 없는 경우도 있을 수 있기 때문이다.
  // 이와 같은 구성을 할 경우, FSM 이, StreamFeedback 을 받기 전에 StreamInput 을 받지 않도록 순서를 조정해야 하는 기능은 필수이다.
  sealed trait StreamFeedback[BT]

  case class NothingToDo[BT]() extends StreamOutput[BT]

  sealed case class Prepare[BT](player: Player) extends StreamInput[BT]

  sealed case class Prepared[BT](player: Player, result: Boolean, prepare: Prepare[BT]) extends StreamOutput[BT]

  sealed case class PrepareFeedback[BT](player: Player) extends StreamFeedback[BT]

  sealed case class Cancel[BT](player: Player) extends StreamInput[BT]

  sealed case class Cancelled[BT](player: Player, result: Boolean, cancel: Cancel[BT]) extends StreamOutput[BT]

  sealed case class CancelFeedback[BT](player: Player) extends StreamFeedback[BT]

  sealed case class Start[BT](player: Player, seed: Int) extends StreamInput[BT]

  // Stream 출력을 받은 구독 단계에서 Player 를 구분하여
  // LocalPlayer 이면 입력을 Server 로 보내서 Broadcast 를 하도록 한다.
  // 내가 보낸 자료를 Server 에 의한 Broadcast 로 중복해서 다시 받지 않도록 하는 과정을
  // Server 에 두거나 Client 에 두거나 선택할 수 있는데, 일단 Server 에 두는 구상을 하기.
  // RemotePlayer 이면 구독 단계에서 Server 로 아무것도 보내지 않는다.
  // 이미 Server 로부터 받은 입력에 대한 처리이기 때문이다.
  sealed case class Started[BT](
    player: Player,
    result: Boolean, // 이미 시작했는데 또 시작할 수는 없다. 실패여부를 나타내야 한다.
    start: Start[BT]
  ) extends StreamOutput[BT]

  sealed case class StartFeedback[BT](player: Player) extends StreamFeedback[BT]

  sealed case class BlockTransform[BT](
    player: Player,
    direction: BlockTransformType
  ) extends StreamInput[BT]

  sealed case class BlockTransformed[BT](
    player: Player,
    // 이 구상에서는, 벽돌을 아래로 움직이는 시도를 해봐서 성공하면 움직인 것이고,
    // 실패하면 바닥에 닿은 것으로 본다. 바닥에 닿으면 줄 깨기/줄 모으기를 한다.
    // 그래서, 벽돌의 아래방향을 제외한 이동/회전이 성공하면 Right(Some(...)) 이 되고 실패하면 Right(None) 이 된다.
    // 벽돌을 아래방향으로 이동해서 성공하면 Right(Some(...)) 이 되고 실패하면 바닥에 닿은 것이 되어
    // 결과는 Left(List(...), List(...), List(...)) 가 된다.
    // 첫째 List 는 줄을 깨고 남은 게임 판 안의 벽돌 이동을 나타내고
    // 둘째 List 는 깨진 줄을 나타낸다. 깨진 줄은 화면에서 지워야 한다.
    // 셋째 List 는 상대방에게 넘어갈 줄을 나타낸다. 이 입력을 받아서 구독단계에서 Server 로 Push 입력까지 이어서 해준다.
    // 구상이 하나 더 늘었다.
    // RunningGame 의 gameMap 과 currentBlock 이 처음부터 겹치도록 만들어졌다면 게임을 중지해야 한다.
    // 가장 바깥의 Option 이 이에 대한 판단이다. 구독 단계에서 result==None 임을 확인하면 Stop 입력을 해준다.
    result: Option[Either[(List[BlockCellList[BT]], List[BlockCellList[BT]], List[BlockCellList[BT]]), CurrentBlock[BT]]],
    move: BlockTransform[BT]
  ) extends StreamOutput[BT]

  sealed case class BlockTransformFeedback[BT](
    player: Player,
    // 아래방향으로 이동할 때마다, 새 벽돌을 만들 때마다 시간초과설정을 갱신한다.
    newTimerId: Option[Int]
  ) extends StreamFeedback[BT]

  // 앞의 설명은 문제를 잘못 짚었고 그래서 설명도 잘못 쓴 듯 하다.
  // PreparePush 입력을 두지 않고 Push 만으로 입력을 구성하면 아래와 같은 자료의 전달 순서가 생길 수 있다.
  // Client1     Client2     Client3     Server
  //             L2:RNB->
  // L1:Push->
  //                                     L1:Push->
  //                                     L2:RNB->
  //                                     <-R2:Push
  // R2:Push<-               R2:Push<-
  //                                     <-L2:Push
  //             L2:Push<-
  // R2:RNB<-                R2:RNB<-
  // 이 경우, 순서가 바뀌는 문제가 생긴다. 입력의 구성 자체에서 아래의 원칙에 맞지 않는 문제가 있는 것이다.
  // 그래서 PreparePush 를 쓰면 아래와 같이 자료를 전달하게 된다.
  // Client1     Client2     Client3     Server
  //             L2:RNB->
  // L1:PP->
  //                                     L1:PP->
  //                                     L2:RNB->*
  //                                     <-L2:PP
  //                                     <-R2:RNB
  //             L2:PP<-
  // R2:RNB<-                R2:RNB<-
  //             L2:Push->
  //                                     L2:Push->*
  //                                     <-R2:Push
  // R2:Push<-               R2:Push<-
  // PreparePush 입력의 역할은, LocalPlayer FSM 으로 하여금 LocalPlayer Push 입력을 Server 로 전달하도록 하는 것이다.
  // 마치, 사용자가 Keyboard 등을 입력해서 만들어진 LocalPlayer BlockTransform 이 아니라
  // Server 에 의해 LocalPlayer BlockTransform 입력이 만들어지는 것과 똑같은 경우라 할 수 있다.
  // 순서에 영향을 준 입력은, L2:RNB->* 와 L2:Push->* 이다. 결론은 L2:Push->* 보다 L2:RNB->* 가 더 빨랐기 때문에,
  // RemotePlayer 에도 이와 같은 순서로 입력을 하게 된다. 아래의 경우도 위의 경우와 마찬가지이다.
  // Client1     Client2     Client3     Server
  //             L2:RNB->
  // L1:PP->
  //                                     L1:PP->
  //                                     <-L2:PP
  //             L2:PP<-
  //                                     L2:RNB->*
  //                                     <-R2:RNB
  //             L2:Push->
  // R2:RNB<-                R2:RNB<-
  //                                     L2:Push->*
  //                                     <-R2:Push
  // R2:Push<-               R2:Push<-
  // 한편, 아래의 그림처럼 L2:RNB->* 가 L2:Push->* 보다 늦게 도착하는 경우는 있을 수 없다.
  // Client1     Client2     Client3     Server
  //             L2:RNB->
  // L1:PP->
  //                                     L1:PP->
  //                                     <-L2:PP
  //             L2:PP<-
  //             L2:Push->
  //                                     L2:Push->*
  //                                     L2:RNB->*
  //                                     <-R2:Push
  // R2:Push<-               R2:Push<-
  //                                     <-R2:RNB
  // R2:RNB<-                R2:RNB<-
  // 왜냐하면, 자료는 WebSocket Stream 방식으로 전달되기 때문에
  // 최선의 경우 L2:RNB-> 의 누락이 있거나 아니면 더 나쁜 경우인 연결종료는 있을 수 있어도
  // L2:RNB-> 보다 먼저 L2:Push-> 가 Server 로 전달될 수는 없다.
  // 그래서 앞의 설명에서 썼던 것과 같은 극단적인 형태의 순서가 바뀌는 문제는 있을 수가 없는 것이다.
  // 이는 UDP/HTTP 등 비연결지향 방식에서나 있을 수 있다.
  // 첫 번째 그림의 경우가 문제되는 경우인데, 입력의 구성이 아래의 원칙에 맞도록 바꿔서 문제를 해결했다.
  // impo 요약하면, 동일한 사용자의 RemotePlayer 가 받는 입력의 순서는 동일한 사용자의 LocalPlayer 가 만든 출력에 의한다는 점이다.

  sealed case class Push[BT](
    player: Player,
    rowCount: Int,
    // bePushed 는 이미 RunningGame.bePushed 에 그대로 넣을 수 있게 자료가 갖춰졌다고 본다.
    bePushed: BlockCellList[BT],
    // Server 가 Broadcast 로 보낸 것인지 여부를 구분한다.
    // Broadcast 로 보낸 것이면 Pushed 구독 단계에서 Server 로 아무것도 보내지 않고
    // Broadcast 로 보낸 것이 아니면, 구독 단계에서 Server 로 isBroadcast 를 뒤집어서 다시 보낸다.
    // Push 뿐만 아니라, Start 를 제외한 모든 입력에서 입력을 보낸 Client 를 제외한 나머지로 Broadcast 를 해야 한다.
    isBroadcast: Boolean
  ) extends StreamInput[BT]

  sealed case class Pushed[BT](
    player: Player,
    // Server 로부터 전달받은 벽돌들은 다르게 말해서 없던 벽돌이 갑자기 생겼다는 말이다.
    // 따라서, ui 식별번호를 할당해야 한다.
    beAllocated: BlockCellList[BT],
    // 줄 쌓기는 바닥으로 밀어올리는 식으로 한다.
    // 가장 최근 것이 아래에 있게 되고 가장 오래된 것이 가장 위에 있게 된다. 그래서 이동을 그려야 한다.
    moved: BlockCellList[BT],
    // 넘치는 것은 지워야 한다.
    deleted: BlockCellList[BT],
    push: Push[BT]
  ) extends StreamOutput[BT]

  // ui 식별번호가 붙은 응답을 받아야 한다.
  sealed case class PushFeedback[BT](
    player: Player,
    // 할당이 실패할 수도 있다.
    allocatedBePushed: Option[BlockCellList[BT]]
  ) extends StreamFeedback[BT]

  // 이 기능은, LocalPlayer 가 깬 줄을 Server 로 보낼 때 쓴다. FSM 에서는 사용하지 않는다.
  // Server 는 이 입력을 받아서 어느 LocalPlayer 로 Push 입력을 할 지 판단하게 되고
  // LocalPlayer 가 이를 받아서 처리한 후 Server 로 Push 입력을 해주면
  // Server 가 이를 받아서 대응하는 RemotePlayer 로 Push Broadcast 를 해준다.
  // 몇 줄 이상을 깼을 때, Server 로 PreparePush 를 보낼 지는 FSM 에서 판단할 문제다.
  // 이 입력에 대한 응답은 없다.
  sealed case class PreparePush[BT](
    player: Player,
    rowCount: Int,
    bePushed: BlockCellList[BT]
  ) extends StreamInput[BT]

  // 이 입력은, RunningGame 의 next 를 currentBlock 으로 하고 새 next 를 계산하라는 명령이다.
  sealed case class RealizeNextBlock[BT](player: Player) extends StreamInput[BT]

  sealed case class RealizedNextBlock[BT](
    player: Player,
    // FSM 에서 새 벽돌 만들기를 시도하여 벽돌을 만들 수 없으면 게임이 끝났다고 판단하여 blockAndNext 는 None 이 된다.
    // 게임을 처음 시작하면 currentBlock 에는 아무것도 없고 next 만 계산이 되어 있는데
    // next 에는 ui 식별번호 할당이 되어 있지 않은 상태이다.
    // 이 때 새 벽돌 만들기를 시도하면 이미 만들어져 있던 next 가 새 currentBlock 이 되고
    // ui 식별번호가 할당되어 있지 않은 새 next 가 만들어진다.
    // 결론은, ui 식별번호를 할당해야 하는 Block 이 두개가 된다.
    // 한편, 게임 진행 중에는 currentBlock 과 next 가 모두 있고 ui 식별번호도 할당되어 있다.
    // 새 벽돌 만들기를 시도하면 next 가 currentBlock 이 되고
    // ui 식별번호가 할당되어 있지 않은 새 next 가 만들어진다.
    // 결론은, ui 식별번호를 할당해야 하는 Block 이 한개가 된다.
    // 그러나, 이번에 currentBlock 이 될 Block 의 ui 는 다음 벽돌을 그리는 자리에 그려져 있었으므로
    // 게임 판에서 그려지도록 위치를 바꿔야 한다.
    // 이상과 같은 처리를 출력을 받는 구독단계에서 해야 한다.
    // ((A,B),C) 에서 A 는 currentBlock, B 는 expectedLanding, C 는 next 이다.
    blockAndNext: Option[((Block[BT], Block[BT]), Block[BT])],
    // 새 벽돌 생성 바로 앞에서 쌓아둔 줄을 채워넣는다.
    // 채워넣기가 실패하면 새 벽돌 생성은 실패해야 한다.
    // 그래서, 있던 줄들이 위로 밀려올라가면서 이동이 있고
    moved: BlockCellList[BT],
    // 만약 넘치면 지워지는 것이 있고
    deleted: BlockCellList[BT],
    // 쌓아둔 줄은 전부 지워져야 한다.
    // 그런데, 이 항목의 벽돌들은 화면에서 지워지고 bePushed 에서도 지워지는 것은 맞지만,
    // 낭비를 줄이기 위해서 bePushed 사용된 ui 객체들을 게임 판으로 옮긴다.
    // 구상에 따르면, 이미 게임 판에 있던 것은 위로 올린다고 했다.
    // 따라서 bePushed 에 있는 줄 수만큼 위로 올릴 것이므로 게임 판에는 bePushed 에 있는 줄 수만큼의 공간이 생긴다.
    // 결론은 그대로 옮기면 된다.
    movedFromBePushedToMap: BlockCellList[BT],
    realizeNextBlock: RealizeNextBlock[BT]
  ) extends StreamOutput[BT]

  sealed case class RealizeNextBlockFeedback[BT](
    player: Player,
    // 첫번째는 새 시간초과설정이고
    // 두번째는 ui 식별번호가 할당된 currentBlock 이고
    // 세번째는 ui 식별번호가 할당된 next 이다.
    // 네번째는 ui 식별번호가 할당된 예상낙하지점 벽돌이다.
    newTimerIdAllocatedBlock: Option[(Int, Block[BT], Block[BT], Block[BT])]
  ) extends StreamFeedback[BT]

  sealed case class Stop[BT](player: Player, reason: String) extends StreamInput[BT]

  sealed case class Stopped[BT](
    player: Player,
    reason: String,
    stop: Stop[BT]
  ) extends StreamOutput[BT]

  sealed case class StopFeedback[BT](player: Player) extends StreamFeedback[BT]

  sealed case class Leave[BT](player: Player) extends StreamInput[BT]

  sealed case class Leaved[BT](player: Player, result: Boolean, leave: Leave[BT]) extends StreamOutput[BT]

  sealed case class LeaveFeedback[BT](player: Player) extends StreamFeedback[BT]
}
