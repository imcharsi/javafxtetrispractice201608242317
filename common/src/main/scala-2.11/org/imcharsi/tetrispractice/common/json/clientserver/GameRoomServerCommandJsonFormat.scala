/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.clientserver

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.json.fsm.PlayerJsonFormat
import spray.json._

/**
 * Created by i on 9/9/16.
 */
object GameRoomServerCommandJsonFormat extends RootJsonFormat[GameRoomServerCommand] with SprayJsonSupport {

  import DefaultJsonProtocol._

  private implicit val playerJsonFormat = PlayerJsonFormat
  private implicit val notifyJoinRoomJsonFormat = jsonFormat2(NotifyJoinRoom)
  private implicit val notifiedJoinRoomJsonFormat = jsonFormat3(NotifiedJoinRoom)
  private implicit val notifyBeAbleToStartJsonFormat = jsonFormat1(NotifyBeAbleToStart)
  private implicit val notifyChangeStarterJsonFormat = jsonFormat1(NotifyChangeStarter)

  def clazzAppend(map: Map[String, JsValue])(clazz: String): JsValue =
    JsObject(map + ("clazz" -> JsString(clazz)))

  override def write(obj: GameRoomServerCommand): JsValue = {
    obj match {
      case obj @ NotifyJoinRoom(_, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(NotifyJoinRoom.toString())
      case obj @ NotifiedJoinRoom(_, _, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(NotifiedJoinRoom.toString())
      case obj @ NotifyBeAbleToStart(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(NotifyBeAbleToStart.toString())
      case NotifiedBeAbleToStart => clazzAppend(Map())(NotifiedBeAbleToStart.toString)
      case obj @ NotifyChangeStarter(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(NotifyChangeStarter.toString())
      case NotifiedChangeStarter => clazzAppend(Map())(NotifiedChangeStarter.toString)
      case _ => serializationError("")
    }
  }

  override def read(json: JsValue): GameRoomServerCommand = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == NotifyJoinRoom.toString() =>
        JsObject(fields - "clazz").convertTo[NotifyJoinRoom]
      case Some(JsString(clazz)) if clazz == NotifiedJoinRoom.toString() =>
        JsObject(fields - "clazz").convertTo[NotifiedJoinRoom]
      case Some(JsString(clazz)) if clazz == NotifyBeAbleToStart.toString() =>
        JsObject(fields - "clazz").convertTo[NotifyBeAbleToStart]
      case Some(JsString(clazz)) if clazz == NotifiedBeAbleToStart.toString() =>
        NotifiedBeAbleToStart
      case Some(JsString(clazz)) if clazz == NotifyChangeStarter.toString() =>
        JsObject(fields - "clazz").convertTo[NotifyChangeStarter]
      case Some(JsString(clazz)) if clazz == NotifiedChangeStarter.toString() =>
        NotifiedChangeStarter
      case _ => deserializationError("")
    }
  }
}
