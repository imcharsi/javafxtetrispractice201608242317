/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.BlockCell

/**
 * Created by i on 8/27/16.
 */
sealed case class GameMap[BT](
  size: Position,
  // 경계로 쓸 벽돌의 위치를 0 을 넘겨서 놓는다.
  // 경계를 그리면, 게임 판 안에 이미 있는 벽돌 때문에 currentBlock 을 움직일 수 없음을 판단하는 기능을
  // 벽 너머로 이동을 시도할 때도 그대로 쓸 수 있다.
  // 게임 판의 크기가 2x2 이면 경계는 아래와 같이 그려진다.
  //    -1 0 1 2
  //  0 ??....??
  //  1 ??....??
  //  2 ????????
  map: Map[Position, BlockCell[BT]]
)
