/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.json.clientserver

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.json.fsm.PlayerJsonFormat
import spray.json._

/**
 * Created by i on 9/9/16.
 */
object GameRoomClientCommandJsonFormat extends RootJsonFormat[GameRoomClientCommand] with SprayJsonSupport {

  import DefaultJsonProtocol._

  private implicit val playerJsonFormat = PlayerJsonFormat
  private implicit val creatorJoinRoomJsonFormat = jsonFormat2(CreatorJoinRoom)
  private implicit val creatorJoinedRoomJsonFormat = jsonFormat1(CreatorJoinedRoom)
  private implicit val joinRoomJsonFormat = jsonFormat1(JoinRoom)
  private implicit val joinedRoomJsonFormat = jsonFormat1(JoinedRoom)
  private implicit val startGamesRoomJsonFormat = jsonFormat1(StartGames)
  private implicit val startedGamesRoomJsonFormat = jsonFormat1(StartedGames)

  override def read(json: JsValue): GameRoomClientCommand = {
    val fields = json.asJsObject.fields
    fields.get("clazz") match {
      case Some(JsString(clazz)) if clazz == CreatorJoinRoom.toString() =>
        JsObject(fields - "clazz").convertTo[CreatorJoinRoom]
      case Some(JsString(clazz)) if clazz == CreatorJoinedRoom.toString() =>
        JsObject(fields - "clazz").convertTo[CreatorJoinedRoom]
      case Some(JsString(clazz)) if clazz == JoinRoom.toString() =>
        JsObject(fields - "clazz").convertTo[JoinRoom]
      case Some(JsString(clazz)) if clazz == JoinedRoom.toString() =>
        JsObject(fields - "clazz").convertTo[JoinedRoom]
      case Some(JsString(clazz)) if clazz == StartGames.toString() =>
        JsObject(fields - "clazz").convertTo[StartGames]
      case Some(JsString(clazz)) if clazz == StartedGames.toString() =>
        JsObject(fields - "clazz").convertTo[StartedGames]
      case Some(JsString(clazz)) if clazz == Ping.toString() =>
        Ping
      case _ => deserializationError("")
    }
  }

  def clazzAppend(map: Map[String, JsValue])(clazz: String): JsValue =
    JsObject(map + ("clazz" -> JsString(clazz)))

  override def write(obj: GameRoomClientCommand): JsValue = {
    obj match {
      case obj @ CreatorJoinRoom(_, _) =>
        clazzAppend(obj.toJson.asJsObject.fields)(CreatorJoinRoom.toString())
      case obj @ CreatorJoinedRoom(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(CreatorJoinedRoom.toString())
      case obj @ JoinRoom(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(JoinRoom.toString())
      case obj @ JoinedRoom(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(JoinedRoom.toString())
      case obj @ StartGames(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(StartGames.toString())
      case obj @ StartedGames(_) =>
        clazzAppend(obj.toJson.asJsObject.fields)(StartedGames.toString())
      case obj @ Ping =>
        clazzAppend(Map())(Ping.toString())
      case _ => serializationError("")
    }
  }
}
