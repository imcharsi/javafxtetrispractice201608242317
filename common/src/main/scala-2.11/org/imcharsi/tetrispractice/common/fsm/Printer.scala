/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import org.imcharsi.tetrispractice.common.data.fsm.GameState
import org.imcharsi.tetrispractice.common.data.fsm._
import BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import GameState._

import scala.collection.mutable

/**
 * Created by i on 8/28/16.
 */
object Printer {
  def mapToString[BT](map: GameMap[BT]): String = {
    0.until(map.size.y).foldLeft(mutable.StringBuilder.newBuilder += '\n') { (stringBuilder, y) =>
      0.until(map.size.x).foldLeft(stringBuilder) { (stringBuilder, x) =>
        map.map.get(Position(x, y)) match {
          case Some(UninitializedBlockCell(_)) => stringBuilder ++= "??"
          case Some(InitializedBlockCell(id, _)) => stringBuilder ++= "%2d".format(id)
          case None => stringBuilder ++= ".."
        }
      }
      stringBuilder += '\n'
    }.toString()
  }

  def overlapMapAndBlock[BT](blockSize: Position)(block: CurrentBlock[BT], map: GameMap[BT]): GameMap[BT] = {
    val result =
      block match {
        case Some((block, expectedLandingPosition)) =>
          0.until(blockSize.y).foldLeft(map.map) { (map, y) =>
            0.until(blockSize.x).foldLeft(map) { (map, x) =>
              val position = Position(x, y)
              val map1 =
                expectedLandingPosition.map.get(position) match {
                  case Some(blockCell) =>
                    map + ((Position(x + expectedLandingPosition.position.x, y + expectedLandingPosition.position.y) -> blockCell))
                  case None => map
                }
              block.map.get(position) match {
                case Some(blockCell) =>
                  map1 + ((Position(x + block.position.x, y + block.position.y) -> blockCell))
                case None => map
              }
            }
          }
        case None => map.map
      }
    map.copy(map = result)
  }

  def blockToString[BT](blockSize: Position)(block: Block[BT]): String = {
    0.until(blockSize.y).foldLeft(mutable.StringBuilder.newBuilder += '\n') { (stringBuilder, y) =>
      0.until(blockSize.x).foldLeft(stringBuilder) { (stringBuilder, x) =>
        block.map.get(Position(x, y)) match {
          case Some(UninitializedBlockCell(_)) => stringBuilder ++= "??"
          case Some(InitializedBlockCell(id, _)) => stringBuilder ++= "%2d".format(id)
          case None => stringBuilder ++= ".."
        }
      }
      stringBuilder += '\n'
    }.toString()
  }

  def runningGameToString[BT](blockSize: Position, expectedLandingBlock: BlockCell[BT], title: String)(runningGame: RunningGame[BT]): String = {
    s"===${title}===\nplayer:${runningGame.player}\nmap:${mapToString(runningGame.gameMap)}\nbePushedCount:${runningGame.bePushed._1}\nbePushed:${mapToString(runningGame.bePushed._2)}\ncurrentBlockPosition:${runningGame.currentBlock.map(_._1.position)}\ncurrentBlock:${runningGame.currentBlock.map(x => blockToString(blockSize)(x._1)).getOrElse("None")}\nexpectedLandingPosition:${runningGame.currentBlock.map(_._2.position)}\nexpectedLanding:${runningGame.currentBlock.map(x => blockToString(blockSize)(x._2)).getOrElse("None")}\noverlapMap:${mapToString(overlapMapAndBlock(blockSize)(runningGame.currentBlock, runningGame.gameMap))}\ncurrentTimerId:${runningGame.currentTimerId}\nseed:${runningGame.seed}\nnext:${blockToString(blockSize)(runningGame.next)}"
  }

  def stoppedGameToString[BT](blockSize: Position, title: String)(runningGame: StoppedGame[BT]): String = {
    s"===${title}===\nplayer:${runningGame.player}\nmap:${mapToString(runningGame.map)}\nbePushed:${mapToString(runningGame.bePushed)}\nnext:${blockToString(blockSize)(runningGame.next)}"
  }

  def notYetStartedGameToString[BT](title: String)(game: NotYetStartedGame[BT]): String = {
    s"===${title}===\nplayer:${game.player}"
  }

  def notYetPreparedGameToString[BT](title: String)(game: NotYetPreparedGame[BT]): String = {
    s"===${title}===\nplayer:${game.player}"
  }

  def gameStateToString[BT](blockSize: Position, expectedLandingBlock: BlockCell[BT], title: String = "")(gameState: GameState[BT]): String = {
    gameState match {
      case runningGame @ RunningGame(_, _, _, _, _, _, _) => runningGameToString(blockSize, expectedLandingBlock, title + ":" + RunningGame.toString)(runningGame)
      case stoppedGame @ StoppedGame(_, _, _, _) => stoppedGameToString(blockSize, title + ":" + StoppedGame.toString)(stoppedGame)
      case notYetStartedGame @ NotYetStartedGame(_) => notYetStartedGameToString(title + ":" + NotYetStartedGame.toString)(notYetStartedGame)
      case notYetPreparedGame @ NotYetPreparedGame(_) => notYetPreparedGameToString(title + ":" + NotYetPreparedGame.toString)(notYetPreparedGame)
    }
  }

  def waitFeedbackToString[BT](blockSize: Position, expectedLandingBlock: BlockCell[BT], title: String = "")(waitFeedback: WaitFeedback[BT]): String = {
    waitFeedback match {
      case WaitRealizeNextBlockFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitRealizeNextBlockFeedback.toString)(gameState)
      case WaitBlockTransformFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitBlockTransformFeedback.toString)(gameState)
      case WaitStartFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitStartFeedback.toString)(gameState)
      case WaitStopFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitStopFeedback.toString)(gameState)
      case WaitPushFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitPushFeedback.toString)(gameState)
      case WaitPrepareFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitPrepareFeedback.toString)(gameState)
      case WaitCancelFeedback(gameState) => gameStateToString(blockSize, expectedLandingBlock, title + ":" + WaitCancelFeedback.toString)(gameState)
    }
  }

  def printState[BT](blockSize: Position, expectedLandingBlock: BlockCell[BT], title: String = "")(either: Either[WaitFeedback[BT], GameState[BT]]): String = {
    either match {
      case Left(waitFeedback) => waitFeedbackToString(blockSize, expectedLandingBlock, title)(waitFeedback)
      case Right(gameState) => gameStateToString(blockSize, expectedLandingBlock, title)(gameState)
    }
  }

}
