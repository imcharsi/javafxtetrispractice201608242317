/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.block5x5

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5.BlockType.BlockType
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ BlockCell, UninitializedBlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.{ LeftToDowToRight, RightToDownToLeft, RotateDirection }
import org.imcharsi.tetrispractice.common.data.fsm.{ Block, Position }
import org.imcharsi.tetrispractice.common.util.Log.Log
import org.imcharsi.tetrispractice.common.util.State
import spray.json.RootJsonFormat

import scala.collection.immutable.{ Map, Queue }

/**
 * Created by i on 9/9/16.
 */
object Block5x5 {
  def drawBlockMap[BT](list: List[Position], blockType: BT): Map[Position, BlockCell[BT]] =
    list.foldLeft(Map[Position, BlockCell[BT]]())((map, position) => map + (position -> UninitializedBlockCell(blockType)))

  object BlockType {

    sealed trait BlockType

    // ||
    // ||
    // ||
    // ||
    case object BlockOne extends BlockType

    // ||||
    // ||||
    case object BlockTwo extends BlockType

    // ||||
    //   ||||
    case object BlockThree extends BlockType

    //   ||||
    // ||||
    case object BlockFour extends BlockType

    // ||||
    //   ||
    //   ||
    case object BlockFive extends BlockType

    // ||||
    // ||
    // ||
    case object BlockSix extends BlockType

    //   ||
    // ||||||
    case object BlockSeven extends BlockType

  }

  val blockMap: Map[Int, Block[BlockType.BlockType]] =
    List(
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(2, 0), Position(2, 1), Position(2, 2), Position(2, 3)), BlockType.BlockOne)),
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(1, 1), Position(2, 1), Position(1, 2), Position(2, 2)), BlockType.BlockTwo)),
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(1, 1), Position(2, 1), Position(2, 2), Position(3, 2)), BlockType.BlockThree)),
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(3, 1), Position(2, 1), Position(2, 2), Position(1, 2)), BlockType.BlockFour)),
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(1, 1), Position(2, 1), Position(2, 2), Position(2, 3)), BlockType.BlockFive)),
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(3, 1), Position(2, 1), Position(2, 2), Position(2, 3)), BlockType.BlockSix)),
      Block[BlockType.BlockType](Position(0, 0), drawBlockMap(List(Position(2, 1), Position(1, 2), Position(2, 2), Position(3, 2)), BlockType.BlockSeven))
    ).zipWithIndex.map(x => (x._2, x._1)).toMap

  def streamOutsidePosition(position: Position): Stream[Position] = {
    position #:: streamOutsidePosition {
      position match {
        case Position(0, 4) => Position(1, 4)
        case Position(0, y) => Position(0, y + 1)
        case Position(4, 0) => Position(3, 0)
        case Position(4, y) => Position(4, y - 1)
        case Position(x, 4) => Position(x + 1, 4)
        case Position(x, 0) => Position(x - 1, 0)
      }
    }
  }

  def streamInsidePosition(position: Position): Stream[Position] =
    position #:: streamInsidePosition {
      position match {
        case Position(1, 3) => Position(2, 3)
        case Position(1, y) => Position(1, y + 1)
        case Position(3, 1) => Position(2, 1)
        case Position(3, y) => Position(3, y - 1)
        case Position(x, 3) => Position(x + 1, 3)
        case Position(x, 1) => Position(x - 1, 1)
      }
    }

  val outsideStream = streamOutsidePosition(Position(0, 0))
  val insideStream = streamInsidePosition(Position(1, 1))
  val centerStream = Stream.continually(Position(2, 2))
  val positionMap =
    outsideStream
      .zip(outsideStream.drop(1))
      .zip(outsideStream.drop(2))
      .map { case ((prev, mid), next) => (mid, (prev, next, 4)) }
      .take(16)
      .append(
        insideStream
          .zip(insideStream.drop(1))
          .zip(insideStream.drop(2))
          .map { case ((prev, mid), next) => (mid, (prev, next, 2)) }
          .take(8)
      )
      .append(
        centerStream
          .zip(centerStream.drop(1))
          .zip(centerStream.drop(2))
          .map { case ((prev, mid), next) => (mid, (prev, next, 0)) }
          .take(1)
      ).toMap
  val outsidePosition = outsideStream.take(16).toSet
  val insidePosition = insideStream.take(8).toSet
  val centerPosition = centerStream.take(1).toSet

  def rotate(
    gameMap: Map[Position, BlockCell[BlockType.BlockType]],
    rotateDirection: RotateDirection,
    blockPosition: Position,
    blockMap: Map[Position, BlockCell[BlockType.BlockType]]
  ): Option[Map[Position, BlockCell[BlockType.BlockType]]] = {
    def validate(curr: Position)(count: Int): Option[Position] = {
      if (gameMap.get(blockPosition + curr).isDefined) None
      else if (count > 0) {
        rotateDirection match {
          case LeftToDowToRight =>
            validate(positionMap.get(curr).get._2)(count - 1)
          case RightToDownToLeft =>
            validate(positionMap.get(curr).get._1)(count - 1)
        }
      } else Some(curr)
    }
    val z: Option[Map[Position, BlockCell[BlockType.BlockType]]] = Some(Map())
    blockMap.foldLeft(z) {
      case (Some(map), (position, blockCell)) =>
        validate(position)(positionMap(position)._3) match {
          case Some(newPosition) =>
            Some(map + (newPosition -> blockCell))
          case _ => None
        }
      case (None, _) => None
    }
  }

  def generateBlock(blockType: Int): Block[BlockType.BlockType] = {
    blockMap.get(blockType % 7).getOrElse(blockMap(0))
  }

  // https://en.wikipedia.org/wiki/Linear_congruential_generator
  private val a = 8121L
  private val c = 28411L
  private val m = 134456L
  val lcg: State.State[(Int, State.State[Queue[Log], Unit]), Int] =
    State.State {
      case (seed, logs) =>
        val random = ((seed.toLong * a + c) % m).toInt
        ((random, logs), random.toInt)
    }

  object Block5x5JsonFormat extends RootJsonFormat[BlockType.BlockType] with SprayJsonSupport {

    import spray.json._

    override def read(json: JsValue): BlockType = {
      val fields = json.asJsObject.fields
      fields.get("clazz") match {
        case Some(JsString(clazz)) if clazz == BlockType.BlockOne.toString => BlockType.BlockOne
        case Some(JsString(clazz)) if clazz == BlockType.BlockTwo.toString => BlockType.BlockTwo
        case Some(JsString(clazz)) if clazz == BlockType.BlockThree.toString => BlockType.BlockThree
        case Some(JsString(clazz)) if clazz == BlockType.BlockFour.toString => BlockType.BlockFour
        case Some(JsString(clazz)) if clazz == BlockType.BlockFive.toString => BlockType.BlockFive
        case Some(JsString(clazz)) if clazz == BlockType.BlockSix.toString => BlockType.BlockSix
        case Some(JsString(clazz)) if clazz == BlockType.BlockSeven.toString => BlockType.BlockSeven
        case _ => deserializationError("")
      }
    }

    override def write(obj: BlockType): JsValue = JsObject(Map(("clazz" -> JsString(obj.toString))))
  }
}
