/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.data.fsm

/**
 * Created by i on 8/25/16.
 */
object Player {

  // Player 를 구분하는 이유는, Client 가, FSM 의 출력을 구독할 때 Server 로 입력을 중계하는지 여부를 가리기 위함이다.
  // LocalPlayer 가 만든 출력은 Server 로 가게 되고
  // Server 는 각 Client 에게 RemotePlayer 의 입력으로서 입력을 Broadcast 한다.
  // FSM 이 RemotePlayer 로부터의 입력을 받으면 구독단계에서 이 입력을 다시 Server 로 보내지 않는다.
  sealed trait Player

  sealed case class LocalPlayer(playerId: Int, name: String) extends Player

  sealed case class RemotePlayer(playerId: Int, name: String) extends Player

  // Server 에서 각 Client 로 Broadcast 할 때 쓸 입력을 만들 때 편리하다.
  // Server 로부터 받는 입력은 RemotePlayer 로서 주어져야 한다.
  def convertToRemotePlayer(player: Player): Player = {
    player match {
      case LocalPlayer(playerId, name) => RemotePlayer(playerId, name)
      case x => x
    }
  }

  def convertToLocalPlayer(player: Player): Player = {
    player match {
      case RemotePlayer(playerId, name) => LocalPlayer(playerId, name)
      case x => x
    }
  }

  def isLocalPlayer(player: Player): Boolean =
    player match {
      case LocalPlayer(_, _) => true
      case RemotePlayer(_, _) => false
    }
}
