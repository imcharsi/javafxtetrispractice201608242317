/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.fsm

import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.{ BlockTransformType, RightToDownToLeft, RotateDirection }
import org.imcharsi.tetrispractice.common.data.fsm.Command.BlockCellList
import org.imcharsi.tetrispractice.common.data.fsm.GameState.CurrentBlock
import org.imcharsi.tetrispractice.common.data.fsm.{ Block, BlockTransformType, GameMap, Position }
import org.imcharsi.tetrispractice.common.fsm.Game.{ MoveBlockR, PushToBePushedR, PushToMapR, RunningGameSS }
import org.imcharsi.tetrispractice.common.util.State

/**
 * Created by i on 8/30/16.
 */
object Util {
  def prepareClearedMap[BT](uninitializedBlockCell: BlockCell[BT], size: Position)(map: Map[Position, BlockCell[BT]]): Map[Position, BlockCell[BT]] = {
    (-1).to(size.x).foldLeft {
      0.until(size.y).foldLeft(map) {
        case (map, y) =>
          // 배열로 할 것이 아니기 때문에, 좌표 범위가 0 을 넘어도 된다.
          map + (Position(-1, y) -> uninitializedBlockCell) + (Position(size.x, y) -> uninitializedBlockCell)
      }
    } {
      case (map, x) =>
        map + (Position(x, size.y) -> uninitializedBlockCell)
    }
  }

  def generateRandom[BT](random: State.State[Game.IntSS, Int]) =
    State.State[Game.RunningGameSS[BT], Int] {
      case (runningGame, queue) =>
        val ((newSeed, newQueue), randomNumber) = random((runningGame.seed, queue))
        ((runningGame.copy(seed = newSeed), newQueue), randomNumber)
    }

  def generateNext[BT](
    random: State.State[Game.IntSS, Int],
    generateBlockF: Int => Block[BT],
    origin: Position,
    rotateF: Game.RotateF[BT]
  ) =
    State.State[Game.IntSS, Block[BT]] {
      case (seed, queue) =>
        val ((newSeed, newQueue), randomNumber) = random((seed, queue))
        val newNext = generateBlockF(randomNumber)
        val ((newSeed2, newQueue2), randomNumber2) = random((newSeed, newQueue))
        val newNextMap =
          0.until((if (randomNumber2 > 0) randomNumber2 else randomNumber2 * -1) % 4)
            .foldLeft(Option(newNext.map)) {
              case (Some(z), _) =>
                rotateF(Map(), RightToDownToLeft, origin, z)
            }.get
        ((newSeed2, newQueue2), newNext.copy(map = newNextMap))
    }

  // 다음 벽돌을 게임 판에 올릴 때 회전하지 않고 다음 벽돌을 만들 때 처음부터 회전시켜 놓고
  // 그대로 게임 판에 올리는 것이 낫겠다.
  // impo generateBlock 은 상태의 seed, 현재 벽돌, 다음 벽돌을 전부 바꿔야 한다.
  def generateBlock[BT](
    random: State.State[Game.IntSS, Int],
    generateBlockF: Int => Block[BT],
    rotateF: Game.RotateF[BT],
    origin: Position
  )(implicit runningGameSMB: State.StateMonadBehavior[RunningGameSS[BT]]) = {
    import org.imcharsi.tetrispractice.common.util.FP.StateMonadSyntaxSugar

    State.State[Game.RunningGameSS[BT], Boolean] {
      case (runningGame, queue) =>
        runningGame.currentBlock match {
          case Some(_) =>
            ((runningGame, queue), false)
          case None =>
            runningGame.next.map.foldLeft(true) {
              case (true, (position, _)) =>
                runningGame.gameMap.map.get(position) match {
                  case Some(_) => false
                  case None => true
                }
              case (false, _) =>
                false
            } match {
              case false =>
                ((runningGame, queue), false)
              case true =>
                val ((newSeed, newQueue), newNext) = generateNext(random, generateBlockF, origin, rotateF)((runningGame.seed, queue))
                // 왜 이와 같이 하는가.
                // 이 시점에서 next 에는 이미 ui 식별번호가 할당되어 있는데, 예상낙하지점을 그릴 때는 이것을 쓰면 안 된다.
                // 다음 벽돌을 그리는 칸은 화면에서 따로 구분되어 있고,
                // 다음 벽돌을 그리던 ui 객체는 그대로 게임 판 안에서 현재 내려오는 벽돌을 그리는 데 옮겨 쓸 것이다.
                // 그래서 예상낙하지점을 그릴 때 같은 ui 객체를 그대로 쓰면 안 되고
                // 예상낙하지점 벽돌은 ui 식별번호를 새로 받아야 한다.
                val newExpectedLanding =
                  runningGame.next.copy(
                    map =
                      runningGame.next.map.foldLeft(runningGame.next.map) {
                        case (map, (position, InitializedBlockCell(_, cellType))) =>
                          map - position + (position -> UninitializedBlockCell(cellType))
                        case (map, (position, blockCell)) => map
                      }
                  )
                runningGame.next.copy(map = runningGame.next.map)
                Tuple2(
                  Tuple2(
                    runningGame.copy(
                      seed = newSeed,
                      currentBlock = Some((runningGame.next, newExpectedLanding)),
                      next = newNext
                    ),
                    newQueue
                  ),
                  true
                )
            }
        }
    }
      .passThrough(Util.expectedLanding)
      .bind { generateResult =>
        State.State[Game.RunningGameSS[BT], Game.GenerateBlockR[BT]] {
          case (runningGame, queue) =>
            Tuple2(
              Tuple2(runningGame, queue),
              runningGame.currentBlock
                .filter(_ => generateResult)
                .map(x => (x, runningGame.next))
            )
        }
      }
  }

  def rotateBlock[BT](cleared: GameMap[BT], rotateF: Game.RotateF[BT])(rotateDirection: RotateDirection) =
    State.State[RunningGameSS[BT], Option[CurrentBlock[BT]]] {
      case (runningGame, queue) =>
        runningGame.currentBlock
          .flatMap {
            case (block, expectedLanding) =>
              rotateF(runningGame.gameMap.map, rotateDirection, block.position, block.map)
                .flatMap(rotateCurrentBlock =>
                  // 예상낙하지점을 그릴 때도 똑같이 회전해야 하는데, 원래 게임 판을 배경으로 회전을 시도하면 실패할 수 있다.
                  // 예상낙하지점을 그리는 데 쓰는 벽돌의 회전은 무조건 성공해야 한다.
                  // 회전의 성공여부는 내려오는 벽돌의 회전에 대해서만 판단해야 한다.
                  rotateF(Map(), rotateDirection, Position(0, 0), expectedLanding.map)
                    .map((rotateCurrentBlock, _)))
          } match {
            case Some((blockMap, expectedLanding)) =>
              val newCurrentBlock =
                runningGame.currentBlock.map(
                  // 이 기능은, 이 구절만 봐서는 이해를 할 수 없게 되어 있다.
                  // 구상은, 예상낙하지점 계산을 분리/재사용하자는 것이고
                  // 따라서 이 단계에서는 예상낙하지점 벽돌의 회전 모양만 갖춰놓고,
                  // 위치는 이어지는 expectLanding 에서 계산하는 것이다.
                  x => (x._1.copy(map = blockMap), x._2.copy(map = expectedLanding))
                )
              Tuple2(
                Tuple2(
                  runningGame.copy(currentBlock = newCurrentBlock),
                  queue
                ),
                Some(newCurrentBlock)
              )
            case None =>
              ((runningGame, queue), Some(None))
          }
    }

  // 이 단계는, 현재 내려오는 벽돌과 배경을 겹쳐도 되는지에 대한 판단은 하지 않는다.
  // 단순히 겹칠 뿐이다.
  // 이 단계는, 벽돌이 바닥에 닿아서 게임 판과 겹치게 하기 위해서 쓴다.
  // 따라서, 이 단계에서 현재 내려오는 벽돌과 시간초과설정을 지운다.
  // 또한, 현재 내려오는 벽돌과 시간초과설정을 지우는 단계는 이 단계 뿐이다.
  // 다른 모든 단계에서는 벽돌을 움직일 수 있는지 살펴보고
  // 움직일 수 있으면 상태를 바꾸고 움직일 수 없으면 상태를 바꾸지 않고 지우지도 않는다.
  def overlap[BT] =
    State.State[RunningGameSS[BT], Unit] {
      case (runningGame, queue) =>
        val newRunningGame =
          runningGame.copy(
            gameMap =
              runningGame.currentBlock
                .foldLeft(runningGame.gameMap) {
                  case (gameMap, (block, _)) =>
                    gameMap.copy(
                      map =
                        block.map
                          .foldLeft(gameMap.map) { (map, blockCell) =>
                            map + (
                              block.position.copy(
                                x = block.position.x + blockCell._1.x,
                                y = block.position.y + blockCell._1.y
                              ) -> blockCell._2
                            )
                          }
                    )
                }
          // 최초의 구상에서는 overlap 단계에서 currentBlock/currentTimerId 를 지우기로 했었는데 구상을 바꿨다.
          // 문제는, 깨진 줄을 넘길 때 깨진 모양대로 넘어가도록 하는 것이 목표였는데
          // 최초의 구상대로 하면 그와 같이 되지 않는다.
          // breakAndCollapse 단계에서, deleted 의 벽돌 중 currentBlock 의 벽돌과 겹치는 것만 deleted 에서 지우면 되는데
          // 이와 같이 하려면 breakAndCollapse 단계에서 상태로서 currentBlock 을 쓸 수 있어야 한다.
          )
        ((newRunningGame, queue), ())
    }

  // 이 기능은 게임판에 대해서만 쓰는 것이 아니라 줄 쌓기에도 쓸 것이기 때문에 이와 같은 형으로 하는 것이 낫겠다.
  def overlapWithBlockCellList[BT](gameMap: GameMap[BT], blockCellList: BlockCellList[BT]): GameMap[BT] = {
    gameMap.copy(
      map =
        blockCellList.foldLeft(gameMap.map) { (map, blockCell) =>
          map + blockCell
        }
    )
  }

  // 게임 판의 각 줄을 row 줄 만큼 위로 올려도 되는지에 대한 판단을, 넘쳐서 지워져야 되는 줄을 알리는 식으로 한다.
  def moveUpGameMap[BT](
    cleared: GameMap[BT],
    row: Int,
    gameMap: GameMap[BT]
  ): (GameMap[BT], BlockCellList[BT], BlockCellList[BT]) = {
    if (row > 0) {
      val moved: BlockCellList[BT] = Nil
      val deleted: BlockCellList[BT] = Nil
      val (newGameMap, movedResult, deletedResult) =
        gameMap.map.foldLeft((cleared.map, moved, deleted)) {
          case ((map, moved, deleted), (position, blockCell)) =>
            if (position.x < 0
              || position.x >= gameMap.size.x
              || position.y >= gameMap.size.y)
              (map, moved, deleted)
            else {
              if (position.y - row < 0)
                (map, moved, ((position, blockCell) :: deleted))
              else {
                val movedBlock = position.copy(y = position.y - row) -> blockCell
                (map + movedBlock, (movedBlock) :: moved, deleted)
              }
            }
        }
      (gameMap.copy(map = newGameMap), movedResult, deletedResult)
    } // 아무것도 할 것이 없다면 아무것도 하지 않는다. 앞선 구상에서는 아무것도 할 것이 없어도 계산을 했다.
    else (gameMap, List(), List())
  }

  // 이 계산은 무조건 성공한다. 왜냐하면 줄 넘기기로 받은 줄은 일단 쌓아두고 넘치면 버리기 때문이다.
  // 쌓아 둔 줄은 언제 게임판에 더하는가. 새 벽돌을 그리기 직전에 더하면 된다.
  // 입력은 쌓을 줄의 수와 쌓을 줄이다.
  def pushToBePushed[BT](cleared: GameMap[BT])(rowCount: Int, bePushed: BlockCellList[BT]) =
    State.State[RunningGameSS[BT], PushToBePushedR[BT]] {
      case (runningGame, queue) =>
        val (newBePushed, moved, deleted) =
          moveUpGameMap(cleared, rowCount, runningGame.bePushed._2)
        val newRowCount = runningGame.bePushed._1 + rowCount
        val newRunningGame =
          runningGame.copy(
            bePushed =
              Tuple2(
                if (runningGame.bePushed._2.size.y < newRowCount)
                  runningGame.bePushed._2.size.y
                else
                  newRowCount,
                overlapWithBlockCellList(newBePushed, bePushed)
              )
          )
        ((newRunningGame, queue), (bePushed, moved, deleted))
    }

  def pushToMap[BT](cleared: GameMap[BT]) =
    State.State[RunningGameSS[BT], PushToMapR[BT]] {
      case (runningGame, queue) =>
        // 게임 판을 위로 밀어올려서 지워진 것이 있으면 게임이 끝났다고 본다.
        val (newGameMap, moved, deleted) =
          moveUpGameMap(cleared, runningGame.bePushed._1, runningGame.gameMap)
        val movedFromBePushedToMap =
          runningGame.bePushed._2.map.toList
            .filterNot(x => x._1.x < 0 || x._1.x >= runningGame.gameMap.size.x || x._1.y >= runningGame.gameMap.size.y)
        Tuple2(
          Tuple2(
            runningGame.copy(
              gameMap = overlapWithBlockCellList(newGameMap, movedFromBePushedToMap),
              bePushed = (0, cleared)
            ),
            queue
          ),
          // 그래서 deleted.isEmpty == true 이면 게임을 계속한다.
          (moved, deleted, movedFromBePushedToMap, deleted.isEmpty)
        )
    }

  // 어느 방향이든 한 번의 이동만 다루는 의도로 썼는데, 거리를 강제하지 않았다.
  // 또한, (1, 1) 과 같은 이동도 있어서는 안 되는데, 이에 대한 강제도 없다.
  def moveBlockOneStep[BT](x: Int, y: Int) =
    State.State[RunningGameSS[BT], Option[CurrentBlock[BT]]] {
      case (runningGame, queue) =>
        val positionAdd = Position(x, y)
        val (result, continueResult) =
          runningGame.currentBlock match {
            case Some((block, _)) =>
              val map = runningGame.gameMap.map
              // 첫번째 true 는 계속 움직일 수 있는지 판단하는 누적값이고
              // 두번째 true 는 게임을 계속 해도 되는지 판단하는 누적값이다.
              block.map.foldLeft((true, true)) {
                case ((movableResult, continueResult), (position, blockCell)) =>
                  if (movableResult) {
                    // RunningGame 의 gameMap 과 currentBlock 이 처음부터 겹쳐 있었다면 게임을 중지한다.
                    // 이에 대한 판단을 여기서 한다.
                    val movedPosition = map.get(position + positionAdd + block.position).isEmpty
                    val samePosition = map.get(position + block.position).isEmpty
                    (movedPosition && samePosition, samePosition)
                  } else (false, continueResult)
              }
            case None => (false, true)
          }
        if (continueResult) {
          if (result) {
            val newBlock: CurrentBlock[BT] =
              runningGame.currentBlock.map {
                case (block, expectedLanding) =>
                  val newBlock: Block[BT] = block.copy(position = block.position + positionAdd)
                  // 일단, 예상낙하지점을 계산하지 않는다. 그래서 현재 위치와 같게 한다.
                  (newBlock, expectedLanding.copy[BT](position = newBlock.position))
              }
            Tuple2(
              Tuple2(
                runningGame.copy(currentBlock = newBlock), queue
              ),
              Some(newBlock) // Some(Some((Block(...), Position(...))))
            )
          } else Tuple2(Tuple2(runningGame, queue), Some(None)) // Some(None)
        } else Tuple2(Tuple2(runningGame, queue), None) // None
    }

  // 게임 판의 특정 한 줄에 대해 깨지는 줄인지 아닌지 판단한다.
  def beBrokenOrNot[BT](y: Int, gameMap: GameMap[BT]): Either[BlockCellList[BT], BlockCellList[BT]] = {
    // 오른쪽으로 모으면 한 줄을 깰 수 있어서 지울 줄이고
    // 왼쪽으로 모으면 깰 수 없어서 이동할 줄이다.
    // 왜 이동하는가. 1, 2, 3 번 줄이 있는데, 2 번 줄을 깼다면 1 번 줄은 2 번 줄로 내려와야 한다.
    // 일단, 깰 수 있는 줄인지 아닌지만 판단한다. 실제 이동은 다른 단계에서 한다.
    val beBrokenOrNot: Either[BlockCellList[BT], BlockCellList[BT]] = Right(List())
    0.until(gameMap.size.x)
      .foldLeft(beBrokenOrNot) { (z, x) =>
        val position = Position(x, y)
        z match {
          case Right(list) =>
            gameMap.map.get(position) match {
              case Some(x) => Right((position, x) :: list)
              case None => Left(list)
            }
          case Left(list) =>
            gameMap.map.get(position) match {
              case Some(x) => Left((position, x) :: list)
              case None => Left(list)
            }
        }
      }
  }

  // 지워질 줄은 빼고, 이동할 줄로 표시된 줄들만 모은다.
  def collapse[BT](cleared: GameMap[BT], gameMap: GameMap[BT], moved: List[(Int, BlockCellList[BT])]): (GameMap[BT], List[BlockCellList[BT]]) = {
    val (resultMap, resultMoved) =
      // 예를 들어 1, 2, 3 번 줄 중에서 2 번 줄만 지워진다고 하면 moved 에는 1, 3 번 줄만 남는데
      // foldLeft 의 특성상 3, 1 순서로 남는다.
      // 이 줄 번호와, 게임 판 바닥부터 거꾸로 시작하는 줄 번호들을 묶으면 (3, 3), (2, 1) 과 같이 묶인다.
      // 결론은 3 번 줄은 3 번 줄 그대로, 1 번 줄은 2 번 줄로 간다.
      (gameMap.size.y - 1).to(0, -1).zip(moved)
        .foldLeft((cleared.map, List[BlockCellList[BT]]())) {
          case ((map, bclList), (newY, (origY, blockCellList))) =>
            val newBlockCellList = blockCellList.map(x => (x._1.copy(y = newY), x._2))
            val newMap =
              newBlockCellList.foldLeft(map) {
                case (map, positionBlockCell) =>
                  map + positionBlockCell
              }
            // 실제로 이동한 줄만 화면 표시에서 다루기 위해, 실제로 이동한 줄만 남긴다.
            // 이동할 벽돌에 해당하는 ui 객체는 ui 식별번호로 찾는다. 좌표로 찾지 않는다.
            // 그래서 좌표는 바뀌어도 된다.
            if (newY == origY) (newMap, bclList) else (newMap, newBlockCellList :: bclList)
        }
    (gameMap.copy(map = resultMap), resultMoved)
  }

  def separateBeCollapsedAndDeleted[BT](gameMap: GameMap[BT]): (List[(Int, BlockCellList[BT])], List[BlockCellList[BT]]) = {
    0.until(gameMap.size.y)
      .foldLeft((List[(Int, BlockCellList[BT])](), List[BlockCellList[BT]]())) {
        case ((moved, deleted), y) =>
          beBrokenOrNot(y, gameMap) match {
            case Right(list) =>
              (moved, list :: deleted)
            case Left(list) =>
              ((y, list) :: moved, deleted)
          }
      }
  }

  // 깰 수 있는 줄은 깨고 모아야 하는 줄은 모은다.
  // 이 구상에서는, 벽돌을 아래로 움직여봐서 실패하면 바닥에 닿은 것으로 판단하고
  // 바닥에 닿았을 때만 줄 깨기와 줄 모으기를 시도한다.
  def breakAndCollapse[BT](cleared: GameMap[BT])(movedBlock: Option[CurrentBlock[BT]])(implicit runningGameSMB: State.StateMonadBehavior[RunningGameSS[BT]]) =
    State.State[RunningGameSS[BT], MoveBlockR[BT]] {
      case (runningGame, queue) =>
        movedBlock match {
          case Some(Some(_)) =>
            // 아직 바닥에 닿지 않았으므로 아무것도 하지 않는다.
            ((runningGame, queue), movedBlock.map(Right(_)))
          case Some(None) =>
            // 아래로 이동할 수 없을 때 이 함수의 앞 단계에서 overlap 이 실행되어
            // 현재 내려오는 벽돌이 게임 판에 겹쳐지게 된다.
            // 이와 같이 해야 깨지지 않고 남은 벽돌의 일부가 줄 모으기 단계에서 움직일 수 있게 된다.
            // 바닥에 닿았으면 무조건 새 벽돌 만들기를 시도하고, 이 표시는 Left 로 한다.
            val (beCollapsed, deleted) = separateBeCollapsedAndDeleted(runningGame.gameMap)
            // LocalPlayer 가 만든 arrangedDeleted 는 Server 로 보낸다. 줄을 바닥 방향으로 모아야 한다.
            // 예를 들어 1, 2, 3 번 줄 중에서 1, 3번 줄만 깨진다면 (1, 2) (3, 3) 과 같이 줄 번호를 바꿔야 한다.
            // 한편, 화면에서 지울 줄은 따로 줄 정리를 할 필요가 없는데, 이유는 각 ui 식별번호만 알면 되기 때문이다.
            val arrangedDeleted =
              runningGame.currentBlock
                .foldLeft(deleted) {
                  case (deleted, (Block(blockPosition, map), _)) =>
                    map.foldLeft(deleted) {
                      case (deleted, (blockCellPosition, _)) =>
                        deleted.map { x =>
                          x.filterNot(_._1 == (blockPosition + blockCellPosition))
                            .map {
                              case (position, InitializedBlockCell(_, cellType)) => (position, UninitializedBlockCell(cellType))
                              case (position, UninitializedBlockCell(cellType)) => (position, UninitializedBlockCell(cellType))
                            }
                        }
                    }
                }
                .zip((runningGame.gameMap.size.y - 1).to(0, -1))
                .map {
                  case (blockCellList, newY) =>
                    blockCellList.map { case (position, blockCell) => (position.copy(y = newY), blockCell) }
                }
            val (resultGameMap, resultMoved) =
              collapse(cleared, runningGame.gameMap, beCollapsed)
            // 예상낙하지점을 그리는 데 쓴 벽돌이 지워지지 않는 문제가 있다.
            // 이와 같이 끼워넣으면 문제는 풀리는데 다른 방법 없나.
            // 이 문제를 해결하기 위해 뭔가를 해야 한다면 이 함수 안에서 해야 한다.
            // 이 함수는 내려오는 벽돌이 바닥에 닿았을 때 실행된다.
            val deletedWithExpectedLanding =
              runningGame.currentBlock
                .foldLeft(deleted) {
                  case (z, (_, expectedLanding)) =>
                    expectedLanding.map.toList :: z
                }
            Tuple2(
              Tuple2(
                runningGame.copy(
                  gameMap = resultGameMap,
                  currentBlock = None,
                  currentTimerId = None
                ),
                queue
              ),
              // resultDeleted 와 arrangedDeleted 를 구분하는 이유는 무엇인가.
              // 화면에서 지워지는 벽돌과 상대방에게 넘어가는 벽돌을 구분해야 한다.
              Some(Left((resultMoved, deletedWithExpectedLanding, arrangedDeleted)))
            )
          case None =>
            ((runningGame, queue), None)
        }
    }

  // 한꺼번에 여러칸을 내렸다는 뜻이다.
  // impo 바닥에 닿았는지 여부를 판단하는 것은 지금 현재 벽돌 위치에서 다시 아래로 내릴 수 있는지 여부이다.
  def moveDownRepeat[BT] =
    State.State[RunningGameSS[BT], Option[CurrentBlock[BT]]] { state =>
      def innerF(state: RunningGameSS[BT], prevSuccess: Option[CurrentBlock[BT]]): (RunningGameSS[BT], Option[CurrentBlock[BT]]) = {
        moveBlockOneStep[BT](0, 1)(state) match {
          case (newState, newResult @ Some(Some(_))) =>
            innerF(newState, newResult)
          case (_, Some(None)) =>
            (state, prevSuccess)
          case (_, None) => (state, None)
        }
      }
      innerF(state, Some(None))
    }

  import org.imcharsi.tetrispractice.common.util.State.StateRecoverSyntaxSugar

  // 이미 이동/회전이 다 되어 있는 상태에서 예상낙하지점을 계산한다.
  def expectedLanding[BT] =
    // 바닥으로 내리기를 시도한 후
    moveDownRepeat[BT].recover { (oldS: RunningGameSS[BT], _: RunningGameSS[BT], currentBlock: Option[CurrentBlock[BT]]) =>
      val (oldRunningGame, oldQueue) = oldS
      val newRunningGame =
        currentBlock match {
          // 결과에서 currentBlock 만 골라서
          case Some(Some((currentBlock @ Block(_, _), _))) =>
            // 계산을 시도하기 전 상태의 예상낙하지점 위치에 쓴다.
            // 이와 같이 하면 결과적으로 예상낙하지점만 바뀌게 된다.
            oldRunningGame.copy(
              currentBlock =
                oldRunningGame.currentBlock.map {
                  case (block, expectedLanding) =>
                    (block, expectedLanding.copy[BT](position = currentBlock.position))
                }
            )
          case _ => oldRunningGame
        }
      ((newRunningGame, oldQueue), ())
    }

  // expectedLanding 은, 예상낙하지점에 대한 계산을 하여 상태에 바꿔쓸 뿐, 함수의 출력을 바꾸지 않는다.
  // 그래서 함수의 출력을 바꾸는 장식기능을 하나 더 썼다.
  def decoratorExpectedLanding[BT](currentBlock: Option[CurrentBlock[BT]]) =
    State.State[RunningGameSS[BT], Option[CurrentBlock[BT]]] {
      case (runningGame, queue) =>
        currentBlock match {
          case Some(Some((currentBlock, _))) => ((runningGame, queue), Some(runningGame.currentBlock))
          case _ => ((runningGame, queue), currentBlock)
        }
    }

  def moveBlock[BT](cleared: GameMap[BT])(direction: BlockTransformType)(implicit runningGameSMB: State.StateMonadBehavior[RunningGameSS[BT]]) =
    State.State[RunningGameSS[BT], MoveBlockR[BT]] {
      case (runningGame, queue) =>
        import org.imcharsi.tetrispractice.common.util.FP.{ StateFunctorSyntaxSugar, StateMonadSyntaxSugar }
        direction match {
          case BlockTransformType.Left =>
            moveBlockOneStep[BT](-1, 0)
              .passThrough(expectedLanding[BT])
              .bind(decoratorExpectedLanding[BT](_))
              .fmap(_.map(Right(_)))((runningGame, queue))
          case BlockTransformType.Right =>
            moveBlockOneStep[BT](1, 0)
              .passThrough(expectedLanding[BT])
              .bind(decoratorExpectedLanding[BT](_))
              .fmap(_.map(Right(_)))((runningGame, queue))
          case BlockTransformType.Down | BlockTransformType.TimeElapsed(_) =>
            // 이동을 시도하여
            moveBlockOneStep[BT](0, 1)
              .bind { (x: Option[CurrentBlock[BT]]) =>
                x match {
                  case x @ Some(Some(_)) =>
                    // 이동에 성공하면 화면 표시에서는 벽돌 이동만 다뤄야 한다.
                    State[RunningGameSS[BT], Option[CurrentBlock[BT]]](x)
                      // 예상낙하지점은 현재 내려오는 벽돌이 바닥에 닿았을 때는 계산할 필요가 없다.
                      // 현재 내려오는 벽돌이 없는 상태가 되기 때문이다.
                      .passThrough(expectedLanding[BT])
                      .bind(decoratorExpectedLanding[BT](_))
                      .fmap[MoveBlockR[BT]](_.map(Right(_)))
                  case x @ Some(None) =>
                    // 이동에 실패하면 바닥에 닿은 것이고 게임 판과 현재 내려오는 벽돌을 겹치고
                    // 현재 내려오는 벽돌이 없도록 한 뒤
                    // 줄 깨기/줄 모으기를 한다.
                    // 화면 표시에서는 깨진 벽돌과 깨지고 남은 벽돌의 이동만 다뤄야 한다.
                    overlap[BT] >> breakAndCollapse[BT](cleared)(x)
                  case None =>
                    State[RunningGameSS[BT], MoveBlockR[BT]](None)
                }
              }((runningGame, queue))
          case BlockTransformType.Bottom =>
            moveDownRepeat[BT]
              .bind {
                case x @ Some(Some(_)) =>
                  State[RunningGameSS[BT], Option[CurrentBlock[BT]]](x)
                    .passThrough(expectedLanding[BT])
                    .bind(decoratorExpectedLanding[BT](_))
                    .fmap[MoveBlockR[BT]](_.map(Right(_)))
                case x @ Some(None) =>
                  overlap[BT] >> breakAndCollapse[BT](cleared)(x)
                case None =>
                  State[RunningGameSS[BT], MoveBlockR[BT]](None)
              }((runningGame, queue))
        }
    }
}
