/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.common.util

import rx.lang.scala.{ Subscription, Observer, Observable }

import scala.collection.immutable.Queue

/**
 * Created by i on 9/1/16.
 */
object Valve {
  def valve[A](determineOnInputF: A => Boolean)(observableA: Observable[A]): Observable[A] = {
    // 이 기능은, FSM 의 입력과 FSM 이 만든 출력에 대한 응답이 교대로 FSM 으로 입력될 수 있도록 순서를 맞추기 위해 쓴다.
    // 문을 닫는 입력이 FSM 에 대한 보통 입력이 되고, 문을 여는 입력이 FSM 이 만든 출력에 대한 응답이 된다.
    var queue: Queue[A] = Queue()
    var open: Boolean = true
    def subscribe(o: Observer[A]): Subscription = {
      def nextA(a: A): Unit = {
        // 문을 여는 입력이든 닫는 입력이든 일단 보낸다.
        o.onNext(a)
        if (!determineOnInputF(a))
          // 문을 여는 입력이면 열려 있던 문을 계속 열어 두고, 문을 닫는 입력이면 문을 닫는다.
          open = false
      }
      def onNext(a: A): Unit = {
        // 문이 열려 있으면
        if (open) {
          nextA(a)
        } else {
          // 문이 닫혀 있으면
          if (determineOnInputF(a)) {
            // 문을 여는 입력이면 문을 열고, 이번 입력은 보낸다.
            open = true
            o.onNext(a)
            queue.dequeueOption match {
              case None =>
              // queue 에 쌓아뒀던 것이 있으면
              case Some((a, newQueue)) =>
                queue = newQueue
                // 문이 열려 있을 때 했던 처리를 반복한다.
                nextA(a)
            }
          } else {
            // 문을 닫는 입력은 문이 열려 있을 때만 보낼 수 있고, 그래서 쌓아 둔다.
            queue = queue.enqueue(a)
          }
        }
      }
      observableA.subscribe(onNext, o.onError, o.onCompleted)
    }
    Observable.create(subscribe)
  }

}
