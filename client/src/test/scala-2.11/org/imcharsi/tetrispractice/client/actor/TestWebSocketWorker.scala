/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.actor

import akka.actor.{ ActorRef, ActorSystem, PoisonPill, Props }
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.ActorMaterializer
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.testkit.{ TestKit, TestProbe }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.{ GameRoomClientCommand, GameRoomServerCommand }
import org.imcharsi.tetrispractice.common.data.fsm.Command
import org.imcharsi.tetrispractice.common.data.fsm.Command.Start
import org.imcharsi.tetrispractice.common.data.fsm.Player.LocalPlayer
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.imcharsi.tetrispractice.common.fsm.TestBlockType.BlockType
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomClientCommandJsonFormat, GameRoomServerCommandJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm.StreamInputJsonFormat
import org.scalatest.{ BeforeAndAfterEach, FunSuiteLike }
import rx.lang.scala.{ Subject, Subscription }
import spray.json.DefaultJsonProtocol._
import spray.json._

/**
 * Created by i on 9/10/16.
 */
class TestWebSocketWorker extends TestKit(ActorSystem()) with FunSuiteLike with BeforeAndAfterEach {
  implicit val actorMaterializer = ActorMaterializer()(system)
  val webSocketWorker: WebSocketWorker[TestBlockType.BlockType] = new WebSocketWorker[BlockType]
  var subject: Subject[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]]] = null
  var subscription: Subscription = null
  var webSocketWorkerActorRef: ActorRef = ActorRef.noSender
  var testProbe: TestProbe = null
  implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
  implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
  implicit val streamInputJsonFormat = new StreamInputJsonFormat[TestBlockType.BlockType]()(TestBlockType.TestBlockTypeJsonFormat)

  test("WebSocketWorkerActor") {
    val testSource = TestSource.probe[Message]
    val testSink = TestSink.probe[Message]
    val (testSourceProbe, testSinkProbe) =
      Flow.fromSinkAndSource(
        Sink.fromSubscriber(ActorSubscriber[Message](webSocketWorkerActorRef)),
        Source.fromPublisher(ActorPublisher[Message](webSocketWorkerActorRef))
      ).runWith(testSource, testSink)
    val input1: Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[TestBlockType.BlockType]]] = Right(Right(Start(LocalPlayer(1, "hi"), 0)))
    testSourceProbe.sendNext(TextMessage.Strict(input1.toJson.prettyPrint))
    testProbe.expectMsg(input1)
    webSocketWorkerActorRef.tell(webSocketWorker.Holder(input1), testActor)
    testSinkProbe.requestNext(TextMessage.Strict(input1.toJson.prettyPrint))
    // 중계역할을 잘 하는지에 관한 검사는 이 정도면 충분한 듯 하다.
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    subject = Subject()
    // 왜 Rx TestSubscriber 를 쓰지 않고 구독단계에서 TestProbe 로 넣도록 했는가.
    // TestProbe 는 입력이 있을 때까지 일정시간 기다리는데, TestSubscriber 는 기다리지 않기 때문에 불편하다.
    subscription = subject.subscribe(x => testProbe.ref.tell(x, ActorRef.noSender))
    webSocketWorkerActorRef = system.actorOf(Props(new webSocketWorker.WebSocketWorkerActor(subject)(TestBlockType.TestBlockTypeJsonFormat, actorMaterializer)))
    testProbe = TestProbe()
  }

  override protected def afterEach(): Unit = {
    webSocketWorkerActorRef.tell(PoisonPill, ActorRef.noSender)
    subscription.unsubscribe()
    testProbe.ref.tell(PoisonPill, ActorRef.noSender)
    super.afterEach()
  }
}
