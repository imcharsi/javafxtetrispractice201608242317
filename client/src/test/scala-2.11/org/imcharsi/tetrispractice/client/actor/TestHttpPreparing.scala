/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.actor

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source, Flow, Keep }
import akka.stream.testkit.scaladsl.{ TestSink, TestSource }
import akka.testkit.TestKit
import akka.util.ByteString
import org.imcharsi.tetrispractice.client.http.HttpPreparing
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.{ GameRoomClientCommand, GameRoomServerCommand, NotifyJoinRoom }
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ CreatedRoom, GotRoomList, WaitRoomResponse }
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ Prepare, Start, StreamInput }
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ Player, RemotePlayer }
import org.imcharsi.tetrispractice.common.fsm.TestBlockType
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomServerCommandJsonFormat, GameRoomClientCommandJsonFormat, WaitRoomResponseJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm.StreamInputJsonFormat
import org.scalatest.FunSuiteLike
import spray.json._

import scala.collection.immutable.Queue
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.{ Success, Try }

/**
 * Created by i on 9/11/16.
 */
class TestHttpPreparing extends TestKit(ActorSystem()) with FunSuiteLike {
  implicit val actorMaterializer = ActorMaterializer()
  implicit val waitRoomResponseJsonFormat = WaitRoomResponseJsonFormat
  // 이와 같이 써야 검사를 편하게 할 수 있다.
  // HttpPreparing 의 Type Parameter 가 이와 같은 필요를 반영하고 있어야 한다.
  // 검사 외의 상황에서는 안 쓰면 된다.
  val superPool =
    Flow
      .fromSinkAndSourceMat(
        TestSink.probe[(HttpRequest, Int)],
        TestSource.probe[(Try[HttpResponse], Int)]
      )(Keep.both)

  // 세번째 인자는 안 쓸 것이기 때문에, 어쨌든 검사를 할 수 있기 위해서는 뭐라도 써야 한다.
  val clientHttpPreparing = new HttpPreparing(system, actorMaterializer, null, superPool)

  test("getRoomList") {
    val (future, (testSubscriberProbe, testPublisherProbe)) =
      clientHttpPreparing.getRoomList("http://localhost:8080")
    testSubscriberProbe.requestNext() match {
      case (HttpRequest(HttpMethods.GET, Uri(_, Uri.Authority(host, port, _), Uri.Path(s), _, _), _, _, _), _) =>
        assertResult("localhost")(host.address())
        assertResult(8080)(port)
        assertResult("/getRoomList")(s)
    }
    val expectedResult: WaitRoomResponse = GotRoomList(Map((1 -> 1), (2 -> 2)))
    testPublisherProbe.sendNext(
      Tuple2(
        Success(
          HttpResponse(
            StatusCodes.OK,
            List(),
            HttpEntity.Strict(
              ContentTypes.`application/json`,
              ByteString(expectedResult.toJson.prettyPrint)
            )
          )
        ),
        1
      )
    )
    assertResult(expectedResult)(Await.result(future, 10.seconds))
  }

  test("createRoom") {
    val (future, (testSubscriberProbe, testPublisherProbe)) =
      clientHttpPreparing.createRoom("http://localhost:8080")
    testSubscriberProbe.requestNext() match {
      case (HttpRequest(HttpMethods.POST, Uri(_, Uri.Authority(host, port, _), Uri.Path(s), _, _), _, _, _), _) =>
        assertResult("localhost")(host.address())
        assertResult(8080)(port)
        assertResult("/createRoom")(s)
    }
    val expectedResult: WaitRoomResponse = CreatedRoom(1, 2)
    testPublisherProbe.sendNext(
      Tuple2(
        Success(
          HttpResponse(
            StatusCodes.OK,
            List(),
            HttpEntity.Strict(
              ContentTypes.`application/json`,
              ByteString(expectedResult.toJson.prettyPrint)
            )
          )
        ),
        1
      )
    )
    assertResult(expectedResult)(Await.result(future, 10.seconds))
  }

  test("getReplayList") {
    import DefaultJsonProtocol._
    val (future, (testSubscriberProbe, testPublisherProbe)) =
      clientHttpPreparing.getReplayList("http://localhost:8080")
    testSubscriberProbe.requestNext() match {
      case (HttpRequest(HttpMethods.GET, Uri(_, Uri.Authority(host, port, _), Uri.Path(s), _, _), _, _, _), _) =>
        assertResult("localhost")(host.address())
        assertResult(8080)(port)
        assertResult("/replay")(s)
    }
    val expectedResult: List[Int] = List(1)
    testPublisherProbe.sendNext(
      Tuple2(
        Success(
          HttpResponse(
            StatusCodes.OK,
            List(),
            HttpEntity.Strict(
              ContentTypes.`application/json`,
              ByteString(expectedResult.toJson.prettyPrint)
            )
          )
        ),
        1
      )
    )
    assertResult(expectedResult)(Await.result(future, 10.seconds))
  }

  test("getReplay") {
    implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
    implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
    implicit val streamInputJsonFormat = new StreamInputJsonFormat[TestBlockType.BlockType]()(TestBlockType.TestBlockTypeJsonFormat)
    import DefaultJsonProtocol._

    val remotePlayer = RemotePlayer(0, "hi")
    val expectedStream: List[Either[GameRoomClientCommand, Either[GameRoomServerCommand, StreamInput[TestBlockType.BlockType]]]] =
      List(Right(Left(NotifyJoinRoom(0, remotePlayer))), Right(Right(Prepare(remotePlayer))), Right(Right(Start(remotePlayer, 0))))

    val (future, (testSubscriberProbe, testPublisherProbe)) =
      clientHttpPreparing.getReplay[TestBlockType.BlockType]("http://localhost:8080", 1)(TestBlockType.TestBlockTypeJsonFormat)
    testSubscriberProbe.requestNext() match {
      case (HttpRequest(HttpMethods.GET, Uri(_, Uri.Authority(host, port, _), Uri.Path(s), _, _), _, _, _), _) =>
        assertResult("localhost")(host.address())
        assertResult(8080)(port)
        assertResult("/replay/1")(s)
    }

    testPublisherProbe.sendNext(
      Tuple2(
        Success(
          HttpResponse(
            StatusCodes.OK,
            List(),
            HttpEntity.Chunked(
              ContentTypes.`application/json`,
              Source
                .fromIterator(() => expectedStream.toIterator)
                .map(x => HttpEntity.ChunkStreamPart(x.toJson.prettyPrint))
            )
          )
        ),
        1
      )
    )

    val result =
      Await.result(
        future
          .flatMap(_.runFold(Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, StreamInput[TestBlockType.BlockType]]]]())((q, x) => q.enqueue(x)))(Implicits.global)
          .map(_.toList)(Implicits.global),
        10.seconds
      )
    assertResult(expectedStream)(result)
  }
}
