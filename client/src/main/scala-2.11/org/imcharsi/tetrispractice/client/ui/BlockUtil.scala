/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.ui

import javafx.scene.layout.{ FlowPane, Pane }
import javafx.scene.paint.Color
import javafx.scene.shape.{ Rectangle, StrokeType }

import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ BlockCell, InitializedBlockCell, UninitializedBlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.Position

/**
 * Created by i on 9/20/16.
 */
trait BlockUtil {
  var gameMapUI: Map[Int, Rectangle] = Map()
  var nextUI: Map[Int, Rectangle] = Map()
  var bePushedUI: Map[Int, Rectangle] = Map()
  var uiIdCounter: Int = 0

  def allocateRectangle(cellType: Block5x5.BlockType.BlockType): Rectangle = {
    val color =
      cellType match {
        case Block5x5.BlockType.BlockOne => Color.BLUE
        case Block5x5.BlockType.BlockTwo => Color.YELLOW
        case Block5x5.BlockType.BlockThree => Color.RED
        case Block5x5.BlockType.BlockFour => Color.GREEN
        case Block5x5.BlockType.BlockFive => Color.BROWN
        case Block5x5.BlockType.BlockSix => Color.PURPLE
        case Block5x5.BlockType.BlockSeven => Color.PINK
      }
    val rectangle = new Rectangle(20, 20, color)
    rectangle.strokeProperty().set(Color.BLACK)
    rectangle.strokeWidthProperty().set(1)
    rectangle.strokeTypeProperty().set(StrokeType.INSIDE)
    rectangle
  }

  def allocateCurrentBlockCell(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(blockPosition: Position, blockCellPosition: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Option[BlockCell[Block5x5.BlockType.BlockType]] = {
    blockCell match {
      case InitializedBlockCell(uiId, cellType) =>
        nextUI.get(uiId) match {
          case Some(rectangle) =>
            paneNextBlock.getChildren.remove(rectangle)
            gameMapUI = gameMapUI + (uiId -> rectangle)
            nextUI = nextUI - uiId
            paneGameMap.getChildren.add(rectangle)
            rectangle.layoutXProperty().set((blockPosition.x + blockCellPosition.x) * 20)
            rectangle.layoutYProperty().set((blockPosition.y + blockCellPosition.y) * 20)
            Some(blockCell)
          case None =>
            None
        }
      case UninitializedBlockCell(cellType) =>
        val rectangle = allocateRectangle(cellType)
        rectangle.layoutXProperty().set((blockPosition.x + blockCellPosition.x) * 20)
        rectangle.layoutYProperty().set((blockPosition.y + blockCellPosition.y) * 20)
        val newUiId = uiIdCounter
        uiIdCounter = uiIdCounter + 1
        gameMapUI = gameMapUI + (newUiId -> rectangle)
        paneGameMap.getChildren.add(rectangle)
        Some(InitializedBlockCell(newUiId, cellType))
    }
  }

  def allocateExpectedLandingBlock(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(blockPosition: Position, blockCellPosition: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Option[BlockCell[Block5x5.BlockType.BlockType]] = {
    blockCell match {
      case UninitializedBlockCell(cellType) =>
        val rectangle = allocateRectangle(cellType)
        rectangle.layoutXProperty().set((blockPosition.x + blockCellPosition.x) * 20)
        rectangle.layoutYProperty().set((blockPosition.y + blockCellPosition.y) * 20)
        rectangle.opacityProperty().set(0.5)
        val newUiId = uiIdCounter
        uiIdCounter = uiIdCounter + 1
        gameMapUI = gameMapUI + (newUiId -> rectangle)
        paneGameMap.getChildren.add(rectangle)
        Some(InitializedBlockCell(newUiId, cellType))
    }
  }

  def allocateNextBlock(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(position: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Option[BlockCell[Block5x5.BlockType.BlockType]] = {
    blockCell match {
      case UninitializedBlockCell(cellType) =>
        val rectangle = allocateRectangle(cellType)
        rectangle.layoutXProperty().set(position.x * 20)
        rectangle.layoutYProperty().set(position.y * 20)
        val newUiId = uiIdCounter
        uiIdCounter = uiIdCounter + 1
        nextUI = nextUI + (newUiId -> rectangle)
        paneNextBlock.getChildren.add(rectangle)
        Some(InitializedBlockCell(newUiId, cellType))
    }
  }

  def allocateBePushed(paneBePushed: Pane)(position: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Option[BlockCell[Block5x5.BlockType.BlockType]] = {
    blockCell match {
      case UninitializedBlockCell(cellType) =>
        val rectangle = allocateRectangle(cellType)
        rectangle.layoutXProperty().set(position.x * 10)
        rectangle.layoutYProperty().set(position.y * 10)
        rectangle.widthProperty().set(10)
        rectangle.heightProperty().set(10)
        val newUiId = uiIdCounter
        uiIdCounter = uiIdCounter + 1
        bePushedUI = bePushedUI + (newUiId -> rectangle)
        paneBePushed.getChildren.add(rectangle)
        Some(InitializedBlockCell(newUiId, cellType))
    }
  }

  def moveGameMapBlockCell(blockCellPosition: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Unit = {
    blockCell match {
      case InitializedBlockCell(uiId, _) =>
        gameMapUI
          .get(uiId)
          .foreach { rectangle =>
            rectangle.layoutXProperty().set(blockCellPosition.x * 20)
            rectangle.layoutYProperty().set(blockCellPosition.y * 20)
          }
      case _ =>
    }
  }

  def moveBePushedBlockCell(blockCellPosition: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Unit = {
    blockCell match {
      case InitializedBlockCell(uiId, _) =>
        bePushedUI
          .get(uiId)
          .foreach { rectangle =>
            rectangle.layoutXProperty().set(blockCellPosition.x * 10)
            rectangle.layoutYProperty().set(blockCellPosition.y * 10)
          }
      case _ =>
    }
  }

  def deleteGameMapBlockCell(paneGameMap: Pane)(blockCell: BlockCell[Block5x5.BlockType.BlockType]): Unit = {
    blockCell match {
      case InitializedBlockCell(uiId, _) =>
        gameMapUI
          .get(uiId)
          .foreach { rectangle =>
            paneGameMap.getChildren.remove(rectangle)
            gameMapUI = gameMapUI - uiId
          }
      case _ =>
    }
  }

  def deleteBePushedBlockCell(paneBePushed: Pane)(blockCell: BlockCell[Block5x5.BlockType.BlockType]): Unit = {
    blockCell match {
      case InitializedBlockCell(uiId, _) =>
        bePushedUI
          .get(uiId)
          .foreach { rectangle =>
            paneBePushed.getChildren.remove(rectangle)
            bePushedUI = bePushedUI - uiId
          }
      case _ =>
    }
  }

  def moveFromBePushedToGameMapBlockCell(paneGameMap: Pane, paneBePushed: Pane)(blockCellPosition: Position, blockCell: BlockCell[Block5x5.BlockType.BlockType]): Unit = {
    blockCell match {
      case InitializedBlockCell(uiId, _) =>
        bePushedUI
          .get(uiId)
          .foreach { rectangle =>
            gameMapUI = gameMapUI + (uiId -> rectangle)
            bePushedUI = bePushedUI - uiId
            paneBePushed.getChildren.remove(rectangle)
            paneGameMap.getChildren.add(rectangle)
            rectangle.widthProperty().set(20)
            rectangle.heightProperty().set(20)
            rectangle.layoutXProperty().set(blockCellPosition.x * 20)
            rectangle.layoutYProperty().set(blockCellPosition.y * 20)
          }
      case _ =>
    }
  }
}
