/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.actor

import akka.actor.Actor
import akka.http.scaladsl.model.ws.{ Message, TextMessage }
import akka.stream.ActorMaterializer
import akka.stream.actor.ActorPublisherMessage.{ Cancel, Request }
import akka.stream.actor.ActorSubscriberMessage.{ OnComplete, OnError, OnNext }
import akka.stream.actor.{ ActorPublisher, ActorSubscriber, RequestStrategy, WatermarkRequestStrategy }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.{ GameRoomClientCommand, GameRoomServerCommand }
import org.imcharsi.tetrispractice.common.data.fsm.Command
import org.imcharsi.tetrispractice.common.data.fsm.Command.{ Leave, Stop }
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomClientCommandJsonFormat, GameRoomServerCommandJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm.StreamInputJsonFormat
import rx.lang.scala.Subject
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.collection.immutable.Queue

/**
 * Created by i on 9/10/16.
 */
class WebSocketWorker[BT] {

  // 왜 이와 같은 번거로운 방법을 쓰는가. Scala Pattern Match 기능에서는 Type 정보를 알 수 없다.
  // 예를 들어, case x: Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]] => 와 같이 써도
  // 실제로 x 는 Either[Boolean, Int] 일 수도 있는 것이다.
  // 그래서 아래와 같은 기능을 쓰면, Holder 가 입력됐다면 무조건
  // Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]] 가 입력되는 것이다.
  // 또한 Holder 를 WebSocketWorkerActor 의 Inner Class 가 아니도록 해야 하는 이유는,
  // Actor 의 instance 참조는 밖에서 알아서는 안되는 원칙 때문이다.
  // 그래서, BT Type Parameter 를 공유해야 하는 필요와 포장용 Class 를 밖에 두어야 하는 필요 때문에 이와 같이 번거롭게 썼다.
  case class Holder(h: Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]])

  // Server 로부터 받는 입력을 보내는 Subject 를 하나만 둔다. 이 Subject 를 걸러서 써야 한다.
  class WebSocketWorkerActor(subject: Subject[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]])(implicit blockTypeJsonFormat: JsonFormat[BT], am: ActorMaterializer) extends Actor
      with ActorPublisher[Message]
      with ActorSubscriber {
    implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
    implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
    implicit val streamInputJsonFormat = new StreamInputJsonFormat[BT]
    // queue 에 쌓이는 것은 전부 LocalPlayer 가 입력한 것이다.
    private[actor] var queue: Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]] = Queue()

    // 이 Actor 의 기능을 최소한으로 줄여야 한다.
    // 가장 중요한 이유는, 이 Actor 가 여러가지 역할을 하도록 한다면 Rx Subject 도 이 Actor 안에 둬야 하는데,
    // 상태를 Actor 안과 밖에서 공유하지 말라는 원칙에 반한다.
    // 따라서, 이 Actor 는 단순히 전송기 역할로만 쓴다.

    override protected def requestStrategy: RequestStrategy = WatermarkRequestStrategy(16)

    def processQueue(queue: Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]]): Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]] = {
      if (totalDemand > 0) {
        queue.dequeueOption match {
          case Some((dequeue, newQueue)) =>
            dequeue match {
              // Actor 종료는 Actor 가 입력을 인식하여 하지 않고
              // Rx 구독단계에서 Leaved 입력을 받았을 때 Actor 로 OnComplete 입력을 해주는 식으로 한다.
              // Actor 가 OnComplete 를 받으면, 생성자로 받은 Rx Subject 로 onComplete() 를 해주고 Actor 를 종료한다.
              // 이와 같이 종료하는 것이, 전체 구상에 맞다.
              // 앞의 구상에서는 Actor 에서 Leave/Stop 입력을 인식하여 Actor 를 종료하도록 했는데,
              // 이와 같이 하면 FSM 으로 Leave/Stop 입력이 주어지지 않는 문제가 생긴다.
              case x =>
                onNext(TextMessage.Strict(x.toJson.prettyPrint))
                processQueue(newQueue)
            }
          case None => Queue()
        }
      } else queue
    }

    override def receive: Actor.Receive = {
      case OnNext(TextMessage.Strict(string)) =>
        subject.onNext(string.parseJson.convertTo[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]])
      case OnNext(TextMessage.Streamed(source)) =>
        source.fold(StringBuilder.newBuilder)((z, s) => z.append(s))
          .runForeach(stringBuilder =>
            subject.onNext(stringBuilder.toString().parseJson.convertTo[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[BT]]]]))
      case OnError(ex) =>
        onError(ex)
        subject.onError(ex)
        context.stop(self)
      case OnComplete =>
        onComplete()
        subject.onCompleted()
        context.stop(self)
      case Request(_) =>
        queue = processQueue(queue)
      case Holder(h) =>
        queue = processQueue(queue.enqueue(h))
      case Cancel =>
        // 이 입력을 다뤄야 한다. 아니면 연결이 제대로 되지 않았을 때 Stream 이 제대로 종료하지 않는다.
        onComplete()
        subject.onCompleted()
        context.stop(self)
    }
  }
}

