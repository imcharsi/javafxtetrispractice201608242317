/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.ui

import java.net.URL
import java.util.ResourceBundle
import javafx.beans.property.{ SimpleBooleanProperty, SimpleObjectProperty }
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.fxml.{ FXML, FXMLLoader, Initializable }
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.scene.control._
import javafx.scene.{ Parent, Scene }
import javafx.stage.Stage
import javafx.util.Callback

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model.{ HttpRequest, HttpResponse }
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import org.imcharsi.tetrispractice.client.http.HttpPreparing
import org.imcharsi.tetrispractice.common.util.RxUtil
import rx.lang.scala.{ JavaConversions => JC, Observable, Scheduler, Worker }
import rx.observables.{ JavaFxObservable => JFO }

import scala.concurrent.ExecutionContext.Implicits
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 9/19/16.
 */
class ReplayRoomController(
    stage: Stage,
    javaFxScheduler: Scheduler,
    ioScheduler: Scheduler,
    javaFxWorker: Worker,
    ioWorker: Worker,
    textFieldServerName: TextField,
    textFieldServerPort: TextField,
    http: HttpExt,
    superPool: Flow[(HttpRequest, Int), (Try[HttpResponse], Int), NotUsed]
)(implicit val actorSystem: ActorSystem, actorMaterializer: ActorMaterializer) extends Initializable {
  @FXML
  private var tableViewReplayList: TableView[Int] = null
  @FXML
  private var tableColumnReplayId: TableColumn[Int, Int] = null
  @FXML
  private var buttonRefresh: Button = null
  @FXML
  private var buttonReplay: Button = null

  val httpPreparing: HttpPreparing[NotUsed] = new HttpPreparing[NotUsed](actorSystem, actorMaterializer, http, superPool)
  val temporaryDisableRefresh: SimpleBooleanProperty = new SimpleBooleanProperty(false)
  val temporaryDisableReplay: SimpleBooleanProperty = new SimpleBooleanProperty(false)

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    val tableViewReplayListVal = tableViewReplayList
    val tableColumnReplayIdVal = tableColumnReplayId
    val buttonRefreshVal = buttonRefresh
    val buttonReplayVal = buttonReplay
    tableColumnReplayIdVal.prefWidthProperty().bind(tableViewReplayListVal.widthProperty())

    val serverPortBinding =
      RxUtil.convertJavaFXProperty((x: String) => Integer.valueOf(x))(textFieldServerPort.textProperty())
    val observableButtonRefresh =
      RxUtil
        .combine(
          JC.toScalaObservable(JFO.fromActionEvents(buttonRefreshVal)),
          JC.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty()))
            .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(serverPortBinding)))
        )
        .filter {
          case (_, Some((_, Some(_)))) => true
          case _ => false
        }
        .map {
          case (_, Some((serverName, Some(serverPort)))) => s"${serverName}:${serverPort.intValue()}"
        }

    val observableButtonEnter =
      RxUtil
        .combine(
          JC.toScalaObservable(JFO.fromActionEvents(buttonReplayVal)),
          JC.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty()))
            .combineLatest(
              JC.toScalaObservable(
                JFO.fromObservableValue(
                  tableViewReplayListVal.getSelectionModel.selectedItemProperty()
                )
              )
                .map(Option(_))
            )
            .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(serverPortBinding)))
        )
        .filter {
          case ((_, Some(((_, Some(_)), Some(_))))) => true
          case _ => false
        }
        .map {
          case ((_, Some(((serverName, Some(replayId)), Some(serverPort))))) =>
            (s"${serverName}:${serverPort.intValue()}", replayId)
        }
    tableColumnReplayIdVal
      .cellValueFactoryProperty()
      .set(
        new Callback[TableColumn.CellDataFeatures[Int, Int], ObservableValue[Int]] {
          override def call(param: CellDataFeatures[Int, Int]): ObservableValue[Int] = {
            new SimpleObjectProperty[Int](param.getValue)
          }
        }
      )
    tableColumnReplayIdVal
      .cellFactoryProperty()
      .set(
        new Callback[TableColumn[Int, Int], TableCell[Int, Int]] {
          override def call(param: TableColumn[Int, Int]): TableCell[Int, Int] = {
            new TableCell[Int, Int] {
              override def updateItem(item: Int, empty: Boolean): Unit = {
                super.updateItem(item, empty)
                if (empty) {
                  setText(null)
                } else {
                  setText(item.toString)
                }
              }
            }
          }
        }
      )
    buttonRefreshVal.disableProperty().bind(temporaryDisableRefresh)
    buttonReplayVal.disableProperty()
      .bind(temporaryDisableReplay.or(tableViewReplayListVal.getSelectionModel.selectedItemProperty().isNull))

    observableButtonRefresh
      .doOnNext(_ => temporaryDisableRefresh.set(true))
      .observeOn(ioScheduler)
      .flatMap {
        case serverNamePort =>
          Observable.from(
            httpPreparing.getReplayList("http://" + serverNamePort)._1
              .map { x => Success(FXCollections.observableArrayList(x: _*)) }(Implicits.global)
              .recover { case ex => Failure(ex) }(Implicits.global)
          )(Implicits.global)
      }
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case Success(observableList) =>
            tableViewReplayListVal.itemsProperty().set(observableList)
          case Failure(ex) =>
            ioWorker.schedule(Console.err.println(ex))
        }
        temporaryDisableRefresh.set(false)
      }

    observableButtonEnter
      .doOnNext(_ => temporaryDisableReplay.set(true))
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case (serverNamePort, replayId) =>
            val myStage = new Stage()
            myStage.setScene(
              new Scene(
                FXMLLoader.load[Parent](
                  getClass.getResource("/org/imcharsi/tetrispractice/client/ui/gameRoom.fxml"),
                  null,
                  null,
                  new Callback[Class[_], AnyRef] {
                    override def call(param: Class[_]): AnyRef =
                      new ReplayController(
                        actorSystem,
                        actorMaterializer,
                        stage,
                        myStage,
                        httpPreparing,
                        javaFxScheduler,
                        ioScheduler,
                        javaFxWorker,
                        ioWorker,
                        serverNamePort,
                        replayId
                      )
                  }
                )
              )
            )
            myStage.show()
            myStage.resizableProperty().set(false)
        }
        ioWorker.schedule(Console.err.println(x))
        temporaryDisableReplay.set(false)
      }

  }
}
