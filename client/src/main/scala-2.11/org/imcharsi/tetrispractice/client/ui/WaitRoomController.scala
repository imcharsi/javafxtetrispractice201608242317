/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.ui

import java.net.URL
import java.util.ResourceBundle
import javafx.beans.property.{ SimpleObjectProperty, SimpleBooleanProperty }
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.fxml.{ FXML, FXMLLoader, Initializable }
import javafx.scene.control.TableColumn.CellDataFeatures
import javafx.scene.control._
import javafx.scene.{ Parent, Scene }
import javafx.stage.{ Stage, WindowEvent }
import javafx.util.Callback

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ HttpRequest, HttpResponse }
import akka.http.scaladsl.{ HttpExt, Http }
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import org.imcharsi.tetrispractice.client.actor.WebSocketWorker
import org.imcharsi.tetrispractice.client.http.HttpPreparing
import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5
import org.imcharsi.tetrispractice.common.util.RxUtil
import rx.lang.scala.{ JavaConversions => JC, Observable, Scheduler, Worker }
import rx.observables.{ JavaFxObservable => JFO }

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.{ Try, Failure, Success }

/**
 * Created by i on 9/10/16.
 */
class WaitRoomController(
    stage: Stage,
    javaFxScheduler: Scheduler,
    ioScheduler: Scheduler,
    javaFxWorker: Worker,
    ioWorker: Worker,
    textFieldServerName: TextField,
    textFieldServerPort: TextField,
    http: HttpExt,
    superPool: Flow[(HttpRequest, Int), (Try[HttpResponse], Int), NotUsed]
)(implicit val actorSystem: ActorSystem, actorMaterializer: ActorMaterializer) extends Initializable {
  @FXML
  private var tableViewRoomList: TableView[(Int, Int)] = _
  @FXML
  private var tableColumnRoomId: TableColumn[(Int, Int), Int] = _
  @FXML
  private var tableColumnPlayerCount: TableColumn[(Int, Int), Int] = _
  @FXML
  private var buttonRefresh: Button = _
  @FXML
  private var buttonCreate: Button = _
  @FXML
  private var buttonEnter: Button = _

  val httpPreparing: HttpPreparing[NotUsed] = new HttpPreparing[NotUsed](actorSystem, actorMaterializer, http, superPool)

  // initialize 바깥에서 UI Control 을 참조하는 기능을 써서는 안 된다. 아직 주입이 되지 않았기 때문이다.
  val temporaryDisableRefresh: SimpleBooleanProperty = new SimpleBooleanProperty(false)
  val temporaryDisableCreate: SimpleBooleanProperty = new SimpleBooleanProperty(false)
  val temporaryDisableEnter: SimpleBooleanProperty = new SimpleBooleanProperty(false)
  val webSocketWorker = new WebSocketWorker[Block5x5.BlockType.BlockType]

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    val serverPortBinding =
      RxUtil.convertJavaFXProperty((x: String) => Integer.valueOf(x))(textFieldServerPort.textProperty())
    val observableButtonRefresh =
      RxUtil
        .combine(
          JC.toScalaObservable(JFO.fromActionEvents(buttonRefresh)),
          JC.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty()))
            .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(serverPortBinding)))
        )
        .filter {
          case (_, Some((_, Some(_)))) => true
          case _ => false
        }
        .map {
          case (_, Some((serverName, Some(serverPort)))) => s"${serverName}:${serverPort.intValue()}"
        }

    val observableButtonCreate =
      RxUtil
        .combine(
          JC.toScalaObservable(JFO.fromActionEvents(buttonCreate)),
          JC.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty()))
            .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(serverPortBinding)))
        )
        .filter {
          case (_, Some((_, Some(_)))) => true
          case _ => false
        }
        .map {
          case (_, Some((serverName, Some(serverPort)))) => s"${serverName}:${serverPort.intValue()}"
        }

    val observableButtonEnter =
      RxUtil
        .combine(
          JC.toScalaObservable(JFO.fromActionEvents(buttonEnter)),
          JC.toScalaObservable(JFO.fromObservableValue(textFieldServerName.textProperty()))
            .combineLatest(
              JC.toScalaObservable(
                JFO.fromObservableValue(
                  tableViewRoomList.getSelectionModel.selectedItemProperty()
                )
              )
                .map(Option(_))
            )
            .combineLatest(JC.toScalaObservable(JFO.fromObservableValue(serverPortBinding)))
        )
        .filter {
          case ((_, Some(((_, Some(_)), Some(_))))) => true
          case _ => false
        }
        .map {
          case ((_, Some(((serverName, Some((roomId, _))), Some(serverPort))))) =>
            (s"${serverName}:${serverPort.intValue()}", roomId)
        }

    // 이와 같이 하면 필요가 없는 ScrollBar 가 생긴다. 좋은 방법 없나.
    tableColumnRoomId.prefWidthProperty()
      .bind(
        tableViewRoomList.widthProperty()
          .divide(2)
      )
    tableColumnPlayerCount.prefWidthProperty()
      .bind(
        tableViewRoomList.widthProperty()
          .subtract(tableColumnRoomId.widthProperty())
      )
    tableColumnRoomId
      .cellValueFactoryProperty()
      .set(
        new Callback[TableColumn.CellDataFeatures[(Int, Int), Int], ObservableValue[Int]] {
          override def call(param: CellDataFeatures[(Int, Int), Int]): ObservableValue[Int] = {
            new SimpleObjectProperty[Int](param.getValue._1)
          }
        }
      )
    tableColumnPlayerCount
      .cellValueFactoryProperty()
      .set(
        new Callback[TableColumn.CellDataFeatures[(Int, Int), Int], ObservableValue[Int]] {
          override def call(param: CellDataFeatures[(Int, Int), Int]): ObservableValue[Int] = {
            new SimpleObjectProperty[Int](param.getValue._2)
          }
        }
      )
    tableColumnRoomId
      .cellFactoryProperty()
      .set(
        new Callback[TableColumn[(Int, Int), Int], TableCell[(Int, Int), Int]] {
          override def call(param: TableColumn[(Int, Int), Int]): TableCell[(Int, Int), Int] = {
            new TableCell[(Int, Int), Int] {
              override def updateItem(item: Int, empty: Boolean): Unit = {
                super.updateItem(item, empty)
                if (empty) {
                  setText(null)
                } else {
                  setText(item.toString)
                }
              }
            }
          }
        }
      )
    tableColumnPlayerCount
      .cellFactoryProperty()
      .set(
        new Callback[TableColumn[(Int, Int), Int], TableCell[(Int, Int), Int]] {
          override def call(param: TableColumn[(Int, Int), Int]): TableCell[(Int, Int), Int] = {
            new TableCell[(Int, Int), Int] {
              override def updateItem(item: Int, empty: Boolean): Unit = {
                super.updateItem(item, empty)
                if (empty) {
                  setText(null)
                } else {
                  setText(item.toString)
                }
              }
            }
          }
        }
      )

    buttonRefresh.disableProperty().bind(temporaryDisableRefresh)
    buttonCreate.disableProperty().bind(temporaryDisableCreate)
    buttonEnter.disableProperty().bind(
      tableViewRoomList.getSelectionModel.selectedItemProperty().isNull
        .or(temporaryDisableEnter)
    )

    val tableViewRoomListVal = tableViewRoomList
    observableButtonRefresh
      .doOnNext(_ => temporaryDisableRefresh.set(true))
      .observeOn(ioScheduler)
      .flatMap {
        case serverNamePort =>
          Observable.from(
            httpPreparing.getRoomList("http://" + serverNamePort)._1
              .map { x => Success(FXCollections.observableArrayList(x.roomMap.toList: _*)) }(Implicits.global)
              .recover { case ex => Failure(ex) }(Implicits.global)
          )(Implicits.global)
      }
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case Success(observableList) =>
            tableViewRoomListVal.itemsProperty().set(observableList)
          case Failure(ex) =>
            ioWorker.schedule(Console.err.println(ex))
        }
        temporaryDisableRefresh.set(false)
      }

    observableButtonCreate
      .doOnNext(_ => temporaryDisableCreate.set(true))
      .observeOn(ioScheduler)
      .flatMap { serverNamePort =>
        Observable.from(
          httpPreparing.createRoom("http://" + serverNamePort)._1
            .map(x => Success((x, serverNamePort)))(Implicits.global)
            .recover { case ex => Failure(ex) }(Implicits.global)
        )(Implicits.global)
      }
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case Success((createdRoom, serverNamePort)) =>
            val myStage = new Stage()
            myStage.setScene(
              new Scene(
                FXMLLoader.load[Parent](
                  getClass.getResource("/org/imcharsi/tetrispractice/client/ui/gameRoom.fxml"),
                  null,
                  null,
                  new Callback[Class[_], AnyRef] {
                    override def call(param: Class[_]): AnyRef =
                      new GameRoomController(
                        actorSystem,
                        actorMaterializer,
                        stage,
                        myStage,
                        httpPreparing,
                        javaFxScheduler,
                        ioScheduler,
                        javaFxWorker,
                        ioWorker,
                        serverNamePort,
                        Right(createdRoom),
                        webSocketWorker
                      )
                  }
                )
              )
            )
            myStage.show()
            // 최대화가 되지 않도록 하는 가장 쉬운 방법은, 최대화 기능을 비활성화시키는 것이다.
            // http://stackoverflow.com/questions/30521822/javafx-disable-maximize-button-from-stagestyle-utility
            myStage.resizableProperty().set(false)
          case Failure(ex) =>
            Console.err.println(ex)
        }
        ioWorker.schedule(Console.err.println(x))
        temporaryDisableCreate.set(false)
      }

    observableButtonEnter
      .doOnNext(_ => temporaryDisableEnter.set(true))
      .observeOn(javaFxScheduler)
      .subscribe { x =>
        x match {
          case (serverNamePort, roomId) =>
            val myStage = new Stage()
            myStage.setScene(
              new Scene(
                FXMLLoader.load[Parent](
                  getClass.getResource("/org/imcharsi/tetrispractice/client/ui/gameRoom.fxml"),
                  null,
                  null,
                  new Callback[Class[_], AnyRef] {
                    override def call(param: Class[_]): AnyRef =
                      new GameRoomController(
                        actorSystem,
                        actorMaterializer,
                        stage,
                        myStage,
                        httpPreparing,
                        javaFxScheduler,
                        ioScheduler,
                        javaFxWorker,
                        ioWorker,
                        serverNamePort,
                        Left(roomId),
                        webSocketWorker
                      )
                  }
                )
              )
            )
            myStage.show()
            myStage.resizableProperty().set(false)
        }
        ioWorker.schedule(Console.err.println(x))
        temporaryDisableEnter.set(false)
      }

    JC.toScalaObservable(JFO.fromWindowEvents(stage, WindowEvent.WINDOW_CLOSE_REQUEST))
      .observeOn(ioScheduler)
      .subscribe { _ =>
        http.shutdownAllConnectionPools().flatMap(_ => actorSystem.terminate())(Implicits.global)
      }
  }
}
