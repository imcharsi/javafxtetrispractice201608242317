/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.ui

import java.net.URL
import java.util.ResourceBundle
import javafx.fxml.{ FXML, FXMLLoader, Initializable }
import javafx.scene.control.{ Button, ToggleButton }
import javafx.scene.layout.{ FlowPane, HBox, Pane }
import javafx.scene.{ Group, Node, Parent }
import javafx.stage.{ Stage, WindowEvent }

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Sink, Source }
import org.imcharsi.tetrispractice.client.http.HttpPreparing
import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ BlockCell, UninitializedBlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.Command._
import org.imcharsi.tetrispractice.common.data.fsm.GameState.{ GameState, NotYetPreparedGame, RunningGame, WaitFeedback }
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ Player, RemotePlayer }
import org.imcharsi.tetrispractice.common.data.fsm._
import org.imcharsi.tetrispractice.common.fsm.{ Game, Util }
import org.imcharsi.tetrispractice.common.util.Log.Log
import org.imcharsi.tetrispractice.common.util.State.StateMonadBehavior
import org.imcharsi.tetrispractice.common.util.{ State, Valve }
import rx.lang.scala.subjects.ReplaySubject
import rx.lang.scala.{ JavaConversions => JC, _ }
import rx.observables.{ JavaFxObservable => JFO }

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 9/19/16.
 */
class ReplayController(
    actorSystem: ActorSystem,
    actorMaterializer: ActorMaterializer,
    stage: Stage,
    myStage: Stage,
    httpPreparing: HttpPreparing[_],
    javaFxScheduler: Scheduler,
    ioScheduler: Scheduler,
    javaFxWorker: Worker,
    ioWorker: Worker,
    url: String,
    replayId: Int
) extends Initializable with BlockUtil {
  @FXML
  private var tetrisPaneLocalPlayer: HBox = null
  @FXML
  private var flowPaneRemotePlayers: FlowPane = null

  val clearedGameMap =
    GameMap[Block5x5.BlockType.BlockType](
      Position(10, 20),
      Util.prepareClearedMap[Block5x5.BlockType.BlockType](
        UninitializedBlockCell(Block5x5.BlockType.BlockOne),
        Position(10, 20)
      )(Map())
    )

  val functionHolder = Game.FunctionHolder(Block5x5.lcg, Block5x5.generateBlock, Block5x5.rotate)
  val runningGameSMB =
    new StateMonadBehavior[(RunningGame[Block5x5.BlockType.BlockType], State.State[Queue[Log], Unit])]
  val subjectMaster = ReplaySubject[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]]
  val subjectMasterFeedback = Subject[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]]
  val subjectMasterConnectable = subjectMaster //.zip(Observable.interval(0.2.seconds)).map(_._1)

  var remotePlayerMap: Map[Player, Node] = Map()
  var remotePlayerSubscriptionMap: Map[Player, Subscription] = Map()
  var alreadyUsedMasterPane: Boolean = false

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    val buttonStart = tetrisPaneLocalPlayer.lookup("#buttonStart").asInstanceOf[Button]
    val buttonPrepare = tetrisPaneLocalPlayer.lookup("#buttonPrepare").asInstanceOf[ToggleButton]
    val buttonLeave = tetrisPaneLocalPlayer.lookup("#buttonLeave").asInstanceOf[Button]
    val paneGameMap = tetrisPaneLocalPlayer.lookup("#paneGameMap").asInstanceOf[Pane]
    val paneNextBlock = tetrisPaneLocalPlayer.lookup("#paneNextBlock").asInstanceOf[Pane]
    val paneBePushed = tetrisPaneLocalPlayer.lookup("#paneBePushed").asInstanceOf[Pane]
    val flowPaneRemotePlayersVal = flowPaneRemotePlayers

    buttonStart.disableProperty().set(true)
    buttonPrepare.disableProperty().set(true)
    buttonLeave.disableProperty().set(true)

    JC.toScalaObservable(JFO.fromWindowEvents(stage, WindowEvent.WINDOW_CLOSE_REQUEST))
      .observeOn(javaFxScheduler)
      .subscribe { _ =>
        myStage.close()
      }

    httpPreparing.getReplay("http://" + url, replayId)(Block5x5.Block5x5JsonFormat)._1.onComplete {
      case Success(source) =>
        // Akka Stream 에서 RxJava 로 한꺼번에 너무 많이 보내면
        // PublishSubject: could not emit value due to lack of requests
        // 이런 오류가 생긴다. 원인을 모르겠다. 아무래도 가장 마지막의 입력이 실제로 처리되기까지의 시간초과설정이 있는 듯 하다.
        // 대안은 두 개인데, RxJava 의 ReplaySubject 기능을 쓰는 것과 Akka Stream 의 속도 조절 기능이다. 둘 다 잘 된다.
        // 이상적인 방법은 RxJava 와 Akka Stream 이 연계되어서 속도를 조절하는 것인데,
        // RxJava 1.x 에서는 속도 조절 기능이 없으므로 한꺼번에 전부 받아서 쓰는 방법으로 했다.
        Observable
          .from {
            source
              .fold(Queue[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[Block5x5.BlockType.BlockType]]]]())((q, x) => q.enqueue(x))
              .map(Observable.from(_).zip(Observable.interval(0.1.seconds)).map(_._1))
              .runWith(Sink.head)(actorMaterializer)
          }(Implicits.global)
          .flatten
          .observeOn(ioScheduler)
          .observeOn(javaFxScheduler)
          .subscribe(
            subscribeFromServerF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayersVal)(_),
            Console.err.println(_),
            () => { Console.out.println("completed.") }
          )
      case Failure(ex) =>
    }(Implicits.global)
  }

  def subscribeFromServerF(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(x: Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[Block5x5.BlockType.BlockType]]]): Unit = {
    x match {
      case Right(Right(streamInput)) =>
        subjectMaster.onNext(Right(streamInput))
      case Right(Left(NotifyJoinRoom(responseId, remotePlayer))) =>
        val result =
          remotePlayerMap.get(remotePlayer) match {
            case Some(_) =>
              myStage.close()
            case None =>
              Try {
                val tetrisPane =
                  FXMLLoader.load[Parent](
                    getClass.getResource("/org/imcharsi/tetrispractice/client/ui/tetrisPane.fxml")
                  )
                tetrisPane.scaleXProperty().set(0.5)
                tetrisPane.scaleYProperty().set(0.5)
                val newNode = new Group(tetrisPane)
                val buttonStart = newNode.lookup("#buttonStart").asInstanceOf[Button]
                val buttonPrepare = newNode.lookup("#buttonPrepare").asInstanceOf[ToggleButton]
                val buttonLeave = newNode.lookup("#buttonLeave").asInstanceOf[Button]
                buttonStart.disableProperty().set(true)
                buttonPrepare.disableProperty().set(true)
                buttonLeave.disableProperty().set(true)
                remotePlayerMap = remotePlayerMap + (remotePlayer -> newNode)
                if (alreadyUsedMasterPane) {
                  flowPaneRemotePlayers.getChildren.add(newNode)

                  val paneGameMap = tetrisPane.lookup("#paneGameMap").asInstanceOf[Pane]
                  val paneNextBlock = tetrisPane.lookup("#paneNextBlock").asInstanceOf[Pane]
                  val paneBePushed = tetrisPane.lookup("#paneBePushed").asInstanceOf[Pane]
                  subscribeStreamOutputF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(_)
                } else {
                  alreadyUsedMasterPane = true
                  subscribeStreamOutputF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(_)
                }
              } match {
                case Success(subscribeF) =>
                  val notYetPreparedGame: (Either[WaitFeedback[Block5x5.BlockType.BlockType], GameState[Block5x5.BlockType.BlockType]], State.State[Queue[Log], Unit], Option[StreamOutput[Block5x5.BlockType.BlockType]]) =
                    (Right(NotYetPreparedGame[Block5x5.BlockType.BlockType](remotePlayer)), State(()), None)
                  val subscription =
                    Valve.valve[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]](_.isLeft)(
                      subjectMasterConnectable
                        .merge(subjectMasterFeedback)
                        .filter { x => Game.filterF[Block5x5.BlockType.BlockType](remotePlayer)(x) }
                    )
                      .observeOn(ioScheduler)
                      .scan(notYetPreparedGame)(Game.scanF[Block5x5.BlockType.BlockType](clearedGameMap, functionHolder, Position(3, 0), runningGameSMB))
                      .flatMap(Game.mapF)
                      .observeOn(javaFxScheduler)
                      .subscribe(subscribeF)
                  remotePlayerSubscriptionMap = remotePlayerSubscriptionMap + (remotePlayer -> subscription)
                case Failure(x) =>
                  myStage.close()
              }
          }
    }
  }

  def subscribeStreamOutputF(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(x: (StreamOutput[Block5x5.BlockType.BlockType], State.State[Queue[Log], Unit])): Unit = {
    val (streamOutput, _) = x
    streamOutput match {
      case Prepared(player @ RemotePlayer(_, _), result, prepare) =>
        subjectMasterFeedback.onNext(Left(PrepareFeedback(player)))
      case Started(player @ RemotePlayer(_, _), result, start) =>
        subjectMasterFeedback.onNext(Left(StartFeedback(player)))
      case RealizedNextBlock(player @ RemotePlayer(_, _), blockAndNext, moved, deleted, movedFromBePushedToMap, realizeNextBlock) =>
        val result =
          blockAndNext
            .flatMap {
              case ((currentBlock, expectedLandingBlock), nextBlock) =>
                val z: Option[List[(Position, BlockCell[Block5x5.BlockType.BlockType])]] = Some(List())
                val newCurrentBlock =
                  currentBlock.map.toList.map {
                    case (position, blockCell) =>
                      allocateCurrentBlockCell(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(currentBlock.position, position, blockCell).map(x => (position, x))
                  }
                val newCurrentBlockMap =
                  newCurrentBlock.foldLeft(z) {
                    case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
                    case _ => None
                  }.map(_.toMap)
                val newExpectedLandingBlock =
                  expectedLandingBlock.map.toList.map {
                    case (position, blockCell) =>
                      allocateExpectedLandingBlock(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(expectedLandingBlock.position, position, blockCell).map(x => (position, x))
                  }
                val newExpectedLandingBlockMap =
                  newExpectedLandingBlock.foldLeft(z) {
                    case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
                    case _ => None
                  }.map(_.toMap)
                val newNextBlock =
                  nextBlock.map.toList.map {
                    case (position, blockCell) =>
                      allocateNextBlock(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(position, blockCell).map(x => (position, x))
                  }
                val newNextBlockMap =
                  newNextBlock.foldLeft(z) {
                    case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
                    case _ => None
                  }.map(_.toMap)

                newCurrentBlockMap
                  .flatMap { r1 =>
                    newExpectedLandingBlockMap.flatMap { r2 =>
                      newNextBlockMap.map { r3 =>
                        Tuple4(
                          0,
                          currentBlock.copy(map = r1),
                          nextBlock.copy(map = r3),
                          expectedLandingBlock.copy(map = r2)
                        )
                      }
                    }
                  }
            }
        moved.foreach {
          case (position, blockCell) =>
            moveGameMapBlockCell(position, blockCell)
        }
        deleted.foreach {
          case (_, blockCell) =>
            deleteGameMapBlockCell(paneGameMap)(blockCell)
        }
        movedFromBePushedToMap.foreach {
          case (position, blockCell) =>
            moveFromBePushedToGameMapBlockCell(paneGameMap, paneBePushed)(position, blockCell)
        }
        subjectMasterFeedback.onNext(Left(RealizeNextBlockFeedback(player, result)))

        if (blockAndNext.isEmpty)
          remotePlayerSubscriptionMap.get(player).foreach { x =>
            x.unsubscribe()
            remotePlayerSubscriptionMap = remotePlayerSubscriptionMap - player
          }
        if (result.isEmpty && blockAndNext.isDefined) {
          myStage.close()
        }
      case BlockTransformed(player @ RemotePlayer(_, _), result, move) =>
        result match {
          case Some(Right(None)) =>
            subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, None)))
          case Some(Right(Some((currentBlock, expectedLandingBlock)))) =>
            currentBlock.map.toList.map {
              case (position, blockCell) =>
                moveGameMapBlockCell(currentBlock.position + position, blockCell)
            }
            expectedLandingBlock.map.toList.map {
              case (position, blockCell) =>
                moveGameMapBlockCell(expectedLandingBlock.position + position, blockCell)
            }
            move match {
              case BlockTransform(_, BlockTransformType.Bottom) =>
                subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, Some(0))))
              case BlockTransform(_, BlockTransformType.Down | BlockTransformType.TimeElapsed(_)) =>
                subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, Some(0))))
              case _ =>
                subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, None)))
            }
          case Some(Left((moved, deleted, preparePush))) =>
            moved.foreach(_.foreach { case (position, blockCell) => moveGameMapBlockCell(position, blockCell) })
            deleted.foreach(_.foreach { case (position, blockCell) => deleteGameMapBlockCell(paneGameMap)(blockCell) })
            subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, None)))
        }
      case Pushed(player @ RemotePlayer(_, _), beAllocated, moved, deleted, push) =>
        val z: Option[List[(Position, BlockCell[Block5x5.BlockType.BlockType])]] = Some(List())
        val newBeAllocated =
          beAllocated
            .map {
              case (position, blockCell) =>
                allocateBePushed(paneBePushed)(position, blockCell).map(x => (position, x))
            }
            .foldLeft(z) {
              case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
              case _ => None
            }
        deleted.foreach { case (position, blockCell) => deleteBePushedBlockCell(paneBePushed)(blockCell) }
        moved.foreach { case (position, blockCell) => moveBePushedBlockCell(position, blockCell) }
        subjectMasterFeedback.onNext(Left(PushFeedback(player, newBeAllocated)))
        if (newBeAllocated.isEmpty) {
          myStage.close()
        }
      case _ =>
    }
  }
}
