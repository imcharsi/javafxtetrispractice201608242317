/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.ui

import java.net.URL
import java.util.ResourceBundle
import javafx.fxml.{ FXMLLoader, FXML, Initializable }
import javafx.scene.Parent
import javafx.scene.control.{ Tab, TabPane, TextField }
import javafx.stage.Stage
import javafx.util.Callback

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import rx.lang.scala.{ Worker, Scheduler }

/**
 * Created by i on 9/19/16.
 */
class ClientMasterController(
    stage: Stage,
    javaFxScheduler: Scheduler,
    ioScheduler: Scheduler,
    javaFxWorker: Worker,
    ioWorker: Worker //,
) extends Initializable {
  @FXML
  private var textFieldServerName: TextField = null
  @FXML
  private var textFieldServerPort: TextField = null
  @FXML
  private var tabPane: TabPane = null
  @FXML
  private var tabWaitRoom: Tab = null
  @FXML
  private var tabReplayRoom: Tab = null

  // Client 와 Server 의 ActorSystem 을 분리해야 한다.
  // Client 는 한 번만 만들면 되는데, Server 는 껐다 켰다를 반복할 수 있기 때문에 켤 때마다 매번 만들어야 한다.
  implicit val actorSystem = ActorSystem()
  implicit val actorMaterializer = ActorMaterializer()
  val http = Http()
  val superPool = http.superPool[Int]()

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    val textFieldServerNameVal = textFieldServerName
    val textFieldServerPortVal = textFieldServerPort
    val tabPaneVal = tabPane
    val tabWaitRoomVal = tabWaitRoom
    val tabReplayRoomVal = tabReplayRoom

    tabWaitRoomVal.contentProperty().set(
      FXMLLoader.load[Parent](
        getClass.getResource("/org/imcharsi/tetrispractice/client/ui/waitRoom.fxml"),
        null,
        null,
        new Callback[Class[_], AnyRef] {
          override def call(param: Class[_]): AnyRef = {
            new WaitRoomController(
              stage,
              javaFxScheduler,
              ioScheduler,
              javaFxWorker,
              ioWorker,
              textFieldServerNameVal,
              textFieldServerPortVal,
              http,
              superPool
            )(actorSystem, actorMaterializer)
          }
        }
      )
    )

    tabReplayRoom.contentProperty().set(
      FXMLLoader.load[Parent](
        getClass.getResource("/org/imcharsi/tetrispractice/client/ui/replayRoom.fxml"),
        null,
        null,
        new Callback[Class[_], AnyRef] {
          override def call(param: Class[_]): AnyRef = {
            new ReplayRoomController(
              stage,
              javaFxScheduler,
              ioScheduler,
              javaFxWorker,
              ioWorker,
              textFieldServerNameVal,
              textFieldServerPortVal,
              http,
              superPool
            )(actorSystem, actorMaterializer)
          }
        }
      )
    )
  }
}
