/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.http

import akka.actor.ActorSystem
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Keep, Sink, Source }
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand.{ GameRoomClientCommand, GameRoomServerCommand }
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.{ CreatedRoom, GotRoomList, WaitRoomResponse }
import org.imcharsi.tetrispractice.common.data.fsm.Command.StreamInput
import org.imcharsi.tetrispractice.common.json.clientserver.{ GameRoomServerCommandJsonFormat, GameRoomClientCommandJsonFormat, WaitRoomResponseJsonFormat }
import org.imcharsi.tetrispractice.common.json.fsm.StreamInputJsonFormat
import spray.json._

import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.Future
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 9/10/16.
 */
class HttpPreparing[M](
    as: ActorSystem,
    am: ActorMaterializer,
    h: HttpExt,
    sp: Flow[(HttpRequest, Int), (Try[HttpResponse], Int), M]
) {
  implicit val actorSystem = as
  implicit val actorMaterializer = am
  val http = h
  val superPool = sp
  // superPool 을 써야 검사를 쉽게 할 수 있다.

  def requestTemplate[A <: WaitRoomResponse](
    url: String,
    remainingUrl: String,
    httpMethod: HttpMethod
  ): (Future[A], M) = {
    implicit val waitRoomResponseJsonFormat = WaitRoomResponseJsonFormat
    val (materialized, future) =
      Source
        .single[(HttpRequest, Int)](
          Tuple2(
            HttpRequest(
              httpMethod,
              s"${url}/${remainingUrl}",
              List(Accept(MediaTypes.`application/json`))
            ),
            1
          )
        )
        .viaMat(superPool)(Keep.right)
        .toMat(Sink.head)(Keep.both)
        .run()
    val result =
      future
        .flatMap {
          case (Success(response), _) => Future.successful(response)
          case (Failure(ex), _) => Future.failed(ex)
        }(Implicits.global)
        .flatMap { x =>
          x.entity.dataBytes
            .map(
              _.utf8String.parseJson
              .convertTo[WaitRoomResponse]
              .asInstanceOf[A]
            )
            .toMat(Sink.head)(Keep.right)
            .run()
        }(Implicits.global)
    (result, materialized)
  }

  def getRoomList(url: String): (Future[GotRoomList], M) =
    requestTemplate(url, "getRoomList", HttpMethods.GET)

  def createRoom(url: String): (Future[CreatedRoom], M) =
    requestTemplate(url, "createRoom", HttpMethods.POST)

  def getReplayList(url: String): (Future[List[Int]], M) = {
    import DefaultJsonProtocol._
    val (materialized, future) =
      Source
        .single[(HttpRequest, Int)](
          Tuple2(
            HttpRequest(
              HttpMethods.GET,
              s"${url}/replay",
              List(Accept(MediaTypes.`application/json`))
            ),
            1
          )
        )
        .viaMat(superPool)(Keep.right)
        .toMat(Sink.head)(Keep.both)
        .run()
    val result =
      future
        .flatMap {
          case (Success(response), _) => Future.successful(response)
          case (Failure(ex), _) => Future.failed(ex)
        }(Implicits.global)
        .flatMap { x =>
          x.entity.dataBytes
            .map(
              _.utf8String.parseJson
              .convertTo[List[Int]]
            )
            .toMat(Sink.head)(Keep.right)
            .run()
        }(Implicits.global)
    (result, materialized)
  }

  def getReplay[BT](url: String, replayId: Int)(implicit blockTypeJsonFormat: JsonFormat[BT]): (Future[Source[Either[GameRoomClientCommand, Either[GameRoomServerCommand, StreamInput[BT]]], _]], M) = {
    implicit val gameRoomClientCommandJsonFormat = GameRoomClientCommandJsonFormat
    implicit val gameRoomServerCommandJsonFormat = GameRoomServerCommandJsonFormat
    implicit val streamInputJsonFormat = new StreamInputJsonFormat[BT]
    import DefaultJsonProtocol._
    val (materialized, future) =
      Source
        .single[(HttpRequest, Int)](
          Tuple2(
            HttpRequest(
              HttpMethods.GET,
              s"${url}/replay/${replayId}",
              List(Accept(MediaTypes.`application/json`))
            ),
            1
          )
        )
        .viaMat(superPool)(Keep.right)
        .toMat(Sink.head)(Keep.both)
        .run()
    val result =
      future
        .flatMap {
          case (Success(response), _) =>
            Future.successful(response)
          case (Failure(ex), _) => Future.failed(ex)
        }(Implicits.global)
        .map { x =>
          x.entity.dataBytes
            .map(
              _.utf8String.parseJson
              .convertTo[Either[GameRoomClientCommand, Either[GameRoomServerCommand, StreamInput[BT]]]]
            )
        }(Implicits.global)
    (result, materialized)
  }
}
