/*
 * Copyright 2016 KangWoo, Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.imcharsi.tetrispractice.client.ui

import java.net.URL
import java.util.ResourceBundle
import javafx.beans.binding.When
import javafx.beans.property.SimpleBooleanProperty
import javafx.fxml.{ FXML, FXMLLoader, Initializable }
import javafx.scene.control.{ Button, ToggleButton }
import javafx.scene.input.{ KeyCode, KeyEvent }
import javafx.scene.layout.{ FlowPane, HBox, Pane }
import javafx.scene.{ Group, Node, Parent }
import javafx.stage.{ Stage, WindowEvent }

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.http.scaladsl.model.ws.{ Message, ValidUpgrade, WebSocketRequest }
import akka.stream.ActorMaterializer
import akka.stream.actor.ActorSubscriberMessage.OnComplete
import akka.stream.actor.{ ActorPublisher, ActorSubscriber }
import akka.stream.scaladsl.{ Flow, Sink, Source }
import org.imcharsi.tetrispractice.client.actor.WebSocketWorker
import org.imcharsi.tetrispractice.client.http.HttpPreparing
import org.imcharsi.tetrispractice.common.data.block5x5.Block5x5
import org.imcharsi.tetrispractice.common.data.clientserver.GameRoomCommand._
import org.imcharsi.tetrispractice.common.data.clientserver.WaitRoomCommand.CreatedRoom
import org.imcharsi.tetrispractice.common.data.fsm.BlockCell.{ BlockCell, UninitializedBlockCell }
import org.imcharsi.tetrispractice.common.data.fsm.BlockTransformType.TimeElapsed
import org.imcharsi.tetrispractice.common.data.fsm.Command._
import org.imcharsi.tetrispractice.common.data.fsm.GameState.{ GameState, NotYetPreparedGame, RunningGame, WaitFeedback }
import org.imcharsi.tetrispractice.common.data.fsm.Player.{ LocalPlayer, Player }
import org.imcharsi.tetrispractice.common.data.fsm._
import org.imcharsi.tetrispractice.common.fsm.{ Game, Util }
import org.imcharsi.tetrispractice.common.util.Log.Log
import org.imcharsi.tetrispractice.common.util.State.StateMonadBehavior
import org.imcharsi.tetrispractice.common.util.{ State, Valve }
import rx.lang.scala.subjects.ReplaySubject
import rx.lang.scala.{ JavaConversions => JC, _ }
import rx.observables.{ JavaFxObservable => JFO }

import scala.collection.immutable.Queue
import scala.concurrent.ExecutionContext.Implicits
import scala.concurrent.duration._
import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 9/11/16.
 */
class GameRoomController(
    actorSystem: ActorSystem,
    actorMaterializer: ActorMaterializer,
    stage: Stage,
    myStage: Stage,
    httpPreparing: HttpPreparing[_],
    javaFxScheduler: Scheduler,
    ioScheduler: Scheduler,
    javaFxWorker: Worker,
    ioWorker: Worker,
    url: String,
    createdRoom: Either[Int, CreatedRoom],
    webSocketWorker: WebSocketWorker[Block5x5.BlockType.BlockType]
) extends Initializable with BlockUtil {
  @FXML
  private var tetrisPaneLocalPlayer: HBox = null
  @FXML
  private var flowPaneRemotePlayers: FlowPane = null

  val clearedGameMap =
    GameMap[Block5x5.BlockType.BlockType](
      Position(10, 20),
      Util.prepareClearedMap[Block5x5.BlockType.BlockType](
        UninitializedBlockCell(Block5x5.BlockType.BlockOne),
        Position(10, 20)
      )(Map())
    )

  val functionHolder = Game.FunctionHolder(Block5x5.lcg, Block5x5.generateBlock, Block5x5.rotate)
  val runningGameSMB =
    new StateMonadBehavior[(RunningGame[Block5x5.BlockType.BlockType], State.State[Queue[Log], Unit])]
  val subjectFromServer = Subject[Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[Block5x5.BlockType.BlockType]]]]
  // 원래 구상은, LocalPlayer 만을 위한 Subject 와 각 RemotePlayer 별로 Subject 를 따로 두는 것이었는데
  // 하다보니 하나의 Subject 를 공유하도록 만들게 되었다. 이름과 용도가 같도록 했다.
  val subjectMaster = ReplaySubject[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]]
  val subjectMasterFeedback = Subject[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]]
  // 아래의 문장과 위의 ReplaySubject 는, 찾아내기 어려운 문제에 대한 대안으로서 사용되었다.
  // 문제의 원인을 알게 되면 해결방법을 찾는 것은 어려운 문제가 아닌데,
  // 이러한 문제가 있다는 것 자체를 알기가 쉽지 않을 뿐만 아니라 원인도 찾기 쉽지 않다.
  // 문제의 원인은, 기계의 실행속도로 인해 각 FSM 이 subjectMaster 와 subjectMasterFeedback 을 구독하도록 하는 과정이 완료되기 전에
  // Prepare 를 입력할 수 있고, 그래서 subjectMaster 가 ReplaySubject 가 아니라서 구독이 완료되기 전의 Prepare 입력은 잃어버리게 된다는 데 있다.
  // 그런데 단순히 ReplaySubject 를 하면, RemotePlayer FSM 도 구독을 할 때마다
  // 여태까지 ReplaySubject 로 입력되어 왔던 것이 매번 전부 반복되므로
  // 최초의 구독이 있기 전까지 입력됐던 것만 반복하도록 하기 위해 ConnectableObservable 기능을 같이 쓰게 됐다.
  // 이 문제를 일부러 드러내는 방법은, ioWorker.scheduler 에 구독과정을 넣고, 구독에 앞서서 적당한 지연시간을 주는 것이다.
  // 이와 같이 하면 구독이 완료되기 전의 지연시간 동안의 Prepare 입력은 FSM 에 입력되지 않게 되고,
  // 구독이 완료된 후 한 번 더 Prepare 를 누르면 그때는 Cancel 이 입력되어 FSM 은 NotYetPreparedGame 상태에서 Cancel 입력을 받게 된다.
  val subjectMasterConnectable = subjectMaster.publish.autoConnect

  val isStarter = new SimpleBooleanProperty(false)
  val receivedBeAbleToStart = new SimpleBooleanProperty(false)
  val alreadyStarted = new SimpleBooleanProperty(false)
  val temporaryDisableButtonStart = new SimpleBooleanProperty(false)
  val gameOver = new SimpleBooleanProperty(false)
  // 위의 subjectMasterConnectable 기능에 대한 또 다른 대안은 구독이 끝날 때까지 어떠한 입력도 가능하지 않도록 막는 것인데
  // 둘 다 쓰기로 했다. 사용자에게 보여지는 모양을 봐도 구독이 끝날 때까지는 입력이 되지 않도록 막는 것이 자연스럽기 때문이다.
  val beforeSubscriptionCompleteDisable = new SimpleBooleanProperty(true)

  var localPlayer: Player = null
  var webSocketWorkerActorRef: Option[ActorRef] = None
  var preparedId: Option[Int] = None
  var timerMap: Map[Int, Subscription] = Map()
  var timerIdCounter: Int = 0
  var remotePlayerMap: Map[Player, Node] = Map()
  // 왜 이 기능이 필요한가. 아주 복잡한 문제이고 대안은 아주 단순하고 우회적이다.
  // 게임이 StoppedGame 상태에 이르게 되는 계기는, 명시적으로 Stop 입력을 했을 때와 다음 벽돌을 만들 수 없을 때 이다.
  // 다음 벽돌을 만들 수 없으면 실패의 RealizedNextBlock 을 출력하게 되고
  // 구독단계에서 관련된 RealizeNextBlock 을 Server 로 전달하면
  // Server 는 이를 받아서 대응하는 RemotePlayer 로 Broadcast 하고,
  // 대응하는 RemotePlayer 도 전부 StoppedGame 상태에 이르게 된다.
  // 지금 UI 구상에서는, 이 시점에서 아무것도 하지 않는다.
  // 이 시점에서 창을 닫으면 Server 는 게임 시작 후 연결종료로 인식하여
  // 대응하는 RemotePlayer 로 Stop Broadcast 를 하게 되고,
  // 결국 StoppedGame 상태에 이른 RemotePlayer FSM 으로 Stop 입력을 하게 되는 문제가 생긴다.
  // 대안은, RemotePlayer FSM 의 출력 중 StoppedGame 상태에 이르게 되는 상황을 인식하여
  // 해당되면 구독을 끊는 것이다.
  // 구체적으로는 RemotePlayer FSM 의 출력 중 실패의 RealizedNextBlock 을 인식하여 RemotePlayer FSM 구독을 끊는다.
  // 아주 단순하면서 우회적인 대안이다.
  // 좀 더 좋은 방법은 GameRoomActor 에서 이 상황에 대한 Stop 입력이 오지 않도록 하는 것인데,
  // 이와 같이 하려면 Stop 입력의 기능을, RemotePlayer FSM 에 대한 Broadcast 를 필요로 하는지 여부를 나타낼 수 있도록 바꿔야 한다.
  var remotePlayerSubscriptionMap: Map[Player, Subscription] = Map()

  var subscriptionFromServer: Subscription = null
  var subscriptionFromLocalPlayer: Subscription = null
  // 아무것도 하지 않고 1분동안 있을 때 휴식시간초과로 연결이 끊기는 문제가 생긴다.
  // 따라서 일정시간 간격으로 Ping 을 보내는데, 게임을 시작한 이후로는 시간초과설정을 지워야 한다.
  var subscriptionPreventIdleTimeout: Option[Subscription] = None

  def setupPing(): Unit = {
    webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Left(Ping)), ActorRef.noSender))
    subscriptionPreventIdleTimeout = Some(ioWorker.schedule(30.seconds)(setupPing()))
  }

  override def initialize(location: URL, resources: ResourceBundle): Unit = {
    Console.err.println("xxx")
    // id 로 Node 찾기.
    // http://stackoverflow.com/questions/12201712/how-to-find-an-element-with-an-id-in-javafx
    val buttonStart = tetrisPaneLocalPlayer.lookup("#buttonStart").asInstanceOf[Button]
    val buttonPrepare = tetrisPaneLocalPlayer.lookup("#buttonPrepare").asInstanceOf[ToggleButton]
    val buttonLeave = tetrisPaneLocalPlayer.lookup("#buttonLeave").asInstanceOf[Button]
    val paneGameMap = tetrisPaneLocalPlayer.lookup("#paneGameMap").asInstanceOf[Pane]
    val paneNextBlock = tetrisPaneLocalPlayer.lookup("#paneNextBlock").asInstanceOf[Pane]
    val paneBePushed = tetrisPaneLocalPlayer.lookup("#paneBePushed").asInstanceOf[Pane]
    // var 을 Closure 안에서 써도 FXML Injection 이 아니면 문제가 되지 않는 듯 하다.
    val flowPaneRemotePlayersVal = flowPaneRemotePlayers

    buttonStart.disableProperty().bind(
      isStarter.not()
        .or(receivedBeAbleToStart.not())
        .or(alreadyStarted)
        .or(temporaryDisableButtonStart)
    )
    buttonPrepare.textProperty().bind(new When(buttonPrepare.selectedProperty()).then("Cancel").otherwise("Prepare"))
    buttonPrepare.disableProperty().bind(temporaryDisableButtonStart.or(alreadyStarted).or(beforeSubscriptionCompleteDisable))
    buttonLeave.disableProperty().bind(temporaryDisableButtonStart.or(alreadyStarted).or(beforeSubscriptionCompleteDisable))

    subscriptionFromServer =
      subjectFromServer
        .observeOn(ioScheduler)
        .observeOn(javaFxScheduler)
        .subscribe(
          subscribeFromServerF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayersVal)(_),
          Console.err.println(_),
          () => {
            Console.out.println("completed.")
            // 창을 닫을 때의 구독단계에서도 webSocketWorkerActorRef 를 바꾸고 있는데, UI Thread 에서 실행되므로 문제 없다.
            webSocketWorkerActorRef = None
            myStage.close()
          }
        )

    webSocketWorkerActorRef =
      Some(
        actorSystem.actorOf(
          Props(
            new webSocketWorker.WebSocketWorkerActor(subjectFromServer)(Block5x5.Block5x5JsonFormat, actorMaterializer)
          )
        )
      )

    webSocketWorkerActorRef
      .foreach { webSocketWorkerActorRef =>
        val webSocketRequest =
          createdRoom match {
            case Right(createdRoom) =>
              WebSocketRequest(s"ws://${url}/room/${createdRoom.roomId}")
            case Left(roomId) =>
              WebSocketRequest(s"ws://${url}/room/${roomId}")
          }
        httpPreparing.http.singleWebSocketRequest(
          webSocketRequest,
          Flow.fromSinkAndSource(
            Sink.fromSubscriber(
              ActorSubscriber[Message](webSocketWorkerActorRef)
            ),
            Source.fromPublisher(
              ActorPublisher[Message](webSocketWorkerActorRef)
            )
          )
        )(actorMaterializer)._1
          .onComplete {
            case Success(ValidUpgrade(_, _)) =>
              val joinRoom =
                createdRoom match {
                  case Right(CreatedRoom(password, _)) =>
                    webSocketWorker.Holder(Left(CreatorJoinRoom(password, "hi")))
                  case Left(roomId) =>
                    webSocketWorker.Holder(Left(JoinRoom("hi")))
                }
              webSocketWorkerActorRef.tell(joinRoom, ActorRef.noSender)
            case _ => subjectFromServer.onCompleted()
          }(Implicits.global)
      }
    JC.toScalaObservable(JFO.fromWindowEvents(stage, WindowEvent.WINDOW_CLOSE_REQUEST))
      .observeOn(javaFxScheduler)
      .subscribe { _ =>
        // Rx Subject 의 onComplete() 에 해당되는 기능이 밖에 드러나 있지 않아서 이와 같이 완료를 해야 한다.
        webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        webSocketWorkerActorRef = None
        timerMap.foreach(_._2.unsubscribe())
        timerMap = Map()
        myStage.close()
      }
    JC.toScalaObservable(JFO.fromWindowEvents(myStage, WindowEvent.WINDOW_CLOSE_REQUEST))
      .observeOn(javaFxScheduler)
      .subscribe { _ =>
        // Rx Subject 의 onComplete() 에 해당되는 기능이 밖에 드러나 있지 않아서 이와 같이 완료를 해야 한다.
        webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        webSocketWorkerActorRef = None
        timerMap.foreach(_._2.unsubscribe())
        timerMap = Map()
      }

    JC.toScalaObservable(JFO.fromActionEvents(buttonStart))
      .observeOn(javaFxScheduler)
      .doOnNext(_ => temporaryDisableButtonStart.set(true))
      .observeOn(ioScheduler)
      .subscribe { _ =>
        preparedId
          .map(preparedId => webSocketWorker.Holder(Left(StartGames(preparedId))))
          .foreach(holder => webSocketWorkerActorRef.foreach(_.tell(holder, ActorRef.noSender)))
      }

    JC.toScalaObservable(JFO.fromActionEvents(buttonPrepare))
      .observeOn(ioScheduler)
      .subscribe { _ =>
        Console.err.println("press prepare.")
        val input: Command.StreamInput[Block5x5.BlockType.BlockType] =
          if (buttonPrepare.selectedProperty().get())
            Prepare(localPlayer)
          else
            Cancel(localPlayer)
        subjectMaster.onNext(Right(input))
      }

    JC.toScalaObservable(JFO.fromActionEvents(buttonLeave))
      .observeOn(ioScheduler)
      .subscribe { _ =>
        webSocketWorkerActorRef
          .foreach(
            _.tell(webSocketWorker.Holder(Right(Right(Leave(localPlayer)))), ActorRef.noSender)
          )
      }

    paneGameMap.disableProperty().bind(gameOver)

    JC.toScalaObservable(JFO.fromNodeEvents(paneGameMap, KeyEvent.KEY_PRESSED))
      .subscribe(
        e => {
          e.getCode match {
            case KeyCode.LEFT =>
              subjectMaster.onNext(Right(BlockTransform(localPlayer, BlockTransformType.Left)))
            case KeyCode.RIGHT =>
              subjectMaster.onNext(Right(BlockTransform(localPlayer, BlockTransformType.Right)))
            case KeyCode.DOWN =>
              subjectMaster.onNext(Right(BlockTransform(localPlayer, BlockTransformType.Down)))
            case KeyCode.SPACE =>
              subjectMaster.onNext(Right(BlockTransform(localPlayer, BlockTransformType.Bottom)))
            case KeyCode.UP =>
              subjectMaster.onNext(Right(BlockTransform(localPlayer, BlockTransformType.Rotate(BlockTransformType.LeftToDowToRight))))
            case _ =>
          }
        }
      )

    subscriptionPreventIdleTimeout = Some(ioWorker.schedule(30.seconds)(setupPing()))
  }

  def subscribeFromServerF(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(x: Either[GameRoomClientCommand, Either[GameRoomServerCommand, Command.StreamInput[Block5x5.BlockType.BlockType]]]): Unit = {
    x match {
      case Left(CreatorJoinedRoom(Some(player))) =>
        localPlayer = player
        stage.titleProperty().set(player.toString)
        isStarter.set(true)
        val notYetPreparedGame: (Either[WaitFeedback[Block5x5.BlockType.BlockType], GameState[Block5x5.BlockType.BlockType]], State.State[Queue[Log], Unit], Option[StreamOutput[Block5x5.BlockType.BlockType]]) =
          (Right(NotYetPreparedGame[Block5x5.BlockType.BlockType](localPlayer)), State(()), None)
        subscriptionFromLocalPlayer =
          Valve.valve[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]](_.isLeft)(
            subjectMasterConnectable
              .merge(subjectMasterFeedback)
              .filter(x => Game.filterF[Block5x5.BlockType.BlockType](localPlayer)(x))
          )
            .observeOn(ioScheduler)
            .scan(notYetPreparedGame)(Game.scanF[Block5x5.BlockType.BlockType](clearedGameMap, functionHolder, Position(3, 0), runningGameSMB))
            .flatMap(Game.mapF)
            .observeOn(javaFxScheduler)
            .subscribe(subscribeStreamOutputF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(_))
        // LocalPlayer FSM 이 구독을 마친 후에는 입력을 할 수 있도록 한다.
        beforeSubscriptionCompleteDisable.set(false)
      case Left(JoinedRoom(Some(player))) =>
        localPlayer = player
        stage.titleProperty().set(player.toString)
        isStarter.set(false)
        val notYetPreparedGame: (Either[WaitFeedback[Block5x5.BlockType.BlockType], GameState[Block5x5.BlockType.BlockType]], State.State[Queue[Log], Unit], Option[StreamOutput[Block5x5.BlockType.BlockType]]) =
          (Right(NotYetPreparedGame[Block5x5.BlockType.BlockType](localPlayer)), State(()), None)
        subscriptionFromLocalPlayer =
          Valve.valve[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]](_.isLeft)(
            subjectMasterConnectable
              .merge(subjectMasterFeedback)
              .filter { x => Game.filterF[Block5x5.BlockType.BlockType](localPlayer)(x) }
          )
            .observeOn(ioScheduler)
            .scan(notYetPreparedGame)(Game.scanF[Block5x5.BlockType.BlockType](clearedGameMap, functionHolder, Position(3, 0), runningGameSMB))
            .flatMap(Game.mapF)
            .observeOn(javaFxScheduler)
            .subscribe(subscribeStreamOutputF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(_))
        // creator 뿐만 아니라 joiner 역시 LocalPlayer FSM 의 구독이 끝나면 입력을 할 수 있도록 한다.
        beforeSubscriptionCompleteDisable.set(false)
      case Right(Left(NotifyChangeStarter(player @ LocalPlayer(_, _)))) =>
        isStarter.set(localPlayer == player)
        webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Left(NotifiedChangeStarter))), ActorRef.noSender))
      case Right(Left(NotifyBeAbleToStart(x))) =>
        preparedId = x
        receivedBeAbleToStart.set(x.isDefined)
        webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Left(NotifiedBeAbleToStart))), ActorRef.noSender))
      case Left(StartedGames(x)) =>
        if (x) {
          Console.err.println("received StartedGames(true)")
        } else
          temporaryDisableButtonStart.set(false)
      case Right(Right(streamInput)) =>
        Console.err.println(s"in RightRight ${streamInput}")
        subjectMaster.onNext(Right(streamInput))
      case Right(Left(NotifyJoinRoom(responseId, remotePlayer))) =>
        val result =
          remotePlayerMap.get(remotePlayer) match {
            case Some(_) =>
              NotifiedJoinRoom(responseId, localPlayer, false)
            case None =>
              Try {
                // scale 을 해도 layout 경계는 달라지지 않는다고 한다. Group 안에 넣어서 쓰라 한다.
                // http://stackoverflow.com/questions/17699623/layout-using-the-transformed-bounds
                // 이 단계에서, tetrisPane.fxml 을 읽어서 Group 안에 담은 다음, flowPaneRemotePlayers 안에 넣어야 한다.
                // 또한, RemotePlayer 용 tetrisPane.fxml 은 Button 을 전부 비활성화 시켜야 한다.
                val tetrisPane =
                  FXMLLoader.load[Parent](
                    getClass.getResource("/org/imcharsi/tetrispractice/client/ui/tetrisPane.fxml")
                  )
                tetrisPane.scaleXProperty().set(0.5)
                tetrisPane.scaleYProperty().set(0.5)
                val newNode = new Group(tetrisPane)
                val buttonStart = newNode.lookup("#buttonStart").asInstanceOf[Button]
                val buttonPrepare = newNode.lookup("#buttonPrepare").asInstanceOf[ToggleButton]
                val buttonLeave = newNode.lookup("#buttonLeave").asInstanceOf[Button]
                buttonStart.disableProperty().set(true)
                buttonPrepare.disableProperty().set(true)
                buttonLeave.disableProperty().set(true)
                remotePlayerMap = remotePlayerMap + (remotePlayer -> newNode)
                flowPaneRemotePlayers.getChildren.add(newNode)

                val paneGameMap = tetrisPane.lookup("#paneGameMap").asInstanceOf[Pane]
                val paneNextBlock = tetrisPane.lookup("#paneNextBlock").asInstanceOf[Pane]
                val paneBePushed = tetrisPane.lookup("#paneBePushed").asInstanceOf[Pane]
                subscribeStreamOutputF(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(_)
              } match {
                case Success(subscribeF) =>
                  val notYetPreparedGame: (Either[WaitFeedback[Block5x5.BlockType.BlockType], GameState[Block5x5.BlockType.BlockType]], State.State[Queue[Log], Unit], Option[StreamOutput[Block5x5.BlockType.BlockType]]) =
                    (Right(NotYetPreparedGame[Block5x5.BlockType.BlockType](remotePlayer)), State(()), None)
                  val subscription =
                    Valve.valve[Either[StreamFeedback[Block5x5.BlockType.BlockType], StreamInput[Block5x5.BlockType.BlockType]]](_.isLeft)(
                      subjectMasterConnectable
                        .merge(subjectMasterFeedback)
                        .filter { x => Game.filterF[Block5x5.BlockType.BlockType](remotePlayer)(x) }
                    )
                      .observeOn(ioScheduler)
                      .scan(notYetPreparedGame)(Game.scanF[Block5x5.BlockType.BlockType](clearedGameMap, functionHolder, Position(3, 0), runningGameSMB))
                      .flatMap(Game.mapF)
                      .observeOn(javaFxScheduler)
                      .subscribe(subscribeF)
                  remotePlayerSubscriptionMap = remotePlayerSubscriptionMap + (remotePlayer -> subscription)
                  NotifiedJoinRoom(responseId, localPlayer, true)
                case Failure(_) =>
                  NotifiedJoinRoom(responseId, localPlayer, false)
              }
          }
        webSocketWorkerActorRef
          .foreach(
            _.tell(
              webSocketWorker.Holder(
                Right(Left(result))
              ),
              ActorRef.noSender
            )
          )
    }
  }

  def subscribeStreamOutputF(paneGameMap: Pane, paneNextBlock: Pane, paneBePushed: Pane, flowPaneRemotePlayers: FlowPane)(x: (StreamOutput[Block5x5.BlockType.BlockType], State.State[Queue[Log], Unit])): Unit = {
    val (streamOutput, _) = x
    streamOutput match {
      case Prepared(player, result, prepare) =>
        if (Player.isLocalPlayer(player)) {
          if (result)
            webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(prepare))), ActorRef.noSender))
          else
            webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        }
        subjectMasterFeedback.onNext(Left(PrepareFeedback(player)))
      case Leaved(player, result, prepare) =>
        // 구상에 따라, 퇴장했을 때는 Feedback 을 보내지 않는다.
        if (Player.isLocalPlayer(player)) {
          if (result)
            webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(prepare))), ActorRef.noSender))
          webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        } else {
          remotePlayerMap.get(player).foreach { x =>
            flowPaneRemotePlayers.getChildren.remove(x)
            remotePlayerMap = remotePlayerMap - player
          }
          remotePlayerSubscriptionMap.get(player).foreach { x =>
            x.unsubscribe()
            remotePlayerSubscriptionMap = remotePlayerSubscriptionMap - player
          }
          Console.err.println(s"${streamOutput}")
        }
      case Cancelled(player, result, cancel) =>
        if (Player.isLocalPlayer(player)) {
          if (result)
            webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(cancel))), ActorRef.noSender))
          else
            webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        }
        subjectMasterFeedback.onNext(Left(CancelFeedback(player)))
      case Started(player, result, start) =>
        if (Player.isLocalPlayer(player)) {
          if (result) {
            webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(start))), ActorRef.noSender))
            import scala.collection.JavaConverters._
            // 아래와 같이 하지 않으면 방향 Key 를 눌렸을 때 paneGameMap 에 있던 Focus 가,
            // Focus 를 받을 수 있는 다른 Node 로 움직인다.
            flowPaneRemotePlayers.getChildrenUnmodifiable.iterator().asScala.foreach { node =>
              node.lookup("#buttonStart").disableProperty().set(true)
              node.lookup("#buttonPrepare").disableProperty().set(true)
              node.lookup("#buttonLeave").disableProperty().set(true)
            }
            paneGameMap.requestFocus()
            subjectMasterFeedback.onNext(Left(StartFeedback(player)))
            if (Player.isLocalPlayer(player))
              subjectMaster.onNext(Right(RealizeNextBlock(player)))
            alreadyStarted.set(true)
            // 게임을 시작했으면 Ping 설정을 취소해야 한다.
            // 방장 뿐만 아니라 모든 Client 들이 Ping 설정을 해야 한다.
            subscriptionPreventIdleTimeout.foreach(_.unsubscribe())
            subscriptionPreventIdleTimeout = None
          } else
            temporaryDisableButtonStart.set(false)
        } else {
          // RemotePlayer 는 Feedback 만 한다.
          // 이어져야 하는 RealizeNextBlock 는 LocalPlayer 가 만든 출력을 구독단계에서 받아서 Server 로 보내면,
          // Server 가 이를 Broadcast 해서 RemotePlayer 로 주어진다.
          subjectMasterFeedback.onNext(Left(StartFeedback(player)))
        }
      case RealizedNextBlock(player, blockAndNext, moved, deleted, movedFromBePushedToMap, realizeNextBlock) =>
        val newTimerId = allocateNewTimer()
        val result =
          blockAndNext
            .flatMap {
              case ((currentBlock, expectedLandingBlock), nextBlock) =>
                val z: Option[List[(Position, BlockCell[Block5x5.BlockType.BlockType])]] = Some(List())
                val newCurrentBlock =
                  currentBlock.map.toList.map {
                    case (position, blockCell) =>
                      allocateCurrentBlockCell(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(currentBlock.position, position, blockCell).map(x => (position, x))
                  }
                val newCurrentBlockMap =
                  newCurrentBlock.foldLeft(z) {
                    case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
                    case _ => None
                  }.map(_.toMap)
                val newExpectedLandingBlock =
                  expectedLandingBlock.map.toList.map {
                    case (position, blockCell) =>
                      allocateExpectedLandingBlock(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(expectedLandingBlock.position, position, blockCell).map(x => (position, x))
                  }
                val newExpectedLandingBlockMap =
                  newExpectedLandingBlock.foldLeft(z) {
                    case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
                    case _ => None
                  }.map(_.toMap)
                val newNextBlock =
                  nextBlock.map.toList.map {
                    case (position, blockCell) =>
                      allocateNextBlock(paneGameMap, paneNextBlock, paneBePushed, flowPaneRemotePlayers)(position, blockCell).map(x => (position, x))
                  }
                val newNextBlockMap =
                  newNextBlock.foldLeft(z) {
                    case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
                    case _ => None
                  }.map(_.toMap)

                newCurrentBlockMap
                  .flatMap { r1 =>
                    newExpectedLandingBlockMap.flatMap { r2 =>
                      newNextBlockMap.map { r3 =>
                        Tuple4(
                          newTimerId,
                          currentBlock.copy(map = r1),
                          nextBlock.copy(map = r3),
                          expectedLandingBlock.copy(map = r2)
                        )
                      }
                    }
                  }
            }
        moved.foreach {
          case (position, blockCell) =>
            moveGameMapBlockCell(position, blockCell)
        }
        deleted.foreach {
          case (_, blockCell) =>
            deleteGameMapBlockCell(paneGameMap)(blockCell)
        }
        movedFromBePushedToMap.foreach {
          case (position, blockCell) =>
            moveFromBePushedToGameMapBlockCell(paneGameMap, paneBePushed)(position, blockCell)
        }
        // blockAndNext 에서 내려오는 벽돌에 해당하는 Block 에는 이미 ui 식별번호가 붙어있을 수도 있는데,
        // 이 경우는 다음 벽돌을 그대로 갖다 쓴다는 말이므로,
        // 다음 벽돌을 그리는 데 쓴 Rectangle 을 떼어서 GameMap 에 붙여야 한다.
        subjectMasterFeedback.onNext(Left(RealizeNextBlockFeedback(player, result)))
        if (blockAndNext.isEmpty) {
          timerMap.foreach(_._2.unsubscribe())
          timerMap = Map()
        }
        if (Player.isLocalPlayer(player)) {
          webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(realizeNextBlock))), ActorRef.noSender))
          if (blockAndNext.isEmpty)
            gameOver.set(true)
        } else {
          if (blockAndNext.isEmpty)
            remotePlayerSubscriptionMap.get(player).foreach { x =>
              x.unsubscribe()
              remotePlayerSubscriptionMap = remotePlayerSubscriptionMap - player
            }
        }
        if (result.isEmpty && blockAndNext.isDefined) {
          // 새 벽돌 할당에 실패했다. 현재 Client 의 연결을 끊고 화면을 종료한다.
          // 벽돌 할당에 실패하게 된 FSM 이 LocalPlayer 인지 아니면 RemotePlayer 인지 가리지 않는다.
          // 지금 그려야 할 것을 그리지 못하고 다음 것을 성공적으로 그리는 것이 큰 의미가 없기 때문이다.
          // 잘못 된 내용을 계속 보고 있는 것보다는 창을 닫는 것이 낫다고 판단했다.
          // 그런데 왜 Stop 입력을 하지 않고, 연결을 끊는가.
          // Stop 입력을 해도 이어서 바로 연결을 끊을 것이므로,
          // Stop 입력이 처리되고 Stop Broadcast 요청을 하도록 WebSocketWorkerActor 로 전달하게 되는 시점에서는
          // 이미 WSWA 는 종료되었기 때문이다.
          // Stopped 출력이 창을 닫을지 여부를 표시할 수 있도록 하면
          // Stopped 출력을 Side-Effect 처리 단계에서 받아서 WebSocket 연결을 끊고 창을 닫거나
          // 아니면 정상적인 게임 종료이면 WebSocket 연결을 끊지 않고 창을 그냥 두거나 선택할 수 있었을 것이다.
          // 한편, 이와 같이 해도 괜찮은 이유는,
          // Server 에서 연결 종료를 인식하고 대응하는 모든 RemotePlayer FSM 으로 Stop 입력을 해주기 때문이다.
          webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        }
      case BlockTransformed(player, result, move) =>
        result match {
          case Some(Right(None)) =>
            subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, None)))
            if (Player.isLocalPlayer(player))
              webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(move))), ActorRef.noSender))
          case Some(Right(Some((currentBlock, expectedLandingBlock)))) =>
            currentBlock.map.toList.map {
              case (position, blockCell) =>
                moveGameMapBlockCell(currentBlock.position + position, blockCell)
            }
            expectedLandingBlock.map.toList.map {
              case (position, blockCell) =>
                moveGameMapBlockCell(expectedLandingBlock.position + position, blockCell)
            }
            // move 의 내용을 보고 시간초과설정을 갱신해야 할 지 판단하는 구상이었던 듯 하다.
            // fixme 문제가 있다. 아래/바닥으로 이동할 때, 취소해야 할 시간초과설정을 지정하지 않았다. 일단 전부 취소하도록 했다.
            move match {
              case BlockTransform(_, BlockTransformType.Bottom) =>
                timerMap.foreach(_._2.unsubscribe())
                timerMap = Map()
                val newTimerId = allocateNewTimer()
                subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, Some(newTimerId))))
                if (Player.isLocalPlayer(player))
                  subjectMaster.onNext(Right(BlockTransform(player, BlockTransformType.Down)))
              case BlockTransform(_, BlockTransformType.Down | BlockTransformType.TimeElapsed(_)) =>
                timerMap.foreach(_._2.unsubscribe())
                timerMap = Map()
                val newTimerId = allocateNewTimer()
                subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, Some(newTimerId))))
              case _ =>
                subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, None)))
            }
            if (Player.isLocalPlayer(player))
              webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(move))), ActorRef.noSender))
          case Some(Left((moved, deleted, preparePush))) =>
            moved.foreach(_.foreach { case (position, blockCell) => moveGameMapBlockCell(position, blockCell) })
            deleted.foreach(_.foreach { case (position, blockCell) => deleteGameMapBlockCell(paneGameMap)(blockCell) })
            subjectMasterFeedback.onNext(Left(BlockTransformFeedback(player, None)))
            if (Player.isLocalPlayer(player)) {
              webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(move))), ActorRef.noSender))
              if (!preparePush.isEmpty)
                webSocketWorkerActorRef.foreach(
                  _.tell(
                    webSocketWorker.Holder(
                      Right(Right(PreparePush(player, preparePush.size, preparePush.flatten)))
                    ),
                    ActorRef.noSender
                  )
                )
            }
            if (Player.isLocalPlayer(player))
              subjectMaster.onNext(Right(RealizeNextBlock(player)))
        }
      case Pushed(player, beAllocated, moved, deleted, push) =>
        val z: Option[List[(Position, BlockCell[Block5x5.BlockType.BlockType])]] = Some(List())
        val newBeAllocated =
          beAllocated
            .map {
              case (position, blockCell) =>
                allocateBePushed(paneBePushed)(position, blockCell).map(x => (position, x))
            }
            .foldLeft(z) {
              case (Some(z), Some((position, blockCell))) => Some((position, blockCell) :: z)
              case _ => None
            }
        deleted.foreach { case (position, blockCell) => deleteBePushedBlockCell(paneBePushed)(blockCell) }
        moved.foreach { case (position, blockCell) => moveBePushedBlockCell(position, blockCell) }
        subjectMasterFeedback.onNext(Left(PushFeedback(player, newBeAllocated)))
        if (Player.isLocalPlayer(player))
          webSocketWorkerActorRef.foreach(_.tell(webSocketWorker.Holder(Right(Right(push))), ActorRef.noSender))
        if (newBeAllocated.isEmpty) {
          // 새 벽돌 할당에 실패했다. 현재 Client 의 연결을 끊고 화면을 종료한다.
          // 벽돌 할당에 실패하게 된 FSM 이 LocalPlayer 인지 아니면 RemotePlayer 인지 가리지 않는다.
          subjectMaster.onNext(Right(Stop(localPlayer, "")))
          webSocketWorkerActorRef.foreach(_.tell(OnComplete, ActorRef.noSender))
        }
      case _ =>
    }
  }

  def allocateNewTimer(): Int = {
    val newTimerId = timerIdCounter
    timerIdCounter = timerIdCounter + 1
    timerMap = timerMap + (newTimerId -> ioWorker.schedule(10.second)(subjectMaster.onNext(Right(BlockTransform(localPlayer, TimeElapsed(newTimerId))))))
    newTimerId
  }
}
